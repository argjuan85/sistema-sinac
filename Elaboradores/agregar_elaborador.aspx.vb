﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64

Partial Class Licenciantes_agregar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String



    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
     
        Label1.Text = ""
        Label2.Text = ""
        If (Trim(nombre.Value).Length > "2") Then
            If (verificaelaborador(Trim(nombre.Value))) Then
                nuevoelaborador()


            Else
                Label2.Text = "El nombre del elaborador ya se encuentra registrado, por favor ingrese otro nombre"
                Return
            End If
        Else
            Label2.Text = "Debe ingresar letras o números"


        End If


        Label1.Text = "Se registró el elaborador correctamente"

        'Else
        'Label2.Text = "El nombre del destinatario ya se encuentra registrado, por favor ingrese otro nombre"
        Return
        'End If
        'Else
        'Label2.Text = "Debe ingresar letras o números"
        'End If


    End Sub

   

    Sub nuevoelaborador()
        Dim u As Elaboradores
        'Dim l As logs
        Dim r As Integer
        'Dim xfecha As String


        Try
            u = New Elaboradores()
            u.Elaboradores(conexion)
            'log
            'l = New logs()
            'l.logs(conexion)
            r = u.Agregarelaborador(Trim(nombre.Value), "1")

            'log
            'xfecha = cambiaformatofechahora(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            'l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", "Alta de Destinatario", "Destinatario", r, nombre1.Value)



        Catch ex As Exception
            Label2.Text = "Error al crear elaborador"
        End Try
    End Sub
 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(32768, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        ElseIf (Session("nivel") <> "0") Then
            Form1.Visible = False

        Else
            'codigo = DecryptText(Request.QueryString("ID")) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos

            Dim message As String = "Confirma la carga del registro?"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("return confirm('")
            sb.Append(message)
            sb.Append("');")
            ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())
        End If

    End Sub

    'check repetido
    Public Shared Function verificaelaborador(ByVal elaborador As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim u As Elaboradores
        u = New Elaboradores() ' nuevo objeto instancia
        u.Elaboradores(conexion) 'invoco el constructor y paso parametros de conexion
        dtresultado = u.Consultaelaborador2()
        Dim band As String = 1
        While (dtresultado.Read())

            If (elaborador.ToUpper() = dtresultado(1).ToString().ToUpper()) Then
                band = 0
                Return (band)
            End If

        End While

        dtresultado.Close()

        Return (band)


    End Function
  
End Class
