﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="editar_elaboradores.aspx.vb" Inherits="Licenciantes_editar_licenciante" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <script type="text/javascript">
        function confirma_elimina() {
           return confirm('Confirma la eliminacion del registro?');
        }
     </script>     
     <script type="text/javascript">
        function confirma_modifica() {
           return confirm('Confirma la modificacion del registro?');
        }
    </script>

       <script type="text/javascript">
 
/*  Uso este javascript para el botón volver, ya que no puedo usar un control asp por que dispara el ajax del vanadium  */   
/* se modifico la funcion por que traiga inconvenientes con la encriptacion de parametros, asi como quedo solo va funionar con 1 parametro, si se pasa mas de uno hay q cambiarla (esto es debido q el split lo hacia con el "=" el cual cortaba el string codificado antes si el signo "=" era parte de la cadena encriptada*/
function redireccionar(){
var src = String( window.location.href ).split('?')[1];
var vrs = src.split('&')
var arr = [];

for (var x = 0, c = vrs.length; x < c; x++) 
{
        arr[x] = vrs[x];
};

location.href="../Elaboradores/consultar_elaboradores.aspx?"+ arr[0] } 

</script>


<div id="contenedor1">
    <div id="contenedordest">
     <div id="titulo_seccion">
Modifique el elaborador
 </div>
    <form id="Form1" runat ="server"> 
    <ul>
    <li class="xli">
    <div class="xtest">
  
 
    <span class="boton">
     <label for="tipo">Nombre</label>
        <input id="nombre1" runat="server" name="nombre1" value="" 
            class=":min_length;2  :only_on_blur" maxlength="50"/>
     
    </span>
 
    
 
    
  </div>
  </li>
  </ul>
  
    <ul>
   
    <li class="panel_boton">
     <div>
      <span class="boton">
                    &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" />
                  <input type="button" value="Volver"  id="volver" onclick="redireccionar(); return false;" >
     
        <asp:Button ID="Button2" runat="server" Text="Eliminar" 
             style="height: 26px" onclientclick="return confirm('Confirma la eliminación?');"/>
         </span></div>
    </li>
        
     </ul>
    
     
     </form>
     
  </div>
  </div>  
  <br >
  <div id="notice">
     <asp:Label ID="Label1" runat="server"></asp:Label>
     </div>  
  <div id="error">
     <asp:Label ID="Label2" runat="server"></asp:Label>
     </div> 
</asp:Content>
