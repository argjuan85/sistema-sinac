﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Licenciantes_editar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(65536, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        ElseIf (Session("nivel") <> "0") Then
            Form1.Visible = False

        Else
            Try
                codigo = Request.QueryString("ID")
                codigo = DecryptText(codigo)


                If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                    buscarlote(codigo) 'carga la interfaz
                End If
            Catch
                Label2.Text = "Error al cargar datos del lote"
                'Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If
    End Sub



    Sub buscarlote(ByVal codigo As String)
        Dim dtresultado As SqlDataReader
        Dim u As Elaboradores
        Try

            u = New Elaboradores() ' nuevo objeto instancia
            u.Elaboradores(conexion) 'invoco el constructor y paso parametros de conexion
            dtresultado = u.Consultarelaborador1(codigo)



            While (dtresultado.Read())

                nombre1.Value = dtresultado(1).ToString()


            End While

            dtresultado.Close()

        Catch ex As Exception
            Label2.Text = "Error al realizar la busqueda del destinatario"
        End Try
    End Sub
    Sub actualizarelaborador(ByVal codigo As String)
        Dim u As Elaboradores
        Dim r As Boolean
        Dim l As logs
        Dim xfecha As String
        'log
        l = New logs()
        l.logs(conexion)

        Try
            u = New Elaboradores() ' nuevo objeto instancia
            u.Elaboradores(conexion) 'invoco el constructor y paso parametros de conexion

            'Label1.Text = "conexion con exito"


            r = u.ActualizarElaborador(codigo, Trim(nombre1.Value), "1")
            'log
            xfecha = cambiaformatofechahora(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Modifica Destinatario", "Destinatario", codigo, nombre1.Value)
            Label1.Text = "Se registró el destinatario correctamente"

        Catch ex As Exception
            Label2.Text = "Se produjo un error al actualizar el destinatario"
            'Label1.Text = "Conexion Error" + ex.Message.ToString()
        End Try
    End Sub
    'modifica
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' verificamos que el destinatario no este repetido y Guardamos  en la base de datos.
        '
        Label1.Text = ""
        Label2.Text = ""
        If (Trim(nombre1.Value).Length > "2") Then
            If (verificaelaborador(Trim(nombre1.Value))) Then
                actualizarelaborador(codigo)



            Else
                Label2.Text = "El nombre del elaborador ya se encuentra registrado, por favor ingrese otro nombre"
                Return
            End If
        Else
            Label2.Text = "Debe ingresar letras o números"


        End If

     
    End Sub
    'check repetido
    Public Shared Function verificaelaborador(ByVal elaborador As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim u As Elaboradores
        u = New Elaboradores() ' nuevo objeto instancia
        u.Elaboradores(conexion) 'invoco el constructor y paso parametros de conexion
        dtresultado = u.Consultaelaborador2()
        Dim band As String = 1
        While (dtresultado.Read())

            If (elaborador.ToUpper() = dtresultado(1).ToString().ToUpper()) Then
                band = 0
                Return (band)
            End If

        End While

        dtresultado.Close()

        Return (band)


    End Function

    'eliminar
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim u As Elaboradores
        Dim r As Boolean
        Dim dtresultado As SqlDataReader
        Dim dtresultado2 As SqlDataReader
        Dim idreg As Integer
        Dim nomreg As String
        Dim l As logs
        Dim xfecha As String

        'log
        l = New logs()
        l.logs(conexion)

        u = New Elaboradores()
        u.Elaboradores(conexion)

        ' reviso que no este asociado
        dtresultado = u.Consultarelaboradoresasoc(codigo)
        dtresultado2 = u.Consultarelaborador1(codigo)
        If (dtresultado2.Read()) Then
            idreg = dtresultado2(0).ToString()
            nomreg = dtresultado2(1).ToString()
        End If
        dtresultado2.Close()
        If (Not dtresultado.Read()) Then

            ' elimino  el elaborador
            r = u.BorrarElaborador(codigo)
            xfecha = cambiaformatofechahora(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            'l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Destinatario", "Destinatario", idreg, nomreg)

            Response.Redirect("consultar_elaboradores.aspx")
        Else
            Label1.Text = "El Elaborador esta asociado a registros, no se puede eliminar"


        End If
        dtresultado.Close()
    End Sub
End Class
