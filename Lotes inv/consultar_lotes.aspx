﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="consultar_lotes.aspx.vb" Inherits="Estandares_consulta_estandares" title="Sistema Sinac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <link rel="stylesheet" type="text/css" href="../js/DataTables/media/css/jquery.dataTables1.css">
    <link rel="stylesheet" type="text/css" href="../js/DataTables/extensions/TableTools/css/dataTables.tableTools.css">
    <link rel="stylesheet" href="../js/datatable/media/css/demo_table.css"type="text/css" />

	<style type="text/css" class="init">
	      	    input {
        }
#ctl00_ContentPlaceHolder1_Div1 {
    margin: auto;
}


	</style>
	<script type="text/javascript" language="javascript" src="../js/DataTables/media/js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="../js/DataTables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="../js/DataTables/extensions/TableTools/js/dataTables.tableTools.js"></script>
	<script type="text/javascript" language="javascript" src="../../../examples/resources/syntax/shCore.js"></script>
	<script type="text/javascript" language="javascript" src="../../../examples/resources/demo.js"></script>




        <script type="text/javascript" charset="utf-8" src="../js/datatable/ColVis/media/js/ColVis.js"></script>
    <script type="text/javascript" charset="utf-8"></script>
    
       <script type="text/javascript">
 
/*  Uso este javascript para el botón volver, ya que no puedo usar un control asp por que dispara el ajax del vanadium  */   
/* se modifico la funcion por que traiga inconvenientes con la encriptacion de parametros, asi como quedo solo va funionar con 1 parametro, si se pasa mas de uno hay q cambiarla (esto es debido q el split lo hacia con el "=" el cual cortaba el string codificado antes si el signo "=" era parte de la cadena encriptada*/
function redireccionar(){
var src = String( window.location.href ).split('?')[1];
var vrs = src.split('&')
var arr = [];

for (var x = 0, c = vrs.length; x < c; x++) 
{
        arr[x] = vrs[x];
};

location.href="../OOT/modifica_oot.aspx?"+ arr[0] } 

</script>
    
<script language="javascript" type="text/javascript">


var asInitVals = new Array();
     $(document).ready(function () {
     
     var  oTable = $('#tbl').dataTable( {
       "dom": 'T<"clear">lfrtip',
      
           "oTableTools": {
                                                                "aButtons": [
                                                                                
                                                                                
                                                                            ]
                                                                },
    
            "sScrollY": "100%",
            //'sPaginationType': 'full_numbers',
            //'iDisplayLength': 5,
            	"oColVis": {
		    		
					"activate": "mouseover",
						
						"aiExclude": [ 5 ]
						
				},
				"oLanguage": {
					"sProcessing":     "Procesando...",
				    "sLengthMenu":     "Mostrar _MENU_ registros",
				    "sZeroRecords":    "No se encontraron resultados",
				    "sEmptyTable":     "Ningún dato disponible en esta tabla",
				    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				    "sInfoPostFix":    "",
				    "sSearch":         "Buscar:",
				    "sUrl":            "",
				    "sInfoThousands":  ",",
				    
				    "oPaginate": {
				        "sFirst":    "Primero",
				        "sLast":     "Último",
				        "sNext":     "Siguiente",
				        "sPrevious": "Anterior"
				    },
				    "sLoadingRecords": "Cargando...",
				    "fnInfoCallback": null,
				    "oAria": {
				        "sSortAscending":  ": Activar para ordernar la columna de manera ascendente",
				        "sSortDescending": ": Activar para ordernar la columna de manera descendente"
				    }
				    },
            "bPaginate": true,
            "bProcessing": true,
            "bServerSide": false,
            "bSortCellsTop": true,
            
            
     
        });
        
        
        /* Add the events etc before DataTables hides a column */
			$("thead input").keyup( function () {
				/* Filter on the column (the index) of this element */
				oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex( 
					oTable.fnSettings(), $("thead input").index(this) ) );
			} );
			
			/*
			 * Support functions to provide a little bit of 'user friendlyness' to the textboxes
			 */
			$("thead input").each( function (i) {
				this.initVal = this.value;
			} );
			
			$("thead input").focus( function () {
				if ( this.className == "search_init" )
				{
					this.className = "";
					this.value = "";
				}
			} );
			
			$("thead input").blur( function (i) {
				if ( this.value == "" )
				{
					this.className = "search_init";
					this.value = this.initVal;
				}
			} );
        
    });




</script>
 <div id="titulo_seccion">
     Administración de Lotes Involucrados
 </div>
       
              
        
  
    <div id="Div1" style="width:50%" runat="server">
        
        
        
        <div align= "center" >
        
          <asp:Button ID="Button2" 
            runat="server" Text=" + Agregar Lote " />
            <input type="button" value="Volver"  id="volver" onclick="redireccionar(); return false;" >
            
         
              <asp:Label ID="label_stock" runat="server"></asp:Label>
                  &nbsp;
&nbsp;
</div>
  

<asp:Repeater ID="repeater" runat="server" OnItemCommand ="RepeaterDeleteitemcommand">
            <HeaderTemplate>
                <table id="tbl" cellpadding="1" cellspacing="0" 
                    border="0" class="display" >
                  <thead id="aaa">
                        <tr>
                        <th></th>
                      
                        <th>Lote</th>
                                              
                    </tr>
             <tr>
			<td align="center"><input type="hidden" size="4" name="search_engine" value="" class="search_init" /></td>
			<td align="center"><input type="hidden" size="10" name="search_engine" value="" class="search_init" /></td>
			
			</tr>
                  </thead>
                <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                  <td align="center"><a><asp:Button ID="Button1" CommandName="Click" Text="Modificar" runat="server" CommandArgument='<%# Eval("idOOT_LoteInv") %>' /></a></td>
             <td align = "center"><%#Eval("Lote")%></td>
         
                           

                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
 
</body>
      <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
         <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IDPdto], [CodPdto] FROM [Productos]">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [Id], [Denominacion] FROM [User]">
        </asp:SqlDataSource>
          <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idEtapa], [nombre_etapa] FROM Etapas">
        </asp:SqlDataSource>
    </form>
      </asp:Content>

