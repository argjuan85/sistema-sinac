﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64

Partial Class Licenciantes_agregar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String



    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' verificamos que el destinatario no este repetido y Guardamos  en la base de datos.
        '
        Label1.Text = ""
        Label2.Text = ""
        ' If (Trim(nombre1.Value).Length > minvalida) Then
        ' If (verificadestinatario(Trim(nombre1.Value))) Then
        nuevolote()
        ' nombre1.Value = ""


        'Else
        'Label2.Text = "El nombre del destinatario ya se encuentra registrado, por favor ingrese otro nombre"
        Return
        'End If
        'Else
        'Label2.Text = "Debe ingresar letras o números"
        'End If


    End Sub

   

    Sub nuevolote()
        Dim u As Lotes
        'Dim l As logs
        Dim r As Integer
        'Dim xfecha As String


        Try
            u = New Lotes()
            u.Lotes(conexion)
            'log
            'l = New logs()
            'l.logs(conexion)

            Label1.Text = ""
            Label2.Text = ""
            If (Trim(lote.Value).Length > "2") Then
                If (u.verificalote(Trim(lote.Value), u.obtenerNroOOT(codigo), codigo)) Then
                    r = u.Agregarlote(codigo, Trim(lote.Value))
                    Label1.Text = "Se registró el lote correctamente"
                Else
                    Label2.Text = "El nombre del lote ya se encuentra registrado, por favor ingrese otro nombre"
                    Return
                End If
            Else
                Label2.Text = "Debe ingresar letras o números"


            End If



            'log
            'xfecha = cambiaformatofechahora(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            'l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", "Alta de Destinatario", "Destinatario", r, nombre1.Value)



        Catch ex As Exception
            Label2.Text = "Error al crear lote"
        End Try
    End Sub
  
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(32768, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        ElseIf (Session("nivel") <> "0") Then
            Form1.Visible = False

        Else
            codigo = DecryptText(Request.QueryString("ID")) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos

            Dim message As String = "Confirma la carga del lote?"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("return confirm('")
            sb.Append(message)
            sb.Append("');")
            ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())
        End If

    End Sub

  
End Class
