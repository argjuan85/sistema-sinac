﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Licenciantes_editar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String
    Public codigo_oot As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(65536, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        ElseIf (Session("nivel") <> "0") Then
            Form1.Visible = False

        Else
            Try
                codigo = Request.QueryString("ID")
                codigo = DecryptText(codigo)

                'Button2.Attributes.Add("onclick", " return confirma_elimina();")
                'Button1.Attributes.Add("onclick", " return confirma_modifica();")

                '  Dim message As String = "Confirma la modificacion del destinatario?"
                '  Dim sb As New System.Text.StringBuilder()
                '  sb.Append("return confirm('")
                '  sb.Append(message)
                '  sb.Append("');")
                '  ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())


                If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                    buscarlote(codigo) 'carga la interfaz
                End If
            Catch
                Label2.Text = "Error al cargar datos del lote"
                'Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If
    End Sub



    Sub buscarlote(ByVal codigo As String)
        Dim dtresultado As SqlDataReader
        Dim u As Lotes
        Try

            u = New Lotes() ' nuevo objeto instancia
            u.Lotes(conexion) 'invoco el constructor y paso parametros de conexion
            dtresultado = u.Consultarlotes1(codigo)



            While (dtresultado.Read())

                nombre1.Value = dtresultado(2).ToString()


            End While

            dtresultado.Close()

        Catch ex As Exception
            Label2.Text = "Error al realizar la busqueda del destinatario"
        End Try
    End Sub
    Sub actualizarlote(ByVal codigo As String)
        Dim u As Lotes
        Dim r As Boolean
        Dim l As logs
        Dim xfecha As String
        'log
        l = New logs()
        l.logs(conexion)

        Try
            u = New Lotes() ' nuevo objeto instancia
            u.Lotes(conexion) 'invoco el constructor y paso parametros de conexion

            'Label1.Text = "conexion con exito"


            r = u.Actualizarlote(codigo, Trim(nombre1.Value))
            'log
            xfecha = cambiaformatofechahora(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Modifica Destinatario", "Destinatario", codigo, nombre1.Value)
            Label1.Text = "Se registró el lote correctamente"

        Catch ex As Exception
            Label2.Text = "Se produjo un error al actualizar el lote"
            'Label1.Text = "Conexion Error" + ex.Message.ToString()
        End Try
    End Sub
    'modifica
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' verificamos que el lote no este repetido y Guardamos  en la base de datos.
        '
        Dim u As Lotes
        u = New Lotes()
        u.Lotes(conexion)
        Label1.Text = ""
        Label2.Text = ""
        If (Trim(nombre1.Value).Length > "2") Then
            If (u.verificalote(Trim(nombre1.Value), u.obtenerNroOOT(codigo), codigo)) Then
                actualizarlote(codigo)

            Else
                Label2.Text = "El nombre del lote ya se encuentra registrado, por favor ingrese otro nombre"
                Return
            End If
        Else
            Label2.Text = "Debe ingresar letras o números"


        End If

     
    End Sub

    'eliminar
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim u As Lotes
        Dim r As Boolean
        Dim idreg As Integer
        Dim nomreg As String
        Dim l As logs
        Dim xfecha As String

        'log
        l = New logs()
        l.logs(conexion)

        u = New Lotes()
        u.Lotes(conexion)

        ' elimino  el lic
        codigo_oot = u.obtenerNroOOT(codigo)
        r = u.Borrarlote(codigo)
        xfecha = cambiaformatofechahora(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
        l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Destinatario", "Destinatario", idreg, nomreg)
      

        Response.Redirect("consultar_lotes.aspx?ID=" + EncryptText(codigo_oot))



    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim u As Lotes
        u = New Lotes()
        u.Lotes(conexion)
        codigo_oot = u.obtenerNroOOT(codigo)
        Response.Redirect("../Lotes inv/consultar_lotes.aspx?ID=" + EncryptText(codigo_oot))

    End Sub
End Class
