﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports System.Net.Mail


Partial Class Estandares_consulta_estandares
    Inherits System.Web.UI.Page
    Public opcion As String
    Public sql As String
    Public codigo As String

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(1048576, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            
            'esto es para que tome el item "seleccione..." en el dropdown
            If (Not Page.IsPostBack) Then

                producto.AppendDataBoundItems = True

            End If
        End If

    End Sub

    Protected Function invertirfecha(ByVal fecha As Object) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 6, 2)
            bDia = Right(Fechatest, 2)
            bAño = Left(Fechatest, 4)
            Fechanueva = bDia & "/" & bMes & "/" & bAño
            Return Fechanueva
        End If


    End Function

   
   

    
    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand


        Dim Codigox As String
        Dim archivo As String
        Dim carpeta As String 'id oot
        Dim filename As String
        Dim es As OOT
        es = New OOT()
        es.OOT(conexion)


        If (e.CommandName = "Click") Then
            'get the id of the clicked row

            Dim comment() As String
            comment = e.CommandArgument.Split("|") ' you can access like comment(0),comment(1)
            Dim tipo As String
            Codigox = comment(0)
            tipo = comment(1)
      
            'Ambiente 
            'ver revisar aca, necesito el tipo de registro
            archivo = es.obtener_nombre_adjunto(Codigox, tipo)
            If tipo = "OOT" Then
                carpeta = es.obtener_registro_adjunto(Codigox)
                filename = rutapdf + "\" + carpeta + "\" + archivo
            Else
                'comentar en productivo, solo para pruebas

                filename = rutapdf + "\" + archivo
            End If

            'productivo
            'filename = rutapdf + "\" + Adjunto.Text
            Response.Clear()
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", archivo))
            Response.ContentType = "application/pdf"
            Response.WriteFile(filename)
            Response.End()

        End If

    End Sub
    Protected Function Vacio(ByVal nombre As Object) As Object
        If (Not nombre.Equals(DBNull.Value)) Then
            Return nombre
        Else
            Return "N/A"

        End If



    End Function
    Protected Function rellena(ByVal nombre As Integer) As String


        Return nombre.ToString("d4")


    End Function

    Protected Function fecha(ByVal fecha_1 As Date) As String
        Dim fechax As Date
        fechax = fecha_1
        Return fechax


    End Function
    Protected Function usuario(ByVal nombre As String) As String
        Dim l As User
        l = New User()
        l.User(conexion)

        Return l.Consultarusuario(nombre)

    End Function

    Protected Function etapa_nom(ByVal nombre As String) As String
        Dim e As Etapa
        e = New Etapa()
        e.Etapa(conexion)
        If (nombre = "0") Then
            Return "N/A"
        Else
            Return e.consulta_nombre_etapa(nombre)
        End If



    End Function


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        buscareo(sql)
    End Sub

    Sub buscareo(ByVal sql As String)
        Dim u As EO
        Dim indice As Integer = 0
        Dim checks As Boolean = False
        Dim band As String = "0"
        'Dim ult As String
        'Dim meses_max As String
        Try
            repeater.Visible = False
            If ((producto.Text <> "0") Or (lote.Text <> "")) Then

                u = New EO() ' nuevo objeto instancia
                u.EO(conexion) 'invoco el constructor y paso parametros de conexion

                sql = "Select {fn concat ('OO', 'T')} as tipo ,OOT_Adjunto.id as idadjunto, OOT_Adjunto.idOOT as idregistro from OOT  inner join OOT_Adjunto on OOT_Adjunto.idOOT=OOT.idOOT"


                If producto.Text <> "0" Then
                    sql = sql & " where OOT.idPdto = '" & producto.Text & "' "
                    band = "1"
                End If

                If lote.Text <> "" And band = "0" Then
                    sql = sql & " where OOT.Lote like '%" & lote.Text & "%' "
                    band = "1"
                ElseIf lote.Text <> "" Then
                    'sql = sql & " and OOT.Lote = '" & lote.Text & "' "
                    sql = sql & " and OOT.Lote like '%" & lote.Text & "%' "

                End If

                sql = sql & "union Select {fn concat ('DE', 'SV')} as tipo ,DesvAdjuntos.id as idadjunto, DesvAdjuntos.idDesv as idregistro from DesviosInt  inner join DesvAdjuntos on DesvAdjuntos.idDesv=DesviosInt.IdDesvioInt"

                If producto.Text <> "0" Then
                    sql = sql & " where DesviosInt.idPdto = '" & producto.Text & "' "
                    band = "1"
                End If

                If lote.Text <> "" And band = "0" Then
                    'sql = sql & " where DesviosInt.Lote = '" & lote.Text & "' "
                    sql = sql & " where DesviosInt.Lote like '%" & lote.Text & "%' "
                    band = "1"
                ElseIf lote.Text <> "" Then
                    'sql = sql & " and DesviosInt.Lote = '" & lote.Text & "' "
                    sql = sql & " and DesviosInt.Lote like '%" & lote.Text & "%' "
                End If

                sql = sql & "union Select {fn concat ('RE', 'CL')} as tipo ,ReclAdjuntos.id as idadjunto, ReclAdjuntos.idRecl as idregistro  from Reclamos  inner join ReclAdjuntos on ReclAdjuntos.idRecl = Reclamos.idReclamo"
                If producto.Text <> "0" Then
                    sql = sql & " where Reclamos.idPdto = '" & producto.Text & "' "
                    band = "1"
                End If

                If lote.Text <> "" And band = "0" Then
                    sql = sql & " where Reclamos.Partida like '%" & lote.Text & "%' "
                    band = "1"
                ElseIf lote.Text <> "" Then
                    sql = sql & " and Reclamos.Partida like '%" & lote.Text & "%' "
                End If


                If lote.Text <> "" Then
                    sql = sql & "union Select {fn concat ('CA', 'PA')} as tipo ,CapaAdjuntos.id as idadjunto, CapaAdjuntos.idCapa as idregistro from Capa  inner join CapaAdjuntos on CapaAdjuntos.idCapa=Capa.idCapa"

                    sql = sql & " where Capa.codAlcance like '%" & lote.Text & "%' "
                    band = "1"
                End If
                Label2.Text = ""
                repeater.Visible = True
                repeater.DataSource = u.ConsultaDoc(sql)
                repeater.DataBind()
            Else
                Label2.Text = "Por favor complete el campo producto y/o lote."
            End If
        Catch ex As Exception
            Label2.Text = "Error al buscar los archivos asociados al prodcuto/lote"

        End Try
    End Sub

    Protected Function nombre_adjunto(ByVal nombre As String, ByVal tipo As String) As String
        Dim e As OOT
        e = New OOT()
        e.OOT(conexion)
        If (nombre = "0") Then
            Return "N/A"
        Else
            Return e.obtener_nombre_adjunto(nombre, tipo)
        End If



    End Function
End Class
