﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration

Partial Class EO_default
    Inherits System.Web.UI.Page
    Dim codigo As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(0, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            'capa_asoc.AppendDataBoundItems = True

            codigo = DecryptText(Request.QueryString("ID"))

            Dim message As String = "Confirma la carga de la conclusión?"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("return confirm('")
            sb.Append(message)
            sb.Append("');")
            ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())


            '          If (Session("departamento") <> "GC") Then


            '              causa.Disabled = True
            '              conclusion.Disabled = True

            '               Button1.Enabled = False
            '
            '      Label2.Text = "Su departamento no tiene permisos para cargar esta investigacion"
            '  End If

        End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        nuevoconcgc()
    End Sub




    Sub nuevoconcgc()
        Dim e As OOT
        Dim r As Boolean
        Dim l As logs
        Dim max_caracteres As String
        Dim ancho_pagina As String


        Dim p As Parametros
        p = New Parametros()
        p.Parametros(conexion)
        max_caracteres = p.consulta_valor_nombre("max_caracteres")
        ancho_pagina = p.consulta_valor_nombre("ancho_pagina")
        l = New logs()
        l.logs(conexion)

        Try

            e = New OOT()
            e.OOT(conexion)
            Dim I As Integer
            Dim stri As Integer = "0"
            'guardo el dato de la tendencia por indice seleccionado 
            For I = 0 To causa.Items.Count - 1

                If (causa.Items(I).Selected = True) Then

                    stri = stri + causa.Items(I).Value



                    'genero la relacion entre el eo y el eolist
                    'e1.AgregarEO_EO(r, desviaciones.Items(I).Value)


                End If
            Next


            ' valido que las cadenas de observaciones no excedan los 2000 caracteres
            Dim bandera As String = "1"
            Dim campo As String

            If (Countcaracteres(causa_conclusion.Value, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Conclusión"
            End If



            If (bandera) Then

                r = e.Agregarnuevoconcgc(codigo, stri, causa_conclusion.Value)
                'valido que se cargue correctamente la investigacion
                If (r <> False) Then

                    'log
                    Dim xfecha As String
                    xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))

                    'log por carga de investigacion
                    Dim nro_oot As String
                    nro_oot = e.obtenerNroOOT(codigo)
                    l.Agregarlog3(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", xfecha, nro_oot, e.obtenerproducto(codigo), e.obtenerlote(codigo), e.obtenerfechacarga(codigo), e.obtenerestado(codigo), "Carga Conclución Garantia de Calidad")


                    Response.Redirect("../modifica_OOT.aspx?ID=" + EncryptText(codigo & "//3"))
                    Label1.Text = "Se cargó la conclusión con éxito"
                    '    
                Else
                    Label2.Text = "Error al cargar la conclusión"
                End If

            Else
                Label2.Text = "El campo: " + campo + " excede los " + max_caracteres + " caracteres. Verifique."

            End If
            '     
            '
        Catch ex As Exception
            Label2.Text = "Error al cargar la conclusión"
        End Try
    End Sub


    Public Shared Function Countcaracteres(ByVal tcString As String, ByVal ancho As Integer) As Integer
        Dim conteo As String = "0"
        For Each item As Char In tcString
            If (item <> vbCr) Then
                conteo = conteo + 1
                Continue For
            ElseIf (item = vbCr) Then
                conteo = conteo + ancho
            End If


        Next
        Return conteo
    End Function
    

  
 
  
 
End Class
