﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="agregar_concgc.aspx.vb" Inherits="EO_Default" title="Sistema Sinac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        #volver
        {
            height: 26px;
        }
    </style>
       <script type="text/javascript">
 
/*  Uso este javascript para el botón volver, ya que no puedo usar un control asp por que dispara el ajax del vanadium  */   
/* se modifico la funcion por que traiga inconvenientes con la encriptacion de parametros, asi como quedo solo va funionar con 1 parametro, si se pasa mas de uno hay q cambiarla (esto es debido q el split lo hacia con el "=" el cual cortaba el string codificado antes si el signo "=" era parte de la cadena encriptada*/
function redireccionar(){
var src = String( window.location.href ).split('?')[1];
var vrs = src.split('&')
var arr = [];

for (var x = 0, c = vrs.length; x < c; x++) 
{
        arr[x] = vrs[x];
};

location.href="../modifica_oot.aspx?"+ arr[0] } 

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
  
    <form id="form2" runat="server">
          <!--  validaciones  --> 
  <style type="text/css">
        body {
            
            font-family: Arial;
            font-size: 10pt;
        }
        input, select
        {
            width: 150px;
        }
    </style>
   
     <link href="../../css/ValidationEngine.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery/languages/jquery.validationEngine-es.js"
        charset="utf-8"></script>
    <script type="text/javascript" src="../../js/jquery/jquery.validationEngine.js"
        charset="utf-8"></script>
    <script type="text/javascript">
        $(function () {
            $("#aspnetForm").validationEngine('attach', { promptPosition: "topRight" });
        });
 

    </script>
    <script type="text/javascript">
        function DateFormat(field, rules, i, options) {
            var regex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
            if (!regex.test(field.val())) {
                return "Please enter date in dd/MM/yyyy format."
            }
        }
    </script>
  <!-- fin validaciones  --> 

<div id="contenedor1">
<div id="contenedor2">
     <div id="titulo_seccion">
         5- Ingrese los datos de la conclusion de Garantia de Calidad.
 </div>
    <ul>
    <li class="paneles">
    <div>
    

   
     <span class="dostercios8">
      <label for="revision">Causa (*)</label>
       
      <asp:ListBox runat="server" ID="causa" SelectionMode="Multiple" CssClass="validate[required]">
      <asp:ListItem  Value="1">Granulación</asp:ListItem>
      <asp:ListItem  Value="2">Compresión</asp:ListItem>
      <asp:ListItem  Value="4">Recubrimiento</asp:ListItem>
      <asp:ListItem  Value="8">Control de Calidad</asp:ListItem>
      </asp:ListBox>
    </span>
    
   
     
                 
               
       
      

 
  
    
  </div>
  </li>
   <li class="paneles">
    <div>
            
                    
           <span class="dostercios">
    <label for="causa_conclusion">Conclusión (*)</label>
           
      <textarea id="causa_conclusion" cols="20" name="S1" maxlength="2000" rows="2" runat="server" class="validate[required] text-input"></textarea>
   
            
      
    </span>
    
           
  

    
    
           </div>
  </li>
     
  </ul>
    <ul>
        
    <li class="panel_boton">
     <div>
     
    
   
      <span class="boton">
       
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" 
             style="height: 26px" />
   
        
    <input type="button" value="Volver"  id="volver" onclick="redireccionar(); return false;" ></span>
   </div>
    </li>
     </ul>
  </div>
  </div>
  <br>
  <div id="confirmacion">
  <asp:Label ID="Label1" runat="server"></asp:Label>
  </div>
    <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
       
        
         <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idCapa], [indentCapa] FROM Capa">
        </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IdDesvioInt], [codDINew] FROM DesviosInt">
        </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IdCocam], [codCCNew] FROM Cocam">
        </asp:SqlDataSource>
    </form>
</asp:Content>

