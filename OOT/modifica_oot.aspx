﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="modifica_oot.aspx.vb" Inherits="EO_modifica_eo" title="Sistema Sinac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        #volver
        {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!--  validaciones  --> 
 
  <style type="text/css">
        body {
            
            font-family: Arial;
            font-size: 10pt;
        }
        input
        {
            width: 250px;
        }
         .btn_input
        {
            width: 150px;
        }
        select
        {
            width: 250px;
        }
        #ctl00_ContentPlaceHolder1_estado
        {
        	width: 250px;
        }
    </style>
   
     <link href="../css/ValidationEngine.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="../js/jquery/languages/jquery.validationEngine-es.js"
        charset="utf-8"></script>
    <script type="text/javascript" src="../js/jquery/jquery.validationEngine.js"
        charset="utf-8"></script>
    <script type="text/javascript">
        $(function () {
            $("#aspnetForm").validationEngine('attach', { promptPosition: "topRight" });
        });
    </script>
    <script type="text/javascript">
        function DateFormat(field, rules, i, options) {
            var regex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
            if (!regex.test(field.val())) {
                return "Please enter date in dd/MM/yyyy format."
            }
        }


    </script>
  <!-- fin validaciones  --> 
    
<form id="form2" runat="server" >
    
<div id="contenedor1">
<div id="contenedor2">
     <div id="titulo_seccion">
         Modifique los datos del OOT.
 </div>
    <ul>
    <li class="paneles">
    <div>
      <span class="tercio">
       <label runat="server" id="label_numero_oot" for="numero_oot">Número de OOT</label>
        <input id="numero_oot" name="numero_oot" value="" runat="server" 
             maxlength="4" readonly="readonly"/>
      
    
    </span>
    <span class="tercio">

     <label for="revision">Tendencia (*)</label>
              
               
            
      <asp:ListBox runat="server" ID="tendencia" SelectionMode="Multiple" CssClass="validate[required]">
            </asp:ListBox>
    </span>
    
 
   
    
    <span class="tercio">
    <label for="analista">Analista(*)</label>
            <asp:DropDownList ID="analista" runat="server" DataSourceID="SqlDataSource2" 
            DataTextField="Denominacion" DataValueField="Id" CssClass="validate[required]">
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
      
    </span>
 
    <span class="dostercios">
      <label for="numlote">Número de Lote(*)</label>
  <asp:TextBox ID="numlote" runat="server" CssClass="validate[required]" MaxLength="15"></asp:TextBox>
      
    </span>
     
   <span class="completo">
      <label for="adecuacion">Archivos Adjuntos</label>
    <asp:ListBox runat="server" ID="Adjunto" SelectionMode="Multiple" DatasourceID="Sqldatasource6" 
            DataTextField="Adjunto" DataValueField="Id">
            

</asp:ListBox> 
    
    
        
    
    
    </span>     
 <span id="completo3">    
      <asp:Button ID="Button9" runat="server" Text="Abrir Seleccionado" />
      <asp:Button ID="Button10" runat="server" Text="Eliminar Seleccionado" />
     
     </span>    
  </div>
  </li>
   <li class="paneles">
    <div>
    <span class="tercio">
      <label for="producto">Codigo Producto(*)</label>
         <asp:DropDownList ID="producto" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="NomProd" DataValueField="IDPdto" CssClass="validate[required]">
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
    </span>
            <span class="dostercios">
      <label for="producto">Activo(*)</label>
      <asp:DropDownList ID="activo" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="NomProd" DataValueField="IDPdto" CssClass="validate[required]">
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
   
    </span>
 
   <span class="dostercios">
    <label for="elaborador">Elaborador(*)</label>
             <asp:DropDownList ID="elaborador" runat="server" DataSourceID="SqlDataSource5" 
            DataTextField="NomElab" DataValueField="IDElab" CssClass="validate[required]">
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
   
    
    </span>
       <span class="tercio">
     <label for="elaborador">Ensayo(*)</label>
          <asp:DropDownList ID="ensayo" runat="server" DataSourceID="SqlDataSource7" 
            DataTextField="nombre_ensayo" DataValueField="id" CssClass="validate[required]">
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
      
     
    </span>
   <span class="tercio">
      <label for="fecha_carga">Fecha de Carga</label>
      <input id="fecha_carga" name="fecha_carga" value="" runat="server" 
            readonly="readonly"/>
     
    </span>   
    
    
         <span class="dostercios">
  
         <label for="estado">Estado(*)</label>
        <input id="estado" name="numero_oot" value="" runat="server" 
             maxlength="4" readonly="readonly"/>
    </span>
   
         <span class="completo3">
             <asp:Button ID="Button11" runat="server" Text="+ Agregar" />
           
    </span>
        <span class="completo">
            
      <label for="certificado">Adjuntos</label>
      <asp:FileUpload ID="FileUpload1" runat="server" />
       
      
    </span>
    
     <span class="dostercios1">
      <label for="adecuacion">Lotes Involucrados</label>
    
    <asp:ListBox runat="server" ID="lotes_inv" SelectionMode="Multiple" DataTextField="nombre" DataValueField="Id">
            

</asp:ListBox> 

    
        
    
    
    </span> 
  <span class="dostercios88">
      <label for="ensayo">Lotes Involucrados</label>
      <input id="lote_i" name="ensayo" value="" runat="server" maxlength="15" placeholder="Escribir Aquí el lote a Agregar"/> 
         <asp:Button ID="Button13" runat="server" Text=" <---   Agregar Lote" />
         <asp:Button ID="Button14" runat="server" Text=" --->   Quitar Lote" />
      </span>
   
  
  </div>
  </li>
     
  </ul>
    <ul>
        
    <li class="panel_boton">
     <div>
     
    
   
      <div class="boton_oot">
      <div>
      <asp:Button class="btn_input" ID="Button6" runat="server" Text="Inv. CC" />
      <asp:Button class="btn_input" ID="Button5" runat="server" Text="Inv. RAP" />
      <asp:Button class="btn_input" ID="Button4" runat="server" Text="Inv. GC" />
      <asp:Button class="btn_input" ID="Button2" runat="server" Text="Inv. Elaboracion" />
      <asp:Button class="btn_input" ID="Button7" runat="server" Text="Conclusion Garantía" />
        </div>
        <div id="boton_oot_inf">
      
      <asp:Button class="btn_input" ID="btnReport" runat="server" Text="Generar FOGL" />
         <asp:Button class="btn_input" ID="Button1" runat="server" Text="Grabar" 
             style="height: 26px" onclientclick="return confirm('Confirma los cambios?');" />
   
        
   <input type="button" class="btn_input" value="Volver" id="volver" onclick="location.href='consulta_oot.aspx'">
   
   <asp:Button class="btn_input" ID="Button8" runat="server" Text="Anular OOT" />
   <asp:Button class="btn_input" ID="Button12" runat="server" Text="Cerrar OOT" />
   </div>
   </div>
   
        
   
   </div>

    </li>
                 <li>
                 </li>
     </ul>
         
  </div>
  </div>
  <br>
  <div id="confirmacion">
  <asp:Label ID="Label1" runat="server"></asp:Label>
  </div>
    <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
    <div id="notice">
  <asp:Label ID="Label3" runat="server"></asp:Label>
  </div>
   <div id="notice">
  <asp:Label ID="Label4" runat="server"></asp:Label>
  </div>
   
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IDPdto] , [CodPdto] + ' ---- ' + [DesPdto] as NomProd FROM [Productos] where CodPdto <> '' and Activado = '1' order by CodPdto">
            
        </asp:SqlDataSource>
        
         <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [Id], [Denominacion] FROM [User]">
        </asp:SqlDataSource>
        
         <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [Id], [Denominacion] FROM [User]">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idEtapa], [nombre_etapa] FROM Etapas">
        </asp:SqlDataSource>
           <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>"> 
              </asp:SqlDataSource>
                 <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IDElab], [NomElab] FROM [Elaboradores]">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource7" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [id], [nombre_ensayo] FROM OOT_ensayos">
        </asp:SqlDataSource>
    </form>
</asp:Content>

