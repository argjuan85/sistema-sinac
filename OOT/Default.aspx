﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Calling a PageMethod with jQuery - Brian Dobberteen - brian.dobberteen.com</title>
    <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery/jquery-1.3.2.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $("#ZipCodeTextBox").bind('change', function(event) {
                // Clear the CityStateLabel each time
                $("#CityStateLabel").text("");
                
                // If it doesn't look like a zip code, don't even bother with the request
                if (/^\d{5}(-\d{4})?$/.test($(this).val()))
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Default.aspx/GetCityStateByZip",
                        data: "{'zip': '" + $(this).val() + "'}",
                        dataType: "json",
                        success: function(msg) {
                            $("#CityStateLabel").text("Zip Code Entered Corresponds To: " + msg.d.City + ", " + msg.d.State);
                        }
                    });
            });
        });
    </script>
    <style type="text/css">
        body { font-family: Verdana, Arial, Helvetica, sans-serif; }
        h1 { font-size: 1em; margin: 0; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Calling a PageMethod with jQuery Demo</h1>
        <p>From: <a href="http://brian.dobberteen.com/code/calling-a-pagemethod-with-jquery/">Calling a PageMethod with jQuery</a></p>
        <br />
        Note: In order to fire our AJAX request, we must trigger the OnChange event of the TextBox.<br />
        In order to do this, simply enter a Zip Code and press the tab key.<br />
        <br />
        <asp:Label runat="server" ID="ZipCodeLabel" Text="Enter a Zip Code:" AssociatedControlID="ZipCodeTextBox" />&nbsp;
        <asp:TextBox runat="server" ID="ZipCodeTextBox" />
        <br />
        <asp:Label runat="server" ID="CityStateLabel" />
    </div>
    </form>
</body>
</html>
