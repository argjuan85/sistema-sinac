﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration

Partial Class EO_default
    Inherits System.Web.UI.Page
    Dim registro_actual As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(4, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        ElseIf (Session("nivel") <> "0") Then
            form2.Visible = False

        Else
            analista.AppendDataBoundItems = True
            producto.AppendDataBoundItems = True
            elaborador.AppendDataBoundItems = True
            activo.AppendDataBoundItems = True
            ensayo.AppendDataBoundItems = True
            'superior.AppendDataBoundItems = True
            'etapa.AppendDataBoundItems = True

            'TE = DecryptText(Request.QueryString("ID"))
            cargaformulario()
            Label3.Text = "(*) Campo Obligatorio"
            Dim message As String = "Confirma la carga del OOT?"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("return confirm('")
            sb.Append(message)
            sb.Append("');")
            ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())

        End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        nuevoeo()
    End Sub
    '
    Sub cargaformulario()
        'numero_oot.Value = "0001" 'asignar por funcion, seguramente lo obtengo de un parametro
        fecha_carga.Value = DateTime.Now.ToString("dd/MM/yyyy")
        If (Session("nivel") <> "0") Then
            'conclusiones.Disabled = True
            'fecha_cierre.Disabled = True
        End If
   
        'analista.Value = "jarganaraz"
        'superior.Value = "jarganaraz"
  

    End Sub


    Sub nuevoeo()
        'estas lineas las coloco para evitar inconvenientes con el trato de decimales con el tipo de datos double tanto en ambiente de desarrollo como subido en el servidor
        'Dim ri As New Globalization.CultureInfo("es-ES")
        '    ri.NumberFormat.NumberDecimalSeparator = "."
        '    System.Threading.Thread.CurrentThread.CurrentCulture = ri
        Dim e As OOT
        'Dim n As nombres
        Dim r As Integer
        Dim xfecha As String
        'Dim xfechacarga As String
        'Dim sext As String

        Dim l As logs
        l = New logs()
        l.logs(conexion)
        '
        Try
            '
            '
            '
            e = New OOT()
            e.OOT(conexion)
            '   
            '
            Dim I As Integer
            Dim stri As Integer = "0"
            Dim o1 As OOT
            o1 = New OOT()
            o1.OOT(conexion)
            Dim lo As Lotes
            lo = New Lotes()
            lo.Lotes(conexion)

            r = e.AgregarOOT(producto.Text, nro_lote.Text, elaborador.Text, revertirfecha(fecha_carga.Value), "0", analista.Text, "En Curso", ensayo.Text, activo.Text)


            ' cargo los lotes inv


            For I = 0 To lotes_inv.Items.Count - 1


                Dim str As String = lotes_inv.Items(I).ToString()

                'genero la relacion entre los lotes y el EO
                lo.Agregarlote(r, lotes_inv.Items(I).Value)



            Next



            Dim x As String
       
            For I = 0 To tendencia.Items.Count - 1

                If (tendencia.Items(I).Selected = True) Then
                    Dim str As String = tendencia.Items(I).ToString()

                    'genero la relacion entre el eo y el eolist
                    x = o1.AgregarOOT_Tendencia(r, tendencia.Items(I).Value)


                End If
            Next

            'log
            If (r <> "-1") Then
                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))

                'una vez definido como es el manejo del user cambiar el segundo parametro, de momento hardcode

                'log por carga de cabecera
                l.Agregarlog3(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", xfecha, e.obtenerNroOOT(r), producto.Text, nro_lote.Text, revertirfecha(fecha_carga.Value), "En Curso", "Carga Cabecera")


                Label1.Text = "Se cargó el OOT (Número: " & e.obtenerNroOOT(r).ToString("d4") & ") con éxito"
                Response.Redirect("consulta_oot.aspx?ID=" + EncryptText(e.obtenerNroOOT(r).ToString("d4") & "//3"))
            Else
                Label2.Text = "Error al cargar el OOT"
            End If
        Catch ex As Exception
            Label2.Text = "Error al cargar el OOT"
        End Try
    End Sub



    

  
 
  
 
    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        If (lote_i.Value <> "") Then
            lotes_inv.Items.Add(lote_i.Value)
            lote_i.Value = ""
        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        If (lotes_inv.Text <> "") Then
            lotes_inv.Items.Remove(lotes_inv.SelectedItem)

        End If
    End Sub
End Class
