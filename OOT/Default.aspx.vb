﻿Imports System.Web.Services
Imports System.Xml

Partial Class _Default
    Inherits System.Web.UI.Page

    Private Shared ReadOnly MAP_URL As String = "http://local.yahooapis.com/MapsService/V1/geocode?appid="

    Private Shared ReadOnly MAP_APP_ID As String = _
        "<ENTER YOUR KEY HERE>" ' Please enter your Yahoo Maps API key (http://developer.yahoo.com/maps/simple/)

    <WebMethod()> _
    Public Shared Function GetCityStateByZip(ByVal zip As String) As CityState
        Dim ret As CityState = New CityState

        Try
            Using reader As XmlReader = XmlReader.Create(MAP_URL & MAP_APP_ID & "&zip=" & zip)
                While reader.Read
                    If reader.Name = "City" And reader.IsStartElement Then
                        ret.City = reader.ReadElementString("City")
                    End If

                    If reader.Name = "State" And reader.IsStartElement Then
                        ret.State = reader.ReadElementString("State")
                    End If
                End While
            End Using
        Catch ex As Exception
            ' Fail silently - not a best practice, I know ;)
        End Try

        Return ret

    End Function

End Class

Public Class CityState
    Private _city As String
    Private _state As String

    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal value As String)
            _city = value
        End Set
    End Property

    Public Property State() As String
        Get
            Return _state
        End Get
        Set(ByVal value As String)
            _state = value
        End Set
    End Property
End Class

