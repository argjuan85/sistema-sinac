﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration

Partial Class EO_default
    Inherits System.Web.UI.Page
    Dim codigo As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(256, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            desv_asoc.AppendDataBoundItems = True
            ccam_asoc.AppendDataBoundItems = True
            capa_asoc.AppendDataBoundItems = True
            codigo = DecryptText(Request.QueryString("ID"))
            cargaformulario()
           
        End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        nuevoinvrap()
    End Sub


    Sub cargaformulario()
      

        If (capa_asoc.SelectedValue <> "1") Then
            'obs_capa.Disabled = True
            obs_capa.Value = "N/A"
        End If
        If (ccam_asoc.SelectedValue <> "1") Then
            'obs_ccam.Disabled = True
            obs_ccam.Value = "N/A"
        End If
        If (desv_asoc.SelectedValue <> "1") Then
            'obs_desv.Disabled = True
            obs_desv.Value = "N/A"
        End If


        'descomentar en productivo 
        '   If (Session("departamento") <> "GC") Then

        '       capa_asoc.Enabled = False
        '       desv_asoc.Enabled = False
        '       ccam_asoc.Enabled = False
        '       capa_asoc.Enabled = False
        '       obs_capa.Disabled = True
        '       obs_ccam.Disabled = True
        '       obs_desv.Disabled = True
        '       Button1.Enabled = False
        '       Label2.Text = "Su departamento no tiene permisos para cargar esta investigacion"
        '   End If

    End Sub

    Sub nuevoinvrap()
        Dim e As OOT
        Dim r As Integer
        Dim l As logs
        Dim max_caracteres As String
        Dim ancho_pagina As String
        Dim bandera As String = "1"
        Dim campo As String

        Dim p As Parametros
        p = New Parametros()
        p.Parametros(conexion)
        max_caracteres = p.consulta_valor_nombre("max_caracteres")
        ancho_pagina = p.consulta_valor_nombre("ancho_pagina")

        l = New logs()
        l.logs(conexion)

        Try
         
            e = New OOT()
            e.OOT(conexion)

            If (Countcaracteres(obs_desv.Value, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones Desvío"
            End If
            If (Countcaracteres(obs_ccam.Value, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones CCAM"
            End If
            If (Countcaracteres(obs_capa.Value, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones CAPA"
            End If
            If (bandera) Then

                r = e.Agregarinvgc(codigo, desv_asoc.Text, obs_desv.Value, ccam_asoc.Text, obs_ccam.Value, capa_asoc.Text, obs_capa.Value, "0", "0")
                'valido que se cargue correctamente la investigacion
                If (r <> "-1") Then

                    'log
                    Dim xfecha As String
                    xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))

                    'log por carga de investigacion
                    Dim nro_oot As String
                    nro_oot = e.obtenerNroOOT(codigo)
                    l.Agregarlog3(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", xfecha, nro_oot, e.obtenerproducto(codigo), e.obtenerlote(codigo), e.obtenerfechacarga(codigo), e.obtenerestado(codigo), "Carga Investigacion Garantía de Calidad")



                    Response.Redirect("../modifica_OOT.aspx?ID=" + EncryptText(codigo & "//3"))
                    Label1.Text = "Se cargó la investigación con éxito"
                    '    
             
                Else
                    Label2.Text = "Error al cargar la investigación"
                End If
            Else
                Label2.Text = "El campo: " + campo + " excede los " + max_caracteres + " caracteres. Verifique."

            End If
            '     
            '
        Catch ex As Exception
            Label2.Text = "Error al cargar la investigación"
        End Try
    End Sub

    '----------------------------------------------------------------------
    'RETORNA EL NUMERO DE caracteres DE UNA CADENA incluyendo espacios y saltos de linea en base a un parametro de ancho de pagina
    '----------------------------------------------------------------------
    Public Shared Function Countcaracteres(ByVal tcString As String, ByVal ancho As Integer) As Integer
        Dim conteo As String = "0"
        For Each item As Char In tcString
            If (item <> vbCr) Then
                conteo = conteo + 1
                Continue For
            ElseIf (item = vbCr) Then
                conteo = conteo + ancho
            End If


        Next
        Return conteo
    End Function
End Class
