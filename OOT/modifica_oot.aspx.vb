﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration
Imports System.Net.Mail

Partial Class EO_modifica_eo
    Inherits System.Web.UI.Page
    Dim registro_actual As Integer = 0
    Public codigo As String

    Public test As Integer = "23"
 
    Dim es As OOT

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(8, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            Try

                Dim uni As String
                Dim aux As String
                Dim estado As String
                Dim eeoo As String
                Dim band As String = "0"
                codigo = DecryptText(Request.QueryString("ID")) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos
                uni = InStr(codigo, "//")

                If (uni = "0") Then
                    codigo = DecryptText(Request.QueryString("ID")) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos
                Else
                    aux = DecryptText(Request.QueryString("ID")).Length - 3
                    eeoo = Right(DecryptText(Request.QueryString("ID")), 1)
                    codigo = Left(DecryptText(Request.QueryString("ID")), aux)
                    If (eeoo = "2") Then
                        band = "2"
                    Else
                        band = "1"
                    End If

                End If

                test = codigo

                SqlDataSource6.SelectParameters.Add("codigo", codigo.ToString())
                SqlDataSource6.SelectCommand = "SELECT id, adjunto FROM  OOT_Adjunto WHERE (idOOT = @codigo)"

                analista.AppendDataBoundItems = True
                producto.AppendDataBoundItems = True
                activo.AppendDataBoundItems = True
                ensayo.AppendDataBoundItems = True
                'Button12.Attributes.Add("onclick", " return confirma_elimina();")
                Label1.Text = ""
                Label2.Text = ""
                Label3.Text = ""
                Label4.Text = ""

                If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                    buscaroot(codigo, "0") 'carga la interfaz
                    If (band = "1") Then
                        Label1.Text = "Se modificaron correctamente los datos del OOT"
                        band = "0"
                    ElseIf (band = "2") Then
                        Label1.Text = "Se modificaron correctamente los datos de la Investigación"
                        band = "0"
                    ElseIf (band = "3") Then
                        Label1.Text = "Se cargaron correctamente los datos de la Investigación"
                        band = "0"
                    End If
                End If


                ' no coloco confirmacion con js por que me afecta los demas botones
                es = New OOT()
                es.OOT(conexion)
                estado = es.obtenerestado(codigo)
                If (estado = "En Curso") Or (estado = "PendienteCC") Then
                    Label3.Text = "(*) Campo Obligatorio. Para confirmar los cambios realizados presione Grabar."
                Else
                    Label3.Text = ""
                End If

            Catch
                Label2.Text = "Error al cargar datos del OOT"
                'Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If



    End Sub
    Sub buscaroot(ByVal codigo As String, ByVal label As String)
        Dim u As OOT
        Dim indice As String
        Try
            u = New OOT()
            u.OOT(conexion)
            Dim e As EO_EO
            e = New EO_EO()
            e.EO_EO(conexion)
            u = u.Consultaroot1(codigo)
            'Button11.Enabled = False
            Dim aus As String
            Dim I As Integer
            FileUpload1.Enabled = False
            numero_oot.Disabled = True
            estado.Disabled = True
            Dim aux1 As Integer
            aux1 = u.NroOOT
            numero_oot.Value = aux1.ToString("d4")
            fecha_carga.Disabled = True
            Dim aux As Date
            aux = u.FeCarga
            fecha_carga.Value = aux
            analista.SelectedValue = u.Analista
            producto.SelectedValue = u.idPdto
            ensayo.SelectedValue = u.ensayo
            activo.SelectedValue = u.activo
            indice = u.Tendencia
            Dim xex As String
            'For I = 0 To tendencia.Items.Count - 1
            'valido por indice y pesos para seleccionar el listbox

            'If ((tendencia.Items(I).Value And indice) = (tendencia.Items(I).Value)) Then

            'tendencia.Items(I).Selected = True
            'End If

            '           Next
            Dim auxe As String
            Dim borrar As String
            Dim cantidad As SqlDataReader
            Dim cont As Integer = 0
            'errores obvios
            cantidad = u.obtener_cantidad_tendencia(codigo)

            While cantidad.Read()
                tendencia.Items.Add(cantidad.GetString(0))
                cont = cont + 1
            End While
            cantidad.Close()
            borrar = tendencia.Items.Count
            For I = 0 To tendencia.Items.Count - 1
                borrar = tendencia.Items(I).Text
                'verifico que exista la relacion
                'comparo los primeros 22 caracteres, ya que comparar la cadena entera no funcona. Ver si se puede solucionar esto despues
                auxe = u.obtener_tendencia(codigo, u.obtenerid_tendencia(Left(Trim(tendencia.Items(I).Text), 22)))
                If (auxe <> "-1") Then
                    'tipo_error.SelectedValue = tipo_error.Items(I).Value
                    tendencia.Items(I).Selected = True
                End If

            Next

            'lotesinv



            cantidad = e.obtener_cantidad_lotes1(codigo)
            While cantidad.Read()
       
                lotes_inv.Items.Add(cantidad.GetString(0))

            End While
            cantidad.Close()

            elaborador.Text = u.Elaborador
            numlote.Text = u.Lote
            estado.Value = u.Estado
            If (Session("nivel") <> "0") Then
                Button11.Enabled = False
                Button12.Enabled = False
                Button1.Enabled = False
                Button8.Enabled = False
                Button10.Enabled = False
                ' deshabilitar edicion de campos para usuarios que no sean admins 
                elaborador.Enabled = False
                producto.Enabled = False
                tendencia.Enabled = False
                analista.Enabled = False
                numlote.Enabled = False

            End If

   


            If (u.Estado <> "En Curso") And (u.Estado <> "PendienteCC") Then
                numero_oot.Disabled = True
                fecha_carga.Disabled = True
                analista.Enabled = False
                producto.Enabled = False
                tendencia.Enabled = False
                elaborador.Enabled = False
                numlote.Enabled = False
                estado.Disabled = True
                activo.Enabled = False
                ensayo.Enabled = False

            End If
            Select Case label

                Case "1"
                    Label2.Text = "Cambio de estado no permitido o no tiene permisos para hacer cambios de estado"
                Case "2"
                    Label2.Text = "No estan dadas las condiciones para el cambio de estado. Verifique Investigaciones"
                Case "3"
                    Label1.Text = "Se modificaron correctamente los datos del OOT"

            End Select

            'validaciones botones
            Select Case estado.Value

                Case "En Curso"
                    Button7.Enabled = False
                    Button5.Enabled = False
                    Button4.Enabled = False
                    Button2.Enabled = False


                Case "PendienteCC"
                    Button7.Enabled = False
                    Button5.Enabled = False
                    Button4.Enabled = False
                    Button2.Enabled = False

                Case "ConcluidoCC"
                    Button7.Enabled = False
                    Button5.Enabled = True
                    Button4.Enabled = False
                    Button2.Enabled = False
                    Button13.Enabled = False
                    Button14.Enabled = False
                    lote_i.Disabled = True

                Case "PendienteQA"
                    Button7.Enabled = False
                    Button5.Enabled = True
                    Button4.Enabled = True
                    Button2.Enabled = False
                    Button13.Enabled = False
                    Button14.Enabled = False
                    lote_i.Disabled = True

                Case "PendienteElab"
                    Button7.Enabled = False
                    Button5.Enabled = True
                    Button4.Enabled = True
                    Button2.Enabled = True
                    Button13.Enabled = False
                    Button14.Enabled = False
                    lote_i.Disabled = True

                Case "PendienteCF"
                    Button7.Enabled = True
                    Button5.Enabled = True
                    Button4.Enabled = True
                    aus = u.verifica_investigacion(codigo, "ELAB")
                    If (aus = "1") Then
                        Button2.Enabled = True
                    Else
                        Button2.Enabled = False
                    End If
                    Button13.Enabled = False
                    Button14.Enabled = False
                    lote_i.Disabled = True


                Case "Concluido"
                    Button7.Enabled = True
                    Button5.Enabled = True
                    Button4.Enabled = True
                    aus = u.verifica_investigacion(codigo, "ELAB")
                    If (aus = "1") Then
                        Button2.Enabled = True
                    Else
                        Button2.Enabled = False
                    End If
                    Button13.Enabled = False
                    Button14.Enabled = False
                    lote_i.Disabled = True

                Case "Cerrado"
                    Button7.Enabled = True
                    Button5.Enabled = True
                    Button4.Enabled = True
                    Button10.Enabled = False
                    Button11.Enabled = False
                    Button13.Enabled = False
                    Button14.Enabled = False
                    lote_i.Disabled = True


                    aus = u.verifica_investigacion(codigo, "ELAB")
                    If (aus = "1") Then
                        Button2.Enabled = True
                    Else
                        Button2.Enabled = False
                    End If

                Case "Anulado"
                    btnReport.Enabled = False
                    Button10.Enabled = False
                    Button11.Enabled = False
                    Button1.Enabled = False
                    Button8.Enabled = False
                    Button12.Enabled = False
                    Button7.Enabled = True
                    Button5.Enabled = True
                    Button4.Enabled = True

                    'valido que hayan cargado investigaciones para dejar disponibles los botones 
                    aus = u.verifica_investigacion(codigo, "ELAB")
                    If (aus = "1") Then
                        Button2.Enabled = True
                    Else
                        Button2.Enabled = False
                    End If
                    'para este chequeo habria que ver si puedo ver si esta la firma electronica. ya que es la misma tabla q invgc
                    aus = u.verifica_investigacion(codigo, "CGC")
                    If (aus = "1") Then
                        Button7.Enabled = True
                    Else
                        Button7.Enabled = False
                    End If
                    aus = u.verifica_investigacion(codigo, "GC")
                    If (aus = "1") Then
                        Button4.Enabled = True
                    Else
                        Button4.Enabled = False
                    End If
                    aus = u.verifica_investigacion(codigo, "RAP")
                    If (aus = "1") Then
                        Button5.Enabled = True
                    Else
                        Button5.Enabled = False
                    End If
                    aus = u.verifica_investigacion(codigo, "CC")
                    If (aus = "1") Then
                        Button6.Enabled = True
                    Else
                        Button6.Enabled = False
                    End If

            End Select
        Catch ex As Exception

        End Try

    End Sub


    Sub actualizaroot()
 
        Dim u As OOT
        Dim r As String
        Dim re As String
        Dim sext As String
        Dim extension As String
        Dim tam As Integer
        Dim nom As String = "-1"
        Dim archivo As FileStream
        Dim band As String = "1"

   
        Dim l As logs
     
        Dim xfecha As String
        Try
            ' 'log
            l = New logs()
            l.logs(conexion)
            u = New OOT()
            u.OOT(conexion)


            If (FileUpload1.HasFile) Then
                Try
                    'Obtener parametros a utilizar 
                    nom = FileUpload1.FileName
                    tam = FileUpload1.FileBytes.Length
                    'NOTA: EL ARCHIVO PDF PRIMERO LO GUARDO EN UNA CARPETA EN EL 
                    'SERVIDOR Y LUEGO LO LEO LOCALMENTE PARA GUARDARLO EN LA BD (FOLDER) 

                    sext = Path.GetExtension(nom)
                    If sext <> ".exe" Then

                        ' Try to create the directory.
                        Dim Path As String
                        Path = Server.MapPath(rutapdf & "\" & codigo)

                        Dim di As DirectoryInfo = Directory.CreateDirectory(Path)
                        'usar esta en ambiente de prueba
                        Dim ruta As String = Server.MapPath(rutapdf & "\" & codigo & "\" & nom)

                        'este en ambiente productivo
                        'Dim ruta As String = rutapdf & "\" & codigo & "\" & nom

                        'ruta = ruta & nom 'DIRECCION EN EL SERVIDOR DONDE SE GUARDAR EL ARCHIVO FISICO 
                        'Guardar Archivo en Carpeta Servidor



                        FileUpload1.SaveAs(ruta)
                        'leer archivo 
                        archivo = New FileStream(ruta, FileMode.Open, FileAccess.Read)
                        Dim imagen(tam) As Byte
                        archivo.Read(imagen, 0, tam)
                        archivo.Close()
                        If (nom <> "-1") Then
                            u.agregaradjuntoOOT(codigo, nom)
                        End If
                    Else
                        extension = "no valida"
                    End If
                Catch ex As Exception
                    band = "-1"
                    Label2.Text = "Problemas al Guardar Archivo: " & ex.Message
                End Try
            End If



            'verifico si hay cambio de estado y si el nivel de user esta autorizado
            Dim mail_check As String
            Dim mail_a_notificar As String
            re = u.obtenerestado(codigo)
            r = u.validacambioestado(estado.Value, re, Session("nivel"), codigo)
            mail_check = r
            If (r = "1") Or (r = "2") Then


                'si habia adjunto y hubo error en el mismo no actualizo los datos
                If (band = "1") Then

                    If (r <> "1") Then
                        r = u.ActualizarestadoOOT(codigo, estado.Value)
                        'log cambio de estado
                        xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))

                        l.Agregarlog3(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", xfecha, u.obtenerNroOOT(codigo), producto.Text, numlote.Text, revertirfecha(fecha_carga.Value), estado.Value, "Cambio de Estado")

                    End If

                    Dim I As Integer
                    Dim stri As Integer = "0"
                    Dim x As String
                    Dim borrar As String

                    ' por simplicidad elimino las relaciones previas y cargo las nuevas luego de la modificacion 
                    u.elimina_tendencias(codigo)
                    borrar = tendencia.Items.Count
                    For I = 0 To tendencia.Items.Count - 1

                        If (tendencia.Items(I).Selected = True) Then
                            Dim str As String = tendencia.Items(I).ToString()

                            'genero la relacion entre el eo y el eolist
                            x = u.AgregarOOT_Tendencia(codigo, u.obtenerid_tendencia(Left(Trim(tendencia.Items(I).Text), 22)))


                        End If
                    Next

                    r = u.ActualizarOOT(codigo, producto.Text, numlote.Text, elaborador.Text, stri, analista.Text, ensayo.Text, activo.Text)
                    Dim lo As Lotes
                    lo = New Lotes()
                    lo.Lotes(conexion)

                    ' por simplicidad elimino las relaciones previas y cargo las nuevas luego de la modificacion 
                    lo.elimina_lotes_oot(codigo)
                    ' cargo los lotes inv


                    For I = 0 To lotes_inv.Items.Count - 1


                        Dim str As String = lotes_inv.Items(I).ToString()
                        lo.Agregarlote(codigo, lotes_inv.Items(I).Value)



                    Next





                    xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
                    'log por carga de cabecera
                    l.Agregarlog3(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", xfecha, u.obtenerNroOOT(codigo), producto.Text, numlote.Text, revertirfecha(fecha_carga.Value), estado.Value, "Modificación de datos de OOT (Cabecera)")

                    'ante un cambio estado debo enviar mail, valido que se haga un cambio exitoso para ello.

                    'se puede hacer un select segun el mail a mandar, y el valida estado devuelve r=2,3,4 segun corresponda
                    Select Case mail_check
                        Case "2"
                            'aca debo recorrer los user a notificar
                            mail_a_notificar = "jarganaraz@raffo.com.ar"
                            EnvioMail(mail_a_notificar, es.obtenerNroOOT(codigo).ToString("d4"))

                    End Select

                    'una vez definido como es el manejo del user cambiar el segundo parametro, de momento hardcode

                    'recargo interfaz para que se vean los cambios
                    Response.Redirect("modifica_OOT.aspx?ID=" + EncryptText(codigo & "//1"))
                    '                    Label1.Text = "Se modificaron correctamente los datos del OOT"
                End If

            ElseIf (r = "0") Then
                buscaroot(codigo, "2")
            Else

                buscaroot(codigo, "1")
            End If


        Catch ex As Exception
            Label2.Text = "Hubo un error al actualizar el OOT"
        End Try
    End Sub
    Private Function GetData(ByVal query As String) As DataTable
        Dim conString As String = ConfigurationManager.ConnectionStrings("EstandaresConnectionString").ConnectionString
        Dim cmd As New SqlCommand(query)
        Using con As New SqlConnection(conString)
            Using sda As New SqlDataAdapter()
                cmd.Connection = con

                sda.SelectCommand = cmd
                Using dt As New DataTable()
                    sda.Fill(dt)

                    Return dt
                End Using
            End Using
        End Using
    End Function
    

    Protected Sub GenerateReport1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            'Dim sql As String
            Dim pagina As Integer = 1
            Dim total_paginas As Integer = "1"
            Dim pagesize1 As String = 510.0F 'ancho de la pagina
            Dim fontsize1 As String = 11 'info inicial
            Dim fontsize2 As String = 12 'titulo cuadros
            Dim fontsize3 As String = 10 'contenido cuadros
            Dim fontsize4 As String = 13 'pie de la pagina
            Dim fontsize5 As String = 10 'firmas
            Dim fontsize6 As String = 16 'cabecera
            Dim fontsize9 As String = 10 'observaciones
            Dim ejex As Integer 'indice de altura de la pagina
            Dim proximo As Integer ' indice para calcular espacios siguientes, si entran en una pagina
            Dim ds As DataSet
            Dim cn As SqlConnection
            Dim cm As SqlCommand
            Dim da As SqlDataAdapter
            Dim observaciones As String
            Dim lineas As String 'se usa para calcular la cantidad de lines de los diferentes bloques de contenido
            'Dim canti As String ' longitud observaciones
            'Dim repe As String 'espacios restantes luego del contenido
            ' inv CC
            Dim espacios_observaciones As String = 29 'cantidad de espacios que debo agregar por default
            Dim espacios_observaciones1 As String = 27 'cantidad de espacios que debo agregar por default
            Dim espacios_observaciones2 As String = 44 'cantidad de espacios que debo agregar por default
            Dim espacios_observaciones_cc As String = 34 'cantidad de espacios que debo agregar por default
            Dim espacios_observaciones_desvcc As String = 18 'cantidad de espacios que debo agregar por default
            Dim espacios_observaciones_capa As String = 18 'cantidad de espacios que debo agregar por default
            Dim espacios_observaciones_conclusion As String = 28 'cantidad de espacios que debo agregar por default
            Dim espacios_conclusioncc As String = 32
            Dim espacios_tendencias As String = 5 'cantidad de espacios que debo agregar
            ' inv RAP
            Dim espacios_observaciones_rap As String = 18 'cantidad de espacios que debo agregar por default
            ' inv desvios ccam
            Dim espacios_observaciones_gc As String = 30 'cantidad de espacios que debo agregar por default
            Dim espacios_observaciones_gc_ccam As String = 18 'cantidad de espacios que debo agregar por default
            Dim espacios_observaciones_gc_capa As String = 30 'cantidad de espacios que debo agregar por default
         

            'espacios para pie de pagina cuando no hay investig
            Dim espacios_piecc As String = 39 'cantidad de espacios que debo agregar por default
            Dim espacios_pierap As String = 18 'cantidad de espacios que debo agregar por default
            Dim espacios_gc As String = 30 'cantidad de espacios que debo agregar por default
            Dim espacios_elab As String = 30 'cantidad de espacios que debo agregar por default
            Dim espacios_gc2 As String = 18 'cantidad de espacios que debo agregar por default

            Dim ancho_pagina As String = "93" ' cantidad de caracters del ancho de pagina
            Dim xancho_pagina As String 'utilizado para el calculo de os contenidos
            Dim alto_pagina As String = "105" 'limite para impresion en eje x
            'Dim contar As Integer
            'Dim contenidototal As String 'cantidad de lineas del contenido de las observaciones (incluidos saltos de linea)
            Dim espaciosrestantes As String 'espacios restantes de las observaciones (sin el contenido y los saltos de linea)
            Dim q As Integer
            Dim separador_cabecera As String = "10"



            Dim document As New Document(PageSize.A4, 88.0F, 88.0F, 10.0F, 10.0F)
            Dim NormalFont As Font = FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)
            'logo monteverde
            Dim image As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/img/logomonteverde.png"))


            Using memoryStream As New System.IO.MemoryStream()
                Dim writer As PdfWriter = PdfWriter.GetInstance(document, memoryStream)
                Dim phrase As Phrase = Nothing
                Dim cell As PdfPCell = Nothing
                Dim table As PdfPTable = Nothing
                Dim color__1 As Color = Nothing
                Dim test As Integer = 0
                Dim sql As String
                Dim aux As String = 0
                Dim es As OOT
                'Dim idprod As String
                Dim p As Parametros
                p = New Parametros()
                p.Parametros(conexion)
                es = New OOT()
                es.OOT(conexion)

                'calculo total de paginas del documento
                ' si faltan algunas investigaciones defino el total por aca
                'Dim xborrarx As String
                'xborrarx = es.verifica_investigacion(codigo, "CGC")
                'If (es.verifica_investigacion(codigo, "CGC") <> "0") Then
                '   total_paginas = "7"
                'ElseIf (es.verifica_investigacion(codigo, "ELAB") <> "-1") Then
                '   total_paginas = "7"
                'ElseIf (es.verifica_investigacion(codigo, "RAP") <> "-1") Then
                '    total_paginas = "5"
                'ElseIf (es.verifica_investigacion(codigo, "CC") <> "-1") Then
                '    total_paginas = "3"
                'Else
                '    total_paginas = "1"
                'End If



                'cargo parametros

                sops = p.consulta_valor_nombre("sops_oot")
                fogl = p.consulta_valor_nombre("fogl_oot")
                titulo = p.consulta_valor_nombre("titulo_fogl_oot")
                titulo2 = p.consulta_valor_nombre("titulo2_fogl_oot")
                vigencia = p.consulta_valor_nombre("vigencia_fogl_oot")



                document.Open()
                Dim cb As PdfContentByte = writer.DirectContent

                'Creamos las  fuentes

                Dim fuente As iTextSharp.text.pdf.BaseFont
                fuente = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont
                Dim fuente_cab As iTextSharp.text.pdf.BaseFont
                fuente_cab = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.BOLD).BaseFont
                Dim fuente_inv As iTextSharp.text.pdf.BaseFont
                fuente_inv = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.BOLD).BaseFont

                fuente = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont

                'como gran parte de la primera hoja es fijo no lo actualice y se trabaja de forma diferente al resto (ver nota apartir de cuando se cambia la forma de proceder)

                cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                'solo agrego esto (tabla sin contenido) para separar

                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.33F})
                For q = 1 To separador_cabecera

                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbCrLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 0.0F
                    cell.BorderWidthRight = 0.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 0.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 0.0F
                    table.AddCell(cell)
                Next

                document.Add(table)


                'fin separador


                table = New PdfPTable(2)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.05F, 0.95F})

                'consulto lotes involucrados

                sql = "select Lote from OOT_LoteInv where idOOT='" + codigo + "'"
                cn = New SqlConnection(conexion3)
                cn.Open()
                cm = New SqlCommand()
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                da = New SqlDataAdapter(cm)
                ds = New DataSet()
                da.Fill(ds)
                Dim Lotes As String
                Dim k As Integer
                For k = 0 To ds.Tables(0).Rows.Count - 1

                    Lotes = Lotes & ds.Tables(0).Rows(k).ItemArray(0).ToString
                    If (k < ds.Tables(0).Rows.Count - 1) Then
                        Lotes = Lotes & " / "
                    End If
                Next


                'consulto tendencias detectadas

                sql = "select idTendencia from OOT_tendencia where idOOT='" + codigo + "'"
                cn = New SqlConnection(conexion3)
                cn.Open()
                cm = New SqlCommand()
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                da = New SqlDataAdapter(cm)
                ds = New DataSet()
                da.Fill(ds)
                Dim tendencias As String = ""
                Dim borrar As String
                borrar = ds.Tables(0).Rows.Count - 1
                For k = 0 To ds.Tables(0).Rows.Count - 1

                    tendencias = tendencias & es.obtener_descripcion_tendencia(ds.Tables(0).Rows(k).ItemArray(0).ToString)
                    If (k < ds.Tables(0).Rows.Count - 1) Then
                        tendencias = tendencias & ". "
                    End If
                Next



                'linea 2

                phrase = New Phrase()
                cell = ImageCell("~/img/dot.png", 5.0F, PdfPCell.ALIGN_RIGHT)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)

                phrase = New Phrase()
                phrase.Add(New Chunk("Producto/Principio activo: " & es.obtenercodpdto(codigo) & " - " & es.obtenerdescpdto(codigo) & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)


                phrase = New Phrase()
                cell = ImageCell("~/img/dot.png", 5.0F, PdfPCell.ALIGN_RIGHT)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)

                phrase = New Phrase()
                phrase.Add(New Chunk("Lote/análisis: " & es.obtenerlote(codigo) & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)


                phrase = New Phrase()
                cell = ImageCell("~/img/dot.png", 5.0F, PdfPCell.ALIGN_RIGHT)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)

                phrase = New Phrase()
                phrase.Add(New Chunk("Lote/análisis involucrados: " & es.obs_na(Lotes) & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)

                'elaborador
                phrase = New Phrase()
                cell = ImageCell("~/img/dot.png", 5.0F, PdfPCell.ALIGN_RIGHT)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)

                phrase = New Phrase()
                phrase.Add(New Chunk("Elaborador: " & es.obtener_nombre_elaborador(es.obtenerelaborador(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)


                phrase = New Phrase()
                cell = ImageCell("~/img/dot.png", 5.0F, PdfPCell.ALIGN_RIGHT)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)

                phrase = New Phrase()
                phrase.Add(New Chunk("Fecha: " & es.obtenerfechacarga(codigo) & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)


                'analista
                phrase = New Phrase()
                cell = ImageCell("~/img/dot.png", 5.0F, PdfPCell.ALIGN_RIGHT)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)

                phrase = New Phrase()
                phrase.Add(New Chunk("Analista que analizó el lote: " & es.obtener_nombre_analista(es.obteneranalista(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)


                phrase = New Phrase()
                cell = ImageCell("~/img/dot.png", 5.0F, PdfPCell.ALIGN_RIGHT)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)

                phrase = New Phrase()
                phrase.Add(New Chunk("Tendencia detectada: " & vbLf & vbLf & tendencias & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 0.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 5.0F
                table.AddCell(cell)
                document.Add(table)

                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True

                'Defino por parametro el ancho (en caracteres) de la pagina. En base a eso calculo la cantidad de lineas (incremento en 1 al llegar al parametro definido o bien en un salto de linea)

                xancho_pagina = ancho_pagina
                lineas = "0"

                For Each item As Char In tendencias
                    If (item <> vbCr) And (xancho_pagina > "0") Then
                        xancho_pagina = xancho_pagina - 1
                        Continue For
                    End If
                    xancho_pagina = ancho_pagina
                    lineas = lineas + 1

                Next

                'estimo los espacio a agregar dinamicamente en base a la cantidad de lineas del contenido y el tamaño total predefinido mas arriba
                espaciosrestantes = espacios_tendencias - lineas

                'imprimo los espacios segun los calculos anteriores
                For k = 1 To espaciosrestantes

                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbCrLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 0.0F
                    cell.BorderWidthRight = 0.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 0.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 0.0F
                    table.AddCell(cell)
                Next

                document.Add(table)

                'Investigacion control de calidad
                'valido que exista si no dejo de generar el documento
                Dim xborrar As String
                xborrar = es.verifica_investigacion(codigo, "CC")
                If (es.verifica_investigacion(codigo, "CC") <> "-1") Then

                    'Tabla de contenido(1 columna)
                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    Dim tend As String
                    Dim indice As String
                    indice = es.obtenertendencia(codigo)

                    'consulto analistas
                    Dim idinv As String
                    idinv = es.obtenerid_InvCC(codigo)
                    sql = "select idAnalista from OOT_InvCC_analistas where idOOTInvCC='" + idinv + "'"
                    cn = New SqlConnection(conexion3)
                    cn.Open()
                    cm = New SqlCommand()
                    cm.CommandText = sql
                    cm.CommandType = CommandType.Text
                    cm.Connection = cn
                    da = New SqlDataAdapter(cm)
                    ds = New DataSet()
                    da.Fill(ds)
                    Dim analistas As String = ""

                    For k = 0 To ds.Tables(0).Rows.Count - 1

                        analistas = analistas & es.obtener_nombre_analistas(ds.Tables(0).Rows(k).ItemArray(0).ToString)
                        If (k < ds.Tables(0).Rows.Count - 1) Then
                            analistas = analistas & " - "
                        End If
                    Next



                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf & "Investigación de Control de Calidad", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)
                    observaciones = Trim(es.obs_na(es.consultar_obs_oos(codigo)))
                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf & "Analistas que analizaron los lotes involucrados: " & analistas & vbLf & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk(vbLf & "OOS/RC asociado a los lotes involucrados?: " & es.si_no(es.consultar_oos_asoc(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk(vbLf & "Observaciones: " & vbLf & vbLf & observaciones & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)
                    document.Add(table)
                    lineas = calcula_lineas(es.obs_na(es.consultar_obs_oos(codigo)), ancho_pagina)

                    'actualizo el ejex 
                    ejex = 351
                    ejex = ejex - lineas

                    '<<<<<<<<<<<<<<<<<<<<<<<<<<
                    ' a partir de aca debo validar si se acaba la pagina para seguir imprimiendo (tambien cambio  la forma de imprimir).

                    'debo calcular los espacios del bloque siguiente para ver si entran en la pagina corriente 

                    proximo = 20 ' espacios del titulo a imprimir
                  
                    'verifico que el proximo bloque de texto entre en el espacio restante de la pagina si no pasa a la hoja siguiente (el limite de x es 105)

                    If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                        pie_pagina(cb, fuente, fontsize4, total_paginas)
                        document.NewPage()
                        total_paginas = total_paginas + 1
                        cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                        'actualizo x
                        ejex = 720
                    End If

                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                    cb.ShowText("Desvios analíticos asociados a los lotes involucrados?: " & es.si_no(es.consultar_desv_asoc(codigo, "CC")) & vbLf & vbLf)
                    cb.EndText()

                    ejex = ejex - 20



                    proximo = 20 ' espacios del titulo a imprimir
                    observaciones = es.obs_na(es.consultar_obs_desv(codigo, "CC"))
                    proximo = proximo + calcula_lineas(observaciones, ancho_pagina) ' espacios dinamicos del contenido

                    'verifico que el proximo bloque de texto entre en el espacio restante de la pagina si no pasa a la hoja siguiente (el limite de x es 105)

                    If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                        pie_pagina(cb, fuente, fontsize4, total_paginas)
                        document.NewPage()
                        total_paginas = total_paginas + 1
                        cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                        'actualizo x
                        ejex = 720
                    End If
               

                    'observaciones desv

                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                    cb.ShowText("Observaciones: ")
                    cb.EndText()

                    ejex = ejex - 10

                    'muestro el bloque observaciones
                    escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                    'actualizo ejex
                    ejex = ejex - proximo - 10
                

                    proximo = 20 ' espacios del titulo de las observacion

                    'verifico que entre en el espacio restante de la pagina si no pasa a la hoja siguiente (el limite de x es 105)

                    If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then

                        pie_pagina(cb, fuente, fontsize4, total_paginas)
                        document.NewPage()
                        total_paginas = total_paginas + 1
                        cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                        'actualizo x
                        ejex = 720
                    End If

                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                    cb.ShowText("Control de cambio analítico reciente, aplicado a los lotes involucrados?: " & es.si_no(es.consultar_ccam_asoc(codigo, "CC")) & vbLf & vbLf)
                    cb.EndText()

                    ejex = ejex - 20

                    observaciones = es.obs_na(es.consultar_obs_ccam(codigo, "CC"))
                    proximo = 20 ' espacios del titulo de las observacion
                    proximo = proximo + calcula_lineas(observaciones, ancho_pagina) ' espacios dinamicos de las observacion


                  
                    If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then

                        pie_pagina(cb, fuente, fontsize4, total_paginas)
                        document.NewPage()
                        total_paginas = total_paginas + 1
                        cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                        ejex = 720
                    End If


                    'observaciones ccam

                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                    cb.ShowText("Observaciones: ")
                    cb.EndText()

                    ejex = ejex - 10

                    'muestro el bloque observaciones
                    escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                    'actualizo ejex
                    ejex = ejex - proximo - 10

                    'obs capa

                    proximo = 20

                    If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then

                        pie_pagina(cb, fuente, fontsize4, total_paginas)
                        document.NewPage()
                        total_paginas = total_paginas + 1
                        cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                        ejex = 720
                    End If


                    'contenido
                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                    cb.ShowText("CA/PA analítico aplicado a los lotes involucrados?: " & es.si_no(es.consultar_capa_asoc(codigo, "CC")) & vbLf & vbLf)
                    cb.EndText()

                    ejex = ejex - 20

                    'obs capa
                    observaciones = es.obs_na(es.consultar_obs_capa(codigo, "CC"))
                    proximo = 20 ' espacios del titulo de las observacion
                    proximo = proximo + calcula_lineas(observaciones, ancho_pagina) ' espacios dinamicos de las observacion


                    If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then

                        pie_pagina(cb, fuente, fontsize4, total_paginas)
                        document.NewPage()
                        total_paginas = total_paginas + 1
                        cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                        ejex = 720
                    End If

                    'observaciones capa

                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                    cb.ShowText("Observaciones: ")
                    cb.EndText()

                    ejex = ejex - 10

                    'muestro el bloque observaciones
                    escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                    ejex = ejex - proximo - 10

                    proximo = 20

                    If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then

                        pie_pagina(cb, fuente, fontsize4, total_paginas)
                        document.NewPage()
                        total_paginas = total_paginas + 1
                        cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                        'actualizo x
                        ejex = 720
                    End If

                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                    cb.ShowText("¿Se revisó la documentación asociada al lote según SOPS-MAR-0460? : " & es.si_no(es.consultar_revision(codigo, "CC")) & vbLf & vbLf)
                    cb.EndText()

                    ejex = ejex - 20

                    proximo = 20

                    If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                        pie_pagina(cb, fuente, fontsize4, total_paginas)
                        document.NewPage()
                        total_paginas = total_paginas + 1
                        cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                        ejex = 720
                    End If

                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                    cb.ShowText("Causa Asignable: " & es.si_no(es.consultar_causa(codigo)) & vbLf)
                    cb.EndText()

                    ejex = ejex - 20

                    proximo = 20

                    If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                        pie_pagina(cb, fuente, fontsize4, total_paginas)
                        document.NewPage()
                        total_paginas = total_paginas + 1
                        cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                        ejex = 720
                    End If


                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                    cb.ShowText("Nº CA/PA: " & es.si_no(es.consultar_capa_gen(codigo)) & vbLf & vbLf)
                    cb.EndText()

                    ejex = ejex - 20


                    observaciones = es.consultar_conclusion(codigo)
                    'espacios conclusion (titulo + contenido)
                    proximo = 20
                    proximo = proximo + calcula_lineas(observaciones, ancho_pagina) ' espacios dinamicos de las observacion
                    ' espacios firma
                    proximo = proximo + 20

                    If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                        pie_pagina(cb, fuente, fontsize4, total_paginas)
                        document.NewPage()
                        total_paginas = total_paginas + 1
                        cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                        ejex = 720
                    End If

                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                    cb.ShowText("Conclusión: ")
                    cb.EndText()

                    ejex = ejex - 10

                    'muestro el bloque observaciones
                    escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)
                    ejex = ejex - proximo

                    'firma electronica

                    firma(cb, fuente, fontsize3, es.obs_na(es.obtener_firmante(codigo, "CC")), es.obs_na(es.obtener_fecha_firma(codigo, "CC")), ejex)
                    ejex = ejex - 40


                    If (es.verifica_investigacion(codigo, "RAP") <> "-1") Then


                        ' espacio titulo inv
                        proximo = 20
                        proximo = proximo + 20
                        observaciones = es.obs_na(es.consultar_obs_invrap(codigo))
                        proximo = proximo + calcula_lineas(observaciones, ancho_pagina)

                        If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                            pie_pagina(cb, fuente, fontsize4, total_paginas)
                            document.NewPage()
                            total_paginas = total_paginas + 1
                            cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                            ejex = 720
                        End If

                        cb.BeginText()
                        cb.SetFontAndSize(fuente_inv, fontsize2)
                        cb.SetTextMatrix(190, ejex) '(xPos, yPos)
                        cb.ShowText(vbLf & "Investigación de Garantía de Calidad" & vbLf & vbLf)
                        cb.EndText()

                        ejex = ejex - 30

                        cb.BeginText()
                        cb.SetFontAndSize(fuente, fontsize3)
                        cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                        cb.ShowText(vbLf & "Observaciones realizadas por Revisión Anual de Producto: N/A")
                        cb.EndText()

                        'resto solo los espacios de obs
                        ejex = ejex - 20

                        'muestro obs

                        escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                        'actualizo eje


                        'obs

                        ejex = ejex - calcula_lineas(observaciones, ancho_pagina)



                        ' espacio titulo ca/pa asoc
                        proximo = 20


                        If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                            pie_pagina(cb, fuente, fontsize4, total_paginas)
                            document.NewPage()
                            total_paginas = total_paginas + 1
                            cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                            'actualizo x
                            ejex = 720
                        End If

                        cb.BeginText()
                        cb.SetFontAndSize(fuente, fontsize3)
                        cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                        cb.ShowText(vbLf & "CA/PA asociado: N/A") '+ es.obtener_nro_capa(es.consultar_capa_asoc(codigo, "RAP")))
                        cb.EndText()
                        ejex = ejex - 30
                        ' espacios firma
                        proximo = proximo + 20

                        If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                            pie_pagina(cb, fuente, fontsize4, total_paginas)
                            document.NewPage()
                            total_paginas = total_paginas + 1
                            cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                            ejex = 720
                        End If

                        'firma electronica

                        firma(cb, fuente, fontsize3, es.obs_na(es.obtener_firmante(codigo, "RAP")), es.obs_na(es.obtener_fecha_firma(codigo, "RAP")), ejex)
                        ejex = ejex - 40


                        ' espacios titulo
                        proximo = 20

                        If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                            pie_pagina(cb, fuente, fontsize4, total_paginas)
                            document.NewPage()
                            total_paginas = total_paginas + 1
                            cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                            ejex = 720
                        End If


                        cb.BeginText()
                        cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                        cb.ShowText(vbLf & "¿Existen desvíos asociados a los lotes involucrados?: " + es.si_no(es.consultar_capa_asoc(codigo, "QA")) & vbLf & vbLf)
                        cb.EndText()
                        ejex = ejex - 20



                        proximo = 20
                        observaciones = es.obs_na(es.consultar_obs_desv(codigo, "QA"))
                        proximo = proximo + calcula_lineas(observaciones, ancho_pagina)

                        If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                            pie_pagina(cb, fuente, fontsize4, total_paginas)
                            document.NewPage()
                            total_paginas = total_paginas + 1
                            cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                            'actualizo x
                            ejex = 720
                        End If

                        cb.BeginText()
                        cb.SetFontAndSize(fuente, fontsize3)
                        cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                        cb.ShowText(vbLf & "Observaciones: ")
                        cb.EndText()
                        ejex = ejex - 10

                        'muestro obs

                        escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                        'actualizo eje
                        ejex = ejex - 30 - calcula_lineas(observaciones, ancho_pagina)


                        ' espacios titulo
                        proximo = 20

                        If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                            pie_pagina(cb, fuente, fontsize4, total_paginas)
                            document.NewPage()
                            total_paginas = total_paginas + 1
                            cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                            'actualizo x
                            ejex = 720
                        End If

                        cb.BeginText()
                        cb.SetFontAndSize(fuente, fontsize3)
                        cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                        cb.ShowText(vbLf & "¿Existen Control de Cambio reciente, aplicado a los lotes involucrados?: " + es.si_no(es.consultar_capa_asoc(codigo, "QA")) & vbLf & vbLf)
                        cb.EndText()
                        ejex = ejex - 20


                        ' espacios obs
                        proximo = 20
                        ' espacio obs
                        proximo = proximo + 20
                        observaciones = es.obs_na(es.consultar_obs_ccam(codigo, "QA"))
                        ' espacios dinamicos de las observaciones
                        proximo = proximo + calcula_lineas(observaciones, ancho_pagina)
                        If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                            pie_pagina(cb, fuente, fontsize4, total_paginas)
                            document.NewPage()
                            total_paginas = total_paginas + 1
                            cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                            'actualizo x
                            ejex = 720
                        End If

                        cb.BeginText()
                        cb.SetFontAndSize(fuente, fontsize3)
                        cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                        cb.ShowText(vbLf & "Observaciones: ")
                        cb.EndText()
                        ejex = ejex - 10



                        'muestro obs

                        escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                        'actualizo eje


                        ejex = ejex - 30 - calcula_lineas(observaciones, ancho_pagina)


                        ' ca/pa

                        ' espacios titulo
                        proximo = 20

                        If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                            pie_pagina(cb, fuente, fontsize4, total_paginas)
                            document.NewPage()
                            total_paginas = total_paginas + 1
                            cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                            'actualizo x
                            ejex = 720
                        End If

                        cb.BeginText()
                        cb.SetFontAndSize(fuente, fontsize3)
                        cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                        cb.ShowText(vbLf & "¿Existen CA/PA aplicado a los lotes involucrados?: " + es.si_no(es.consultar_capa_asoc(codigo, "QA")) & vbLf)
                        cb.EndText()
                        ejex = ejex - 20


                        ' espacios obs
                        proximo = 20
                        proximo = proximo + 30
                        observaciones = es.obs_na(es.consultar_obs_capa(codigo, "QA"))
                        proximo = proximo + calcula_lineas(observaciones, ancho_pagina)
                        If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                            pie_pagina(cb, fuente, fontsize4, total_paginas)
                            document.NewPage()
                            total_paginas = total_paginas + 1
                            cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                            'actualizo x
                            ejex = 720
                        End If

                        cb.BeginText()
                        cb.SetFontAndSize(fuente, fontsize3)
                        cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                        cb.ShowText(vbLf & "Observaciones: ")
                        cb.EndText()
                        ejex = ejex - 10


                        'muestro obs

                        escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                        'actualizo eje


                        ejex = ejex - calcula_lineas(observaciones, ancho_pagina)
                        ejex = ejex - 40

                       
                        'firma electronica

                        ' espacios obs
                        proximo = 40
                       
                        If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                            pie_pagina(cb, fuente, fontsize4, total_paginas)
                            document.NewPage()
                            total_paginas = total_paginas + 1
                            cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                            'actualizo x
                            ejex = 720
                        End If

                        firma(cb, fuente, fontsize3, es.obs_na(es.obtener_firmante(codigo, "GC")), es.obs_na(es.obtener_fecha_firma(codigo, "GC")), ejex)

                        ejex = ejex - 40



                        If (es.verifica_investigacion(codigo, "ELAB") <> "-1") Then

                            ' espacios titulo
                            proximo = 20

                            If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                                pie_pagina(cb, fuente, fontsize4, total_paginas)
                                document.NewPage()
                                total_paginas = total_paginas + 1
                                cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                                ejex = 720
                            End If

                            cb.BeginText()
                            cb.SetFontAndSize(fuente_inv, fontsize2)
                            cb.SetTextMatrix(190, ejex) '(xPos, yPos)
                            cb.ShowText(vbLf & "Investigación de Elaboración" & vbLf & vbLf)
                            cb.EndText()
                            ejex = ejex - 30

                            ' espacios titulo
                            proximo = 20

                            If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                                pie_pagina(cb, fuente, fontsize4, total_paginas)
                                document.NewPage()
                                total_paginas = total_paginas + 1
                                cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                                'actualizo x
                                ejex = 720
                            End If

                            cb.BeginText()
                            cb.SetFontAndSize(fuente, fontsize3)
                            cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                            cb.ShowText(vbLf & "Revisión de protocolo de Elaboración: " & es.si_no(es.consultar_revision(codigo, "ELAB")) & vbLf)
                            cb.EndText()
                            ejex = ejex - 20


                            ' espacios titulo
                            proximo = 20
                            proximo = proximo + 30
                            observaciones = es.obs_na(es.consultar_causa_elab(codigo))
                            proximo = proximo + calcula_lineas(observaciones, ancho_pagina)

                            If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                                pie_pagina(cb, fuente, fontsize4, total_paginas)
                                document.NewPage()
                                total_paginas = total_paginas + 1
                                cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                                ejex = 720
                            End If



                            cb.BeginText()
                            cb.SetFontAndSize(fuente, fontsize3)
                            cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                            cb.ShowText(vbLf & "Causa: ")
                            cb.EndText()
                            ejex = ejex - 10


                            'muestro obs

                            escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                            'actualizo eje


                            ejex = ejex - calcula_lineas(observaciones, ancho_pagina)
                            ejex = ejex - 30

                            proximo = 20

                            If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                                pie_pagina(cb, fuente, fontsize4, total_paginas)
                                document.NewPage()
                                total_paginas = total_paginas + 1
                                cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                                ejex = 720
                            End If

                            cb.BeginText()
                            cb.SetFontAndSize(fuente, fontsize3)
                            cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                            'es un campo de texto comun en este caso, no es un id como los otros
                            cb.ShowText(vbLf & "Número de CA/PA asociado: " & es.obtener_nro_capa(es.consultar_capa_asoc(codigo, "ELAB")) & vbLf)
                            cb.EndText()
                            ejex = ejex - 20



                            ' espacios titulo
                            proximo = 20

                            proximo = proximo + 30
                            observaciones = es.obs_na(es.consultar_observaciones_elab(codigo))
                            proximo = proximo + calcula_lineas(observaciones, ancho_pagina)


                            If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                                pie_pagina(cb, fuente, fontsize4, total_paginas)
                                document.NewPage()
                                total_paginas = total_paginas + 1
                                cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                                ejex = 720
                            End If

                            cb.BeginText()
                            cb.SetFontAndSize(fuente, fontsize3)
                            cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                            cb.ShowText(vbLf & "Observaciones: ")
                            cb.EndText()
                            ejex = ejex - 10

                            'muestro obs

                            escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                            'actualizo eje


                            ejex = ejex - calcula_lineas(observaciones, ancho_pagina)
                            ejex = ejex - 30


                            proximo = 20
                            proximo = proximo + 20
                            observaciones = es.obs_na(es.consultar_causa_conclusion(codigo))
                            proximo = proximo + calcula_lineas(observaciones, ancho_pagina)

                            cb.BeginText()
                            cb.SetFontAndSize(fuente, fontsize3)
                            cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                            cb.ShowText(vbLf & "Causa/Conclusión: ")
                            cb.EndText()
                            ejex = ejex - 10

                            'muestro obs

                            escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                            'actualizo eje


                            ejex = ejex - calcula_lineas(observaciones, ancho_pagina)
                            ejex = ejex - 40

                            proximo = 40

                            If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                                pie_pagina(cb, fuente, fontsize4, total_paginas)
                                document.NewPage()
                                total_paginas = total_paginas + 1
                                cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                                ejex = 720
                            End If

                        

                            'firma electronica
                            firma(cb, fuente, fontsize3, es.obs_na(es.obtener_firmante(codigo, "ELAB")), es.obs_na(es.obtener_fecha_firma(codigo, "ELAB")), ejex)
                            ejex = ejex - 40



                            If (es.verifica_investigacion(codigo, "CGC") = "0") Then


                                proximo = 20

                                If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                                    pie_pagina(cb, fuente, fontsize4, total_paginas)
                                    document.NewPage()
                                    total_paginas = total_paginas + 1
                                    cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                                    ejex = 720
                                End If

                                cb.BeginText()
                                cb.SetFontAndSize(fuente, fontsize3)
                                cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                                'es un campo de texto comun en este caso, no es un id como los otros
                                cb.ShowText(vbLf & "Causa: " & es.obtener_nombre_causa(es.consultar_causaasignable(codigo)) & vbLf)
                                cb.EndText()
                                ejex = ejex - 20



                                ' espacios titulo
                                proximo = 20
                                proximo = proximo + 40
                                observaciones = es.consultar_conclusiongc(codigo)
                                proximo = proximo + calcula_lineas(observaciones, ancho_pagina)
                                If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                                    pie_pagina(cb, fuente, fontsize4, total_paginas)
                                    document.NewPage()
                                    cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                                    'actualizo x
                                    ejex = 720
                                End If

                                cb.BeginText()
                                cb.SetFontAndSize(fuente, fontsize3)
                                cb.SetTextMatrix(45, ejex) '(xPos, yPos)
                                cb.ShowText(vbLf & "Conclusiones de QA: ")
                                cb.EndText()
                                ejex = ejex - 10

                                'muestro obs

                                escribe_texto1(cb, observaciones, fontsize3, 45, ejex, 550)

                                'actualizo eje
                                ejex = ejex - calcula_lineas(observaciones, ancho_pagina)
                                ejex = ejex - 40

                                proximo = 40

                                If (verifica_final_pagina(ejex, proximo, alto_pagina)) Then
                                    pie_pagina(cb, fuente, fontsize4, total_paginas)
                                    document.NewPage()
                                    total_paginas = total_paginas + 1
                                    cabecera(cb, fuente_cab, fontsize6, fuente, fontsize3, total_paginas, rellena(es.obtenerNroOOT(codigo)), es.obtenerestado(codigo), fontsize4, image)
                                    ejex = 720
                                End If

                                'firma electronica
                                firma(cb, fuente, fontsize3, es.obs_na(es.obtener_firmante(codigo, "CGC")), es.obs_na(es.obtener_fecha_firma(codigo, "CGC")), ejex)

                                ejex = ejex - 40


                                pie_pagina(cb, fuente, fontsize4, total_paginas)



                                'if de validacion de conclusion qa
                            Else

                                pie_pagina(cb, fuente, fontsize4, total_paginas)


                            End If
                            'if de validacion de inv elab
                        Else
                            'en este caso nada
                        End If


                        'if de validacion de inv qc
                    Else
                        'en este caso nada
                    End If


                    'validacion invcc
                Else

                    pie_pagina(cb, fuente, fontsize4, total_paginas)


                End If


                document.Close()
                Dim bytes As Byte() = memoryStream.ToArray()
                memoryStream.Close()
                'cn.Close()
                Response.Clear()
                Response.ContentType = "application/pdf"
                Response.AddHeader("Content-Disposition", "attachment; filename=FOGL-MAR-2142.pdf")
                Response.ContentType = "application/pdf"
                Response.Buffer = True
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.BinaryWrite(bytes)
                Response.[End]()
                Response.Close()
            End Using
        Catch ex As Exception
            If (ex.Message = "No hay ninguna fila en la posición 0.") Then
                Label2.Text = "No hay registrados para este OOT"
            Else
                Label2.Text = "Error al generar el informe" + ex.Message
            End If


        End Try
    End Sub

    Private Shared Sub DrawLine(ByVal writer As PdfWriter, ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single, ByVal color As Color)
        Dim contentByte As PdfContentByte = writer.DirectContent
        contentByte.SetColorStroke(color)
        contentByte.MoveTo(x1, y1)
        contentByte.LineTo(x2, y2)
        contentByte.Stroke()
    End Sub
    Private Shared Function PhraseCell(ByVal phrase As Phrase, ByVal align As Integer) As PdfPCell
        Dim cell As New PdfPCell(phrase)
        cell.BorderColor = Color.WHITE
        cell.VerticalAlignment = PdfCell.ALIGN_TOP
        cell.HorizontalAlignment = align
        cell.PaddingBottom = 2.0F
        cell.PaddingTop = 0.0F
        Return cell
    End Function
    Private Shared Function ImageCell(ByVal path As String, ByVal scale As Single, ByVal align As Integer) As PdfPCell
        Dim image As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path))
        image.ScalePercent(scale)
        Dim cell As New PdfPCell(image)
        cell.BorderColor = Color.WHITE
        cell.VerticalAlignment = PdfCell.ALIGN_TOP
        cell.HorizontalAlignment = align
        cell.PaddingBottom = 0.0F
        cell.PaddingTop = 0.0F
        Return cell
    End Function
    Protected Function rellena(ByVal nombre As Integer) As String


        Return nombre.ToString("d4")


    End Function

  

   

    Protected Sub Button6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button6.Click
        'si ya se cargo una investigacion abro una ventana de edicion
        Dim aus As String
        aus = es.verifica_investigacion(codigo, "CC")
        If (aus = "1") Then
            Response.Redirect("./invcc/modificar_invcc.aspx?ID=" + EncryptText(codigo))
        Else
            Response.Redirect("./invcc/agregar_invcc.aspx?ID=" + EncryptText(codigo))
        End If


    End Sub

    Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button5.Click
        'si ya se cargo una investigacion abro una ventana de edicion
        Dim aus As String
        aus = es.verifica_investigacion(codigo, "RAP")
        If (aus = "1") Then
            Response.Redirect("./invrap/modificar_invrap.aspx?ID=" + EncryptText(codigo))
        Else
            Response.Redirect("./invrap/agregar_invrap.aspx?ID=" + EncryptText(codigo))
        End If

    End Sub

    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click
        'si ya se cargo una investigacion abro una ventana de edicion
        Dim aus As String
        aus = es.verifica_investigacion(codigo, "GC")
        If (aus = "1") Then
            Response.Redirect("./invgc/modificar_invgc.aspx?ID=" + EncryptText(codigo))
        Else
            Response.Redirect("./invgc/agregar_invgc.aspx?ID=" + EncryptText(codigo))
        End If
    End Sub

    Protected Sub Button2_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        'si ya se cargo una investigacion abro una ventana de edicion
        Dim aus As String
        aus = es.verifica_investigacion(codigo, "ELAB")
        If (aus = "1") Then
            Response.Redirect("./invelab/modificar_invelab.aspx?ID=" + EncryptText(codigo))
        ElseIf (es.obtenerestado(codigo) = "PendienteElab") Then
            Response.Redirect("./invelab/agregar_invelab.aspx?ID=" + EncryptText(codigo))
        End If
    End Sub

    Protected Sub Button7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button7.Click
        'si ya se cargo una investigacion abro una ventana de edicion
        Dim aus As String
        aus = es.verifica_investigacion(codigo, "CGC")
        If (aus = "0") Then ' siempre va ir al modificar, ya que esta la primer inv de qa cargada
            Response.Redirect("./concgc/modificar_concgc.aspx?ID=" + EncryptText(codigo))
        Else
            Response.Redirect("./concgc/agregar_concgc.aspx?ID=" + EncryptText(codigo))
        End If
    End Sub

    Protected Sub Button8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button8.Click
        estado.Value = "Anulado"
        Label4.Text = "Debe confirmar los cambios realizados presionando el botón Grabar"
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        actualizaroot()

    End Sub

 
    
    Protected Sub Button9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button9.Click
        Try
            If (Adjunto.Text <> "") Then
                Dim filename As String
                Dim es As OOT
                Dim nomarchivo As String
                es = New OOT()
                es.OOT(conexion)
                'Ambiente 
                nomarchivo = es.obtenerarchivoadjunto(Adjunto.Text)
                filename = rutapdf + "\" + codigo + "\" + nomarchivo
                'productivo
                'filename = rutapdf + "\" + Adjunto.Text
                Response.Clear()
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", nomarchivo))
                Response.ContentType = "application/pdf"
                Response.WriteFile(filename)
                Response.End()
            Else
                label2.text = "Por favor Seleccione un archivo para abrir."
            End If

        Catch ex As Exception
            Label2.Text = "Problemas al cargar Archivo: " & ex.Message
        End Try
    End Sub

    Protected Sub Button11_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button11.Click
        FileUpload1.Enabled = True
        Label4.Text = "Para confirmar los cambios, presione Grabar"
    End Sub

    Protected Sub Button10_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button10.Click

        If (Adjunto.Text <> "") Then
            'necesito el nombre de archivo guardaddo
            Dim u As OOT
            Dim snom As String

            u = New OOT()
            u.OOT(conexion)

            'obtengo nombre archivo
            snom = u.obtenerarchivoadjunto(Adjunto.Text)

            'elimino ruta de la base
            u.eliminaradjunto(Adjunto.Text)

            'usar esta en ambiente de prueba
            Dim druta As String = Server.MapPath(rutapdf & "\" & codigo & "\" & snom)

            'defino la ruta 
            'este en ambiente productivo
            'Dim ruta As String = rutapdf & "\" & codigo & "\" & nom

            'rutas fisicas fuera 
            'druta = rutapdf & "\" & snom


            File.Delete(druta)

            Response.Redirect("modifica_OOT.aspx?ID=" + EncryptText(codigo & "//1"))
        Else
            label2.text = "Por favor Seleccione un archivo para eliminar."
        End If


    End Sub



    Public Sub EnvioMail(ByVal mail As String, ByVal registro As String)
        Try
            Dim p As Parametros
            p = New Parametros()
            p.Parametros(conexion)

            'cargo parametros

            sops = p.consulta_valor_nombre("sops_oot")
            fogl = p.consulta_valor_nombre("fogl_oot")
            titulo = p.consulta_valor_nombre("titulo_fogl_oot")
            vigencia = p.consulta_valor_nombre("vigencia_fogl_oot")

            Dim message As New MailMessage

            Dim smtp As New SmtpClient

            message.From = New MailAddress(p.consulta_valor_nombre("Mail"))

            message.To.Add(mail)

            message.Body = "Ya se encuentra disponible para concluir el OOT de referencia." & vbLf & vbLf

            message.Body = message.Body & "AVISO: Este es un mail automatico generado por el Sistema Sinac. No responder sobre este mail. Cualquier duda comunicarse con QA"

            message.Subject = "OOT: " & registro

            message.Priority = MailPriority.Normal

            smtp.EnableSsl = False

            smtp.Port = p.consulta_valor_nombre("puerto")

            smtp.Host = p.consulta_valor_nombre("Servidor")

            smtp.Credentials = New Net.NetworkCredential(p.consulta_valor_nombre("Alias"), p.consulta_valor_nombre("Pass"))

            smtp.Send(message)
        Catch ex As Exception

        End Try


    End Sub


    '------------------------------------------------------
    ' CUENTA EL NUMERO DE APARICIONES DE LA SUBCADENA
    ' BASE 1:
    '------------------------------------------------------
    Public Shared Function ContarCad(ByVal miCadena As String, ByVal search As String, Optional ByVal start As Long = 1, Optional ByVal Compare As CompareMethod = vbBinaryCompare) As Long
        Dim i As Long, result As Long
        start = start - 1 'convertir a base 1
        i = InStr(start, miCadena, search, Compare)
        Do While i
            result = result + 1
            i = InStr(i + 1, miCadena, search, Compare)
        Loop
        Return result
    End Function

    Public Shared Function Countcaracteres(ByVal tcString As String, ByVal ancho As Integer) As Integer
        Dim conteo As String = "0"
        For Each item As Char In tcString
            If (item <> vbCr) Then
                conteo = conteo + 1
                Continue For
            ElseIf (item = vbCr) Then
                conteo = conteo + ancho
            End If


        Next
        Return conteo
    End Function

    Public Shared Function pie_pagina(ByVal cb As PdfContentByte, ByVal fuente As iTextSharp.text.pdf.BaseFont, ByVal fontsize4 As String, ByVal total_paginas As String) As Boolean

        ' nuevo pie de pagina 
        fuente = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont
        cb.BeginText()
        cb.SetFontAndSize(fuente, fontsize4)
        cb.SetTextMatrix(45, 45) '(xPos, yPos)
        cb.ShowText(sops & "                  Fecha de emisión: " & String.Format("{0:dd/MM/yyyy - H:mm:ss}", Date.Now()) & "     Página 3 de " & total_paginas)
        cb.EndText()
        'fin nuevo pie

    End Function

    Public Shared Function cabecera(ByVal cb As PdfContentByte, ByVal fuente_cab As iTextSharp.text.pdf.BaseFont, ByVal fontsize6 As String, ByVal fuente As iTextSharp.text.pdf.BaseFont, ByVal fontsize3 As String, ByVal total_paginas As String, ByVal oot As String, ByVal estado As String, ByVal fontsize4 As String, ByVal image As iTextSharp.text.Image) As Boolean
        'nueva cab
        cb.BeginText()
        cb.SetFontAndSize(fuente_cab, fontsize6)
        cb.SetTextMatrix(45, 805) '(xPos, yPos)
        cb.ShowText(fogl)
        cb.EndText()

        cb.BeginText()
        cb.SetFontAndSize(fuente_cab, fontsize6)
        cb.SetTextMatrix(210, 805) '(xPos, yPos)
        cb.ShowText(titulo)
        cb.EndText()

        cb.BeginText()
        cb.SetFontAndSize(fuente_cab, fontsize6)
        cb.SetTextMatrix(300, 790) '(xPos, yPos)
        cb.ShowText(titulo2)
        cb.EndText()

        'logo
        cb.BeginText()
        cb.SetFontAndSize(fuente, fontsize3)
        image.SetAbsolutePosition(480, 775)
        image.ScalePercent(12.0F)
        cb.AddImage(image)
        '                    cb.ShowText("Imagen")
        cb.EndText()

        ' estado y numero de registro 

        cb.BeginText()
        cb.SetFontAndSize(fuente_cab, fontsize4)
        cb.SetTextMatrix(120, 765) '(xPos, yPos)
        cb.ShowText("Número: " & oot)
        cb.EndText()

        cb.BeginText()
        cb.SetFontAndSize(fuente_cab, fontsize4)
        cb.SetTextMatrix(240, 765) '(xPos, yPos)
        cb.ShowText("Estado: " & estado)
        cb.EndText()


    End Function

    Public Shared Function firma(ByVal cb As PdfContentByte, ByVal fuente As iTextSharp.text.pdf.BaseFont, ByVal fontsize3 As String, ByVal firmante As String, ByVal fecha_firma As String, ByVal ejex As String) As Boolean

        cb.BeginText()
        cb.SetFontAndSize(fuente, fontsize3)
        cb.SetTextMatrix(45, ejex) '(xPos, yPos)
        cb.ShowText(vbLf & "Concluyó: " & firmante & "                                                                                                                     Fecha:  " & fecha_firma & vbLf)
        cb.EndText()
        ejex = ejex - 10
        'linea
        cb.MoveTo(45, ejex)
        cb.LineTo(570, ejex)

        cb.Stroke()

    End Function

    Public Shared Function escribe_texto2(ByVal cb As PdfContentByte, ByVal fuente As iTextSharp.text.pdf.BaseFont, ByVal fontsize3 As String, ByVal texto As String, ByVal longitud As String, ByVal x As String, ByVal y As String) As Boolean

        cb.BeginText()
        cb.SetFontAndSize(fuente, fontsize3)
        'es menos de una linea
        If (texto.Length() <= longitud) Then
            cb.SetTextMatrix(x, y) '(xPos, yPos)
            cb.ShowText(texto)
        Else
            'es mas de una linea
            Dim aux As String = texto
            Dim nuevalong As String
            While (aux.Length() > longitud)
                cb.SetTextMatrix(x, y) '(xPos, yPos)
                cb.ShowText(aux.Substring(0, longitud))
                y = y - 10
                nuevalong = aux.Length()
                aux = aux.Substring(longitud, nuevalong - longitud)
            End While
            'el resto de la cadena
            cb.SetTextMatrix(x, y) '(xPos, yPos)
            cb.ShowText(aux)
        End If

        cb.EndText()


    End Function

    Public Shared Function escribe_texto(ByVal cb As PdfContentByte, ByVal fuente As iTextSharp.text.pdf.BaseFont, ByVal fontsize3 As String, ByVal texto As String, ByVal longitud As String, ByVal x As String, ByVal y As String) As Boolean
        Dim ct As New ColumnText(cb)
        'Dim frase As New Phrase("Texto1" & vbNewLine & "Texto2")
        Dim frase As New Phrase(texto)
        ct.SetSimpleColumn(frase, 45, 750, 580, 318, 18, Element.ALIGN_LEFT)
        ct.Go()

        cb.BeginText()
        cb.SetFontAndSize(fuente, fontsize3)
        'es menos de una linea
        If (texto.Length() <= longitud) Then
            cb.SetTextMatrix(x, y) '(xPos, yPos)
            cb.ShowText(texto)
        Else
            'es mas de una linea
            Dim aux As String = texto
            Dim nuevalong As String
            While (aux.Length() > longitud)
                cb.SetTextMatrix(x, y) '(xPos, yPos)
                cb.ShowText(aux.Substring(0, longitud))
                y = y - 10
                nuevalong = aux.Length()
                aux = aux.Substring(longitud, nuevalong - longitud)
            End While
            'el resto de la cadena
            cb.SetTextMatrix(x, y) '(xPos, yPos)
            cb.ShowText(aux)
        End If

        cb.EndText()


    End Function
    Public Shared Function escribe_texto1(ByVal cb As PdfContentByte, ByVal text As String, ByVal fontsize3 As String, ByVal x As String, ByVal y As String, ByVal longitud As String) As Boolean

        Dim ct As New ColumnText(cb)
        'Dim frase As New Phrase("Texto1" & vbNewLine & "Texto2")
        Dim frase As New Phrase()
        If (text <> Nothing) And (text <> "0") Then
            frase.Add(New Chunk(" " & text, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
        Else
            frase.Add(New Chunk("N/A", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
        End If
        ct.SetSimpleColumn(frase, x, y, longitud, 0, 10, Element.ALIGN_LEFT)
        ct.Go()

    End Function


    Public Shared Function verifica_final_pagina(ByVal ejex As String, ByVal nuevo_bloque As Integer, ByVal alto_pagina As Integer) As Boolean
        If (ejex - nuevo_bloque < alto_pagina) Then
            Return True
        Else
            Return False
        End If


    End Function

    Public Shared Function calcula_lineas(ByVal texto As String, ByVal ancho_pagina As Integer) As Integer

        Dim observaciones As String
        Dim canti As String
        Dim xancho_pagina As String
        Dim lineas As Integer
        'codigo para rellenar con espacios el campo de observaciones en funcion del contenido y que no se vayan los margenes
        observaciones = Trim(texto)
        'longitud de la cadena incluido espacios
        canti = Countcaracteres(observaciones, "107")
        Dim palabra As String
        Dim peso_item As String 'cantidad de caracteres que ocupa en el renglon
        Dim caracteres_impresos As String = "0"
        Dim caracteres_mostrados As String = ""
        Dim caracteresPermitidos As String = "1234567890.,-+/*()°|" & Convert.ToChar(8)
        Dim borrar As String
        Dim borrar2 As String
        Dim segui As Decimal
        Dim band1 As String
        Dim band2 As String
        xancho_pagina = ancho_pagina
        lineas = "0"

        For Each item As Char In observaciones

            'voy almacenando la palabra  q se va formando para luego cargarla en caso que haya un final de renglon
            If (item = " ") Then
                'si viene un espacio la palabra termino, blanqueo
                palabra = ""
            Else
                palabra = palabra & item
            End If
            'para control despues borrar

            borrar2 = lineas
            borrar = canti

            If (item = UCase(item) And item <> " " And Not (caracteresPermitidos.Contains(item))) Then
                ' mayus
                peso_item = 1.25

            ElseIf (item = " ") Then
                peso_item = 0.5
            ElseIf (item = "-") Then
                peso_item = 0.5
            Else
                peso_item = 1
            End If




            If (item = "b") Or (item = "c") Or (item = "d") Or (item = "e") Then
                borrar = ""
            End If
            'fin control
          
            band1 = False
            band2 = False
            segui = xancho_pagina - peso_item
            If (segui >= "0") Then
                band2 = True
            End If

            If (item <> vbCr) Then
                band1 = True
            End If
         
            If (band1 = True) And (band2 = True) Then
                borrar = UCase(item)
                If (item = UCase(item) And item <> " " And Not (caracteresPermitidos.Contains(item))) Then
                    'mayuscula es 1 cada 3
                    xancho_pagina = xancho_pagina - 1.25
                    caracteres_impresos = caracteres_impresos + 1.25

                ElseIf (item = " ") Then
                    'espacio es 1 cada 2
                    xancho_pagina = xancho_pagina - 0.5
                    caracteres_impresos = caracteres_impresos + 0.5
                ElseIf (item = "-") Then
                    'espacio es 1 cada 2
                    xancho_pagina = xancho_pagina - 0.5
                    caracteres_impresos = caracteres_impresos + 0.5
                Else
                    xancho_pagina = xancho_pagina - 1
                    caracteres_impresos = caracteres_impresos + 1
                End If
                caracteres_mostrados = caracteres_mostrados & item
                Continue For
            Else
                xancho_pagina = ancho_pagina
                lineas = lineas + 1
                caracteres_impresos = "0"
                caracteres_mostrados = ""
                borrar = UCase(item)
                'si la cadena "palabra" es mas larga de un caracter cargo todo (ya que se produjo un salto de pagina por que la palabra entera no entraba en el renglon anterior)
                If (Len(palabra) > "1") Then
                    'al producirse el salto de linea por final de renglon debo sumar los caracteres que llegaba leido con la palabra
                    For Each xitem As Char In palabra
                        'debo omitir la ultima letra si no se agrega dos veces
                        If (item = UCase(item)) Then
                            caracteres_impresos = caracteres_impresos + 1.25
                            xancho_pagina = xancho_pagina - 1.25
                        ElseIf (item = "-") Then
                            'espacio es 1 cada 2
                            xancho_pagina = xancho_pagina - 0.5
                            caracteres_impresos = caracteres_impresos + 0.5
                            caracteres_mostrados = caracteres_mostrados & item
                        Else
                            caracteres_impresos = caracteres_impresos + 1
                            xancho_pagina = xancho_pagina - 1
                        End If
                    Next
                Else


                    If (item = UCase(item) And item <> " " And Not (caracteresPermitidos.Contains(item))) Then
                        'mayuscula es 1 cada 3
                        xancho_pagina = xancho_pagina - 1.25
                        caracteres_impresos = caracteres_impresos + 1.25
                        caracteres_mostrados = caracteres_mostrados & item
                    ElseIf (item = " ") Then
                        'espacio es 1 cada 2
                        ' en un salto de linea no imprime el espacio al comienzo y tampoco lo agrego en la cadenta de seguimiento caracteres impresos
                        'xancho_pagina = xancho_pagina - 0.5
                        'caracteres_impresos = caracteres_impresos + 0.5
                    ElseIf (item = "-") Then
                        'espacio es 1 cada 2
                        xancho_pagina = xancho_pagina - 0.5
                        caracteres_impresos = caracteres_impresos + 0.5
                        caracteres_mostrados = caracteres_mostrados & item
                    Else
                        xancho_pagina = xancho_pagina - 1
                        caracteres_impresos = caracteres_impresos + 1
                        caracteres_mostrados = caracteres_mostrados & item
                    End If


                End If
            End If


        Next
        'control
        'borrar2 = lineas
        'borrar = canti
        Return lineas * 10
    End Function

    'imprime un parrafo sin cortar palabras y devuelve actualizado el eje y 
    Public Shared Function escribe_parrafo(ByVal cb As PdfContentByte, ByVal fuente As iTextSharp.text.pdf.BaseFont, ByVal fontsize3 As String, ByVal texto As String, ByVal ancho_pagina As String, ByVal x As String, ByVal y As String) As Integer

        Dim observaciones As String
        Dim canti As String
        Dim xancho_pagina As String
        Dim lineas As Integer
        'codigo para rellenar con espacios el campo de observaciones en funcion del contenido y que no se vayan los margenes
        observaciones = Trim(texto)
        'longitud de la cadena incluido espacios
        canti = Countcaracteres(observaciones, "107")
        Dim palabra As String
        Dim peso_item As String 'cantidad de caracteres que ocupa en el renglon
        Dim caracteres_impresos As String = "0"
        Dim caracteres_mostrados As String = ""
        Dim caracteresPermitidos As String = "1234567890.,-+/*()°|" & Convert.ToChar(8)
        Dim linea_imprimir As String ' es la linea que muestro
        Dim borrar As String
        Dim borrar2 As String
        xancho_pagina = ancho_pagina
        lineas = "0"

        For Each item As Char In observaciones

            'voy almacenando la palabra  q se va formando para luego cargarla en caso que haya un final de renglon
            If (item = " ") Then
                'si viene un espacio la palabra termino, blanqueo
                palabra = ""
            Else
                palabra = palabra & item
            End If
            'para control despues borrar

            borrar2 = lineas
            borrar = canti

            If (item = UCase(item) And item <> " " And Not (caracteresPermitidos.Contains(item))) Then
                ' mayus
                peso_item = 1.25

            ElseIf (item = " ") Then
                peso_item = 0.5
            ElseIf (item = "-") Then
                peso_item = 0.5
            Else
                peso_item = 1
            End If




            If (item = "b") Or (item = "c") Or (item = "d") Or (item = "e") Then
                borrar = ""
            End If
            'fin control
            If (item <> vbCr) And (xancho_pagina - peso_item >= "0") Then
                borrar = UCase(item)
                If (item = UCase(item) And item <> " " And Not (caracteresPermitidos.Contains(item))) Then
                    'mayuscula es 1 cada 3
                    xancho_pagina = xancho_pagina - 1.25
                    caracteres_impresos = caracteres_impresos + 1.25

                ElseIf (item = " ") Then
                    'espacio es 1 cada 2
                    xancho_pagina = xancho_pagina - 0.5
                    caracteres_impresos = caracteres_impresos + 0.5
                ElseIf (item = "-") Then
                    'espacio es 1 cada 2
                    xancho_pagina = xancho_pagina - 0.5
                    caracteres_impresos = caracteres_impresos + 0.5
                Else
                    xancho_pagina = xancho_pagina - 1
                    caracteres_impresos = caracteres_impresos + 1
                End If
                caracteres_mostrados = caracteres_mostrados & item
                Continue For
            Else
                xancho_pagina = ancho_pagina
                lineas = lineas + 1
                linea_imprimir = caracteres_mostrados
                caracteres_impresos = "0"
                caracteres_mostrados = ""
                borrar = UCase(item)
                'si la cadena "palabra" es mas larga de un caracter cargo todo (ya que se produjo un salto de pagina por que la palabra entera no entraba en el renglon anterior)
                If (Len(palabra) > "1") Then
                    'imprimo la linea
                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(x, y) '(xPos, yPos)
                    cb.ShowText(linea_imprimir.Substring(0, ancho_pagina))
                    cb.EndText()
                    y = y + 10
                    'fin impresion

                    'al producirse el salto de linea por final de renglon debo sumar los caracteres que llegaba leido con la palabra
                    For Each xitem As Char In palabra
                        'debo omitir la ultima letra si no se agrega dos veces
                        If (item = UCase(item)) Then
                            caracteres_impresos = caracteres_impresos + 1.25
                            xancho_pagina = xancho_pagina - 1.25
                        ElseIf (item = "-") Then
                            'espacio es 1 cada 2
                            xancho_pagina = xancho_pagina - 0.5
                            caracteres_impresos = caracteres_impresos + 0.5
                            caracteres_mostrados = caracteres_mostrados & item
                        Else
                            caracteres_impresos = caracteres_impresos + 1
                            xancho_pagina = xancho_pagina - 1
                        End If
                    Next
                Else

                    'imprimo la linea
                    cb.BeginText()
                    cb.SetFontAndSize(fuente, fontsize3)
                    cb.SetTextMatrix(x, y) '(xPos, yPos)
                    cb.ShowText(linea_imprimir.Substring(0, ancho_pagina))
                    cb.EndText()
                    y = y + 10
                    'fin impresion

                    If (item = UCase(item) And item <> " " And Not (caracteresPermitidos.Contains(item))) Then
                        'mayuscula es 1 cada 3
                        xancho_pagina = xancho_pagina - 1.25
                        caracteres_impresos = caracteres_impresos + 1.25
                        caracteres_mostrados = caracteres_mostrados & item
                    ElseIf (item = " ") Then
                        'espacio es 1 cada 2
                        ' en un salto de linea no imprime el espacio al comienzo y tampoco lo agrego en la cadenta de seguimiento caracteres impresos
                        'xancho_pagina = xancho_pagina - 0.5
                        'caracteres_impresos = caracteres_impresos + 0.5
                    ElseIf (item = "-") Then
                        'espacio es 1 cada 2
                        xancho_pagina = xancho_pagina - 0.5
                        caracteres_impresos = caracteres_impresos + 0.5
                        caracteres_mostrados = caracteres_mostrados & item
                    Else
                        xancho_pagina = xancho_pagina - 1
                        caracteres_impresos = caracteres_impresos + 1
                        caracteres_mostrados = caracteres_mostrados & item
                    End If


                End If
            End If


        Next
        'control
        'borrar2 = lineas
        'borrar = canti
        Return y
    End Function


    Protected Sub Button12_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button12.Click
        estado.Value = "Cerrado"
        Label4.Text = "Debe confirmar los cambios realizados presionando el botón Grabar"
    End Sub

    Protected Sub Button13_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button13.Click
        If (lote_i.Value <> "") Then
            lotes_inv.Items.Add(lote_i.Value)
            lote_i.Value = ""
        End If
    End Sub

    Protected Sub Button14_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button14.Click
        If (lotes_inv.Text <> "") Then
            lotes_inv.Items.Remove(lotes_inv.SelectedItem)

        End If
    End Sub
End Class
