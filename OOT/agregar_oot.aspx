﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="agregar_oot.aspx.vb" Inherits="EO_Default" title="Sistema Sinac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
   
        <!--  validaciones  --> 
  <style type="text/css">
        body {
            
            font-family: Arial;
            font-size: 10pt;
        }
        input, select
        {
            width: 250px;
            
        }
        #ctl00_ContentPlaceHolder1_analista
        {
        	width: 250px;
        	height: 21px;
        }
        #ctl00_ContentPlaceHolder1_producto, #ctl00_ContentPlaceHolder1_activo, #ctl00_ContentPlaceHolder1_ensayo, #ctl00_ContentPlaceHolder1_elaborador
        {
        	width: 250px;
        	height:19px;
        }
        #ctl00_ContentPlaceHolder1_nro_lote
        {width: 250px;
        	
        }
     #ctl00_ContentPlaceHolder1_fecha_carga
      {width: 250px;
        	
        }
        #ctl00_ContentPlaceHolder1_lotes_inv
        {
        width: 250px;
        height:72px;
        }
    </style>
   
     <link href="../css/ValidationEngine.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="../js/jquery/languages/jquery.validationEngine-es.js"
        charset="utf-8"></script>
    <script type="text/javascript" src="../js/jquery/jquery.validationEngine.js"
        charset="utf-8"></script>
    <script type="text/javascript">
        $(function () {
            $("#aspnetForm").validationEngine('attach', { promptPosition: "topRight" });
        });
    </script>
    <script type="text/javascript">
        function DateFormat(field, rules, i, options) {
            var regex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
            if (!regex.test(field.val())) {
                return "Please enter date in dd/MM/yyyy format."
            }
        }
    </script>
  <!-- fin validaciones  --> 
  
   
    <form id="form2" runat="server">
    
<div id="contenedor1">
<div id="contenedor2">
     <div id="titulo_seccion">
         Ingrese los datos del OOT.
 </div>
    <ul>
    <li class="paneles">
    <div>
    <span class="completo">
     <label for="revision">Tendencia (*)</label>
              
               
            
      <asp:ListBox runat="server" ID="tendencia" SelectionMode="Multiple" CssClass="validate[required]" DataSourceID="SqlDataSource6" 
            DataTextField="tendencia" DataValueField="id">
            </asp:ListBox>
    </span>
 
    <span class="tercio">
    <label for="analista">Analista(*)</label>
            <asp:DropDownList ID="analista" runat="server" DataSourceID="SqlDataSource2" 
            DataTextField="Denominacion" DataValueField="Id" CssClass="validate[required]>
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
      
    </span>
 

   <span class="dostercios">
      <label for="tipo_error">Número de Lote(*)</label>
       <asp:TextBox ID="nro_lote" runat="server" CssClass="validate[required]" 
            MaxLength="15"></asp:TextBox>
    
    
    </span>
    
     <span class="tercio"><label for="fecha_carga">Fecha de Carga(*)</label>
      <input id="fecha_carga" name="fecha_carga" value="" runat="server" 
            readonly="readonly"/>
     
    </span>     
 
 
   
    
   
    
    
  </div>
  </li>
   <li class="paneles">
    <div>
    
    <span class="tercio">
      <label for="producto">Codigo Producto(*)</label>
      <asp:DropDownList ID="producto" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="NomProd" DataValueField="IDPdto" CssClass="validate[required]">
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
   
    </span>
        <span class="tercio">
      <label for="producto">Activo(*)</label>
      <asp:DropDownList ID="activo" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="NomProd" DataValueField="IDPdto" CssClass="validate[required]">
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
   
    </span>

 <span class="tercio">
     <label for="elaborador">Elaborador(*)</label>
          <asp:DropDownList ID="elaborador" runat="server" DataSourceID="SqlDataSource5" 
            DataTextField="NomElab" DataValueField="IDElab" CssClass="validate[required]">
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
      
     
    </span>
   <span class="tercio">
     <label for="elaborador">Ensayo(*)</label>
          <asp:DropDownList ID="ensayo" runat="server" DataSourceID="SqlDataSource7" 
            DataTextField="nombre_ensayo" DataValueField="id" CssClass="validate[required]">
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
      
     
    </span>
    
    
    <span class="dostercios1">
      <label for="adecuacion">Lotes Involucrados</label>
    
    <asp:ListBox runat="server" ID="lotes_inv" SelectionMode="Multiple" DataTextField="nombre" DataValueField="Id">
            

</asp:ListBox> 

    
        
    
    
    </span> 
  <span class="dostercios88">
      <label for="ensayo">Lotes Involucrados</label>
      <input id="lote_i" name="ensayo" value="" runat="server" maxlength="15" placeholder="Escribir Aquí el lote a Agregar"/> 
         <asp:Button ID="Button3" runat="server" Text=" <---   Agregar Lote" />
         <asp:Button ID="Button2" runat="server" Text=" --->   Quitar Lote" />
      </span>
    
 
  
  
 

   
      

 
     
   
   
      
        
           
    
 
   
  
  </div>
  </li>
     
  </ul>
    <ul>
        
    <li class="panel_boton">
     <div>
     
    
   
      <span class="boton">
       
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" 
             style="height: 26px" />
   
        
   <input type="button" value="Volver" id="volver" onclick="location.href='consulta_oot.aspx'"></span>
   
    
   </div>

    </li>
                 
     </ul>
         
  </div>
  </div>
  <br>
  <div id="confirmacion">
  <asp:Label ID="Label1" runat="server"></asp:Label>
  </div>
   <div id="notice">
  <asp:Label ID="Label3" runat="server"></asp:Label>
  </div>
    <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
   
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IDPdto] , [CodPdto] + ' ---- ' + [DesPdto] as NomProd FROM [Productos] where CodPdto <> '' and Activado = '1' order by CodPdto">
            
        </asp:SqlDataSource>
        
         <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [Id], [Denominacion] FROM [User] order by [Denominacion] asc">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idEtapa], [nombre_etapa] FROM Etapas">
        </asp:SqlDataSource>
        
    <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IDPdto], [DesPdto] FROM [Productos]">
        </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IDElab], [NomElab] FROM [Elaboradores]">
        </asp:SqlDataSource>
        
          <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [id], [tendencia] FROM Tendencias">
        </asp:SqlDataSource>
        
          <asp:SqlDataSource ID="SqlDataSource7" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [id], [nombre_ensayo] FROM OOT_ensayos">
        </asp:SqlDataSource>
    </form>
</asp:Content>

