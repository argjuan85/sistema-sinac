﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="consulta_oot.aspx.vb" Inherits="Logs_consulta_logs" title="Sistema Sinac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:Label ID="Label1" runat="server"></asp:Label>
    
     <form id="form2" runat="server">
    
 <!-- librerias calendario y css -->
       <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.8.20.custom.css" type="text/css" media="screen" charset="utf-8"/>
       <script src="js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>        
       <script src="js/jquery-ui-1.8.20.custom.min.js" type="text/javascript" charset="utf-8"></script> 
       
       <link rel="stylesheet" href="../js/datatable/media/css/demo_table.css"type="text/css" />
<link rel="stylesheet" href="../css/informe.css"type="text/css" />
	<script type="text/javascript" language="javascript" src="../js/DataTables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="../js/DataTables/extensions/TableTools/js/dataTables.tableTools.js"></script>
	<script type="text/javascript" language="javascript" src="../../../examples/resources/syntax/shCore.js"></script>
	<script type="text/javascript" language="javascript" src="../../../examples/resources/demo.js"></script>
	
	        <script type="text/javascript" charset="utf-8" src="../js/datatable/ColVis/media/js/ColVis.js"></script>
    <script type="text/javascript" charset="utf-8"></script>
   
<script type="text/javascript" src="js/jquery.numeric.js"></script>



	<style type="text/css" class="init">
	    input {	height: 15px;
}

	</style>
 
   	<style type="text/css" class="init">
	      	#ctl00_ContentPlaceHolder1_producto    
	      	{
	      		width: 120px;
	      		height: 20px;
	      		}

</style>
 
    <script type="text/javascript" charset="utf-8"></script>
<script language="javascript" type="text/javascript">

var asInitVals = new Array();
     $(document).ready(function () {
     
     
     	       $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

	    
		$("#<%= fecha_carga.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true,changeYear: true ,yearRange: '-10:+10', maxDate: 'today', stepMonths: 0}).val();
		//Con este codigo indico que la fecha de ingreso no puede ser menor a la pedido
	

		
	 $('#ctl00_ContentPlaceHolder1_Button1').click(function(){
 
   });

     
     
     var  oTable = $('#tbl').dataTable( {
    
    
    "dom": 'T<"clear">lfrtip',
      
           "oTableTools": {
                                                                "aButtons": [
                                                                                "xls",
                                                                                
                                                                            ]
                                                                },
    
            "sScrollY": "100%",
            //'sPaginationType': 'full_numbers',
            //'iDisplayLength': 5,
            	"oColVis": {
		    		
					"activate": "mouseover",
						
						"aiExclude": [ 5 ]
						
				},
				"oLanguage": {
					"sProcessing":     "Procesando...",
				    "sLengthMenu":     "Mostrar _MENU_ registros",
				    "sZeroRecords":    "No se encontraron resultados",
				    "sEmptyTable":     "Ningún dato disponible en esta tabla",
				    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				    "sInfoPostFix":    "",
				    "sSearch":         "Buscar:",
				    "sUrl":            "",
				    "sInfoThousands":  ",",
				    
				    "oPaginate": {
				        "sFirst":    "Primero",
				        "sLast":     "Último",
				        "sNext":     "Siguiente",
				        "sPrevious": "Anterior"
				    },
				    "sLoadingRecords": "Cargando...",
				    "fnInfoCallback": null,
				    "oAria": {
				        "sSortAscending":  ": Activar para ordernar la columna de manera ascendente",
				        "sSortDescending": ": Activar para ordernar la columna de manera descendente"
				    }
				    },
            "bPaginate": true,
            "bProcessing": true,
            "bServerSide": false,
            "bSortCellsTop": true
        });
        
        
        /* Add the events etc before DataTables hides a column */
			$("thead input").keyup( function () {
				/* Filter on the column (the index) of this element */
				oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex( 
					oTable.fnSettings(), $("thead input").index(this) ) );
			} );
			
			/*
			 * Support functions to provide a little bit of 'user friendlyness' to the textboxes
			 */
			$("thead input").each( function (i) {
				this.initVal = this.value;
			} );
			
			$("thead input").focus( function () {
				if ( this.className == "search_init" )
				{
					this.className = "";
					this.value = "";
				}
			} );
			
			$("thead input").blur( function (i) {
				if ( this.value == "" )
				{
					this.className = "search_init";
					this.value = this.initVal;
				}
			} );
        
    });
</script>

           
    <div id="titulo_seccion">
     Administración de OOT 
    
 </div>
         <div id="titulo_seccion">
     Filtros de Búsqueda   
    
 </div>
             
               <div align= "center" >
          <span >
        <label runat="server" id="label_IDOOT" for="cantidad_pedida"  class=":required :integer" >NRO OOT</label>
          
    <asp:TextBox ID="IDOOT" runat="server" Width="53px"></asp:TextBox>
    
    </span>
      <span >
      
      <label for="analista">Analista</label>
             <asp:DropDownList ID="analista" runat="server" DataSourceID="SqlDataSource2" 
            DataTextField="Denominacion" DataValueField="Id" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
    </span>
        <span >
      
      <label for="producto">Producto</label>
    <asp:DropDownList ID="producto" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="NomProd" DataValueField="IDPdto" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
    </span>
        <span >
      
      <label for="fecha_carga">Fecha de Carga</label>
        <input id="fecha_carga" name="fecha_carga" value="" runat="server" 
             readonly="readonly" />
    </span>
    <span >
      
      <label for="lote">Lote</label>
    <asp:TextBox ID="lote" runat="server" Width="53px"></asp:TextBox>
    </span>
    <span >
      
      <label for="lotei">Lotes Inv</label>
         <asp:TextBox ID="lotei" runat="server" Width="53px"></asp:TextBox>
  
    </span>
       <span >
      
      <label for="lotei">CA/PA</label>
             <asp:DropDownList ID="capa" runat="server" DataSourceID="SqlDataSource4" 
            DataTextField="indentCapa" DataValueField="idCapa">
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
  
    </span>
    <span >
      
      
                   
    
    
    </div>
   <br>
    <div id="Div1" style="width:100%" runat="server">
        
        
        
        <div align= "center" >
        <asp:Button ID="Button1" runat="server" Text=" Buscar "  />
          <asp:Button ID="Button2" 
            runat="server" Text=" + Agregar OOT " />
            
         
              <asp:Label ID="label_stock" runat="server"></asp:Label>
                  &nbsp;
&nbsp;
</div>
  

<asp:Repeater ID="repeater" runat="server" OnItemCommand ="RepeaterDeleteitemcommand">
            <HeaderTemplate>
                <table id="tbl" cellpadding="1" cellspacing="0" 
                    border="0" class="display" >
                  <thead id="aaa">
                    <tr>
                        <th></th>
                    
                        <th>NroOOT</th>
                        <th>Codigo</th>
                        <th>Lote</th>
                        <th>Elaborador</th>
                        <th>Fecha Carga</th>
                        <th>Estado</th>
                         </tr>
             <tr>
			
			<td align="center"></td>
			<td align="center"><input type="text" size="7" name="search_platform" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="5" name="search_version" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_grade" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_grade" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_version1" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_browser1" value="" class="search_init" /></td>
			
			</tr>
                  </thead>
                <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                  <td align="center"><a><asp:Button ID="Button1" CommandName="Click" Text="Modificar" runat="server" CommandArgument='<%# Eval("idOOT") %>' /></a>
                  </td>
      <td align="center"><%#rellena(Eval("NroOOT"))%></td>
      <td align="center"><%#Eval("CodPdto")%></td>
      <td align="center"><%#Eval("Lote")%></td>
      <td align="center"><%#elaborador(Eval("Elaborador"))%></td>
      <td align="center"><%#fecha(Eval("FeCarga"))%></td>                     
      <td align="center"><%#Eval("Estado")%></td>  
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
 
</body>
      <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
     <div id="confirmacion">
  <asp:Label ID="Label3" runat="server"></asp:Label>
  </div>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IDPdto] , [CodPdto] + ' ---- ' + [DesPdto] as NomProd FROM [Productos] where CodPdto <> '' and Activado = '1' order by CodPdto">
                 
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [Id], [Denominacion] FROM [User] order by [Denominacion] asc">
        </asp:SqlDataSource>
          <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idEtapa], [nombre_etapa] FROM Etapas">
        </asp:SqlDataSource>
          <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idCapa], [indentCapa] FROM Capa">
        </asp:SqlDataSource>
    </form>
      </asp:Content>
