﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration
Imports System.Net.Mail

Partial Class EO_default
    Inherits System.Web.UI.Page
    Dim codigo As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(128, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            capa_asoc.AppendDataBoundItems = True

            codigo = DecryptText(Request.QueryString("ID"))

            Label1.Text = ""
            Label2.Text = ""
            If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                buscarinvcc(codigo) 'carga la interfaz

            End If

        End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
      
            actualizarinvrap("0")
   
        Catch ex As Exception

        End Try
    End Sub


    Sub buscarinvcc(ByVal codigo As String)
        Dim u As OOT

        Try
            u = New OOT()
            u.OOT(conexion)
            Dim estado As String
            estado = u.obtenerestado(codigo)
            observaciones.Value = u.consultar_obs_invrap(codigo)
            capa_asoc.Text = u.consultar_capa_invrap(codigo)
            If ((estado <> "PendienteCC") And (estado <> "ConcluidoCC")) Then
                observaciones.Disabled = True
                capa_asoc.Enabled = False
                Button1.Visible = False
                Button2.Visible = False
            End If


            ' descomentar en productivo
            '          If (Session("departamento") <> "RAP") Then

            '               capa_asoc.Enabled = False
            '               observaciones.Disabled = True
            '               Button1.Enabled = False
            '               Button2.Enabled = False
            ''
            '      Label2.Text = "Su departamento no tiene permisos para modificar esta investigacion"
            '  End If

        Catch ex As Exception

        End Try

    End Sub


    Sub actualizarinvrap(ByVal llave As String)

        Dim u As OOT

        Dim r As String

        Dim l As logs
        Dim max_caracteres As String
        Dim ancho_pagina As String


        Dim p As Parametros
        p = New Parametros()
        p.Parametros(conexion)
        max_caracteres = p.consulta_valor_nombre("max_caracteres")
        ancho_pagina = p.consulta_valor_nombre("ancho_pagina")

        Try
            ' 'log
            l = New logs()
            l.logs(conexion)
            u = New OOT()
            u.OOT(conexion)

            ' valido que las cadenas de observaciones no excedan los 2000 caracteres
            Dim bandera As String = "1"
            Dim campo As String

            If (Countcaracteres(observaciones.Value, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones"
            End If
            If (bandera) Then
                r = u.Actualizarinvrap(codigo, observaciones.Value, capa_asoc.Text)
            Else
                Label2.Text = "El campo: " + campo + " excede los " + max_caracteres + " caracteres. Verifique."

            End If

            Dim xfecha As String
            xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog3(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", xfecha, u.obtenerNroOOT(codigo), u.obtenerproducto(codigo), u.obtenerlote(codigo), u.obtenerfechacarga(codigo), u.obtenerestado(codigo), "Modifica Investigacion RAP")
            If (llave <> "1") Then
                Response.Redirect("../modifica_OOT.aspx?ID=" + EncryptText(codigo & "//2"))
                Label1.Text = "Se modificaron correctamente los datos de la investigación"
            End If



        Catch ex As Exception
            Label2.Text = "Hubo un error al actualizar la investigación"
        End Try
    End Sub

    

  
 
  
 
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        actualizarinvrap("1")
        Dim us As User
        us = New User()
        us.User(conexion)
        Dim word As String
        Dim u As OOT

        u = New OOT()
        u.OOT(conexion)
        u.ActualizarestadoOOT(codigo, "PendienteQA")
        u.cargar_firma(Session("usuario"), DateTime.Now.ToString("yyyy-MM-dd"), codigo, "PendienteQA")
        Dim l As logs
        l = New logs()
        l.logs(conexion)
        Dim xfecha As String

        'log cambio de estado
        xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
        l.Agregarlog3(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", xfecha, u.obtenerNroOOT(codigo), u.obtenerproducto(codigo), u.obtenerlote(codigo), u.obtenerfechacarga(codigo), "PendienteQA", "Cambio de Estado")


        'descomentar a la hora de poner en productivo
        'Dim s As String = us.Consultarmails_Sector("QA - Documentacion", "1")
        'Dim words As String() = s.Split(New Char() {"/"c})
        'For Each word In words
        'intercepto aca para que no envie mails reales
        '   word = "jarganaraz@raffo.com.ar"
        '   EnvioMail(word, u.obtenerNroOOT(codigo).ToString("d4"))
        'Next
        'aca debo recorrer los user a notificar
        word = "jarganaraz@raffo.com.ar"
        EnvioMail(word, u.obtenerNroOOT(codigo).ToString("d4"))
        Response.Redirect("../modifica_OOT.aspx?ID=" + EncryptText(codigo & "//2"))

    End Sub

    Public Sub EnvioMail(ByVal mail As String, ByVal registro As String)
        Try
            Dim p As Parametros
            p = New Parametros()
            p.Parametros(conexion)

            'cargo parametros

            sops = p.consulta_valor_nombre("sops_oot")
            fogl = p.consulta_valor_nombre("fogl_oot")
            titulo = p.consulta_valor_nombre("titulo_fogl_oot")
            vigencia = p.consulta_valor_nombre("vigencia_fogl_oot")

            Dim message As New MailMessage

            Dim smtp As New SmtpClient

            message.From = New MailAddress(p.consulta_valor_nombre("Mail"))

            message.To.Add(mail)

            message.Body = "Ya se encuentra disponible para concluir el OOT de referencia." & vbLf & vbLf

            message.Body = message.Body & "AVISO: Este es un mail automatico generado por el Sistema Sinac. No responder sobre este mail. Cualquier duda comunicarse con QA"

            message.Subject = "OOT: " & registro

            message.Priority = MailPriority.Normal

            smtp.EnableSsl = False

            smtp.Port = p.consulta_valor_nombre("puerto")

            smtp.Host = p.consulta_valor_nombre("Servidor")

            smtp.Credentials = New Net.NetworkCredential(p.consulta_valor_nombre("Alias"), p.consulta_valor_nombre("Pass"))

            smtp.Send(message)
        Catch ex As Exception

        End Try


    End Sub
    Public Shared Function Countcaracteres(ByVal tcString As String, ByVal ancho As Integer) As Integer
        Dim conteo As String = "0"
        For Each item As Char In tcString
            If (item <> vbCr) Then
                conteo = conteo + 1
                Continue For
            ElseIf (item = vbCr) Then
                conteo = conteo + ancho
            End If


        Next
        Return conteo
    End Function
End Class
