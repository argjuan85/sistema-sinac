﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports System.Net.Mail

Partial Class Logs_consulta_logs
    Inherits System.Web.UI.Page
    Public opcion As String
    Public sql As String



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(2, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            'esto es para que tome el item "seleccione..." en el dropdown
            analista.AppendDataBoundItems = True
            producto.AppendDataBoundItems = True
            capa.AppendDataBoundItems = True
        End If

        'solo administradores podran cargar la cabecera (ver que nivel va sser en productivo
        Dim codigo As String
        codigo = DecryptText(Request.QueryString("ID")) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos

        If (codigo <> "") Then

            codigo = Left(DecryptText(Request.QueryString("ID")), 4)

            Label3.Text = "Se cargó el OOT (Número: " & codigo & ") con éxito"


        End If
        If (Session("nivel") <> "0") Then
            Button2.Enabled = "False"
        End If
    End Sub

    Protected Function invertirfecha(ByVal fecha As Object) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 6, 2)
            bDia = Right(Fechatest, 2)
            bAño = Left(Fechatest, 4)
            Fechanueva = bDia & "/" & bMes & "/" & bAño
            Return Fechanueva
        End If


    End Function





    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand

        Dim strURL As String
        Dim Codigox As String
        If (e.CommandName = "Click") Then
            'get the id of the clicked row
            Codigox = Convert.ToString(e.CommandArgument)

            strURL = "modifica_oot.aspx?ID=" + EncryptText(Codigox)
            Response.Redirect(strURL)
        End If

    End Sub
    Protected Function Vacio(ByVal nombre As Object) As Object
        If (Not nombre.Equals(DBNull.Value)) Then
            Return nombre
        Else
            Return "N/A"

        End If



    End Function
    Protected Function rellena(ByVal nombre As Integer) As String


        Return nombre.ToString("d4")


    End Function
    Protected Function elaborador(ByVal nombre As Integer) As String
        Dim e As OOT
        e = New OOT() ' nuevo objeto instancia
        e.OOT(conexion) 'invoco el constructor y paso parametros de conexion


        Return e.obtener_elaborador(nombre)


    End Function

    Protected Function fecha(ByVal fecha_1 As Date) As String
        Dim fechax As Date
        fechax = fecha_1
        Return fechax


    End Function
    Protected Function usuario(ByVal nombre As String) As String
        Dim l As User
        l = New User()
        l.User(conexion)

        Return l.Consultarusuario(nombre)

    End Function

    Protected Function etapa_nom(ByVal nombre As String) As String
        Dim e As Etapa
        e = New Etapa()
        e.Etapa(conexion)
        Return e.consulta_nombre_etapa(nombre)


    End Function

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("agregar_oot.aspx")
    End Sub






    Public Sub EnvioMail()

        Dim message As New MailMessage

        Dim smtp As New SmtpClient

        message.From = New MailAddress("mtvsj@raffo.com.ar")

        message.To.Add("jarganaraz@raffo.com.ar")

        message.Body = "EL CUERPO O EL CONTENIDO DEL MENSAJE"

        message.Subject = "Estandares Proximos a Vencer"

        message.Priority = MailPriority.Normal

        smtp.EnableSsl = False

        smtp.Port = "587"

        smtp.Host = "192.168.40.33"

        smtp.Credentials = New Net.NetworkCredential("mtvsj", "mtvsj1234")

        smtp.Send(message)

    End Sub



    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Label3.Text = ""
        buscaroot(sql)


    End Sub

    Sub buscaroot(ByVal sql As String)
        Dim u As OOT
        Dim indice As Integer = 0
        Dim checks As Boolean = False
        Dim xfechac As String
      
        Try
            u = New OOT() ' nuevo objeto instancia
            u.OOT(conexion) 'invoco el constructor y paso parametros de conexion
            'limpiar posibles mensajes de error
            Label2.Text = ""
            If (lotei.Text <> "") Then
                sql = "Select distinct OOT.idOOT, OOT.NroOOT, OOT.Lote, Productos.CodPdto, OOT.Elaborador, OOT.FeCarga, OOT.Estado from OOT  inner join Productos on Productos.IDPdto=OOT.idPdto inner join [User] on [User].Id=OOT.Analista inner join OOT_LoteInv on OOT_LoteInv.idOOT=OOT.idOOT"
                sql = sql & " and OOT_LoteInv.Lote like '%" & lotei.Text & "%' "
                If (capa.Text <> "") Then
                    sql = "Select distinct OOT.idOOT, OOT.NroOOT, OOT.Lote, Productos.CodPdto, OOT.Elaborador, OOT.FeCarga, OOT.Estado from OOT  inner join Productos on Productos.IDPdto=OOT.idPdto inner join OOT_InvElab on OOT_InvElab.idOOT=OOT.idOOT inner join [User] on [User].Id=OOT.Analista inner join OOT_LoteInv on OOT_LoteInv.idOOT=OOT.idOOT"
                    sql = sql & " and OOT_LoteInv.Lote like '%" & lotei.Text & "%' "
                End If
            Else
                sql = "Select * from OOT  inner join Productos on Productos.IDPdto=OOT.idPdto inner join [User] on [User].Id=OOT.Analista"
                If (capa.Text <> "") Then
                    sql = "Select * from OOT  inner join Productos on Productos.IDPdto=OOT.idPdto inner join OOT_InvElab on OOT_InvElab.idOOT=OOT.idOOT inner join [User] on [User].Id=OOT.Analista"
                End If
            End If



            If IDOOT.Text <> "" Then
                Dim nueva As String
                Dim ceros As String = "0"
                'debo quitar los ceros ya que en la base se guardan sin ellos
                If IDOOT.Text.Length > 1 Then
                    For indice = 0 To IDOOT.Text.Length - 1
                        If (IDOOT.Text.Substring(indice, 1) = 0) Then
                            nueva = IDOOT.Text.Substring(indice + 1, IDOOT.Text.Length - (1 + indice))
                            ceros = "1" 'encontre al menos un cero
                        End If
                    Next
                Else
                    'si cargo un numero de 1 digito asigno directo ya que no habrian ceros
                    nueva = IDOOT.Text

                End If
                If (ceros = "0") Then
                    'quiere decir que no habia ningun cero en el filtro por ende asigno directo
                    nueva = IDOOT.Text
                End If

                sql = sql & " and OOT.NroOOT  like '%" & nueva & "%' "
            End If

            If analista.Text <> "0" Then
                sql = sql & " and OOT.Analista = '" & analista.Text & "' "
            End If

            If producto.Text <> "0" Then
                sql = sql & " and OOT.idPdto = '" & producto.Text & "' "
            End If


            If lote.Text <> "" Then
                sql = sql & " and OOT.lote like '%" & lote.Text & "%' "
            End If


            If fecha_carga.Value <> "" Then
                xfechac = cambiaformatofecha3(fecha_carga.Value)
                sql = sql & " and OOT.FeCarga = '" & xfechac & "' "
            End If
            If capa.Text <> "" Then
                sql = sql & " and OOT_InvElab.Capa_Asoc = '" & capa.Text & "' "
            End If

            repeater.DataSource = u.ConsultaOOT(sql)
            repeater.DataBind()

        Catch ex As Exception
            Label2.Text = "Error al buscar el OOT"

        End Try
    End Sub

End Class
