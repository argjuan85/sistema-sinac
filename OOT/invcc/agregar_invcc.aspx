﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="agregar_invcc.aspx.vb" Inherits="EO_Default" title="Sistema Sinac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   	<style type="text/css" class="init">
	      	#ctl00_ContentPlaceHolder1_conclusion    
	      	{
	      		width: 96%;
	      		height: 145px;
	      		}

</style>
   
        <!--  validaciones  --> 
  <style type="text/css">
        body {
            
            font-family: Arial;
            font-size: 10pt;
        }
        input
        {
            width: 150px;
        }
          select
        {
            width: 250px;
        }
        #ctl00_ContentPlaceHolder1_revision
           {
            width: 250px;
        }
    </style>
   
     <link href="../../css/ValidationEngine.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery/languages/jquery.validationEngine-es.js"
        charset="utf-8"></script>
    <script type="text/javascript" src="../../js/jquery/jquery.validationEngine.js"
        charset="utf-8"></script>
    <script type="text/javascript">
        $(function () {
            $("#aspnetForm").validationEngine('attach', { promptPosition: "topRight" });
        });
 
    </script>
    <script type="text/javascript">
        function DateFormat(field, rules, i, options) {
            var regex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
            if (!regex.test(field.val())) {
                return "Please enter date in dd/MM/yyyy format."
            }
        }
    </script>
  <!-- fin validaciones  --> 
     <script type="text/javascript">
 
/*  Uso este javascript para el botón volver, ya que no puedo usar un control asp por que dispara el ajax del vanadium  */   
/* se modifico la funcion por que traiga inconvenientes con la encriptacion de parametros, asi como quedo solo va funionar con 1 parametro, si se pasa mas de uno hay q cambiarla (esto es debido q el split lo hacia con el "=" el cual cortaba el string codificado antes si el signo "=" era parte de la cadena encriptada*/
function redireccionar(){
var src = String( window.location.href ).split('?')[1];
var vrs = src.split('&')
var arr = [];

for (var x = 0, c = vrs.length; x < c; x++) 
{
        arr[x] = vrs[x];
};

location.href="../modifica_oot.aspx?"+ arr[0] } 

</script>
   
            <form id="form2" runat="server">
       
 <!-- comments: select operates with dropdownlist -->
    
<div id="contenedor1">
<div id="contenedor2">
     <div id="titulo_seccion">
         1- Ingrese los datos de la investigacion de Control de Calidad.
 </div>
    <ul>
    <li class="paneles">
    <div>
    <span class="tercio">
    <label for="analistas">Analistas</label>
        <asp:ListBox runat="server" ID="analistas" SelectionMode="Multiple"  DataSourceID="SqlDataSource4" 
            DataTextField="Denominacion" DataValueField="Id" Height="140px">
            

</asp:ListBox>
            
      
    </span>
      <span class="dostercios9">
      <label for="desv_asoc">Causa Asignable(*)</label>
      
           <asp:DropDownList ID="causa_asig" runat="server" AutoPostBack="True" CssClass="validate[required]">
            <asp:ListItem Value="">Seleccione</asp:ListItem>
            <asp:ListItem Value="1">Si</asp:ListItem>
            <asp:ListItem Value="0">No</asp:ListItem>
           </asp:DropDownList>
      
           
       
    </span>
      <span class="dostercios9">
      <label for="revision">Revisión según SOPS-MAR-0460 (*)</label>
                 
                 <asp:DropDownList ID="revision" runat="server" CssClass="validate[required]">
            <asp:ListItem Value="">Seleccione</asp:ListItem>
            <asp:ListItem Value="1">Si</asp:ListItem>
            <asp:ListItem Value="0">No</asp:ListItem>
            
        </asp:DropDownList>     
                
            
    
    
    </span>
    
    <span class="dostercios9">
              <asp:Label runat="server" Text="Label" ID="lbl_capa_gen"> </asp:Label>
      <asp:DropDownList ID="capa_gen" runat="server" CssClass="validate[required]">
            <asp:ListItem Value="">Seleccione</asp:ListItem>
            <asp:ListItem Value="1">Si</asp:ListItem>
            <asp:ListItem Value="0">No</asp:ListItem>
            
        </asp:DropDownList>
                 
                 
            
    
    
    </span>
   

   <span class="tercio">
      <label for="desv_asoc">Desvios Asociados(*)</label>
      
           <asp:DropDownList ID="desv_asoc" runat="server"  CssClass="validate[required]">
            <asp:ListItem Value="">Seleccione</asp:ListItem>
            <asp:ListItem Value="1">Si</asp:ListItem>
            <asp:ListItem Value="0">No</asp:ListItem>
            
        </asp:DropDownList>
      
           
       
    </span>
   
    
    <span class="tercio">
         <label for="desv_asoc">OOS Asociados(*)</label>
               <asp:DropDownList ID="oos_Asoc" runat="server"  CssClass="validate[required]">
            <asp:ListItem Value="">Seleccione</asp:ListItem>
            <asp:ListItem Value="1">Si</asp:ListItem>
            <asp:ListItem Value="0">No</asp:ListItem>
            
        </asp:DropDownList>
           <select id="myList2" runat="server" visible="false" onkeypress="return KeySortDropDownList_onkeypress(this,false)">
</select> 
      
    </span>




    <span class="tercio">
       <label for="capa_asoc">Observaciones Desvios (*)</label>
    <textarea id="obs_desv" cols="32" name="S1" rows="5" runat="server" maxlength="2000" class="validate[required] text-input"></textarea>
   </span>
   <span class="tercio">
      <label for="capa_asoc">Observaciones OOS (*)</label>
<textarea id="obs_oos" cols="32" name="S1" rows="5" runat="server"  maxlength="2000" class="validate[required] text-input"></textarea>
   </span>

    
  </div>
  </li>
   <li class="paneles">
    <div>
    
           
      

   
    
    
      
    
    
    
        <span class="completo">
      <label for="conclusion">Causa/Conclusión (*)</label>
  <textarea id="conclusion" cols="20" name="S1" rows="15" runat="server" maxlength="2000" class="validate[required] text-input"></textarea>
            


    
    
    </span>
           <span class="tercio">
    <label for="ccam_asoc">CCAM Asociado (*)</label>
    
             <asp:DropDownList ID="ccam_asoc" runat="server"  CssClass="validate[required]">
            <asp:ListItem Value="">Seleccione</asp:ListItem>
            <asp:ListItem Value="1">Si</asp:ListItem>
            <asp:ListItem Value="0">No</asp:ListItem>
            
       </asp:DropDownList>
            
      
    </span>
    
      <span class="dostercios">
     
             <label for="capa_asoc">CA/PA Asociado? (*)</label>
            <asp:DropDownList ID="capa_asoc" runat="server"  CssClass="validate[required]">
            <asp:ListItem Value="">Seleccione</asp:ListItem>
            <asp:ListItem Value="1">Si</asp:ListItem>
            <asp:ListItem Value="0">No</asp:ListItem>
            
        </asp:DropDownList>
       
    </span>
    
      <span class="tercio">
  <label for="capa_asoc">Observaciones CCAM (*)</label>
<textarea id="obs_ccam" cols="32" name="S1" rows="5" runat="server" maxlength="2000" class="validate[required] text-input"></textarea>
   </span>
    <span class="tercio">
  
           <label for="obs_capa">Observaciones CA/PA (*)</label>
<textarea id="obs_capa" cols="32" name="S1" rows="5" runat="server" maxlength="2000" class="validate[required] text-input"></textarea>
   </span>



      
    
           </div>
  </li>
     
  </ul>
    <ul>
        
    <li class="panel_boton">
     <div>
     
    
   
      <span class="boton">
       
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" 
             style="height: 26px" onclientclick="return confirm('Confirma la carga de la investigación?');"/>
   
        
     <input type="button" value="Volver"  id="volver" onclick="redireccionar(); return false;" ></span></span>
   
    
   </div>

    </li>
                 <li>
                 </li>
     </ul>
         
  </div>
  </div>
  <br>
  <div id="confirmacion">
  <asp:Label ID="Label1" runat="server"></asp:Label>
  </div>
    <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
   
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IdCocam], [codCCNew] FROM Cocam">
        </asp:SqlDataSource>
        
         <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idOOS], [nroIdent] FROM OOS">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IdDesvioInt], [codDINew] FROM DesviosInt">
        </asp:SqlDataSource>
        
   <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [Id], [Denominacion] FROM [User] order by Denominacion asc">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idCapa], [indentCapa] FROM Capa">
        </asp:SqlDataSource>
    </form>
    
    
    
</asp:Content>

