﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration

Partial Class EO_default
    Inherits System.Web.UI.Page
    Dim registro_actual As Integer = 0
    Dim codigo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(16, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            codigo = DecryptText(Request.QueryString("ID")) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos

            oos_Asoc.AppendDataBoundItems = True
            desv_asoc.AppendDataBoundItems = True
            capa_asoc.AppendDataBoundItems = True

            ccam_asoc.AppendDataBoundItems = True
   
            If (Not Page.IsPostBack) Then

                cargaformulario()
            End If

            End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        nuevoeo()
    End Sub
    '
    Sub cargaformulario()
        capa_gen.Visible = False
        'caga_gen_fake.Visible = True
        'lbl_capa_gen.Text = "CA/PA Generado (*)"
        lbl_capa_gen.Text = ""
        'lbl_fake.Text = "ssssssssss"
        'If (capa_asoc.SelectedValue <> "1") Then
        'obs_capa.Disabled = True
        obs_capa.Value = "N/A"
        ' End If
        'If (ccam_asoc.SelectedValue <> "1") Then
        'obs_ccam.Disabled = True
        obs_ccam.Value = "N/A"
        'End If
        'If (desv_asoc.SelectedValue <> "1") Then
        'obs_desv.Disabled = True
        obs_desv.Value = "N/A"
        'End If
        'If (oos_Asoc.SelectedValue <> "1") Then
        'obs_oos.Disabled = True
        obs_oos.Value = "N/A"
        'End If
        conclusion.Value = "N/A"

        If causa_asig.SelectedValue = "1" Then
            capa_gen.Visible = True

            'obs_capa.Disabled  = False
            'lbl_obs_capa.Text = "Observaciones CA/PA (*)"
        Else
            capa_gen.Visible = False

        End If


        'descomentar en productivo
        '  If (Session("departamento") <> "CC") Then
        '      analistas.Enabled = False
        '      causa_asig.Enabled = False
        '      revision.Enabled = False
        '      capa_asoc.Enabled = False
        '      desv_asoc.Enabled = False
        '      oos_Asoc.Enabled = False
        '      ccam_asoc.Enabled = False
        '      capa_asoc.Enabled = False
        '      obs_capa.Disabled = True
        '      obs_ccam.Disabled = True
        '      obs_desv.Disabled = True
        '      obs_oos.Disabled = True
        '      conclusion.Disabled = True
        '      Button1.Enabled = False
        '      Label2.Text = "Su departamento no tiene permisos para cargar esta investigacion"
        '  End If

        'analista.Value = "jarganaraz"
        'superior.Value = "jarganaraz"


    End Sub


    Sub nuevoeo()
   
        Dim e As OOT
        'Dim n As nombres
        Dim r As Integer
        Dim xfecha As String
        'Dim xfechacarga As String
        'Dim sext As String
        Dim I As Integer
        Dim l As logs
        Dim sed As String

        Dim oos_obs As String

        Dim desv_obs As String

        Dim ccam_obs As String

        Dim capa_obs As String
        Dim max_caracteres As String
        Dim ancho_pagina As String
        Dim porcentaje_mayus As String

        Dim p As Parametros
        p = New Parametros()
        p.Parametros(conexion)
        max_caracteres = p.consulta_valor_nombre("max_caracteres")
        ancho_pagina = p.consulta_valor_nombre("ancho_pagina")
        porcentaje_mayus = "10" ' si excede el 10% de la cadea deberia dar error

        l = New logs()
        l.logs(conexion)
        '
        Try
            '
            '
            '
            e = New OOT()
            e.OOT(conexion)
            '   
            '

            ' si esta elegida la opcion no , no cargo nada
            If (oos_Asoc.Text = "0") Then
                oos_obs = "0"
            Else
                oos_obs = obs_oos.Value
            End If
            If (desv_asoc.Text = "0") Then
                desv_obs = "0"
            Else
                desv_obs = obs_desv.Value
            End If
            If (ccam_asoc.Text = "0") Then
                ccam_obs = "0"
            Else
                ccam_obs = obs_ccam.Value
            End If
            If (capa_asoc.Text = "0") Then
                capa_obs = "0"
            Else
                capa_obs = obs_capa.Value
            End If

            ' valido que las cadenas de observaciones no excedan los 2000 caracteres
            Dim bandera As String = "1"
            Dim campo As String

            If (Countcaracteres(oos_obs, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones OOS"
            End If
            If (Countcaracteres(desv_obs, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones Desvíos"
            End If
            If (Countcaracteres(ccam_obs, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones CCAM"
            End If
            If (Countcaracteres(capa_obs, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones CAPA"
            End If
            If (Countcaracteres(conclusion.Value, ancho_pagina) > max_caracteres) Then
                bandera = "-1"
                campo = "Conclusión"
            End If
            'valido mayus
            If (Countmayus(oos_obs, porcentaje_mayus) > max_caracteres) Then
                bandera = "-1"
                campo = "Observaciones OOS"
            End If
            If (Countmayus(desv_obs, porcentaje_mayus) > max_caracteres) Then
                bandera = "-1"
                campo = "Observaciones Desvíos"
            End If
            If (Countmayus(ccam_obs, porcentaje_mayus) > max_caracteres) Then
                bandera = "-1"
                campo = "Observaciones CCAM"
            End If
            If (Countmayus(capa_obs, porcentaje_mayus) > max_caracteres) Then
                bandera = "-1"
                campo = "Observaciones CAPA"
            End If
            If (Countmayus(conclusion.Value, ancho_pagina) > max_caracteres) Then
                bandera = "-1"
                campo = "Conclusión"
            End If



            If (bandera) Then

                If (causa_asig.Text = "1") Then
                    r = e.Agregarinvcc(codigo, oos_Asoc.Text, Trim(oos_obs), desv_asoc.Text, Trim(desv_obs), ccam_asoc.Text, Trim(ccam_obs), capa_asoc.Text, Trim(capa_obs), revision.Text, causa_asig.Text, capa_gen.Text, Trim(conclusion.Value))
                Else
                    r = e.Agregarinvcc(codigo, oos_Asoc.Text, Trim(oos_obs), desv_asoc.Text, Trim(desv_obs), ccam_asoc.Text, Trim(ccam_obs), capa_asoc.Text, Trim(capa_obs), revision.Text, causa_asig.Text, "0", Trim(conclusion.Value))
                End If


                For I = 0 To analistas.Items.Count - 1

                    If (analistas.Items(I).Selected = True) Then
                        Dim str As String = analistas.Items(I).ToString()

                        'genero la relacion entre los analistas y la invcc
                        e.AgregarAnalista_InvCC(analistas.Items(I).Value, r)


                    End If
                Next
               

                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))

               
            
                'log por carga de investigacion
                Dim nro_oot As String
                nro_oot = e.obtenerNroOOT(codigo)
                l.Agregarlog3(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", xfecha, nro_oot, e.obtenerproducto(codigo), e.obtenerlote(codigo), e.obtenerfechacarga(codigo), e.obtenerestado(codigo), "Carga Investigacion CC")

                Label1.Text = "Se cargó la investigación con éxito"
                Response.Redirect("../modifica_OOT.aspx?ID=" + EncryptText(codigo & "//3"))
                '    
                'Else
                'Label2.Text = "Error al cargar el OOT"
                ' End If
                '     
                '
            ElseIf (bandera = "-1") Then
                Label2.Text = "El campo: " + campo + " posee demasiadas mayúsculas, por favor verifique."
            Else
                Label2.Text = "El campo: " + campo + " excede los " + max_caracteres + " caracteres. Verifique."

            End If


        Catch ex As Exception
            Label2.Text = "Error al cargar la investigación"
        End Try
    End Sub


    Protected Sub causa_asig_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles causa_asig.SelectedIndexChanged
        If causa_asig.SelectedValue = "1" Then
            capa_gen.Visible = True
            lbl_capa_gen.Text = "CA/PA Generado (*)"
            'anulo el boton que no hace nada
            'caga_gen_fake.Visible = False
            'lbl_fake.Text = ""
            'obs_capa.Disabled  = False
            'lbl_obs_capa.Text = "Observaciones CA/PA (*)"
        Else
            capa_gen.Visible = False
            lbl_capa_gen.Text = ""
            'caga_gen_fake.Visible = False
            'lbl_fake.Text = ""
        End If
    End Sub
    '----------------------------------------------------------------------
    'RETORNA EL NUMERO DE caracteres DE UNA CADENA incluyendo espacios y saltos de linea en base a un parametro de ancho de pagina
    '----------------------------------------------------------------------
    Public Shared Function Countcaracteres(ByVal tcString As String, ByVal ancho As Integer) As Integer
        Dim conteo As String = "0"
        For Each item As Char In tcString
            If (item <> vbCr) Then
                conteo = conteo + 1
                Continue For
            ElseIf (item = vbCr) Then
                conteo = conteo + ancho
            End If


        Next
        Return conteo
    End Function

    '----------------------------------------------------------------------
    'En base a la cantidad de mayusculas de la cadena devuelve true o false si estas superan cierto %  (Es para la impresion de FOGL)
    '----------------------------------------------------------------------
    Public Shared Function Countmayus(ByVal tcString As String, ByVal ancho As Integer) As Boolean
        Dim conteo As String = "0"
        Dim caracteresPermitidos As String = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ" & Convert.ToChar(8)
        For Each item As Char In tcString
            If (caracteresPermitidos.Contains(item)) Then
                conteo = conteo + 1

            End If

            Continue For
        Next
        If (Len(tcString) * ancho / 100 < conteo) Then
            Return False
        Else
            Return True
        End If

    End Function
    'Protected Sub capa_asoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles capa_asoc.SelectedIndexChanged
    '    If capa_asoc.SelectedValue = "1" Then
    '        obs_capa.Disabled = False
    'lbl_obs_capa.Text = "CA/PA Asociado (*)"
    '     End If
    ' End Sub

    'Protected Sub oos_Asoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles oos_Asoc.SelectedIndexChanged

    '   If oos_Asoc.SelectedValue = "1" Then
    '       obs_oos.Disabled = False

    '  End If
    ' End Sub

    'Protected Sub ccam_asoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ccam_asoc.SelectedIndexChanged
    '     If ccam_asoc.SelectedValue = "1" Then
    '          obs_ccam.Disabled = False

    '       End If
    '    End Sub

    'Protected Sub desv_asoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles desv_asoc.SelectedIndexChanged
    '   If desv_asoc.SelectedValue = "1" Then
    '  obs_desv.Disabled = False

    '    End If
    ' End Sub
End Class
