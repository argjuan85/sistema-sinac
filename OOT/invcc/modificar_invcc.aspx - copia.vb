﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration
Imports System.Net.Mail


Partial Class EO_default
    Inherits System.Web.UI.Page
    Dim codigo As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(32, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else

            capa_asoc.AppendDataBoundItems = True
            codigo = DecryptText(Request.QueryString("ID"))
            analistas.AppendDataBoundItems = True
            Label1.Text = ""
            Label2.Text = ""
            If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                buscarinvcc(codigo) 'carga la interfaz

            End If

            If (capa_asoc.SelectedValue <> "1") Then
                'obs_capa.Disabled = True
                obs_capa.Value = "N/A"
            End If
            If (ccam_asoc.SelectedValue <> "1") Then
                'obs_ccam.Disabled = True
                obs_ccam.Value = "N/A"
            End If


            If (desv_asoc.SelectedValue <> "1") Then
                'obs_desv.Disabled = True
                obs_desv.Value = "N/A"
            End If
            If (oos_Asoc.SelectedValue <> "1") Then
                'obs_oos.Disabled = True
                obs_oos.Value = "N/A"
            End If
            If causa_asig.SelectedValue = "1" Then
                capa_gen.Visible = True
                lbl_capa_gen.Text = "CA/PA Generado (*)"
                'obs_capa.Disabled  = False
                'lbl_obs_capa.Text = "Observaciones CA/PA (*)"
            Else
                capa_gen.Visible = False
                lbl_capa_gen.Text = ""
            End If

        
        End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
      
            actualizarinvcc("0")
   
        Catch ex As Exception

        End Try
    End Sub

    Sub buscarinvcc(ByVal codigo As String)
        Dim u As OOT
        Dim I As Integer
        Dim auxe As Integer
        Dim cantidad As SqlDataReader
        Dim idinv As Integer
        Try
            u = New OOT()
            u.OOT(conexion)
            Dim estado As String

            idinv = u.obtenerid_InvCC(codigo)
            cantidad = u.obtener_analistas()
            While cantidad.Read()
                analistas.Items.Add(cantidad.GetString(0))
            End While
            cantidad.Close()
            For I = 0 To analistas.Items.Count - 1
               

                'verifico que exista la relacion
                auxe = u.obtenerid_InvCC_analistas(idinv, u.obtener_id_analistas(analistas.Items(I).Value))
                If (auxe <> "-1") Then
                    'tipo_error.SelectedValue = tipo_error.Items(I).Value
                    analistas.Items(I).Selected = True
                End If

            Next


            causa_asig.Text = u.consultar_causa(codigo)

            capa_gen.Visible = False
         



            estado = u.obtenerestado(codigo)
            oos_Asoc.Text = u.consultar_oos_asoc(codigo)
            desv_asoc.Text = u.consultar_desv_asoc(codigo, "CC")
            ccam_asoc.Text = u.consultar_ccam_asoc(codigo, "CC")
            capa_asoc.Text = u.consultar_capa_asoc(codigo, "CC")
            capa_gen.Text = u.consultar_capa_gen(codigo)
            obs_oos.Value = u.consultar_obs_oos(codigo)
            obs_desv.Value = u.consultar_obs_desv(codigo, "CC")
            obs_ccam.Value = u.consultar_obs_ccam(codigo, "CC")
            obs_capa.Value = u.consultar_obs_capa(codigo, "CC")
            conclusion.Value = u.consultar_conclusion(codigo)
            revision.Text = u.consultar_revision(codigo, "CC")
            If (estado <> "En Curso") And (estado <> "PendienteCC") Then
                analistas.Enabled = False
                oos_Asoc.Enabled = False
                desv_asoc.Enabled = False
                ccam_asoc.Enabled = False
                capa_asoc.Enabled = False
                capa_gen.Enabled = False
                obs_oos.Disabled = True
                obs_desv.Disabled = True
                obs_ccam.Disabled = True
                obs_capa.Disabled = True
                conclusion.Disabled = True
                revision.Enabled = False
                Button1.Visible = False
                causa_asig.Enabled = False

            End If


            If (capa_asoc.SelectedValue <> "1") Then
                'obs_capa.Disabled = True
                'obs_capa.Value = "N/A"
            End If
            If (ccam_asoc.SelectedValue <> "1") Then
                'obs_ccam.Disabled = True
                'obs_ccam.Value = "N/A"
            End If
            If (desv_asoc.SelectedValue <> "1") Then
                'obs_desv.Disabled = True
                'obs_desv.Value = "N/A"
            End If
            If (oos_Asoc.SelectedValue <> "1") Then
                'obs_oos.Disabled = True
                'obs_oos.Value = "N/A"
            End If
            If causa_asig.SelectedValue = "1" Then
                capa_gen.Visible = True
                lbl_capa_gen.Text = "CA/PA Generado (*)"
                'obs_capa.Disabled  = False
                'lbl_obs_capa.Text = "Observaciones CA/PA (*)"
            Else
                capa_gen.Visible = False
                lbl_capa_gen.Text = ""
            End If


            If (estado = "En Curso") Then
                Button2.Visible = True
            Else
                Button2.Visible = False
            End If

            If (estado = "PendienteCC") Then
                Button3.Visible = True
            Else
                Button3.Visible = False
            End If


            'If (Session("departamento") <> "CC") Then
            ' analistas.Enabled = False
            '  causa_asig.Enabled = False
            '   revision.Enabled = False
            '    capa_asoc.Enabled = False
            '    desv_asoc.Enabled = False
            '    oos_Asoc.Enabled = False
            '    ccam_asoc.Enabled = False
            '    capa_asoc.Enabled = False
            '    obs_capa.Disabled = True
            '    obs_ccam.Disabled = True
            '    obs_desv.Disabled = True
            '    obs_oos.Disabled = True
            '    conclusion.Disabled = True
            '    Button1.Enabled = False
            '    Button2.Enabled = False
            '    Button3.Enabled = False
            '    Label2.Text = "Su departamento no tiene permisos para modificar esta investigacion"
            'End If


        Catch ex As Exception

        End Try

    End Sub


    Sub actualizarinvcc(ByVal llave As String)

        Dim u As OOT

        Dim r As String

        Dim l As logs

        Dim oos_obs As String

        Dim desv_obs As String

        Dim ccam_obs As String

        Dim capa_obs As String
        Dim xfecha As String
        Dim max_caracteres As String
        Dim ancho_pagina As String


        Dim p As Parametros
        p = New Parametros()
        p.Parametros(conexion)
        max_caracteres = p.consulta_valor_nombre("max_caracteres")
        ancho_pagina = p.consulta_valor_nombre("ancho_pagina")

        Try
            ' 'log
            l = New logs()
            l.logs(conexion)
            u = New OOT()
            u.OOT(conexion)


            ' si esta elegida la opcion no , no cargo nada
            If (oos_Asoc.Text = "0") Then
                oos_obs = "0"
            Else
                oos_obs = obs_oos.Value
            End If
            If (desv_asoc.Text = "0") Then
                desv_obs = "0"
            Else
                desv_obs = obs_desv.Value
            End If
            If (ccam_asoc.Text = "0") Then
                ccam_obs = "0"
            Else
                ccam_obs = obs_ccam.Value
            End If
            If (capa_asoc.Text = "0") Then
                capa_obs = "0"
            Else
                capa_obs = obs_capa.Value
            End If

            ' valido que las cadenas de observaciones no excedan los 2000 caracteres
            Dim bandera As String = "1"
            Dim campo As String

            If (Countcaracteres(oos_obs, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones OOS"
            End If
            If (Countcaracteres(desv_obs, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones Desvíos"
            End If
            If (Countcaracteres(ccam_obs, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones CCAM"
            End If
            If (Countcaracteres(capa_obs, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Observaciones CAPA"
            End If
            If (Countcaracteres(conclusion.Value, ancho_pagina) > max_caracteres) Then
                bandera = "0"
                campo = "Conclusión"
            End If
            If (bandera) Then
                If (causa_asig.Text = "1") Then
                    r = u.Actualizarinvcc(codigo, oos_Asoc.Text, oos_obs, desv_asoc.Text, desv_obs, ccam_asoc.Text, ccam_obs, capa_asoc.Text, capa_obs, revision.Text, causa_asig.Text, capa_gen.Text, conclusion.Value)
                Else
                    r = u.Actualizarinvcc(codigo, oos_Asoc.Text, oos_obs, desv_asoc.Text, desv_obs, ccam_asoc.Text, ccam_obs, capa_asoc.Text, capa_obs, revision.Text, causa_asig.Text, "0", conclusion.Value)
                End If

                ' actualizo analistas
                Dim borrar As String
                Dim i As Integer
                Dim x As String
                Dim idinvcc As String
                idinvcc = u.obtenerid_InvCC(codigo)
                ' por simplicidad elimino las relaciones previas y cargo las nuevas luego de la modificacion 
                u.elimina_analistas(idinvcc)

                borrar = analistas.Items.Count
                For i = 0 To analistas.Items.Count - 1

                    If (analistas.Items(i).Selected = True) Then
                        Dim str As String = analistas.Items(i).ToString()

                         'genero la relacion entre los analistas y la invcc
                        x = u.AgregarAnalista_InvCC(u.obtener_id_analistas(analistas.Items(i).Value), idinvcc)

                    End If
                Next


                'fin actualizacion analistas



                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))

                'una vez definido como es el manejo del user cambiar el segundo parametro, de momento hardcode

                l.Agregarlog3(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", xfecha, u.obtenerNroOOT(codigo), u.obtenerproducto(codigo), u.obtenerlote(codigo), u.obtenerfechacarga(codigo), u.obtenerestado(codigo), "Modifica Investigacion CC/Conclusion CC")


                If (llave <> "1") Then
                    Label1.Text = "Se modificaron correctamente los datos de la investigación"
                    Response.Redirect("../modifica_OOT.aspx?ID=" + EncryptText(codigo & "//2"))
                End If
            Else
                Label2.Text = "El campo: " + campo + " excede los " + max_caracteres + " caracteres. Verifique."

            End If

        Catch ex As Exception
            Label2.Text = "Hubo un error al actualizar la investigación"
        End Try
    End Sub

    

  
 
  
    Protected Sub causa_asig_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles causa_asig.SelectedIndexChanged
        If causa_asig.SelectedValue = "1" Then
            capa_gen.Visible = True
            lbl_capa_gen.Text = "CA/PA Generado (*)"
            'obs_capa.Disabled  = False
            'lbl_obs_capa.Text = "Observaciones CA/PA (*)"
        Else
            capa_gen.Visible = False
            lbl_capa_gen.Text = ""
        End If
    End Sub

    Protected Sub capa_asoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles capa_asoc.SelectedIndexChanged
        If capa_asoc.SelectedValue = "1" Then
            obs_capa.Disabled = False
            'lbl_obs_capa.Text = "CA/PA Asociado (*)"
        End If
    End Sub

    Protected Sub oos_Asoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles oos_Asoc.SelectedIndexChanged

        If oos_Asoc.SelectedValue = "1" Then
            obs_oos.Disabled = False

        End If
    End Sub

    Protected Sub ccam_asoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ccam_asoc.SelectedIndexChanged
        If ccam_asoc.SelectedValue = "1" Then
            obs_ccam.Disabled = False

        End If
    End Sub

    Protected Sub desv_asoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles desv_asoc.SelectedIndexChanged
        If desv_asoc.SelectedValue = "1" Then
            obs_desv.Disabled = False

        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim u As OOT


        Dim word As String
        u = New OOT()
        u.OOT(conexion)
        u.ActualizarestadoOOT(codigo, "PendienteCC")
        Dim us As User
        us = New User()
        us.User(conexion)

        u.ActualizarestadoOOT(codigo, "PendienteCC")
        'aca debo recorrer los user a notificar
        'descomentar a la hora de poner en productivo
        'Dim s As String = us.Consultarmails_Sector("Control de Calidad", "1")
        'Dim words As String() = s.Split(New Char() {"/"c})
        'For Each word In words
        'intercepto aca para que no envie mails reales
        '   word = "jarganaraz@raffo.com.ar"
        '   EnvioMail(word, u.obtenerNroOOT(codigo).ToString("d4"))
        'Next
        'para pruebas
        word = "jarganaraz@raffo.com.ar"
        EnvioMail(word, u.obtenerNroOOT(codigo).ToString("d4"))

        Response.Redirect("../modifica_OOT.aspx?ID=" + EncryptText(codigo & "//2"))
    End Sub



    Public Sub EnvioMail(ByVal mail As String, ByVal registro As String)
        Try
            Dim p As Parametros
            p = New Parametros()
            p.Parametros(conexion)

            'cargo parametros

            sops = p.consulta_valor_nombre("sops_oot")
            fogl = p.consulta_valor_nombre("fogl_oot")
            titulo = p.consulta_valor_nombre("titulo_fogl_oot")
            vigencia = p.consulta_valor_nombre("vigencia_fogl_oot")

            Dim message As New MailMessage

            Dim smtp As New SmtpClient

            message.From = New MailAddress(p.consulta_valor_nombre("Mail"))

            message.To.Add(mail)

            message.Body = "Ya se encuentra disponible para concluir el OOT de referencia." & vbLf & vbLf

            message.Body = message.Body & "AVISO: Este es un mail automatico generado por el Sistema Sinac. No responder sobre este mail. Cualquier duda comunicarse con QA"

            message.Subject = "OOT: " & registro

            message.Priority = MailPriority.Normal

            smtp.EnableSsl = False

            smtp.Port = p.consulta_valor_nombre("puerto")

            smtp.Host = p.consulta_valor_nombre("Servidor")

            smtp.Credentials = New Net.NetworkCredential(p.consulta_valor_nombre("Alias"), p.consulta_valor_nombre("Pass"))

            smtp.Send(message)
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub Button3_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        actualizarinvcc("1")
        Dim us As User
        us = New User()
        us.User(conexion)
        Dim word As String
        Dim u As OOT

        u = New OOT()
        u.OOT(conexion)
        u.ActualizarestadoOOT(codigo, "ConcluidoCC")
        u.cargar_firma(Session("usuario"), DateTime.Now.ToString("yyyy-MM-dd"), codigo, "PendienteCC")
        'aca debo recorrer los user a notificar
        'aca debo recorrer los user a notificar
        'descomentar a la hora de poner en productivo
        'Dim s As String = us.Consultarmails_Sector("QA - Revisión anual de producto", "1")
        'Dim words As String() = s.Split(New Char() {"/"c})
        'For Each word In words
        'intercepto aca para que no envie mails reales
        '   word = "jarganaraz@raffo.com.ar"
        '   EnvioMail(word, u.obtenerNroOOT(codigo).ToString("d4"))
        'Next
        'para pruebas
        word = "jarganaraz@raffo.com.ar"
        EnvioMail(word, u.obtenerNroOOT(codigo).ToString("d4"))
        Response.Redirect("../modifica_OOT.aspx?ID=" + EncryptText(codigo & "//2"))
    End Sub

    Public Shared Function Countcaracteres(ByVal tcString As String, ByVal ancho As Integer) As Integer
        Dim conteo As String = "0"
        For Each item As Char In tcString
            If (item <> vbCr) Then
                conteo = conteo + 1
                Continue For
            ElseIf (item = vbCr) Then
                conteo = conteo + ancho
            End If


        Next
        Return conteo
    End Function
End Class
