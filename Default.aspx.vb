﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports globales
Imports System.Net.Mail
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/inicio.aspx"))
        ElseIf (Not validapermiso(0, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        Else







        End If

    End Sub

   




  
    Public Sub EnvioMail(ByVal estandar As String, ByVal fecha As String, ByVal stock As String, ByVal uni As String)


        Dim message As New MailMessage

        Dim smtp As New SmtpClient

        message.From = New MailAddress(mailsistema)

        If (Session("sector") = sectoradmin) Then
            message.To.Add(mailadmincc)
        Else
            message.To.Add(mailadminve)
        End If

        message.Body = "El estándar: " & estandar & " se encuentra vencido." & vbLf & vbLf

        message.Body = message.Body & "Fecha de Vencimiento: " & fecha & "." & vbLf & vbLf

        message.Body = message.Body & "Stock: " & stock & " " & uni & "." & vbLf & vbLf

        message.Body = message.Body & "Este es un correo generado automáticamente por el sistema de estándares, no responda este correo."

        message.Subject = "Estándar vencido."

        message.Priority = MailPriority.Normal

        smtp.EnableSsl = False

        smtp.Port = mailport

        smtp.Host = mailserver

        smtp.Credentials = New Net.NetworkCredential(mailuser, mailpass)

        smtp.Send(message)

    End Sub



End Class
