﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="consulta_logs2.aspx.vb" Inherits="Logs_consulta_logs" title="Sistema Sinac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:Label ID="Label1" runat="server"></asp:Label>
    
     <form id="form2" runat="server">
    
  <!-- librerias calendario y css -->
       <link rel="stylesheet" href="../EO/css/ui-lightness/jquery-ui-1.8.20.custom.css" type="text/css" media="screen" charset="utf-8"/>
       <script src="../EO/js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>        
       <script src="../EO/js/jquery-ui-1.8.20.custom.min.js" type="text/javascript" charset="utf-8"></script> 
       
       <link rel="stylesheet" href="../js/datatable/media/css/demo_table.css"type="text/css" />
<link rel="stylesheet" href="../css/informe.css"type="text/css" />
	<script type="text/javascript" language="javascript" src="../js/DataTables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="../js/DataTables/extensions/TableTools/js/dataTables.tableTools.js"></script>
	<script type="text/javascript" language="javascript" src="../../../examples/resources/syntax/shCore.js"></script>
	<script type="text/javascript" language="javascript" src="../../../examples/resources/demo.js"></script>
	
	        <script type="text/javascript" charset="utf-8" src="../js/datatable/ColVis/media/js/ColVis.js"></script>
    <script type="text/javascript" charset="utf-8"></script>
   
<script type="text/javascript" src="../EO/js/jquery.numeric.js"></script>



	<style type="text/css" class="init">
	    input {
        }

	</style>
 
   	<style type="text/css" class="init">
	      	#ctl00_ContentPlaceHolder1_producto    
	      	{
	      		width: 120px;
	      		height: 20px;
	      		}

</style>
 
    <script type="text/javascript" charset="utf-8"></script>
<script language="javascript" type="text/javascript">

var asInitVals = new Array();
     $(document).ready(function () {
     
     
     	       $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

	    
		$("#<%= fecha_desde.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true,changeYear: true ,yearRange: '-10:+10', maxDate: 'today', stepMonths: 0}).val();
		$("#<%= fecha_hasta.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true,changeYear: true ,yearRange: '-10:+10', maxDate: 'today', stepMonths: 0}).val();
		
		//Con este codigo indico que la fecha de ingreso no puede ser menor a la pedido
	

		
	 $('#ctl00_ContentPlaceHolder1_Button1').click(function(){
    if($("#<%= fecha_desde.ClientID %>").val().length < 1) {
    alert("El campo 'fecha desde' es obligatorio");
    return false;
}
   if($("#<%= fecha_hasta.ClientID %>").val().length < 1) {
    alert("El campo 'fecha hasta' es obligatorio");
    return false;
}
   });

     
     
     var  oTable = $('#tbl').dataTable( {
    
    
    "dom": 'T<"clear">lfrtip',
      
           "oTableTools": {
                                                                "aButtons": [
                                                                                "xls",
                                                                                
                                                                            ]
                                                                },
    
            "sScrollY": "100%",
            //'sPaginationType': 'full_numbers',
            //'iDisplayLength': 5,
            	"oColVis": {
		    		
					"activate": "mouseover",
						
						"aiExclude": [ 5 ]
						
				},
				"oLanguage": {
					"sProcessing":     "Procesando...",
				    "sLengthMenu":     "Mostrar _MENU_ registros",
				    "sZeroRecords":    "No se encontraron resultados",
				    "sEmptyTable":     "Ningún dato disponible en esta tabla",
				    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				    "sInfoPostFix":    "",
				    "sSearch":         "Buscar:",
				    "sUrl":            "",
				    "sInfoThousands":  ",",
				    
				    "oPaginate": {
				        "sFirst":    "Primero",
				        "sLast":     "Último",
				        "sNext":     "Siguiente",
				        "sPrevious": "Anterior"
				    },
				    "sLoadingRecords": "Cargando...",
				    "fnInfoCallback": null,
				    "oAria": {
				        "sSortAscending":  ": Activar para ordernar la columna de manera ascendente",
				        "sSortDescending": ": Activar para ordernar la columna de manera descendente"
				    }
				    },
            "bPaginate": true,
            "bProcessing": true,
            "bServerSide": false,
            "bSortCellsTop": true
        });
        
        
        /* Add the events etc before DataTables hides a column */
			$("thead input").keyup( function () {
				/* Filter on the column (the index) of this element */
				oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex( 
					oTable.fnSettings(), $("thead input").index(this) ) );
			} );
			
			/*
			 * Support functions to provide a little bit of 'user friendlyness' to the textboxes
			 */
			$("thead input").each( function (i) {
				this.initVal = this.value;
			} );
			
			$("thead input").focus( function () {
				if ( this.className == "search_init" )
				{
					this.className = "";
					this.value = "";
				}
			} );
			
			$("thead input").blur( function (i) {
				if ( this.value == "" )
				{
					this.className = "search_init";
					this.value = this.initVal;
				}
			} );
        
    });
</script>

           
    <div id="contenedor_informeconsumos">
    <div id="contenedorenvios"> <div id="titulo_seccion">
Consulta de logs de auditoria (OOT)
 </div>
        
    <ul>
    <li class="paneles">
    <div>
  
 
    <span class="tercio">
 <label for="user"  runat="server" id="labeluser">Usuario</label>
    <asp:TextBox ID="user" runat="server" ></asp:TextBox>

    </span>
        <span class="tercio">
      
      <label for="nombre"  runat="server" id="labeltipo">Tipo</label>
    <asp:TextBox ID="tipo" runat="server" ></asp:TextBox>
    </span>
  
  
 </div>
 </li>
   </ul>
   
                   
    <ul>  
    <li class="paneles">
   
    <div>
    
      <span class="tercio">
     
    <label for="fecha_desde">Fecha Desde</label>
        <input id="fecha_desde" name="fecha_desde" value="" runat="server" 
             readonly="readonly" />
        
     
    </span>
   <span class="dostercios">
    <label for="fecha_hasta">Fecha Hasta</label>
        <input id="fecha_hasta" name="fecha_hasta" value="" runat="server" 
             readonly="readonly" />
      </span>
    
 
    
  </div>
  </li>
  </ul>
    
  
 
    
    <ul>
    <li class="panel_boton">
        <div>
      <span class="boton">
           
         <asp:Button ID="Button1" runat="server" Text="Buscar" />
      </span></div>
    </li>
    </ul>
              
                   

    </form>
    
  </div>
  </div>
        

    
<asp:Repeater ID="repeater" runat="server" OnItemCommand ="RepeaterDeleteitemcommand">
            <HeaderTemplate>
                <table id="tbl" cellpadding="1" cellspacing="0" 
                    border="0" class="display" >
                  <thead>
                    <tr>
                       
                        <th>Usuario Sistema</th>
                        <th>Usuario Windows</th>
                        <th>Departamento</th>
                        <th>PC</th>
                        <th>Tipo Mov.</th>
                        <th>Fecha Mov.</th>
                        <th>NroOOT</th>
                        <th>Producto</th>
                        <th>Lote</th>
                        <th>Estado</th>
                      <th>Observaciones</th>
                   
                     
                                              
                    </tr>
                    <tr>
			
			<td align="center"><input type="text" size="8" name="search_browser1" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_engine2" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="3" name="search_browser2" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="18" name="search_engine3" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="5" name="search_browser3" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="14" name="search_engine4" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_browser4" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="14 name="search_browser2" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_engine3" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="3" name="search_browser3" value="" class="search_init" /></td>
		    <td align="center"><input type="text" size="15" name="search_browser3" value="" class="search_init" /></td>
		
			
			
						
		</tr>
                  </thead>
                <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                  
                  <td align="center"><%#Eval("UserSist")%></td>
                  <td align="center"><%#Eval("UserSO")%></td>
                  <td align="center"><%#Eval("DptoUSist")%></td>
                  <td align="center"><%#Pc(Eval("Pc"))%></td>
                  <td align="center"><%#Eval("Accion")%></td>
                  <td align="center"><%#Eval("FechaAccion")%></td>
                  <td align="center"><%#Eval("NroOOT")%></td>
                  <td align="center"><%#Eval("Pdto")%></td>
                  <td align="center"><%#Eval("Lote")%></td>
                  <td align="center"><%#Eval("Estado")%></td>
                   <td align="center"><%#Eval("Observaciones")%></td>
            
            
               
         
                           

                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
       
    
   <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>

</asp:Content>

