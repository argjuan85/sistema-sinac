﻿Imports Encryption64
Imports globales
Partial Class Default2
    Inherits System.Web.UI.Page
    Public codigo As String
    Public codigo2 As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'valido IE para que no ingrese
        If Request.Browser.Browser = "IE" Or Request.Browser.Browser = "InternetExplorer" Or Request.Browser.Browser = "Mozilla" Then
            'ocultar los campos del login
            Label1.Text = " Por favor ejecute el sistema desde Chrome, si no está instalado en el equipo por favor comuníquese con Mesa de Ayuda (Int. 2025) "
        Else

            ' tomo de la url variables que necesito y las dejo en var de sesion
            ' hay q probar desde vb6 si puede encriptar
            'codigo = DecryptText(Request.QueryString("ID"))
            codigo = Request.QueryString("ID")
            'TextBox4.Text = codigo
            codigo = decrypt(codigo, "key")
            'If (codigo = Nothing) Then
            'Response.Redirect(ResolveUrl("~/acceso.aspx"))
            'Else
            parametros()
            'End If

            ultimo_acceso = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            'redirecciono para que no vean url
            Response.Redirect(ResolveUrl("~/principal.aspx"))
            'TextBox2.Text = codigo
        End If
    End Sub

    Sub parametros()
        ' We want to split this input string.
        'COMENTAR LAS DOS LINEAS DE ABAJO EN PRODUCTIVO, ESTO ES SOLO PARA DESARROLLO LOCAL
        Dim s As String = "0*RAP*sistemas*pruebas*"
        s = s & DateTime.Now.ToString("dd/MM/yyyy HH:mm")
        Dim indice As Integer = 0
        Dim fecha_net As String
        Dim fecha_vb6 As String
        Dim hora_net As String


        'DESCOMENTAR EN PRODUCTIVO
        'Dim words As String() = codigo.Split(New Char() {"*"c})

        ' COMENTAR EN PRODUCTIVO LA LINEA DE ABAJO harcodeo desde el string s, descomentar arriba para probar url
        Dim words As String() = s.Split(New Char() {"*"c})

        ' Use For Each loop over words and display them.
        Dim word As String
        Dim test As String
        For Each word In words
            Select Case indice
                Case "0"
                    'TextBox1.Text = word
                    indice += 1
                    Session("nivel") = word
                    test = Session("nivel")
                Case "1"
                    'TextBox2.Text = word
                    Session("departamento") = word
                    indice += 1
                Case "2"
                    'TextBox3.Text = word
                    Session("usuario") = word
                    indice += 1
                Case "3"
                    'TextBox4.Text = word
                    Session("so") = word
                    indice += 1
                Case "4"
                    'TextBox4.Text = word
                    Session("fecha") = word
                    indice += 1
                Case "5"
                    'TextBox4.Text = word
                    Session("hora") = word
                    indice += 1

            End Select

        Next



        'valido fecha y hora por seguridad, es decir, que este desencriptando antes de transcurrido un minuto, si no la cadena generada no es valida
        fecha_net = DateTime.Now.ToString("dd/MM/yyyy HH:mm")
        'comentar en productivo
        fecha_vb6 = Session("fecha")
        'descomentar en prod
        'fecha_vb6 = Session("fecha") & " " & Session("hora")
        'fecha_vb6 = Session("dia") & "/" & Session("mes") & "/" & Session("año") & " " & Session("hora")
        'TextBox1.text = fecha_net
        'TextBox2.Text = fecha_vb6
        'TextBox3.Text = codigo
  
        'si difiere la fecha en un minuto ya no va ingresar
        If (fecha_net <> fecha_vb6) Then
            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        End If

        ' aqui cargo los permisos de los usuarios en el modulo
        Select Case Session("nivel")
            Case "0"

                Session("permiso") = "8388607" ' valor definido

            Case "1"

                Session("permiso") = "1731243" ' valor definido
            Case "2"

                Session("permiso") = "8388607" ' valor definido

            Case "4"

                Session("permiso") = "1731243" ' valor definido
            Case "5"

                Session("permiso") = "8388607" ' valor definido

            Case "6"

                Session("permiso") = "1731243" ' valor definido
        End Select


    End Sub

    '//Función para Desencriptar
    Public Function decrypt(ByVal cadena As String, ByVal key As String) As String
        Dim result As String
        Dim crter, keychar, cadenaM As String
        Dim resu As String
        Dim i As Integer
        Dim X As Integer

        result = ""
        cadenaM = cadena
        Dim borrar As String
        Dim borrar2 As String
        Dim borrar3 As String
        Dim borrar4 As String
        borrar = Len(cadenaM)
        For i = 1 To (Len(cadenaM)) Step 1

            If (i = "63") Then
                borrar2 = crter
                borrar2 = crter
            End If
            crter = Mid(cadenaM, i, 1)
            X = i / (Len(key) - 1)
            If X < 1 Then
                X = 1
            End If
            keychar = Mid(key, X, 1)
            If keychar = "" Then
                keychar = "a"
            End If
            borrar3 = Asc(crter)
            borrar4 = Asc(keychar)
            If (borrar3 - borrar4 < "0") Then
                result = result + "?"
            Else
                crter = Chr(Asc(crter) - Asc(keychar))
                result = result + crter
            End If
        Next i

        Return result
    End Function


End Class
