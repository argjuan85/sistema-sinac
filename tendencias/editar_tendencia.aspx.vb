﻿Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports Encryption64
Partial Class Licenciantes_editar_licenciante
    Inherits System.Web.UI.Page
    Public codigo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(65536, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        ElseIf (Session("nivel") <> "0") Then
            Form1.Visible = False

        Else
            Try
                codigo = Request.QueryString("ID")
                codigo = DecryptText(codigo)
               

                If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                    buscartendencia(codigo) 'carga la interfaz
                End If
            Catch
                Label2.Text = "Error al cargar datos de las tendencias"
                'Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If
    End Sub



    Sub buscartendencia(ByVal codigo As String)
        Dim dtresultado As SqlDataReader
        Dim u As Tendencias
        Try

            u = New Tendencias() ' nuevo objeto instancia
            u.Tendencias(conexion) 'invoco el constructor y paso parametros de conexion
            dtresultado = u.Consultartendencia1(codigo)

            While (dtresultado.Read())

                nombre1.Value = dtresultado(1).ToString()


            End While

            dtresultado.Close()

        Catch ex As Exception
            Label2.Text = "Error al realizar la busqueda de las tendencias"
        End Try
    End Sub
    Sub actualizartendencia(ByVal codigo As String)
        Dim u As Tendencias
        Dim r As Boolean
        Dim l As logs
        Dim xfecha As String
        'log
        l = New logs()
        l.logs(conexion)

        Try
            u = New Tendencias() ' nuevo objeto instancia
            u.Tendencias(conexion) 'invoco el constructor y paso parametros de conexion

            'Label1.Text = "conexion con exito"


            r = u.ActualizarTendencia(codigo, Trim(nombre1.Value), "1")
            'log
            xfecha = cambiaformatofechahora(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            'l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", "Modifica Destinatario", "Destinatario", codigo, nombre1.Value)
            Label1.Text = "Se realizó el registro correctamente"

        Catch ex As Exception
            Label2.Text = "Se produjo un error al actualizar "
            'Label1.Text = "Conexion Error" + ex.Message.ToString()
        End Try
    End Sub
    'modifica
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' verificamos que el destinatario no este repetido y Guardamos  en la base de datos.
        '
        Label1.Text = ""
        Label2.Text = ""
        If (Trim(nombre1.Value).Length > "2") Then
            If (verificatendencia(Trim(nombre1.Value))) Then
                actualizartendencia(codigo)



            Else
                Label2.Text = "El registro ya se encuentra cargado, por favor ingrese otro valor"
                Return
            End If
        Else
            Label2.Text = "Debe ingresar letras o números"


        End If

     
    End Sub
    'check repetido
    Public Shared Function verificatendencia(ByVal tendencia As String) As Integer
        Dim dtresultado As SqlDataReader
        Dim u As Tendencias
        u = New Tendencias() ' nuevo objeto instancia
        u.Tendencias(conexion) 'invoco el constructor y paso parametros de conexion
        dtresultado = u.Consultatendencia2()
        Dim band As String = 1
        While (dtresultado.Read())

            If (tendencia.ToUpper() = dtresultado(1).ToString().ToUpper()) Then
                band = 0
                Return (band)
            End If

        End While

        dtresultado.Close()

        Return (band)


    End Function

    'eliminar
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim u As Tendencias
        Dim r As Boolean
        Dim dtresultado As SqlDataReader
        Dim dtresultado2 As SqlDataReader
        Dim idreg As Integer
        Dim nomreg As String
        Dim l As logs
        Dim xfecha As String

        'log
        l = New logs()
        l.logs(conexion)

        u = New Tendencias()
        u.Tendencias(conexion)

        ' reviso que no este asociado
        dtresultado = u.Consultartendenciasasoc(codigo)
        dtresultado2 = u.Consultartendencia1(codigo)
        If (dtresultado2.Read()) Then
            idreg = dtresultado2(0).ToString()
            nomreg = dtresultado2(1).ToString()
        End If
        dtresultado2.Close()
        If (Not dtresultado.Read()) Then

            ' elimino  el lic
            r = u.BorrarTendencia(codigo)
            xfecha = cambiaformatofechahora(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
            l.Agregarlog(xfecha, User.Identity.Name, System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "B", "Baja Destinatario", "Destinatario", idreg, nomreg)

            Response.Redirect("consultar_tendencias.aspx")
        Else
            Label1.Text = "No se puede eliminar el registro, el mismo esta asociado a alguna OOT. Solo se puede deshabilitar"


        End If
        dtresultado.Close()
    End Sub
End Class
