﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports System.Net.Mail


Partial Class Estandares_consulta_estandares
    Inherits System.Web.UI.Page
    Public opcion As String
    Public sql As String
    Public codigo As String

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' debo tomar el codigo por parametro
        'codigo = DecryptText(Request.QueryString("ID"))

        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(16384, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            'solo administradores podran cargar 

            If (Session("nivel") <> "0") Then
                Button2.Enabled = "False"
            End If

            If (Not Page.IsPostBack) Then
                'CheckBox10.Checked = True
                Label2.Text = ""
                buscartendencias()
                ' Deshabilito el boton eliminar para los usuarios nivel 2 y 4 o si el estandar esta fuera de vigencia
                If (Session("nivel") <> "0") Then

                    Dim ri As RepeaterItem
                    Dim boton As Button
                    For Each ri In repeater.Items
                        boton = ri.FindControl("Button1")
                        boton.Enabled = False

                    Next
                Else
                    Dim ri As RepeaterItem
                    Dim boton As Button
                    For Each ri In repeater.Items
                        boton = ri.FindControl("Button1")
                        'boton.Attributes.Add("onclick", " return confirma_elimina();")
                    Next


                End If



            End If

         
        End If
    End Sub

    Protected Function invertirfecha(ByVal fecha As Object) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 6, 2)
            bDia = Right(Fechatest, 2)
            bAño = Left(Fechatest, 4)
            Fechanueva = bDia & "/" & bMes & "/" & bAño
            Return Fechanueva
        End If


    End Function

   
   

    
    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand

        Dim strURL As String
        Dim Codigox As String
        If (e.CommandName = "Click") Then
            'get the id of the clicked row
            Codigox = Convert.ToString(e.CommandArgument)

                       strURL = "editar_tendencia.aspx?ID=" + EncryptText(Codigox)
            Response.Redirect(strURL)
        End If

    End Sub
    Protected Function Vacio(ByVal nombre As Object) As Object
        If (Not nombre.Equals(DBNull.Value)) Then
            Return nombre
        Else
            Return "N/A"

        End If



    End Function
    Protected Function rellena(ByVal nombre As Integer) As String


        Return nombre.ToString("d4")


    End Function

    Protected Function fecha(ByVal fecha_1 As Date) As String
        Dim fechax As Date
        fechax = fecha_1
        Return fechax


    End Function
    Protected Function usuario(ByVal nombre As String) As String
        Dim l As User
        l = New User()
        l.User(conexion)

        Return l.Consultarusuario(nombre)

    End Function

    Protected Function etapa_nom(ByVal nombre As String) As String
        Dim e As Etapa
        e = New Etapa()
        e.Etapa(conexion)
        Return e.consulta_nombre_etapa(nombre)


    End Function

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("agregar_tendencias.aspx")
    End Sub

    

  
   

   
   
  
    Sub buscartendencias()
        Dim u As Tendencias

        Try
            u = New Tendencias()
            u.Tendencias(conexion)
            repeater.DataSource = u.Consultartendencias()
            repeater.DataBind()
        Catch ex As Exception
            Label2.Text = " Error al realizar la búsqueda de lotes involucrados " + ex.Message

        End Try
    End Sub
    
End Class
