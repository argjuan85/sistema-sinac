﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration

Partial Class EO_default
    Inherits System.Web.UI.Page
    Dim registro_actual As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(262144, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            'fecha_cierre.Visible = False
            If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                analista.AppendDataBoundItems = True
                producto.AppendDataBoundItems = True
                superior.AppendDataBoundItems = True
                laboratorio.AppendDataBoundItems = True
                etapa.AppendDataBoundItems = True
            End If
            'TE = DecryptText(Request.QueryString("ID"))
            cargaformulario()

            Dim message As String = "Confirma la carga del EO?"
            Dim sb As New System.Text.StringBuilder()
            sb.Append("return confirm('")
            sb.Append(message)
            sb.Append("');")
            ClientScript.RegisterOnSubmitStatement(Me.GetType(), "alert", sb.ToString())

            End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        nuevoeo()
    End Sub
    '
    Sub cargaformulario()
        'numero_oot.Value = "0001" 'asignar por funcion, seguramente lo obtengo de un parametro
        fecha_carga.Value = DateTime.Now.ToString("dd/MM/yyyy")
        If (Session("nivel") <> "0") Then
            conclusiones.Disabled = True
            fecha_cierre.Disabled = True
        End If
   
        'analista.Value = "jarganaraz"
        'superior.Value = "jarganaraz"
  

    End Sub






    Sub nuevoeo()
     
        Dim e As EO
        Dim e1 As EO_EO

        Dim r As Integer
        Dim xfecha As String
        'Dim xfechacarga As String
        Dim lo As Lotes
        lo = New Lotes()
        lo.Lotes(conexion)
        Dim l As logs
        l = New logs()
        l.logs(conexion)
        '
        Try
            '
            '
            '
            e = New EO()
            e.EO(conexion)
            '   
            '

            r = e.AgregarEO(laboratorio.Text, producto.Text, analista.Text, etapa.Text, superior.Text, lote.Text, ensayo.Value, numero_cuaderno.Text, conclusiones.Value, "En Curso", revertirfecha(fecha_carga.Value), TextArea1.Value, TextArea2.Value, TextArea3.Value, TextArea4.Value)
            ' cargo los lotes inv
            Dim I As Integer

            For I = 0 To lotes_inv.Items.Count - 1


                Dim str As String = lotes_inv.Items(I).ToString()

                'genero la relacion entre los lotes y el EO
                lo.Agregarlote_eo(r, lotes_inv.Items(I).Value)



            Next
            e1 = New EO_EO()
            e1.EO_EO(conexion)


        
            For I = 0 To tipo_error.Items.Count - 1

                If (tipo_error.Items(I).Selected = True) Then
                    Dim str As String = tipo_error.Items(I).ToString()

                    'genero la relacion entre el eo y el eolist
                    e1.AgregarEO_EO(r, tipo_error.Items(I).Value)


                End If
            Next

            For I = 0 To desviaciones.Items.Count - 1

                If (desviaciones.Items(I).Selected = True) Then
                    Dim str As String = desviaciones.Items(I).ToString()

                    'genero la relacion entre el eo y el eolist
                    e1.AgregarEO_EO(r, desviaciones.Items(I).Value)


                End If
            Next

            For I = 0 To adecuacion.Items.Count - 1

                If (adecuacion.Items(I).Selected = True) Then
                    Dim str As String = adecuacion.Items(I).ToString()

                    'genero la relacion entre el eo y el eolist
                    e1.AgregarEO_EO(r, adecuacion.Items(I).Value)


                End If
            Next


            For I = 0 To calculo.Items.Count - 1

                If (calculo.Items(I).Selected = True) Then
                    Dim str As String = calculo.Items(I).ToString()

                    'genero la relacion entre el eo y el eolist
                    e1.AgregarEO_EO(r, calculo.Items(I).Value)


                End If
            Next

            'valido que se cargue correctamente el eo
            If (r <> "-1") Then
                'Dim p As Parametros
                'p = New Parametros()
                ' p.Parametros(conexion)



                '                    n = New nombres()
                '                    n.Nombres(conexion)
                'log

                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))

                'una vez definido como es el manejo del user cambiar el segundo parametro, de momento hardcode

                l.Agregarlog2(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "A", xfecha, e.obtenerNroEO(r), producto.Text, lote.Text, revertirfecha(fecha_carga.Value), "En Curso", "0", conclusiones.Value, "")
                '
                Label1.Text = "Se cargó el EO (Número: " & e.obtenerNroEO(r).ToString("d4") & ") con éxito"
                Response.Redirect("consulta_eo.aspx?ID=" + EncryptText(e.obtenerNroEO(r).ToString("d4") & "//3"))
                '    
            Else
                Label2.Text = "Error al cargar el EO"
            End If
            '     
            '
        Catch ex As Exception
            Label2.Text = "Error al cargar el EO"
        End Try
    End Sub

    <System.Web.Services.WebMethod()> _
Public Shared Function GetCurrentTime(ByVal name As String) As String
        Return "Hello " & name & Environment.NewLine & "The Current Time is: " & _
                    DateTime.Now.ToString()
    End Function





    'Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
    '    lotes_inv.Items.Add(lote_i.Value)
    'End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        If (lote_i.Value <> "") Then
            lotes_inv.Items.Add(lote_i.Value)
            lote_i.Value = ""
        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click

        If (lotes_inv.Text <> "") Then
            lotes_inv.Items.Remove(lotes_inv.SelectedItem)

        End If
    End Sub
End Class
