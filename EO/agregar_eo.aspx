﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="agregar_eo.aspx.vb" Inherits="EO_Default" title="Sistema Sinac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <!--  validaciones  --> 
  <style type="text/css">
        body {
            
            font-family: Arial;
            font-size: 10pt;
        }
           input
        {
            width: 250px;
        }
         .btn_input
        {
            width: 150px;
        }
        select
        {
            width: 250px;
        }
    </style>
	<style type="text/css" class="init">
	      	#ctl00_ContentPlaceHolder1_TextArea1, #ctl00_ContentPlaceHolder1_TextArea2, #ctl00_ContentPlaceHolder1_TextArea3, #ctl00_ContentPlaceHolder1_TextArea4, #ctl00_ContentPlaceHolder1_conclusiones       
	      	{
	      		width: 96%;
	      		height: 50px;
	      		}

 	#ctl00_ContentPlaceHolder1_producto     
	      	{
	      		width: 96%;
	      		height: 19px;
	      		}
	      		
	      		#ctl00_ContentPlaceHolder1_conclusiones
	      		{
	      		width: 98%;
	      		height: 104px;
	      		}
	      		#ctl00_ContentPlaceHolder1_lote
	      		{
	      			width:250px;
	      		}
	      		
</style>


  <link href="../css/ValidationEngine.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="../js/jquery/languages/jquery.validationEngine-es.js"
        charset="utf-8"></script>
    <script type="text/javascript" src="../js/jquery/jquery.validationEngine.js"
        charset="utf-8"></script>
       
    <script type="text/javascript">
        $(function () {
            $("#aspnetForm").validationEngine('attach', { promptPosition: "topRight" });
        });
    </script>


    <script type="text/javascript">
        function DateFormat(field, rules, i, options) {
            var regex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
            if (!regex.test(field.val())) {
                return "Please enter date in dd/MM/yyyy format."
            }
        }

function aaa () 
{ 
var TestVar = document.getElementById('<%= lote_i.ClientID %>').value;
var lista = document.getElementById('<%= lotes_inv.ClientID %>');
var combo = lista.options;

if (TestVar != "")
{

lista.options[combo.length]=new Option(TestVar,combo.length);
}
}



    </script>
    <script language="javascript" type="text/javascript">
          var i = 0;
    function addValue() {
    alert("Hola");
       var v = document.getElementById('<%= lote_i.ClientID %>').value;
       var a = document.getElementById('<%= lotes_inv.ClientID %>');
       // get the TextBox Value and assign it into the variable
       AddOpt = new Option(v, v);
       document.form2.a.options[i++] = AddOpt;
       return true;
    }
function Button2_onclick() {

}

</script>
  <!-- fin validaciones  --> 


    <form id="form2" runat="server">
    
<div id="contenedor1">
<div id="contenedor2">
     <div id="titulo_seccion">
         Ingrese los datos del Error Obvio.
 </div>
    <ul>
    <li class="paneles">
    <div>
  
    <span class="tercio">
    <label for="laboratorio">Laboratorio(*)</label>
    <asp:DropDownList ID="laboratorio" runat="server" DataSourceID="SqlDataSource8" 
            DataTextField="nombre_lab" DataValueField="idLab" CssClass="validate[required]>
          <asp:ListItem Selected="True" Value="">Seleccione una...</asp:ListItem>
        </asp:DropDownList>
        
      
    </span>
    
        <span class="tercio">
      <label for="numero_cuaderno">Nº de cuaderno/Folio</label>
    
       <asp:TextBox ID="numero_cuaderno" name="numero_cuaderno" value="" runat="server" maxlength="20"></asp:TextBox>
      
    </span>
 
 
    <span class="tercio">
    <label for="analista">Analista (*)</label>
            <asp:DropDownList ID="analista" runat="server" DataSourceID="SqlDataSource2" 
            DataTextField="Denominacion" DataValueField="Id" CssClass="validate[required] >
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
      
    </span>
 
    <span class="dostercios">
      <label for="superior">Encargado/Supervisor(*)</label>
      <asp:DropDownList ID="superior" runat="server" DataSourceID="SqlDataSource2" 
            DataTextField="Denominacion" DataValueField="Id" CssClass="validate[required]>
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
    </span>
 

  <span class="completo">
   <label for="conclusiones">Conclusiones(*)</label>
         <textarea id="conclusiones" cols="20" name="S1" rows="2" runat="server" class="validate[required] text-input"></textarea>
    
      
    </span>
    
      
    <span class="dostercios">
      <label for="adecuacion">Fallas en el Sistema de Adecuación</label>
    <asp:ListBox runat="server" ID="adecuacion" SelectionMode="Multiple"  DataSourceID="SqlDataSource6" 
            DataTextField="Descripcion" DataValueField="Id">
            

</asp:ListBox> 
    
    
    </span>     

  <span class="dostercios">
      <label for="tipo_error">Desviaciones de Procedimientos o Métodos</label>
    <asp:ListBox runat="server" ID="desviaciones" SelectionMode="Multiple"  DataSourceID="SqlDataSource5" 
            DataTextField="Descripcion" DataValueField="Id">
            

</asp:ListBox> 
    
    
    </span>
      <span class="dostercios">
   <label for="conclusiones">Observaciones (Fallas en el Sistema de Adecuación)</label>
      <textarea id="TextArea1" cols="20" name="S1" rows="2" runat="server"></textarea>
    
    </span>
    <span class="dostercios">
<label for="conclusiones">
        Observaciones (Desv. de Procedimientos o Métodos)</label>
      <textarea id="TextArea2" cols="20" name="S1" rows="2" runat="server"></textarea>
      
    </span>
   
    
    
  </div>
  </li>
   <li class="paneles">
    <div>
    
    <span class="dostercios">
      <label for="producto">Producto (*)</label>
       <asp:DropDownList ID="producto" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="NomProd" DataValueField="IDPdto" CssClass="validate[required]>
          <asp:ListItem Selected="True" Value="">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
      
        <asp:AccessDataSource ID="AccessDataSource1" runat="server">
        </asp:AccessDataSource>
        
      
    </span>
 <span class="tercio">
     <label for="etapa">Etapa</label>
          <asp:DropDownList ID="etapa" runat="server" DataSourceID="SqlDataSource3" 
            DataTextField="nombre_etapa" DataValueField="idEtapa" CssClass="validate[required]>
          <asp:ListItem Selected="True" Value="">Seleccione una...</asp:ListItem>
        </asp:DropDownList>
        
    
  
     
    </span>
 
 
    <span class="dostercios">
     <label for="lote">Lote(*)</label>
     
  <asp:TextBox ID="lote" name="lote" value="" runat="server" maxlength="15" 
            CssClass="validate[required]"></asp:TextBox>
      
     
    </span>
 
   <span class="tercio">
      <label for="ensayo">Ensayo</label>
      <input id="ensayo" name="ensayo" value="" runat="server" maxlength="20"/> 
      
      
    </span>
  
    <span class="dostercios1">
      <label for="adecuacion">Lotes Involucrados</label>
    
    <asp:ListBox runat="server" ID="lotes_inv" SelectionMode="Multiple" DataTextField="nombre" DataValueField="Id">
            

</asp:ListBox> 

    
        
    
    
    </span> 
  <span class="dostercios88">
      <label for="ensayo">Lotes Involucrados</label>
      <input id="lote_i" name="ensayo" value="" runat="server" maxlength="15" placeholder="Escribir Aquí el lote a Agregar" /> 
         <asp:Button ID="Button3" runat="server" Text="<---  Agregar Lote" />
         <asp:Button ID="Button2" runat="server" Text="--->  Quitar Lote" />
      </span>
       <span class="tercio">
      <label for="fecha_carga">
        
        Fecha de Carga</label>
      <input id="fecha_carga" name="fecha_carga" value="" runat="server" 
            readonly="readonly"/>
     
    </span>
      

 
    <span class="dostercios">
    
      <label for="fecha_cierre" runat="server" id="label_fecha_ingreso">Fecha de Cierre</label>
      <input id="fecha_cierre" name="fecha_cierre" value="" runat="server" 
             readonly="readonly"/>
            
       
    </span>
    
       <span class="dostercios">
      <label for="tipo_error">Error de Calculo</label>
    <asp:ListBox runat="server" ID="calculo" SelectionMode="Multiple"  DataSourceID="SqlDataSource7" 
            DataTextField="Descripcion" DataValueField="Id">
            

</asp:ListBox> 
    
    
    </span>
   
         <span class="dostercios">
      <label for="tipo_error">Tipo de Error</label>
    <asp:ListBox runat="server" ID="tipo_error" SelectionMode="Multiple"  DataSourceID="SqlDataSource4" 
            DataTextField="Descripcion" DataValueField="Id">
            

</asp:ListBox> 
    
    
    </span>
        
                 <span class="dostercios">
   <label for="conclusiones">Observaciones (Error de cálculo)</label>
      <textarea id="TextArea3" cols="20" name="S1" rows="2" runat="server"></textarea>
      
    </span>
      <span class="dostercios">
   <label for="conclusiones">Observaciones (Fallas en el Sistema de Adecuación)</label>
        <textarea id="TextArea4" cols="20" name="S1" rows="2" runat="server"></textarea>
      
    </span>
    
 
   
  
  </div>
  </li>
     
  </ul>
    <ul>
        
    <li class="panel_boton">
     <div>
     
    
   
      <span class="boton">
       
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" 
             style="height: 26px" />
   
        
   <input type="button" value="Volver" id="volver" onclick="location.href='consulta_eo.aspx'"></span>
   
   </div>

    </li>
                 <li>
                 </li>
     </ul>
         
  </div>
  </div>
  <br>
  <div id="confirmacion">
  <asp:Label ID="Label1" runat="server"></asp:Label>
  </div>
    <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
   
      <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IDPdto] , [CodPdto] + ' ---- ' + [DesPdto] as NomProd FROM [Productos] where CodPdto <> '' and Activado = '1' order by CodPdto">
                 
        </asp:SqlDataSource>
        
          <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [Id], [Denominacion] FROM [User] order by [Denominacion] asc">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idEtapa], [nombre_etapa] FROM Etapas">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [id], [Descripcion] FROM EO_list where tipo='EO'">
        </asp:SqlDataSource>
          <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [id], [Descripcion] FROM EO_list where tipo='DE'">
        </asp:SqlDataSource>
          <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [id], [Descripcion] FROM EO_list where tipo='FA'">
        </asp:SqlDataSource>
          <asp:SqlDataSource ID="SqlDataSource7" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [id], [Descripcion] FROM EO_list where tipo='EC'">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource8" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idLab], [nombre_lab] FROM laboratorios where deshabilitado='0'">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource9" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idLab], [nombre_lab] FROM laboratorios where deshabilitado='0'">
        </asp:SqlDataSource>
    </form>
</asp:Content>

