﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports System.Net.Mail
Imports iTextSharp.text
Imports iTextSharp.text.pdf


Partial Class Estandares_consulta_estandares
    Inherits System.Web.UI.Page
    Public opcion As String
    Public sql As String

  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(131072, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            'esto es para que tome el item "seleccione..." en el dropdown
            If (Not Page.IsPostBack) Then
                analista.AppendDataBoundItems = True
                producto.AppendDataBoundItems = True
                etapa.AppendDataBoundItems = True
                laboratorio.AppendDataBoundItems = True
                tipoerror.AppendDataBoundItems = True
            End If


            Dim codigo As String
            Dim aux As String
            codigo = DecryptText(Request.QueryString("ID")) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos


            If (codigo <> "") Then

                codigo = Left(DecryptText(Request.QueryString("ID")), 4)

                Label1.Text = "Se cargó el EO (Número: " & codigo & ") con éxito"


            End If
        End If

    End Sub

    Protected Function invertirfecha(ByVal fecha As Object) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 6, 2)
            bDia = Right(Fechatest, 2)
            bAño = Left(Fechatest, 4)
            Fechanueva = bDia & "/" & bMes & "/" & bAño
            Return Fechanueva
        End If


    End Function

   
   

    
    Protected Sub RepeaterDeleteitemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repeater.ItemCommand

        Dim strURL As String
        Dim Codigox As String
        Dim codigo As String
        If (e.CommandName = "Click") Then
            'get the id of the clicked row
            Codigox = Convert.ToString(e.CommandArgument)

                       strURL = "modifica_eo.aspx?ID=" + EncryptText(Codigox)
            Response.Redirect(strURL)
        ElseIf (e.CommandName = "Click_f") Then
            Codigox = Convert.ToString(e.CommandArgument)
            codigo = Codigox
            Dim sql As String
            Dim pagina As Integer = 1
            Dim pagesize1 As String = 510.0F 'ancho de la pagina
            Dim fontsize1 As String = 11 'info inicial
            Dim fontsize2 As String = 13 'titulo cuadros
            Dim fontsize3 As String = 10 'contenido cuadros
            Dim fontsize4 As String = 12 'pie de la pagina
            Dim fontsize5 As String = 10 'firmas
            Dim fontsize9 As String = 10 'observaciones
            Dim observaciones As String
            Dim repe As Integer
            Dim canti As Integer
            Dim contar As Integer
            Dim contenidototal As String 'cantidad de lineas del contenido de las observaciones (incluidos saltos de linea)
            Dim espaciosrestantes As String 'espacios restantes de las observaciones (sin el contenido y los saltos de linea)
            Dim indice As String 'para relleno dinamico de secciones

            Dim document As New Document(PageSize.A4, 88.0F, 88.0F, 10.0F, 10.0F)
            Dim NormalFont As Font = FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)
            Dim es As EO
            Dim p As Parametros
            p = New Parametros()
            p.Parametros(conexion)
            Try
                Using memoryStream As New System.IO.MemoryStream()
                    Dim writer As PdfWriter = PdfWriter.GetInstance(document, memoryStream)
                    Dim phrase As Phrase = Nothing
                    Dim cell As PdfPCell = Nothing
                    Dim table As PdfPTable = Nothing
                    Dim color__1 As Color = Nothing
                    Dim test As Integer = 0

                    Dim ds As DataSet
                    Dim cn As SqlConnection
                    Dim cm As SqlCommand
                    Dim da As SqlDataAdapter
                    Dim aux As String = 0

                    'cargo parametros

                    sops = p.consulta_valor_nombre("sops")
                    fogl = p.consulta_valor_nombre("fogl")
                    titulo = p.consulta_valor_nombre("titulo_fogl")
                    vigencia = p.consulta_valor_nombre("vigencia_fogl")
                    Dim espacios_observaciones As String = 10 ' cantidad de espacios por defecto en el cuadro de observaciones

                    Dim espacios_secciones As String = 8 ' cantidad de espacios por defecto en el cuadro de secciones cuando esta en blanco

                    Dim espacios_dinamicos As String ' va contener la cantidad de espacios a agregar cuando las observaciones ya esten cargadas

                    Dim contador_pagina As String = 0 ' para saber si la seccion entra en la pagina o no, cuenta los items que se van mostrando
                    Dim rellena_final As String = 17 'espacios para completar la primer pagina
                    Dim rellena_final2 As String = 9 'espacios para completar la segunda pagina
                    Dim completa_seccion_dinamico As String = 6 ' cantidad de espacios para completar cuando hay contenido
                    'traigo los valores de la bd
                    sql = "select Descripcion from EO_list inner join EO_EO on EO_EO.idEOlist=EO_list.id where tipo='EO' and EO_EO.idEO='" + codigo + "'"
                    cn = New SqlConnection(conexion3)
                    cn.Open()
                    cm = New SqlCommand()
                    cm.CommandText = sql
                    cm.CommandType = CommandType.Text
                    cm.Connection = cn
                    da = New SqlDataAdapter(cm)
                    ds = New DataSet()
                    da.Fill(ds)


                    document.Open()

                    'solo agrego esto (tabla sin contenido) para separar

                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)

                    'fin separador
                    ' este bucle determina el numero de paginas del documento, registro actual guarda los registros ya impresos y lo compara versus el total que trae la consulta

                    'Tabla de cabecera (3 columnas)
                    table = New PdfPTable(3)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.31F, 0.48F, 0.2F})

                    ' para armar la cabecera

                    es = New EO()
                    es.EO(conexion)



                    'Columna 1
                    phrase = New Phrase()
                    phrase.Add(New Chunk(fogl & vbLf, FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)))

                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    table.AddCell(cell)



                    'Columna2


                    phrase = New Phrase()
                    phrase.Add(New Chunk(titulo, FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                    'columna3
                    cell = ImageCell("~/img/logomonteverde.png", 12.0F, PdfPCell.ALIGN_RIGHT)
                    table.AddCell(cell)
                    document.Add(table)

                    'solo agrego esto (tabla sin contenido) para separar

                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)

                    'solo agrego esto (tabla sin contenido) para separar

                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)

                    'fin separador

                    'Tabla de fecha de vigencia (3 columnas)
                    table = New PdfPTable(3)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.41F, 0.3F, 0.26F})




                    'Columna 1
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Fecha de Vigencia: " & vigencia, FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 1.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    'Columna2  (esta en blanco)


                    phrase = New Phrase()
                    phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                    'columna3  (blanco)
                    phrase = New Phrase()
                    phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'solo agrego esto (tabla sin contenido) para separar

                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)

                    'fin separador

                    'Tabla de contenido(2 columna)


                    table = New PdfPTable(2)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.5F, 0.5F})


                    'Columna 1
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Registro N° " & rellena(es.obtenerNroEO(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Laboratorio de: " & es.obtenerlaboratorio(codigo) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Fecha de investigación: " & es.obtenerfechacarga(codigo) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Analista: " & es.obtenernombreAnalista(es.obteneranalista(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Cuaderno / Folio: " & es.obtenercuadernofolio(codigo) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))

                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 0.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 1.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    phrase = New Phrase()
                    phrase.Add(New Chunk("Ensayo: " & rellena(es.obtenerNroEO(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Lote: " & rellena(es.obtenerNroEO(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Etapa: " & rellena(es.obtenerNroEO(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Metodología: " & rellena(es.obtenerNroEO(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                    phrase.Add(New Chunk("Producto/Materia Prima: " & es.obtenercodpdto(es.obtenerproducto(codigo)) & vbLf & vbLf & vbLf & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 0.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 1.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    document.Add(table)
                    'Tabla de contenido(1 columna)
                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    'table.SetWidths(New Single() {0.41F, 0.3F, 0.26F})

                    'titulo errores obvios
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Errores obvios", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 1.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)
                    document.Add(table)



                    'detalle 

                    'Tabla de contenido errores obvios (2 columnas)
                    table = New PdfPTable(2)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.05F, 0.95F})
                    Dim k As Integer
                    'si la seccion esta vacia genero la tabla con un valor parametrizado para que no se me vayan los margenes
                    If (ds.Tables(0).Rows.Count = 0) Then

                        For k = 0 To espacios_secciones - 1
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 0.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)


                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 0.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)

                        Next

                    End If

                    For k = 0 To ds.Tables(0).Rows.Count - 1



                        'lineas dinamicas de errores obvios
                        phrase = New Phrase()
                        cell = ImageCell("~/img/tilde.png", 2.0F, PdfPCell.ALIGN_RIGHT)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk(ds.Tables(0).Rows(k).ItemArray(0).ToString, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                    Next

                    ' tengo que ver cuanto es lo max q se puede tildar para cada seccion y poder terminar de estimar los espacios
                    ' de momento dejo hardcode suponiendo un max de tres items




                    If (ds.Tables(0).Rows.Count > "0") Then

                        'de acuerdo a la cantidad de items en la seccion sera la cantidad de espacios que agrego
                        Select Case ds.Tables(0).Rows.Count

                            Case "1"
                                indice = completa_seccion_dinamico - (1)
                            Case "2"
                                indice = completa_seccion_dinamico - (3)
                            Case "3"
                                indice = completa_seccion_dinamico - (5)
                            Case "4"
                                indice = completa_seccion_dinamico - (7)
                        End Select


                        For k = 0 To indice
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 0.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)


                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 0.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)

                        Next
                    End If



                    document.Add(table)


                    'observaciones EO
                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True


                    phrase = New Phrase()

                    'codigo para rellenar con espacios el campo de observaciones en funcion del contenido y que no se vayan los margenes
                    observaciones = Trim(es.obtenerobservaciones(codigo, "Obs_EO"))
                    'longitud de la cadena incluido espacios
                    Dim sss As String
                    canti = Len(observaciones)
                    sss = canti
                    If (canti > "0") Then

                        'match ceiling calcula la cantidad de lineas que va ocupar la cadena contenida en funicion de la cantidad de caracteres
                        'repe tendria los espacios que restan sin contar el contenido
                        repe = espacios_observaciones - Math.Ceiling(canti / 108)

                    Else
                        ' si no hay observaciones  agrego los blancos
                        phrase = New Phrase()
                        phrase.Add(New Chunk("Registrar Observaciones: " & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 1.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)

                        For k = 0 To espacios_observaciones
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 1.0F
                            table.AddCell(cell)
                        Next
                        document.Add(table)

                    End If
                    ' hay observaciones
                    If (canti > "0") Then
                        phrase = New Phrase()
                        phrase.Add(New Chunk("Registrar Observaciones: " & vbLf & vbLf & observaciones & espacios_dinamicos, FontFactory.GetFont("Arial", fontsize9, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 1.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)

                        'de acuerdo a la cantidad de lineas en el cuadro observaciones sera la cantidad de espacios que agregue. 
                        'regular de aca una vez q se sepa cant max de caracteres



                        'cuento los saltos de linea en la cadena
                        contar = 0

                        For Each item As Char In observaciones

                            If item <> vbCr Then
                                Continue For
                            End If

                            contar += 1

                        Next


                        contenidototal = Math.Ceiling(canti / 108) + contar
                        espaciosrestantes = repe - contar

                        ' ajustar de aca los espacios segun la cantidad de lineas (por defecto se calculo con 3)
                        Select Case contenidototal
                            Case "1"
                                indice = espaciosrestantes - 2
                            Case "2"
                                indice = espaciosrestantes - 3
                            Case "3"
                                indice = espaciosrestantes - 4

                        End Select

                        For k = 0 To indice

                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 1.0F
                            table.AddCell(cell)



                        Next
                        document.Add(table)
                    End If






                    sql = "select Descripcion from EO_list inner join EO_EO on EO_EO.idEOlist=EO_list.id where tipo='DE' and EO_EO.idEO='" + codigo + "'"
                    cn = New SqlConnection(conexion3)
                    cn.Open()
                    cm = New SqlCommand()
                    cm.CommandText = sql
                    cm.CommandType = CommandType.Text
                    cm.Connection = cn
                    da = New SqlDataAdapter(cm)
                    ds = New DataSet()
                    da.Fill(ds)




                    'Tabla de Desviaciones de proc o metodos(1 columna)
                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True


                    'titulo 
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Desviaciones de Procedimientos o Métodos", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 1.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)
                    document.Add(table)



                    'detalle 

                    'Tabla de contenido Desviaciones de proc o metodos (2 columnas)
                    table = New PdfPTable(2)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.05F, 0.95F})

                    'si la seccion esta vacia genero la tabla con un valor parametrizado para que no se me vayan los margenes
                    If (ds.Tables(0).Rows.Count = 0) Then

                        'el tamaño de la seccion vacia se regula con la variable (espacios_Secciones)
                        For k = 0 To espacios_secciones - 1
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 0.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)


                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 0.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)

                        Next

                    End If


                    For k = 0 To ds.Tables(0).Rows.Count - 1
                        'linea 1
                        phrase = New Phrase()
                        cell = ImageCell("~/img/tilde.png", 2.0F, PdfPCell.ALIGN_RIGHT)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk(ds.Tables(0).Rows(k).ItemArray(0).ToString, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)



                    Next


                    ' tengo que ver cuanto es lo max q se puede tildar para cada seccion y poder terminar de estimar los espacios
                    ' de momento dejo hardcode suponiendo un max de tres items




                    If (ds.Tables(0).Rows.Count > "0") Then

                        'de acuerdo a la cantidad de items en la seccion sera la cantidad de espacios que agrego
                        Select Case ds.Tables(0).Rows.Count

                            Case "1"
                                indice = completa_seccion_dinamico - (1)
                            Case "2"
                                indice = completa_seccion_dinamico - (3)
                            Case "3"
                                indice = completa_seccion_dinamico - (5)
                            Case "4"
                                indice = completa_seccion_dinamico - (7)
                        End Select


                        For k = 0 To indice
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 0.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)


                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 0.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)

                        Next
                    End If




                    document.Add(table)


                    'observaciones 
                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True



                    phrase = New Phrase()

                    'codigo para rellenar con espacios el campo de observaciones en funcion del contenido y que no se vayan los margenes
                    observaciones = Trim(es.obtenerobservaciones(codigo, "Obs_DE"))
                    'longitud de la cadena incluido espacios

                    canti = Len(observaciones)

                    If (canti > "0") Then

                        'match ceiling calcula la cantidad de lineas que va ocupar la cadena contenida en funicion de la cantidad de caracteres
                        'repe tendria los espacios que restan sin contar el contenido
                        repe = espacios_observaciones - Math.Ceiling(canti / 108)

                    Else
                        ' si no hay observaciones  agrego los blancos
                        phrase = New Phrase()
                        phrase.Add(New Chunk("Registrar Observaciones: " & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 1.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)

                        For k = 0 To espacios_observaciones
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            If (k = espacios_observaciones) Then
                                cell.BorderWidthBottom = 1.0F
                            Else
                                cell.BorderWidthBottom = 0.0F
                            End If

                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 1.0F
                            table.AddCell(cell)
                        Next
                        document.Add(table)

                    End If
                    ' hay observaciones
                    If (canti > "0") Then
                        phrase = New Phrase()
                        phrase.Add(New Chunk("Registrar Observaciones: " & vbLf & vbLf & observaciones & espacios_dinamicos, FontFactory.GetFont("Arial", fontsize9, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 1.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)

                        'de acuerdo a la cantidad de lineas en el cuadro observaciones sera la cantidad de espacios que agregue. 
                        'regular de aca una vez q se sepa cant max de caracteres



                        'cuento los saltos de linea en la cadena
                        contar = 0

                        For Each item As Char In observaciones

                            If item <> vbCr Then
                                Continue For
                            End If

                            contar += 1

                        Next


                        contenidototal = Math.Ceiling(canti / 108) + contar
                        espaciosrestantes = repe - contar

                        ' ajustar de aca los espacios segun la cantidad de lineas (por defecto se calculo con 3)
                        Select Case contenidototal
                            Case "1"
                                indice = espaciosrestantes - 2
                            Case "2"
                                indice = espaciosrestantes - 3
                            Case "3"
                                indice = espaciosrestantes - 4

                        End Select

                        For k = 0 To indice

                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            If (k = indice) Then
                                cell.BorderWidthBottom = 1.0F
                            Else
                                cell.BorderWidthBottom = 0.0F
                            End If
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 1.0F
                            table.AddCell(cell)



                        Next
                        document.Add(table)
                    End If






                    'pie de pagina


                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    ' relleno con celdas vacias en funcion del contenido previo ( lo calculo con un contador por registro mostrado)
                    Dim i As Integer
                    For i = 1 To rellena_final
                        phrase = New Phrase()
                        phrase.Add(New Chunk(vbLf, FontFactory.GetFont("Arial", 10, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                        cell.VerticalAlignment = PdfCell.ALIGN_TOP
                        table.AddCell(cell)

                    Next


                    phrase = New Phrase()
                    phrase.Add(New Chunk(sops & "                             Fecha de emisión: " & String.Format("{0:dd/MM/yyyy - H:mm:ss}", Date.Now()) & "     Página 1 de 2", FontFactory.GetFont("Arial", fontsize4, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'agrego una nueva pagina

                    document.NewPage()
                    pagina = pagina + 1



                    'solo agrego esto (tabla sin contenido) para separar

                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.33F})
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    phrase = New Phrase()
                    phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)

                    'fin separador

                    'Tabla de cabecera (3 columnas)
                    table = New PdfPTable(3)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.31F, 0.48F, 0.2F})



                    'Columna 1
                    phrase = New Phrase()
                    phrase.Add(New Chunk(fogl & vbLf, FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)))

                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    table.AddCell(cell)



                    'Columna2


                    phrase = New Phrase()
                    phrase.Add(New Chunk(titulo, FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)))

                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                    'columna3
                    cell = ImageCell("~/img/logomonteverde.png", 12.0F, PdfPCell.ALIGN_RIGHT)
                    table.AddCell(cell)
                    document.Add(table)


                    sql = "select Descripcion from EO_list inner join EO_EO on EO_EO.idEOlist=EO_list.id where tipo='FA' and EO_EO.idEO='" + codigo + "'"
                    cn = New SqlConnection(conexion3)
                    cn.Open()
                    cm = New SqlCommand()
                    cm.CommandText = sql
                    cm.CommandType = CommandType.Text
                    cm.Connection = cn
                    da = New SqlDataAdapter(cm)
                    ds = New DataSet()
                    da.Fill(ds)

                    'Tabla de Fallas en el Sistema de Adecuación(1 columna)
                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True


                    'espacio en blanco por margen
                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf, FontFactory.GetFont("Arial", fontsize3, Font.BOLD, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    table.AddCell(cell)
                    'titulo 
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Fallas en el Sistema de Adecuación", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 1.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)
                    document.Add(table)



                    'detalle 


                    table = New PdfPTable(2)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.05F, 0.95F})

                    'si la seccion esta vacia genero la tabla con un valor parametrizado para que no se me vayan los margenes
                    If (ds.Tables(0).Rows.Count = 0) Then

                        For k = 0 To espacios_secciones - 1
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 0.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)


                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 0.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)

                        Next

                    End If



                    For k = 0 To ds.Tables(0).Rows.Count - 1


                        'linea 1
                        phrase = New Phrase()
                        cell = ImageCell("~/img/tilde.png", 2.0F, PdfPCell.ALIGN_RIGHT)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk(ds.Tables(0).Rows(k).ItemArray(0).ToString, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)

                    Next

                    ' tengo que ver cuanto es lo max q se puede tildar para cada seccion y poder terminar de estimar los espacios
                    ' de momento dejo hardcode suponiendo un max de tres items




                    If (ds.Tables(0).Rows.Count > "0") Then

                        'de acuerdo a la cantidad de items en la seccion sera la cantidad de espacios que agrego
                        Select Case ds.Tables(0).Rows.Count

                            Case "1"
                                indice = completa_seccion_dinamico - (1)
                            Case "2"
                                indice = completa_seccion_dinamico - (3)
                            Case "3"
                                indice = completa_seccion_dinamico - (5)
                            Case "4"
                                indice = completa_seccion_dinamico - (7)
                        End Select


                        For k = 0 To indice
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 0.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)


                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 0.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)

                        Next
                    End If



                    document.Add(table)



                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True



                    phrase = New Phrase()

                    'codigo para rellenar con espacios el campo de observaciones en funcion del contenido y que no se vayan los margenes
                    observaciones = Trim(es.obtenerobservaciones(codigo, "Obs_FA"))
                    'longitud de la cadena incluido espacios

                    canti = Len(observaciones)

                    If (canti > "0") Then

                        'match ceiling calcula la cantidad de lineas que va ocupar la cadena contenida en funicion de la cantidad de caracteres
                        'repe tendria los espacios que restan sin contar el contenido
                        repe = espacios_observaciones - Math.Ceiling(canti / 108)

                    Else
                        ' si no hay observaciones  agrego los blancos
                        phrase = New Phrase()
                        phrase.Add(New Chunk("Registrar Observaciones: " & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 1.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)

                        For k = 0 To espacios_observaciones
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 1.0F
                            table.AddCell(cell)
                        Next
                        document.Add(table)

                    End If
                    ' hay observaciones
                    If (canti > "0") Then
                        phrase = New Phrase()
                        phrase.Add(New Chunk("Registrar Observaciones: " & vbLf & vbLf & observaciones & espacios_dinamicos, FontFactory.GetFont("Arial", fontsize9, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 1.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)

                        'de acuerdo a la cantidad de lineas en el cuadro observaciones sera la cantidad de espacios que agregue. 
                        'regular de aca una vez q se sepa cant max de caracteres



                        'cuento los saltos de linea en la cadena
                        contar = 0

                        For Each item As Char In observaciones

                            If item <> vbCr Then
                                Continue For
                            End If

                            contar += 1

                        Next


                        contenidototal = Math.Ceiling(canti / 108) + contar
                        espaciosrestantes = repe - contar

                        ' ajustar de aca los espacios segun la cantidad de lineas (por defecto se calculo con 3)
                        Select Case contenidototal
                            Case "1"
                                indice = espaciosrestantes - 2
                            Case "2"
                                indice = espaciosrestantes - 3
                            Case "3"
                                indice = espaciosrestantes - 4

                        End Select

                        For k = 0 To indice

                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 1.0F
                            table.AddCell(cell)



                        Next
                        document.Add(table)
                    End If


                    'Tabla de Desviaciones de proc o metodos(1 columna)
                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True

                    'titulo error de calculo

                    phrase = New Phrase()
                    phrase.Add(New Chunk("Error de Cálculo", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 1.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)
                    document.Add(table)



                    sql = "select Descripcion from EO_list inner join EO_EO on EO_EO.idEOlist=EO_list.id where tipo='EC' and EO_EO.idEO='" + codigo + "'"
                    cn = New SqlConnection(conexion3)
                    cn.Open()
                    cm = New SqlCommand()
                    cm.CommandText = sql
                    cm.CommandType = CommandType.Text
                    cm.Connection = cn
                    da = New SqlDataAdapter(cm)
                    ds = New DataSet()
                    da.Fill(ds)

                    'detalle 

                    'Tabla de contenido errores obvios (2 columnas)
                    table = New PdfPTable(2)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.05F, 0.95F})

                    'si la seccion esta vacia genero la tabla con un valor parametrizado para que no se me vayan los margenes
                    If (ds.Tables(0).Rows.Count = 0) Then

                        For k = 0 To espacios_secciones - 1
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 0.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)


                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 0.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)

                        Next

                    End If



                    For k = 0 To ds.Tables(0).Rows.Count - 1

                        'linea 1
                        phrase = New Phrase()
                        cell = ImageCell("~/img/tilde.png", 2.0F, PdfPCell.ALIGN_RIGHT)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk(ds.Tables(0).Rows(k).ItemArray(0).ToString, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                    Next

                    ' tengo que ver cuanto es lo max q se puede tildar para cada seccion y poder terminar de estimar los espacios
                    ' de momento dejo hardcode suponiendo un max de tres items




                    If (ds.Tables(0).Rows.Count > "0") Then

                        'de acuerdo a la cantidad de items en la seccion sera la cantidad de espacios que agrego
                        Select Case ds.Tables(0).Rows.Count

                            Case "1"
                                indice = completa_seccion_dinamico - (1)
                            Case "2"
                                indice = completa_seccion_dinamico - (3)
                            Case "3"
                                indice = completa_seccion_dinamico - (5)
                            Case "4"
                                indice = completa_seccion_dinamico - (7)
                        End Select


                        For k = 0 To indice
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 0.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)


                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 0.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            cell.BorderWidthBottom = 0.0F
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 5.0F
                            table.AddCell(cell)

                        Next
                    End If




                    document.Add(table)


                    'observaciones EO
                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True


                    phrase = New Phrase()

                    'codigo para rellenar con espacios el campo de observaciones en funcion del contenido y que no se vayan los margenes
                    observaciones = Trim(es.obtenerobservaciones(codigo, "Obs_EC"))
                    'longitud de la cadena incluido espacios

                    canti = Len(observaciones)

                    If (canti > "0") Then

                        'match ceiling calcula la cantidad de lineas que va ocupar la cadena contenida en funicion de la cantidad de caracteres
                        'repe tendria los espacios que restan sin contar el contenido
                        repe = espacios_observaciones - Math.Ceiling(canti / 108)

                    Else
                        ' si no hay observaciones  agrego los blancos
                        phrase = New Phrase()
                        phrase.Add(New Chunk("Registrar Observaciones: " & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 1.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)

                        For k = 0 To espacios_observaciones
                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            If (k = espacios_observaciones) Then
                                cell.BorderWidthBottom = 1.0F
                            Else
                                cell.BorderWidthBottom = 0.0F
                            End If

                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 1.0F
                            table.AddCell(cell)
                        Next
                        document.Add(table)

                    End If
                    ' hay observaciones
                    If (canti > "0") Then
                        phrase = New Phrase()
                        phrase.Add(New Chunk("Registrar Observaciones: " & vbLf & vbLf & observaciones & espacios_dinamicos, FontFactory.GetFont("Arial", fontsize9, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 1.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)

                        'de acuerdo a la cantidad de lineas en el cuadro observaciones sera la cantidad de espacios que agregue. 
                        'regular de aca una vez q se sepa cant max de caracteres



                        'cuento los saltos de linea en la cadena
                        contar = 0

                        For Each item As Char In observaciones

                            If item <> vbCr Then
                                Continue For
                            End If

                            contar += 1

                        Next


                        contenidototal = Math.Ceiling(canti / 108) + contar
                        espaciosrestantes = repe - contar

                        ' ajustar de aca los espacios segun la cantidad de lineas (por defecto se calculo con 3)
                        Select Case contenidototal
                            Case "1"
                                indice = espaciosrestantes - 2
                            Case "2"
                                indice = espaciosrestantes - 3
                            Case "3"
                                indice = espaciosrestantes - 4

                        End Select

                        For k = 0 To indice

                            phrase = New Phrase()
                            phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                            color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                            cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                            cell.BorderColorLeft = color__1
                            cell.BorderColorRight = color__1
                            cell.BorderColorTop = color__1
                            cell.BorderColorBottom = color__1
                            cell.BorderWidthLeft = 1.0F
                            cell.BorderWidthRight = 1.0F
                            cell.BorderWidthTop = 0.0F
                            If (k = indice) Then
                                cell.BorderWidthBottom = 1.0F
                            Else
                                cell.BorderWidthBottom = 0.0F
                            End If
                            cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                            cell.PaddingBottom = 5.0F
                            cell.PaddingLeft = 5.0F
                            cell.PaddingTop = 1.0F
                            table.AddCell(cell)



                        Next
                        document.Add(table)
                    End If




                    'Tabla de Desviaciones de proc o metodos(1 columna)
                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True

                    'titulo errores obvios
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Conclusiones - Medidas Correctivas / Preventivas", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 1.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)
                    phrase = New Phrase()
                    phrase.Add(New Chunk(es.obtenerobservaciones(codigo, "Obs_EO") & vbLf & vbLf & vbLf & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 1.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    document.Add(table)

                    'Tabla de cabecera (3 columnas)
                    table = New PdfPTable(3)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.2F, 0.35F, 0.15F})


                    'Columna 1
                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf & vbLf & "Analista: ", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    table.AddCell(cell)



                    'Columna2


                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf & vbLf & "Nombre: " & es.obtenernombreAnalista(es.obteneranalista(codigo)), FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                    'columna3
                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf & vbLf & "Fecha: " & String.Format("{0:dd/MM/yyyy}", Date.Now()), FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'linea firma

                    'Tabla de cabecera (3 columnas)
                    table = New PdfPTable(3)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.2F, 0.35F, 0.15F})


                    'Columna 1
                    phrase = New Phrase()
                    phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    table.AddCell(cell)



                    'Columna2


                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf & vbLf & "Firma: _________________________ ", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                    'columna3
                    phrase = New Phrase()
                    phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    table = New PdfPTable(3)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.2F, 0.35F, 0.15F})


                    'Columna 1
                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf & vbLf & vbLf & "Supervisor o Encargado: ", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    table.AddCell(cell)



                    'Columna2


                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf & vbLf & vbLf & "Nombre: " & es.obtenernombreAnalista(es.obtenersuperior(codigo)), FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                    'columna3
                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf & vbLf & vbLf & "Fecha: " & String.Format("{0:dd/MM/yyyy}", Date.Now()), FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)


                    'linea firma

                    'Tabla de cabecera (3 columnas)
                    table = New PdfPTable(3)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    table.SetWidths(New Single() {0.2F, 0.4F, 0.12F})


                    'Columna 1
                    phrase = New Phrase()
                    phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    table.AddCell(cell)



                    'Columna2


                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf & vbLf & " Firma: _________________________ ", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                    'columna3
                    phrase = New Phrase()
                    phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)



                    'pie de pagina


                    table = New PdfPTable(1)
                    table.TotalWidth = pagesize1
                    table.LockedWidth = True
                    ' relleno con celdas vacias en funcion del contenido previo ( lo calculo con un contador por registro mostrado)
                    For i = 1 To rellena_final2
                        phrase = New Phrase()
                        phrase.Add(New Chunk(vbLf, FontFactory.GetFont("Arial", 10, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                        cell.VerticalAlignment = PdfCell.ALIGN_TOP
                        table.AddCell(cell)

                    Next


                    phrase = New Phrase()
                    phrase.Add(New Chunk(sops & "                             Fecha de emisión: " & String.Format("{0:dd/MM/yyyy - H:mm:ss}", Date.Now()) & "     Página 1 de 2", FontFactory.GetFont("Arial", fontsize4, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)
                    document.Add(table)
                    document.Close()
                    Dim bytes As Byte() = memoryStream.ToArray()
                    memoryStream.Close()
                    'cn.Close()
                    Response.Clear()
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("Content-Disposition", "attachment; filename=FOGL-MAR-2020.pdf")
                    Response.ContentType = "application/pdf"
                    Response.Buffer = True
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.BinaryWrite(bytes)
                    Response.[End]()
                    Response.Close()
                End Using
            Catch ex As Exception
                If (ex.Message = "No hay ninguna fila en la posición 0.") Then
                    Label2.Text = "No hay consumos registrados para este estandar"
                Else
                    Label2.Text = "Error al generar el informe"
                End If
            End Try
           




        End If

    End Sub
    Protected Function Vacio(ByVal nombre As Object) As Object
        If (Not nombre.Equals(DBNull.Value)) Then
            Return nombre
        Else
            Return "N/A"

        End If



    End Function
    Protected Function N_A_Blanc(ByVal nombre As String) As String
        If (nombre = "") Then
            Return "N/A"
        Else
            Return nombre

        End If



    End Function


    Protected Function fecha(ByVal fecha_1 As Date) As String
        Dim fechax As Date
        fechax = fecha_1
        Return fechax


    End Function
    Protected Function usuario(ByVal nombre As String) As String
        Dim l As User
        l = New User()
        l.User(conexion)

        Return l.Consultarusuario(nombre)

    End Function

    Protected Function etapa_nom(ByVal nombre As String) As String
        Dim e As Etapa
        e = New Etapa()
        e.Etapa(conexion)
        If (nombre = "0") Then
            Return "N/A"
        Else
            Return e.consulta_nombre_etapa(nombre)
        End If



    End Function
    Protected Function laboratorio_nom(ByVal nombre As String) As String

        Dim es As EO
        es = New EO()
        es.EO(conexion)
        If (nombre = "0") Then
            Return "N/A"
        Else
            Return es.obtener_laboratorio(nombre)
        End If



    End Function

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("agregar_eo.aspx")
    End Sub

    

  
   

    Public Sub EnvioMail()

        Dim message As New MailMessage

        Dim smtp As New SmtpClient

        message.From = New MailAddress("mtvsj@raffo.com.ar")

        message.To.Add("jarganaraz@raffo.com.ar")

        message.Body = "EL CUERPO O EL CONTENIDO DEL MENSAJE"

        message.Subject = "Estandares Proximos a Vencer"

        message.Priority = MailPriority.Normal

        smtp.EnableSsl = False

        smtp.Port = "587"

        smtp.Host = "192.168.40.33"

        smtp.Credentials = New Net.NetworkCredential("mtvsj", "mtvsj1234")

        smtp.Send(message)

    End Sub
   
   
   
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Label1.Text = ""
        buscareo(sql)
    End Sub

    Sub buscareo(ByVal sql As String)
        Dim u As EO
        Dim indice As Integer = 0
        Dim checks As Boolean = False
        'Dim ult As String
        'Dim meses_max As String
        Try
            u = New EO() ' nuevo objeto instancia
            u.EO(conexion) 'invoco el constructor y paso parametros de conexion

            If tipoerror.Text = "0" Then
                sql = "Select * from EO  inner join Productos on Productos.IDPdto=EO.idPdto inner join [User] on [User].Id=EO.Analista"
            Else

                sql = "Select * from EO  inner join Productos on Productos.IDPdto=EO.idPdto inner join EO_EO on EO_EO.idEO = EO.idEO inner join [User] on [User].Id=EO.Analista"
            End If
            If CheckBox1.Checked = True Then
                sql = sql & " and (EO.Estado = 'En Curso' "
                checks = True
            End If
            If (CheckBox2.Checked = True) And (checks = False) Then
                sql = sql & " and EO.Estado = 'Concluido' "
                checks = True
            ElseIf (CheckBox2.Checked = True) Then
                sql = sql & " or EO.Estado = 'Concluido') "
            ElseIf (checks = True) Then
                sql = sql & ")"
            End If



            If IDEO.Text <> "" Then
                Dim nueva As String = ""
                Dim a As String
                Dim b As String
                Dim aux As String = "0"
                a = IDEO.Text.Length

                'debo quitar los ceros ya que en la base se guardan sin ellos
                If IDEO.Text.Length > 1 Then
                    For indice = 0 To IDEO.Text.Length - 1
                        b = IDEO.Text.Substring(indice, 1)
                        If ((IDEO.Text.Substring(indice, 1) = 0) And (aux = "0")) Then
                            'nueva = IDEO.Text.Substring(indice + 1, IDEO.Text.Length - (1 + indice))
                        Else
                            nueva = nueva & IDEO.Text.Substring(indice, 1)
                            'seteo esta bandera ya que todo 0 posterior a un numero ya no es un cero que deberia 
                            aux = 1
                        End If
                    Next
                Else
                    'si cargo un numero de 1 digito asigno directo ya que no habrian ceros
                    nueva = IDEO.Text

                End If

                sql = sql & " and EO.NroEO like '%" & nueva & "%' "
            End If

            If analista.Text <> "0" Then
                sql = sql & " and EO.Analista = '" & analista.Text & "' "
            End If

            If producto.Text <> "0" Then
                sql = sql & " and EO.idPdto = '" & producto.Text & "' "
            End If

            'If etapa.Text <> "0" Then
            ' sql = sql & " and EO.idEtapa = '" & etapa.Text & "' "
            ' End If

            If tipoerror.Text <> "0" Then
                sql = sql & " and EO_EO.idEOlist like '%" & tipoerror.Text & "%' "
            End If

            If ensayo.Text <> "" Then
                sql = sql & " and EO.Ensayo like '%" & ensayo.Text & "%' "
            End If

            If conclusiones.Text <> "" Then
                sql = sql & " and EO.Conclusiones like '%" & conclusiones.Text & "%' "
            End If

            If laboratorio.Text <> "0" Then

                sql = sql & " and EO.Laboratorio like '%" & u.obtener_laboratorio(laboratorio.Text) & "%' "
            End If

            If etapa.Text <> "0" Then
                sql = sql & " and EO.Etapa = '" & etapa.Text & "' "
            End If

            repeater.DataSource = u.ConsultaEO(sql)
            repeater.DataBind()

        Catch ex As Exception
            Label2.Text = "Error al buscar el EO"

        End Try
    End Sub

    '//Función para Desencriptar
    Public Function decrypt(ByVal cadena As String, ByVal key As String) As String
        Dim result As String
        Dim crter, keychar, cadenaM As String
        Dim resu As String
        Dim i As Integer
        Dim X As Integer

        result = ""
        cadenaM = cadena

        For i = 1 To (Len(cadenaM)) Step 1
            crter = Mid(cadenaM, i, 1)
            X = i / (Len(key) - 1)
            If X < 1 Then
                X = 1
            End If
            keychar = Mid(key, X, 1)
            If keychar = "" Then
                keychar = "a"
            End If

            crter = Chr(Asc(crter) - Asc(keychar))
            result = result + crter
        Next i

        Return result
    End Function

    'funciones para fogl (son las mismas que hay en modificar eo) arreglar despues

    Private Shared Sub DrawLine(ByVal writer As PdfWriter, ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single, ByVal color As Color)
        Dim contentByte As PdfContentByte = writer.DirectContent
        contentByte.SetColorStroke(color)
        contentByte.MoveTo(x1, y1)
        contentByte.LineTo(x2, y2)
        contentByte.Stroke()
    End Sub
    Private Shared Function PhraseCell(ByVal phrase As Phrase, ByVal align As Integer) As PdfPCell
        Dim cell As New PdfPCell(phrase)
        cell.BorderColor = Color.WHITE
        cell.VerticalAlignment = PdfCell.ALIGN_TOP
        cell.HorizontalAlignment = align
        cell.PaddingBottom = 2.0F
        cell.PaddingTop = 0.0F
        Return cell
    End Function
    Private Shared Function ImageCell(ByVal path As String, ByVal scale As Single, ByVal align As Integer) As PdfPCell
        Dim image As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path))
        image.ScalePercent(scale)
        Dim cell As New PdfPCell(image)
        cell.BorderColor = Color.WHITE
        cell.VerticalAlignment = PdfCell.ALIGN_TOP
        cell.HorizontalAlignment = align
        cell.PaddingBottom = 0.0F
        cell.PaddingTop = 0.0F
        Return cell
    End Function
    Private Function GetData(ByVal query As String) As DataTable
        Dim conString As String = ConfigurationManager.ConnectionStrings("EstandaresConnectionString").ConnectionString
        Dim cmd As New SqlCommand(query)
        Using con As New SqlConnection(conString)
            Using sda As New SqlDataAdapter()
                cmd.Connection = con

                sda.SelectCommand = cmd
                Using dt As New DataTable()
                    sda.Fill(dt)

                    Return dt
                End Using
            End Using
        End Using
    End Function
    Protected Function rellena(ByVal nombre As Integer) As String


        Return nombre.ToString("d4")


    End Function


End Class
