﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="consulta_eo.aspx.vb" Inherits="Estandares_consulta_estandares" title="Sistema Sinac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <link rel="stylesheet" type="text/css" href="../js/DataTables/media/css/jquery.dataTables1.css">
    <link rel="stylesheet" type="text/css" href="../js/DataTables/extensions/TableTools/css/dataTables.tableTools.css">
    <link rel="stylesheet" href="../js/datatable/media/css/demo_table.css"type="text/css" />

	<style type="text/css" class="init">
	      	    input {
        }

	</style>
	<script type="text/javascript" language="javascript" src="../js/DataTables/media/js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="../js/DataTables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="../js/DataTables/extensions/TableTools/js/dataTables.tableTools.js"></script>
	<script type="text/javascript" language="javascript" src="../../../examples/resources/syntax/shCore.js"></script>
	<script type="text/javascript" language="javascript" src="../../../examples/resources/demo.js"></script>




        <script type="text/javascript" charset="utf-8" src="../js/datatable/ColVis/media/js/ColVis.js"></script>
    <script type="text/javascript" charset="utf-8"></script>
<script language="javascript" type="text/javascript">

var asInitVals = new Array();
     $(document).ready(function () {
     
     var  oTable = $('#tbl').dataTable( {
       "dom": 'T<"clear">lfrtip',
      
           "oTableTools": {
                                                                "aButtons": [
                                                                                "xls",
                                                                                
                                                                            ]
                                                                },
    
    
            "sScrollY": "100%",
            //'sPaginationType': 'full_numbers',
            //'iDisplayLength': 5,
            	"oColVis": {
		    		
					"activate": "mouseover",
						
						"aiExclude": [ 5 ]
						
				},
				"oLanguage": {
					"sProcessing":     "Procesando...",
				    "sLengthMenu":     "Mostrar _MENU_ registros",
				    "sZeroRecords":    "No se encontraron resultados",
				    "sEmptyTable":     "Ningún dato disponible en esta tabla",
				    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				    "sInfoPostFix":    "",
				    "sSearch":         "Buscar:",
				    "sUrl":            "",
				    "sInfoThousands":  ",",
				    
				    "oPaginate": {
				        "sFirst":    "Primero",
				        "sLast":     "Último",
				        "sNext":     "Siguiente",
				        "sPrevious": "Anterior"
				    },
				    "sLoadingRecords": "Cargando...",
				    "fnInfoCallback": null,
				    "oAria": {
				        "sSortAscending":  ": Activar para ordernar la columna de manera ascendente",
				        "sSortDescending": ": Activar para ordernar la columna de manera descendente"
				    }
				    },
            "bPaginate": true,
            "bProcessing": true,
            "bServerSide": false,
            "bSortCellsTop": true,
            
            
     
        });
        
        
        /* Add the events etc before DataTables hides a column */
			$("thead input").keyup( function () {
				/* Filter on the column (the index) of this element */
				oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex( 
					oTable.fnSettings(), $("thead input").index(this) ) );
			} );
			
			/*
			 * Support functions to provide a little bit of 'user friendlyness' to the textboxes
			 */
			$("thead input").each( function (i) {
				this.initVal = this.value;
			} );
			
			$("thead input").focus( function () {
				if ( this.className == "search_init" )
				{
					this.className = "";
					this.value = "";
				}
			} );
			
			$("thead input").blur( function (i) {
				if ( this.value == "" )
				{
					this.className = "search_init";
					this.value = this.initVal;
				}
			} );
        
    });




</script>
 <style type="text/css" class="init">
	
#ctl00_ContentPlaceHolder1_producto
{width: 12%;
	}
</style>
 <div id="titulo_seccion">
     Administración de EO
 </div>
      <div id="titulo_seccion">
     Filtros de Búsqueda   
    
 </div>
       
              
               <div align= "center" >
          <span >
        <label runat="server" id="label_IDEO" for="cantidad_pedida"  class=":required :integer" >ID EO</label>
          
    <asp:TextBox ID="IDEO" runat="server" Width="53px"></asp:TextBox>
    
    </span>
      <span >
      
      <label for="analista">Analista</label>
             <asp:DropDownList ID="analista" runat="server" DataSourceID="SqlDataSource2" 
            DataTextField="Denominacion" DataValueField="Id" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
    </span>
    &nbsp; &nbsp; &nbsp;
        <span >
      
      <label for="producto">Producto</label>
      <asp:DropDownList ID="producto" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="NomProd" DataValueField="IDPdto" CssClass="validate[required]>
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
    </span>
    &nbsp; &nbsp; &nbsp;
        <span >
      
      <label for="tipoerror">Tipo de Error</label>
       <asp:DropDownList ID="tipoerror" runat="server" DataSourceID="SqlDataSource4" 
            DataTextField="Descripcion" DataValueField="id" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
    </span>
    &nbsp; &nbsp; &nbsp;
    <span >
      
      <label for="ensayo">Ensayo</label>
    <asp:TextBox ID="ensayo" runat="server" Width="80px"></asp:TextBox>
    </span>
    &nbsp; &nbsp; &nbsp;
    <span >
      
      <label for="etapa">Etapa</label>
      <asp:DropDownList ID="etapa" runat="server" DataSourceID="SqlDataSource3" 
            DataTextField="nombre_etapa" DataValueField="idEtapa" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione una...</asp:ListItem>
        </asp:DropDownList>
  
    </span>
    
    &nbsp; &nbsp; &nbsp;
 
                 
    
    </div>
    <div id="separador">
    </div>
    <div align="center">
            <span >
      
      
                   
      <label for="estado"> Estado: </label>
                   <asp:CheckBox ID="CheckBox1" runat="server" Text="En Curso" />
                   <asp:CheckBox ID="CheckBox2" runat="server" Text="Concluido"/>
                   <asp:CheckBox ID="CheckBox3" runat="server" Text="Anulado"/>
                   
    </span>
            &nbsp; &nbsp; &nbsp;
             <span >
      
      <label for="conclusiones">Conclusiones</label>
    <asp:TextBox ID="conclusiones" runat="server" Width="200px"></asp:TextBox>
   
    </span>&nbsp; &nbsp; &nbsp;
        <span >
        
      
      <label for="laboratorio">Laboratorio</label>
    <asp:DropDownList ID="laboratorio" runat="server" DataSourceID="SqlDataSource5" 
            DataTextField="nombre_lab" DataValueField="idLab" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione una...</asp:ListItem>
        </asp:DropDownList>
    </span>
    </div>
   <br>
    <div id="Div1" style="width:100%" runat="server">
        
        
        
        <div align= "center" >
        <asp:Button ID="Button1" runat="server" Text=" Buscar "  />
          <asp:Button ID="Button2" 
            runat="server" Text=" + Agregar EO " />
            
         
              <asp:Label ID="label_stock" runat="server"></asp:Label>
                  &nbsp;
&nbsp;
</div>
  

<asp:Repeater ID="repeater" runat="server" OnItemCommand ="RepeaterDeleteitemcommand">
            <HeaderTemplate>
                <table id="tbl" cellpadding="1" cellspacing="0" 
                    border="0" class="display" >
                  <thead id="aaa">
                    <tr>
                        <th></th>
                        <th>NroEO</th>
                        <th>Fecha Carga</th>
                        <th>Estado</th>
                        <th>Laboratorio</th>
                        <th>Analista</th>
                        <th>Etapa</th>
                        <th>Producto</th>
                        <th>Superior</th>
                        <th>Lote</th>
                        <th>Ensayo</th>
                        <th>Cuaderno/Folio</th>
                        <th>Tipo de Error</th>
                        <th>Conclusiones</th>
                        
                        <th>Fecha de Cierre</th>
                         </tr>
             <tr>
			<td align="center"><input type="hidden" size="14" name="search_engine" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_browser" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="7" name="search_platform" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="5" name="search_version" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_grade" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_grade" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_version1" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_browser1" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_platform1" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_version2" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_grade2" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_version3" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_engine1" value="" class="search_init" /></td>
			<td align="center"><input type="text" size="8" name="search_browser2" value="" class="search_init" /></td>
				<td align="center"><input type="text" size="8" name="search_version12" value="" class="search_init" /></td>
			</tr>
                  </thead>
                <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                  <td align="center"><a><asp:Button ID="Button1" CommandName="Click" Text="Modificar" runat="server" CommandArgument='<%# Eval("idEO") %>' /></a></td>
                                    <td align="center"><%#rellena(Eval("NroEO"))%></td>
                  <td align="center"><%#fecha(Eval("FeCarga"))%></td>
                  <td align="center"><%#Eval("Estado")%></td>
                  <td align="center"><%#laboratorio_nom(Eval("Laboratorio"))%></td>
                  <td align="center"><%#Eval("Denominacion")%></td>
                  <td align="center"><%#etapa_nom(Eval("Etapa"))%></td>
                  <td align="center"><%#Eval("CodPdto")%></td>
                  <td align="center"><%#usuario(Eval("Supervisor"))%></td>
                  <td align="center"><%#Eval("Lote")%></td>
                  <td align="center"><%#N_A_Blanc(Eval("Ensayo"))%></td>
                  <td align="center"><%#N_A_Blanc(Eval("CuadernoFolio"))%></td>
                  <td align="center"><%#Vacio(Eval("TipoError"))%></td>
                  <td align="center"><%#Vacio(Eval("Conclusiones"))%></td>
                  <td align="center"><%#Vacio(Eval("FeCierre")) %></td>
                               

                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
 
</body>
      <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
      <div id="confirmacion">
  <asp:Label ID="Label1" runat="server"></asp:Label>
  </div>
       <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IDPdto] , [CodPdto] + ' ---- ' + [DesPdto] as NomProd FROM [Productos] where CodPdto <> '' and Activado = '1' order by CodPdto">
                 
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [Id], [Denominacion] FROM [User] order by Denominacion asc">
        </asp:SqlDataSource>
          <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idEtapa], [nombre_etapa] FROM Etapas where deshabilitado='0'">
        </asp:SqlDataSource>
              <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [id], [Descripcion] FROM EO_list order by Descripcion asc">
        </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idLab], [nombre_lab] FROM laboratorios where deshabilitado='0'">
        </asp:SqlDataSource>
    </form>
      </asp:Content>

