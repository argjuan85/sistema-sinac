﻿Imports System.Data                 ' FOR "DataTable"
Imports System.Data.SqlClient
Imports System.IO                   ' FOR FILE ACCESS.
Imports Encryption64
Imports globales
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Configuration
Partial Class EO_modifica_eo
    Inherits System.Web.UI.Page
    Dim registro_actual As Integer = 0
    Public codigo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Islogged(Session("nivel"))) Then

            Response.Redirect(ResolveUrl("~/acceso.aspx"))
        ElseIf (Not validapermiso(524288, Session("permiso"))) Then
            Response.Redirect(ResolveUrl("~/permiso.aspx"))
        ElseIf (validacaducidad() = False) Then
            Response.Redirect(ResolveUrl("~/caduca.aspx"))

        Else
            Try
                Dim es As EO
                Dim uni As String
                codigo = DecryptText(Request.QueryString("ID")) 'recupera el codigo q le pasa como dato en el get, response redirect desde la grilla desde el editar lo poniamos
                analista.AppendDataBoundItems = True
                producto.AppendDataBoundItems = True
                superior.AppendDataBoundItems = True
                tipo_error.AppendDataBoundItems = True
                etapa.AppendDataBoundItems = True
                laboratorio.AppendDataBoundItems = True

                'Button12.Attributes.Add("onclick", " return confirma_elimina();")
                Label1.Text = ""
                Label2.Text = ""
                confirma_cambios.Text = "Para confirmar los cambios presione el boton guardar"
                If (Not Page.IsPostBack) Then 'ponemos esto por q queremos q se ejecute una sola vez
                    buscareo(codigo) 'carga la interfaz
                End If
                es = New EO()
                es.EO(conexion)



            Catch
                Label2.Text = "Error al cargar datos del EO"
                Response.Redirect(ResolveUrl("~/Default.aspx"))
            End Try
        End If



    End Sub
    Sub buscareo(ByVal codigo As String)

        Dim u As EO

        Dim e As EO_EO


        Try

            u = New EO()
            u.EO(conexion)
            u = u.Consultareo1(codigo)

            e = New EO_EO()
            e.EO_EO(conexion)

            'idstock.Disabled = True
            numero_eo.Disabled = True
            'conclusiones se activa desde el boton
            conclusiones.Disabled = True
            'la fecha de cierre solo se carga cuando esta concluido
            fecha_cierre.Enabled = False

            Dim aux1 As Integer
            aux1 = u.NroEO
            numero_eo.Value = aux1.ToString("d4")
            fecha_carga.Disabled = True
            Dim aux As Date
            aux = u.FeCarga
            fecha_carga.Value = aux

            If (u.FeCierre <> "") Then
                aux = u.FeCierre
                fecha_cierre.Text = aux
            Else
                fecha_cierre.Text = ""
            End If
            laboratorio.Text = u.Laboratorio
            analista.SelectedValue = u.Analista
            producto.SelectedValue = u.idPdto
            superior.SelectedValue = u.Encargado
            etapa.SelectedValue = u.Etapa
            lote.Text = u.Lote
            ensayo.Value = u.Ensayo
            numero_cuaderno.Value = u.CuadernoFolio

            'lotes inv


            'tipo_error.Value = u.TipoError
            Dim I As Integer

            Dim cantidad As SqlDataReader

            Dim auxe As String

            'errores obvios
            cantidad = e.obtener_cantidad_eolist("EO")
            While cantidad.Read()
                tipo_error.Items.Add(cantidad.GetString(0))
            End While
            cantidad.Close()
            For I = 0 To tipo_error.Items.Count - 1

                'verifico que exista la relacion
                auxe = e.obteneridEO_EO(codigo, e.obteneridEO_list(tipo_error.Items(I).Value), "EO")
                If (auxe <> "-1") Then
                    'tipo_error.SelectedValue = tipo_error.Items(I).Value
                    tipo_error.Items(I).Selected = True
                End If

            Next

            'desviaciones
            cantidad = e.obtener_cantidad_eolist("DE")
            While cantidad.Read()
                desviaciones.Items.Add(cantidad.GetString(0))
            End While
            cantidad.Close()
            For I = 0 To desviaciones.Items.Count - 1

                'verifico que exista la relacion
                auxe = e.obteneridEO_EO(codigo, e.obteneridEO_list(desviaciones.Items(I).Value), "DE")
                If (auxe <> "-1") Then
                    'tipo_error.SelectedValue = tipo_error.Items(I).Value
                    desviaciones.Items(I).Selected = True
                End If

            Next

            'fallas en el sist 
            cantidad = e.obtener_cantidad_eolist("FA")
            While cantidad.Read()
                adecuacion.Items.Add(cantidad.GetString(0))
            End While
            cantidad.Close()
            For I = 0 To adecuacion.Items.Count - 1

                'verifico que exista la relacion
                auxe = e.obteneridEO_EO(codigo, e.obteneridEO_list(adecuacion.Items(I).Value), "FA")
                If (auxe <> "-1") Then
                    'tipo_error.SelectedValue = tipo_error.Items(I).Value
                    adecuacion.Items(I).Selected = True
                End If

            Next

            'error de calculo
            cantidad = e.obtener_cantidad_eolist("EC")
            While cantidad.Read()
                calculo.Items.Add(cantidad.GetString(0))
            End While
            cantidad.Close()
            For I = 0 To calculo.Items.Count - 1

                'verifico que exista la relacion
                auxe = e.obteneridEO_EO(codigo, e.obteneridEO_list(calculo.Items(I).Value), "EC")
                If (auxe <> "-1") Then
                    'tipo_error.SelectedValue = tipo_error.Items(I).Value
                    calculo.Items(I).Selected = True
                End If

            Next
            'lotesinv
            Dim borrar As String
            Dim borrar1 As String

            cantidad = e.obtener_cantidad_lotes(codigo)
            While cantidad.Read()
                borrar = cantidad.GetString(0)
                ' borrar1 = cantidad.GetString(1)
                lotes_inv.Items.Add(cantidad.GetString(0))

            End While
            cantidad.Close()
            ' For I = 0 To calculo.Items.Count - 1

            'verifico que exista la relacion
            'auxe = e.obteneridEO_EO(codigo, e.obteneridEO_list(calculo.Items(I).Value), "EC")
            'If (auxe <> "-1") Then
            'tipo_error.SelectedValue = tipo_error.Items(I).Value
            'calculo.Items(I).Selected = True
            'End If

            'Next


            conclusiones.Value = u.Conclusiones
            TextArea1.Value = u.Obs_EO
            TextArea2.Value = u.Obs_DE
            TextArea3.Value = u.Obs_FA
            TextArea4.Value = u.Obs_EC
            estado.Text = u.Estado

            If (estado.Text = "Concluido") And (Session("nivel") <> "0") Then

                laboratorio.Enabled = False
                analista.Enabled = False
                producto.Enabled = False
                superior.Enabled = False
                etapa.Enabled = False
                lote.Enabled = False
                ensayo.Disabled = True
                numero_cuaderno.Disabled = True
                tipo_error.Enabled = False
                conclusiones.Disabled = True
                fecha_cierre.Enabled = False
                estado.Enabled = False
            End If

            If (estado.Text = "Concluido") And (Session("nivel") = "0") Then
                fecha_cierre.Enabled = True
            End If
            If (Session("nivel") <> "0") Then
                fecha_cierre.Enabled = False
                conclusiones.Disabled = True
                Button2.Enabled = False
                fecha_cierre.Enabled = False
                estado.Enabled = False
            End If

            If (estado.Text = "Anulado") Then
                laboratorio.Enabled = False
                analista.Enabled = False
                producto.Enabled = False
                superior.Enabled = False
                etapa.Enabled = False
                lote.Enabled = False
                ensayo.Disabled = True
                numero_cuaderno.Disabled = True
                tipo_error.Enabled = False
                adecuacion.Enabled = False
                calculo.Enabled = False
                desviaciones.Enabled = False
                conclusiones.Disabled = True
                fecha_cierre.Enabled = False
                lote_i.Disabled = True
                TextArea1.Disabled = True
                TextArea2.Disabled = True
                TextArea3.Disabled = True
                TextArea4.Disabled = True
                estado.Enabled = False
                Button1.Enabled = False
                Button2.Enabled = False
                Button3.Enabled = False
                Button4.Enabled = False
                btnReport.Enabled = False
                confirma_cambios.Text = ""



            End If







        Catch ex As Exception

        End Try
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            'If (Page.IsPostBack) Then
            'If extension <> "no valida" Then
            actualizareo()
            'vuelvo a cargar la interfaz para que tome los cambios
            'buscarestandar(codigo)
            ' Else
            ' Label2.Text = "Solo se pueden adjuntar archivos con extensión PDF"
            ' End If

            'End If
        Catch ex As Exception
            'Label2.Text = "Problemas al Guardar Archivo: " & ex.Message
        End Try
    End Sub


    Sub actualizareo()
        
        Dim u As EO
        Dim e As EO_EO
      
        Dim r As String
    
        Dim l As logs
    
        Dim xfecha As String
        Try
            ' 'log
            l = New logs()
            l.logs(conexion)
            u = New EO()
            u.EO(conexion)


            Dim trimtex1 As String
            trimtex1 = TextArea1.Value.Trim
 
            If (u.valida_cierre(fecha_carga.Value, fecha_cierre.Text)) Then
                r = u.Actualizareo(codigo, fecha_carga.Value, laboratorio.Text, analista.Text, etapa.Text, producto.Text, superior.Text, superior.Text, lote.Text, ensayo.Value, numero_cuaderno.Value, Trim(conclusiones.Value), estado.Text, revertirfecha(fecha_cierre.Text), TextArea1.Value.Trim, TextArea2.Value.Trim, TextArea3.Value.Trim, TextArea4.Value.Trim)
                e = New EO_EO()
                e.EO_EO(conexion)
                Dim I As Integer

                ' por simplicidad elimino las relaciones previas y cargo las nuevas luego de la modificacion 
                e.elimina_rel(codigo)
                For I = 0 To tipo_error.Items.Count - 1

                    If (tipo_error.Items(I).Selected = True) Then
                        Dim str As String = tipo_error.Items(I).ToString()

                        'genero la relacion entre el eo y el eolist
                        e.AgregarEO_EO(codigo, e.obteneridEO_list(tipo_error.Items(I).Value))


                    End If
                Next

                For I = 0 To desviaciones.Items.Count - 1

                    If (desviaciones.Items(I).Selected = True) Then
                        Dim str As String = desviaciones.Items(I).ToString()

                        'genero la relacion entre el eo y el eolist
                        e.AgregarEO_EO(codigo, e.obteneridEO_list(desviaciones.Items(I).Value))



                    End If
                Next

                For I = 0 To adecuacion.Items.Count - 1

                    If (adecuacion.Items(I).Selected = True) Then
                        Dim str As String = adecuacion.Items(I).ToString()

                        'genero la relacion entre el eo y el eolist
                        e.AgregarEO_EO(codigo, e.obteneridEO_list(adecuacion.Items(I).Value))



                    End If
                Next


                For I = 0 To calculo.Items.Count - 1

                    If (calculo.Items(I).Selected = True) Then
                        Dim str As String = calculo.Items(I).ToString()

                        'genero la relacion entre el eo y el eolist
                        e.AgregarEO_EO(codigo, e.obteneridEO_list(calculo.Items(I).Value))



                    End If
                Next

                Dim lo As Lotes
                lo = New Lotes()
                lo.Lotes(conexion)
                ' por simplicidad elimino las relaciones previas y cargo las nuevas luego de la modificacion 
                lo.elimina_lotes_eo(codigo)
                ' cargo los lotes inv


                For I = 0 To lotes_inv.Items.Count - 1

                    Dim borrar As String
                    Dim str As String = lotes_inv.Items(I).ToString()
                    borrar = lotes_inv.Items(I).Value
                    'genero la relacion entre los lotes y el EO
                    lo.Agregarlote_eo(codigo, lotes_inv.Items(I).Value)



                Next

                xfecha = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))

                'una vez definido como es el manejo del user cambiar el segundo parametro, de momento hardcode

                l.Agregarlog2(Session("so"), Session("usuario"), Session("departamento"), Session("nivel"), System.Net.Dns.GetHostEntry(Request.ServerVariables("remote_addr")).HostName, "M", xfecha, u.obtenerNroEO(codigo), producto.Text, lote.Text, revertirfecha(fecha_carga.Value), estado.Text, "0", conclusiones.Value, fecha_cierre.Text)



                Label1.Text = "Se modificaron correctamente los datos del EO"
            Else
                Label2.Text = "La fecha de cierre no puede ser anterior a la fecha de carga"
            End If


        Catch ex As Exception
            Label2.Text = "Hubo un error al actualizar el EO"
        End Try
    End Sub
    Private Function GetData(ByVal query As String) As DataTable
        Dim conString As String = ConfigurationManager.ConnectionStrings("EstandaresConnectionString").ConnectionString
        Dim cmd As New SqlCommand(query)
        Using con As New SqlConnection(conString)
            Using sda As New SqlDataAdapter()
                cmd.Connection = con

                sda.SelectCommand = cmd
                Using dt As New DataTable()
                    sda.Fill(dt)

                    Return dt
                End Using
            End Using
        End Using
    End Function
    Protected Sub GenerateReport(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            Dim sql As String
            Dim pagina As Integer = 1
            Dim pagesize1 As String = 510.0F 'ancho de la pagina
            Dim fontsize1 As String = 11 'info inicial
            Dim fontsize2 As String = 13 'titulo cuadros
            Dim fontsize3 As String = 10 'contenido cuadros
            Dim fontsize4 As String = 12 'pie de la pagina
            Dim fontsize5 As String = 10 'firmas
            Dim fontsize9 As String = 10 'observaciones
            Dim observaciones As String
            Dim repe As Integer
            Dim canti As Integer
            Dim contar As Integer
            Dim contenidototal As String 'cantidad de lineas del contenido de las observaciones (incluidos saltos de linea)
            Dim espaciosrestantes As String 'espacios restantes de las observaciones (sin el contenido y los saltos de linea)
            Dim indice As String 'para relleno dinamico de secciones

            Dim document As New Document(PageSize.A4, 88.0F, 88.0F, 10.0F, 10.0F)
            Dim NormalFont As Font = FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)
            Dim es As EO
            Dim p As Parametros
            p = New Parametros()
            p.Parametros(conexion)

            Using memoryStream As New System.IO.MemoryStream()
                Dim writer As PdfWriter = PdfWriter.GetInstance(document, memoryStream)
                Dim phrase As Phrase = Nothing
                Dim cell As PdfPCell = Nothing
                Dim table As PdfPTable = Nothing
                Dim color__1 As Color = Nothing
                Dim test As Integer = 0

                Dim ds As DataSet
                Dim cn As SqlConnection
                Dim cm As SqlCommand
                Dim da As SqlDataAdapter
                Dim aux As String = 0

                'cargo parametros

                sops = p.consulta_valor_nombre("sops")
                fogl = p.consulta_valor_nombre("fogl")
                titulo = p.consulta_valor_nombre("titulo_fogl")
                vigencia = p.consulta_valor_nombre("vigencia_fogl")
                Dim espacios_observaciones As String = 10 ' cantidad de espacios por defecto en el cuadro de observaciones

                Dim espacios_secciones As String = 8 ' cantidad de espacios por defecto en el cuadro de secciones cuando esta en blanco

                Dim espacios_dinamicos As String ' va contener la cantidad de espacios a agregar cuando las observaciones ya esten cargadas

                Dim contador_pagina As String = 0 ' para saber si la seccion entra en la pagina o no, cuenta los items que se van mostrando
                Dim rellena_final As String = 17 'espacios para completar la primer pagina
                Dim rellena_final2 As String = 9 'espacios para completar la segunda pagina
                Dim completa_seccion_dinamico As String = 6 ' cantidad de espacios para completar cuando hay contenido
                'traigo los valores de la bd
                sql = "select Descripcion from EO_list inner join EO_EO on EO_EO.idEOlist=EO_list.id where tipo='EO' and EO_EO.idEO='" + codigo + "'"
                cn = New SqlConnection(conexion3)
                cn.Open()
                cm = New SqlCommand()
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                da = New SqlDataAdapter(cm)
                ds = New DataSet()
                da.Fill(ds)


                document.Open()

                'solo agrego esto (tabla sin contenido) para separar

                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.33F})
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)

                'fin separador
                ' este bucle determina el numero de paginas del documento, registro actual guarda los registros ya impresos y lo compara versus el total que trae la consulta

                'Tabla de cabecera (3 columnas)
                table = New PdfPTable(3)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.31F, 0.48F, 0.2F})

                ' para armar la cabecera
             
                es = New EO()
                es.EO(conexion)
            


                'Columna 1
                phrase = New Phrase()
                phrase.Add(New Chunk(fogl & vbLf, FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)))

                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                table.AddCell(cell)



                'Columna2


                phrase = New Phrase()
                phrase.Add(New Chunk(titulo, FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)

                'columna3
                cell = ImageCell("~/img/logomonteverde.png", 12.0F, PdfPCell.ALIGN_RIGHT)
                table.AddCell(cell)
                document.Add(table)

                'solo agrego esto (tabla sin contenido) para separar

                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.33F})
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)

                'solo agrego esto (tabla sin contenido) para separar

                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.33F})
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)

                'fin separador

                'Tabla de fecha de vigencia (3 columnas)
                table = New PdfPTable(3)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.41F, 0.3F, 0.26F})

               


                'Columna 1
                phrase = New Phrase()
                phrase.Add(New Chunk("Fecha de Vigencia: " & vigencia, FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)))
                color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 1.0F
                cell.BorderWidthRight = 1.0F
                cell.BorderWidthTop = 1.0F
                cell.BorderWidthBottom = 1.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 1.0F
                table.AddCell(cell)

                'Columna2  (esta en blanco)


                phrase = New Phrase()
                phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)

                'columna3  (blanco)
                phrase = New Phrase()
                phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)


                'solo agrego esto (tabla sin contenido) para separar

                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.33F})
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)

                'fin separador

                'Tabla de contenido(2 columna)
          

                table = New PdfPTable(2)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.5F, 0.5F})


                'Columna 1
                phrase = New Phrase()
                phrase.Add(New Chunk("Registro N° " & rellena(es.obtenerNroEO(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                phrase.Add(New Chunk("Laboratorio de: " & es.obtenerlaboratorio(codigo) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                phrase.Add(New Chunk("Fecha de investigación: " & es.obtenerfechacarga(codigo) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                phrase.Add(New Chunk("Analista: " & es.obtenernombreAnalista(es.obteneranalista(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                phrase.Add(New Chunk("Cuaderno / Folio: " & es.obtenercuadernofolio(codigo) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
              
                color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 1.0F
                cell.BorderWidthRight = 0.0F
                cell.BorderWidthTop = 1.0F
                cell.BorderWidthBottom = 1.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 1.0F
                table.AddCell(cell)

                phrase = New Phrase()
                phrase.Add(New Chunk("Ensayo: " & rellena(es.obtenerNroEO(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                phrase.Add(New Chunk("Lote: " & rellena(es.obtenerNroEO(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                phrase.Add(New Chunk("Etapa: " & rellena(es.obtenerNroEO(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                phrase.Add(New Chunk("Metodología: " & rellena(es.obtenerNroEO(codigo)) & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                phrase.Add(New Chunk("Producto/Materia Prima: " & es.obtenercodpdto(es.obtenerproducto(codigo)) & vbLf & vbLf & vbLf & vbLf, FontFactory.GetFont("Arial", fontsize1, Font.NORMAL, Color.BLACK)))
                color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 0.0F
                cell.BorderWidthRight = 1.0F
                cell.BorderWidthTop = 1.0F
                cell.BorderWidthBottom = 1.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 1.0F
                table.AddCell(cell)

                document.Add(table)
                'Tabla de contenido(1 columna)
                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                'table.SetWidths(New Single() {0.41F, 0.3F, 0.26F})

                'titulo errores obvios
                phrase = New Phrase()
                phrase.Add(New Chunk("Errores obvios", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 1.0F
                cell.BorderWidthRight = 1.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 1.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 1.0F
                table.AddCell(cell)
                document.Add(table)



                'detalle 

                'Tabla de contenido errores obvios (2 columnas)
                table = New PdfPTable(2)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.05F, 0.95F})
                Dim k As Integer
                'si la seccion esta vacia genero la tabla con un valor parametrizado para que no se me vayan los margenes
                If (ds.Tables(0).Rows.Count = 0) Then

                    For k = 0 To espacios_secciones - 1
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)

                    Next
                    
                End If

                For k = 0 To ds.Tables(0).Rows.Count - 1



                    'lineas dinamicas de errores obvios
                    phrase = New Phrase()
                    cell = ImageCell("~/img/tilde.png", 2.0F, PdfPCell.ALIGN_RIGHT)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 0.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 5.0F
                    table.AddCell(cell)


                    phrase = New Phrase()
                    phrase.Add(New Chunk(ds.Tables(0).Rows(k).ItemArray(0).ToString, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 0.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 5.0F
                    table.AddCell(cell)


                Next

                ' tengo que ver cuanto es lo max q se puede tildar para cada seccion y poder terminar de estimar los espacios
                ' de momento dejo hardcode suponiendo un max de tres items

              


                If (ds.Tables(0).Rows.Count > "0") Then

                    'de acuerdo a la cantidad de items en la seccion sera la cantidad de espacios que agrego
                      Select ds.Tables(0).Rows.Count

                        Case "1"
                            indice = completa_seccion_dinamico - (1)
                        Case "2"
                            indice = completa_seccion_dinamico - (3)
                        Case "3"
                            indice = completa_seccion_dinamico - (5)
                        Case "4"
                            indice = completa_seccion_dinamico - (7)
                    End Select


                    For k = 0 To indice
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)

                    Next
                End If



                document.Add(table)


                'observaciones EO
                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True


                phrase = New Phrase()

                'codigo para rellenar con espacios el campo de observaciones en funcion del contenido y que no se vayan los margenes
                observaciones = Trim(es.obtenerobservaciones(codigo, "Obs_EO"))
                'longitud de la cadena incluido espacios
                Dim sss As String
                canti = Len(observaciones)
                sss = canti
                If (canti > "0") Then

                    'match ceiling calcula la cantidad de lineas que va ocupar la cadena contenida en funicion de la cantidad de caracteres
                    'repe tendria los espacios que restan sin contar el contenido
                    repe = espacios_observaciones - Math.Ceiling(canti / 108)

                Else
                    ' si no hay observaciones  agrego los blancos
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Registrar Observaciones: " & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    For k = 0 To espacios_observaciones
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)
                    Next
                    document.Add(table)

                End If
                ' hay observaciones
                If (canti > "0") Then
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Registrar Observaciones: " & vbLf & vbLf & observaciones & espacios_dinamicos, FontFactory.GetFont("Arial", fontsize9, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    'de acuerdo a la cantidad de lineas en el cuadro observaciones sera la cantidad de espacios que agregue. 
                    'regular de aca una vez q se sepa cant max de caracteres

               

                    'cuento los saltos de linea en la cadena
                    contar = 0

                    For Each item As Char In observaciones

                        If item <> vbCr Then
                            Continue For
                        End If

                        contar += 1

                    Next


                    contenidototal = Math.Ceiling(canti / 108) + contar
                    espaciosrestantes = repe - contar

                    ' ajustar de aca los espacios segun la cantidad de lineas (por defecto se calculo con 3)
                    Select Case contenidototal
                        Case "1"
                            indice = espaciosrestantes - 2
                        Case "2"
                            indice = espaciosrestantes - 3
                        Case "3"
                            indice = espaciosrestantes - 4

                    End Select

                    For k = 0 To indice

                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)



                    Next
                    document.Add(table)
                End If






                sql = "select Descripcion from EO_list inner join EO_EO on EO_EO.idEOlist=EO_list.id where tipo='DE' and EO_EO.idEO='" + codigo + "'"
                cn = New SqlConnection(conexion3)
                cn.Open()
                cm = New SqlCommand()
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                da = New SqlDataAdapter(cm)
                ds = New DataSet()
                da.Fill(ds)




                'Tabla de Desviaciones de proc o metodos(1 columna)
                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True


                'titulo 
                phrase = New Phrase()
                phrase.Add(New Chunk("Desviaciones de Procedimientos o Métodos", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 1.0F
                cell.BorderWidthRight = 1.0F
                cell.BorderWidthTop = 1.0F
                cell.BorderWidthBottom = 1.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 1.0F
                table.AddCell(cell)
                document.Add(table)



                'detalle 

                'Tabla de contenido Desviaciones de proc o metodos (2 columnas)
                table = New PdfPTable(2)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.05F, 0.95F})

                'si la seccion esta vacia genero la tabla con un valor parametrizado para que no se me vayan los margenes
                If (ds.Tables(0).Rows.Count = 0) Then

                    'el tamaño de la seccion vacia se regula con la variable (espacios_Secciones)
                    For k = 0 To espacios_secciones - 1
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)

                    Next

                End If


                For k = 0 To ds.Tables(0).Rows.Count - 1
                    'linea 1
                    phrase = New Phrase()
                    cell = ImageCell("~/img/tilde.png", 2.0F, PdfPCell.ALIGN_RIGHT)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 0.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 5.0F
                    table.AddCell(cell)


                    phrase = New Phrase()
                    phrase.Add(New Chunk(ds.Tables(0).Rows(k).ItemArray(0).ToString, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 0.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 5.0F
                    table.AddCell(cell)



                Next


                ' tengo que ver cuanto es lo max q se puede tildar para cada seccion y poder terminar de estimar los espacios
                ' de momento dejo hardcode suponiendo un max de tres items




                If (ds.Tables(0).Rows.Count > "0") Then

                    'de acuerdo a la cantidad de items en la seccion sera la cantidad de espacios que agrego
                    Select Case ds.Tables(0).Rows.Count

                        Case "1"
                            indice = completa_seccion_dinamico - (1)
                        Case "2"
                            indice = completa_seccion_dinamico - (3)
                        Case "3"
                            indice = completa_seccion_dinamico - (5)
                        Case "4"
                            indice = completa_seccion_dinamico - (7)
                    End Select


                    For k = 0 To indice
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)

                    Next
                End If




                document.Add(table)


                'observaciones 
                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True



                phrase = New Phrase()

                'codigo para rellenar con espacios el campo de observaciones en funcion del contenido y que no se vayan los margenes
                observaciones = Trim(es.obtenerobservaciones(codigo, "Obs_DE"))
                'longitud de la cadena incluido espacios

                canti = Len(observaciones)

                If (canti > "0") Then

                    'match ceiling calcula la cantidad de lineas que va ocupar la cadena contenida en funicion de la cantidad de caracteres
                    'repe tendria los espacios que restan sin contar el contenido
                    repe = espacios_observaciones - Math.Ceiling(canti / 108)

                Else
                    ' si no hay observaciones  agrego los blancos
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Registrar Observaciones: " & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    For k = 0 To espacios_observaciones
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        If (k = espacios_observaciones) Then
                            cell.BorderWidthBottom = 1.0F
                        Else
                            cell.BorderWidthBottom = 0.0F
                        End If

                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)
                    Next
                    document.Add(table)

                End If
                ' hay observaciones
                If (canti > "0") Then
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Registrar Observaciones: " & vbLf & vbLf & observaciones & espacios_dinamicos, FontFactory.GetFont("Arial", fontsize9, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    'de acuerdo a la cantidad de lineas en el cuadro observaciones sera la cantidad de espacios que agregue. 
                    'regular de aca una vez q se sepa cant max de caracteres



                    'cuento los saltos de linea en la cadena
                    contar = 0

                    For Each item As Char In observaciones

                        If item <> vbCr Then
                            Continue For
                        End If

                        contar += 1

                    Next


                    contenidototal = Math.Ceiling(canti / 108) + contar
                    espaciosrestantes = repe - contar

                    ' ajustar de aca los espacios segun la cantidad de lineas (por defecto se calculo con 3)
                    Select Case contenidototal
                        Case "1"
                            indice = espaciosrestantes - 2
                        Case "2"
                            indice = espaciosrestantes - 3
                        Case "3"
                            indice = espaciosrestantes - 4

                    End Select

                    For k = 0 To indice

                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        If (k = indice) Then
                            cell.BorderWidthBottom = 1.0F
                        Else
                            cell.BorderWidthBottom = 0.0F
                        End If
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)



                    Next
                    document.Add(table)
                End If






                'pie de pagina


                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                ' relleno con celdas vacias en funcion del contenido previo ( lo calculo con un contador por registro mostrado)
                Dim i As Integer
                For i = 1 To rellena_final
                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf, FontFactory.GetFont("Arial", 10, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                Next


                phrase = New Phrase()
                phrase.Add(New Chunk(sops & "                             Fecha de emisión: " & String.Format("{0:dd/MM/yyyy - H:mm:ss}", Date.Now()) & "     Página 1 de 2", FontFactory.GetFont("Arial", fontsize4, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)


                'agrego una nueva pagina

                document.NewPage()
                pagina = pagina + 1



                'solo agrego esto (tabla sin contenido) para separar

                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.33F})
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                phrase = New Phrase()
                phrase.Add(New Chunk("" & vbLf, FontFactory.GetFont("Arial", 4, Font.BOLD, Color.RED)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)

                'fin separador

                'Tabla de cabecera (3 columnas)
                table = New PdfPTable(3)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.31F, 0.48F, 0.2F})



                'Columna 1
                phrase = New Phrase()
                phrase.Add(New Chunk(fogl & vbLf, FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)))

                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                table.AddCell(cell)



                'Columna2


                phrase = New Phrase()
                phrase.Add(New Chunk(titulo, FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)))

                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)

                'columna3
                cell = ImageCell("~/img/logomonteverde.png", 12.0F, PdfPCell.ALIGN_RIGHT)
                table.AddCell(cell)
                document.Add(table)


                sql = "select Descripcion from EO_list inner join EO_EO on EO_EO.idEOlist=EO_list.id where tipo='FA' and EO_EO.idEO='" + codigo + "'"
                cn = New SqlConnection(conexion3)
                cn.Open()
                cm = New SqlCommand()
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                da = New SqlDataAdapter(cm)
                ds = New DataSet()
                da.Fill(ds)

                'Tabla de Fallas en el Sistema de Adecuación(1 columna)
                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True


                'espacio en blanco por margen
                phrase = New Phrase()
                phrase.Add(New Chunk(vbLf, FontFactory.GetFont("Arial", fontsize3, Font.BOLD, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                table.AddCell(cell)
                'titulo 
                phrase = New Phrase()
                phrase.Add(New Chunk("Fallas en el Sistema de Adecuación", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 1.0F
                cell.BorderWidthRight = 1.0F
                cell.BorderWidthTop = 1.0F
                cell.BorderWidthBottom = 1.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 1.0F
                table.AddCell(cell)
                document.Add(table)



                'detalle 


                table = New PdfPTable(2)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.05F, 0.95F})

                'si la seccion esta vacia genero la tabla con un valor parametrizado para que no se me vayan los margenes
                If (ds.Tables(0).Rows.Count = 0) Then

                    For k = 0 To espacios_secciones - 1
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)

                    Next

                End If



                For k = 0 To ds.Tables(0).Rows.Count - 1


                    'linea 1
                    phrase = New Phrase()
                    cell = ImageCell("~/img/tilde.png", 2.0F, PdfPCell.ALIGN_RIGHT)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 0.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 5.0F
                    table.AddCell(cell)


                    phrase = New Phrase()
                    phrase.Add(New Chunk(ds.Tables(0).Rows(k).ItemArray(0).ToString, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 0.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 5.0F
                    table.AddCell(cell)

                Next

                ' tengo que ver cuanto es lo max q se puede tildar para cada seccion y poder terminar de estimar los espacios
                ' de momento dejo hardcode suponiendo un max de tres items




                If (ds.Tables(0).Rows.Count > "0") Then

                    'de acuerdo a la cantidad de items en la seccion sera la cantidad de espacios que agrego
                    Select Case ds.Tables(0).Rows.Count

                        Case "1"
                            indice = completa_seccion_dinamico - (1)
                        Case "2"
                            indice = completa_seccion_dinamico - (3)
                        Case "3"
                            indice = completa_seccion_dinamico - (5)
                        Case "4"
                            indice = completa_seccion_dinamico - (7)
                    End Select


                    For k = 0 To indice
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)

                    Next
                End If



                document.Add(table)



                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True



                phrase = New Phrase()

                'codigo para rellenar con espacios el campo de observaciones en funcion del contenido y que no se vayan los margenes
                observaciones = Trim(es.obtenerobservaciones(codigo, "Obs_FA"))
                'longitud de la cadena incluido espacios

                canti = Len(observaciones)

                If (canti > "0") Then

                    'match ceiling calcula la cantidad de lineas que va ocupar la cadena contenida en funicion de la cantidad de caracteres
                    'repe tendria los espacios que restan sin contar el contenido
                    repe = espacios_observaciones - Math.Ceiling(canti / 108)

                Else
                    ' si no hay observaciones  agrego los blancos
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Registrar Observaciones: " & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    For k = 0 To espacios_observaciones
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)
                    Next
                    document.Add(table)

                End If
                ' hay observaciones
                If (canti > "0") Then
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Registrar Observaciones: " & vbLf & vbLf & observaciones & espacios_dinamicos, FontFactory.GetFont("Arial", fontsize9, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    'de acuerdo a la cantidad de lineas en el cuadro observaciones sera la cantidad de espacios que agregue. 
                    'regular de aca una vez q se sepa cant max de caracteres



                    'cuento los saltos de linea en la cadena
                    contar = 0

                    For Each item As Char In observaciones

                        If item <> vbCr Then
                            Continue For
                        End If

                        contar += 1

                    Next


                    contenidototal = Math.Ceiling(canti / 108) + contar
                    espaciosrestantes = repe - contar

                    ' ajustar de aca los espacios segun la cantidad de lineas (por defecto se calculo con 3)
                    Select Case contenidototal
                        Case "1"
                            indice = espaciosrestantes - 2
                        Case "2"
                            indice = espaciosrestantes - 3
                        Case "3"
                            indice = espaciosrestantes - 4

                    End Select

                    For k = 0 To indice

                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)



                    Next
                    document.Add(table)
                End If


                'Tabla de Desviaciones de proc o metodos(1 columna)
                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True

                'titulo error de calculo

                phrase = New Phrase()
                phrase.Add(New Chunk("Error de Cálculo", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 1.0F
                cell.BorderWidthRight = 1.0F
                cell.BorderWidthTop = 1.0F
                cell.BorderWidthBottom = 1.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 1.0F
                table.AddCell(cell)
                document.Add(table)



                sql = "select Descripcion from EO_list inner join EO_EO on EO_EO.idEOlist=EO_list.id where tipo='EC' and EO_EO.idEO='" + codigo + "'"
                cn = New SqlConnection(conexion3)
                cn.Open()
                cm = New SqlCommand()
                cm.CommandText = sql
                cm.CommandType = CommandType.Text
                cm.Connection = cn
                da = New SqlDataAdapter(cm)
                ds = New DataSet()
                da.Fill(ds)

                'detalle 

                'Tabla de contenido errores obvios (2 columnas)
                table = New PdfPTable(2)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.05F, 0.95F})

                'si la seccion esta vacia genero la tabla con un valor parametrizado para que no se me vayan los margenes
                If (ds.Tables(0).Rows.Count = 0) Then

                    For k = 0 To espacios_secciones - 1
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)

                    Next

                End If



                For k = 0 To ds.Tables(0).Rows.Count - 1

                    'linea 1
                    phrase = New Phrase()
                    cell = ImageCell("~/img/tilde.png", 2.0F, PdfPCell.ALIGN_RIGHT)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 0.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 5.0F
                    table.AddCell(cell)


                    phrase = New Phrase()
                    phrase.Add(New Chunk(ds.Tables(0).Rows(k).ItemArray(0).ToString, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 0.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 0.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 5.0F
                    table.AddCell(cell)


                Next

                ' tengo que ver cuanto es lo max q se puede tildar para cada seccion y poder terminar de estimar los espacios
                ' de momento dejo hardcode suponiendo un max de tres items




                If (ds.Tables(0).Rows.Count > "0") Then

                    'de acuerdo a la cantidad de items en la seccion sera la cantidad de espacios que agrego
                    Select Case ds.Tables(0).Rows.Count

                        Case "1"
                            indice = completa_seccion_dinamico - (1)
                        Case "2"
                            indice = completa_seccion_dinamico - (3)
                        Case "3"
                            indice = completa_seccion_dinamico - (5)
                        Case "4"
                            indice = completa_seccion_dinamico - (7)
                    End Select


                    For k = 0 To indice
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 0.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)


                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 0.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        cell.BorderWidthBottom = 0.0F
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.VerticalAlignment = PdfCell.ALIGN_CENTER
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 5.0F
                        table.AddCell(cell)

                    Next
                End If




                document.Add(table)


                'observaciones EO
                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True


                phrase = New Phrase()

                'codigo para rellenar con espacios el campo de observaciones en funcion del contenido y que no se vayan los margenes
                observaciones = Trim(es.obtenerobservaciones(codigo, "Obs_EC"))
                'longitud de la cadena incluido espacios

                canti = Len(observaciones)

                If (canti > "0") Then

                    'match ceiling calcula la cantidad de lineas que va ocupar la cadena contenida en funicion de la cantidad de caracteres
                    'repe tendria los espacios que restan sin contar el contenido
                    repe = espacios_observaciones - Math.Ceiling(canti / 108)

                Else
                    ' si no hay observaciones  agrego los blancos
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Registrar Observaciones: " & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    For k = 0 To espacios_observaciones
                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        If (k = espacios_observaciones) Then
                            cell.BorderWidthBottom = 1.0F
                        Else
                            cell.BorderWidthBottom = 0.0F
                        End If

                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)
                    Next
                    document.Add(table)

                End If
                ' hay observaciones
                If (canti > "0") Then
                    phrase = New Phrase()
                    phrase.Add(New Chunk("Registrar Observaciones: " & vbLf & vbLf & observaciones & espacios_dinamicos, FontFactory.GetFont("Arial", fontsize9, Font.NORMAL, Color.BLACK)))
                    color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                    cell.BorderColorLeft = color__1
                    cell.BorderColorRight = color__1
                    cell.BorderColorTop = color__1
                    cell.BorderColorBottom = color__1
                    cell.BorderWidthLeft = 1.0F
                    cell.BorderWidthRight = 1.0F
                    cell.BorderWidthTop = 1.0F
                    cell.BorderWidthBottom = 0.0F
                    cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                    cell.PaddingBottom = 5.0F
                    cell.PaddingLeft = 5.0F
                    cell.PaddingTop = 1.0F
                    table.AddCell(cell)

                    'de acuerdo a la cantidad de lineas en el cuadro observaciones sera la cantidad de espacios que agregue. 
                    'regular de aca una vez q se sepa cant max de caracteres



                    'cuento los saltos de linea en la cadena
                    contar = 0

                    For Each item As Char In observaciones

                        If item <> vbCr Then
                            Continue For
                        End If

                        contar += 1

                    Next


                    contenidototal = Math.Ceiling(canti / 108) + contar
                    espaciosrestantes = repe - contar

                    ' ajustar de aca los espacios segun la cantidad de lineas (por defecto se calculo con 3)
                    Select Case contenidototal
                        Case "1"
                            indice = espaciosrestantes - 2
                        Case "2"
                            indice = espaciosrestantes - 3
                        Case "3"
                            indice = espaciosrestantes - 4

                    End Select

                    For k = 0 To indice

                        phrase = New Phrase()
                        phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                        color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                        cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                        cell.BorderColorLeft = color__1
                        cell.BorderColorRight = color__1
                        cell.BorderColorTop = color__1
                        cell.BorderColorBottom = color__1
                        cell.BorderWidthLeft = 1.0F
                        cell.BorderWidthRight = 1.0F
                        cell.BorderWidthTop = 0.0F
                        If (k = indice) Then
                            cell.BorderWidthBottom = 1.0F
                        Else
                            cell.BorderWidthBottom = 0.0F
                        End If
                        cell.HorizontalAlignment = PdfCell.ALIGN_LEFT
                        cell.PaddingBottom = 5.0F
                        cell.PaddingLeft = 5.0F
                        cell.PaddingTop = 1.0F
                        table.AddCell(cell)



                    Next
                    document.Add(table)
                End If




                'Tabla de Desviaciones de proc o metodos(1 columna)
                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True

                'titulo errores obvios
                phrase = New Phrase()
                phrase.Add(New Chunk("Conclusiones - Medidas Correctivas / Preventivas", FontFactory.GetFont("Arial", fontsize2, Font.BOLD, Color.BLACK)))
                color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 1.0F
                cell.BorderWidthRight = 1.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 1.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 1.0F
                table.AddCell(cell)
                phrase = New Phrase()
                phrase.Add(New Chunk(es.obtenerobservaciones(codigo, "Obs_EO") & vbLf & vbLf & vbLf & vbLf, FontFactory.GetFont("Arial", fontsize3, Font.NORMAL, Color.BLACK)))
                color__1 = New Color(System.Drawing.ColorTranslator.FromHtml("#000000"))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.BorderColorLeft = color__1
                cell.BorderColorRight = color__1
                cell.BorderColorTop = color__1
                cell.BorderColorBottom = color__1
                cell.BorderWidthLeft = 1.0F
                cell.BorderWidthRight = 1.0F
                cell.BorderWidthTop = 0.0F
                cell.BorderWidthBottom = 1.0F
                cell.HorizontalAlignment = PdfCell.ALIGN_CENTER
                cell.PaddingBottom = 5.0F
                cell.PaddingLeft = 5.0F
                cell.PaddingTop = 1.0F
                table.AddCell(cell)

                document.Add(table)

                'Tabla de cabecera (3 columnas)
                table = New PdfPTable(3)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.2F, 0.35F, 0.15F})


                'Columna 1
                phrase = New Phrase()
                phrase.Add(New Chunk(vbLf & vbLf & "Analista: ", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                table.AddCell(cell)



                'Columna2


                phrase = New Phrase()
                phrase.Add(New Chunk(vbLf & vbLf & "Nombre: " & es.obtenernombreAnalista(es.obteneranalista(codigo)), FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)

                'columna3
                phrase = New Phrase()
                phrase.Add(New Chunk(vbLf & vbLf & "Fecha: " & String.Format("{0:dd/MM/yyyy}", Date.Now()), FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)


                'linea firma

                'Tabla de cabecera (3 columnas)
                table = New PdfPTable(3)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.2F, 0.35F, 0.15F})


                'Columna 1
                phrase = New Phrase()
                phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                table.AddCell(cell)



                'Columna2


                phrase = New Phrase()
                phrase.Add(New Chunk(vbLf & vbLf & "Firma: _________________________ ", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)

                'columna3
                phrase = New Phrase()
                phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)


                table = New PdfPTable(3)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.2F, 0.35F, 0.15F})


                'Columna 1
                phrase = New Phrase()
                phrase.Add(New Chunk(vbLf & vbLf & vbLf & "Supervisor o Encargado: ", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                table.AddCell(cell)



                'Columna2


                phrase = New Phrase()
                phrase.Add(New Chunk(vbLf & vbLf & vbLf & "Nombre: " & es.obtenernombreAnalista(es.obtenersuperior(codigo)), FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)

                'columna3
                phrase = New Phrase()
                phrase.Add(New Chunk(vbLf & vbLf & vbLf & "Fecha: " & String.Format("{0:dd/MM/yyyy}", Date.Now()), FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)


                'linea firma

                'Tabla de cabecera (3 columnas)
                table = New PdfPTable(3)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                table.SetWidths(New Single() {0.2F, 0.4F, 0.12F})


                'Columna 1
                phrase = New Phrase()
                phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                table.AddCell(cell)



                'Columna2


                phrase = New Phrase()
                phrase.Add(New Chunk(vbLf & vbLf & " Firma: _________________________ ", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)

                'columna3
                phrase = New Phrase()
                phrase.Add(New Chunk("", FontFactory.GetFont("Arial", fontsize5, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_CENTER)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)



                'pie de pagina


                table = New PdfPTable(1)
                table.TotalWidth = pagesize1
                table.LockedWidth = True
                ' relleno con celdas vacias en funcion del contenido previo ( lo calculo con un contador por registro mostrado)
                For i = 1 To rellena_final2
                    phrase = New Phrase()
                    phrase.Add(New Chunk(vbLf, FontFactory.GetFont("Arial", 10, Font.NORMAL, Color.BLACK)))
                    cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                    cell.VerticalAlignment = PdfCell.ALIGN_TOP
                    table.AddCell(cell)

                Next


                phrase = New Phrase()
                phrase.Add(New Chunk(sops & "                             Fecha de emisión: " & String.Format("{0:dd/MM/yyyy - H:mm:ss}", Date.Now()) & "     Página 1 de 2", FontFactory.GetFont("Arial", fontsize4, Font.NORMAL, Color.BLACK)))
                cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT)
                cell.VerticalAlignment = PdfCell.ALIGN_TOP
                table.AddCell(cell)
                document.Add(table)
                document.Close()
                Dim bytes As Byte() = memoryStream.ToArray()
                memoryStream.Close()
                'cn.Close()
                Response.Clear()
                Response.ContentType = "application/pdf"
                Response.AddHeader("Content-Disposition", "attachment; filename=FOGL-MAR-2020.pdf")
                Response.ContentType = "application/pdf"
                Response.Buffer = True
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.BinaryWrite(bytes)
                Response.[End]()
                Response.Close()
            End Using
        Catch ex As Exception
            If (ex.Message = "No hay ninguna fila en la posición 0.") Then
                Label2.Text = "No hay consumos registrados para este estandar"
            Else
                Label2.Text = "Error al generar el informe"
            End If


        End Try
    End Sub


    Private Shared Sub DrawLine(ByVal writer As PdfWriter, ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single, ByVal color As Color)
        Dim contentByte As PdfContentByte = writer.DirectContent
        contentByte.SetColorStroke(color)
        contentByte.MoveTo(x1, y1)
        contentByte.LineTo(x2, y2)
        contentByte.Stroke()
    End Sub
    Private Shared Function PhraseCell(ByVal phrase As Phrase, ByVal align As Integer) As PdfPCell
        Dim cell As New PdfPCell(phrase)
        cell.BorderColor = Color.WHITE
        cell.VerticalAlignment = PdfCell.ALIGN_TOP
        cell.HorizontalAlignment = align
        cell.PaddingBottom = 2.0F
        cell.PaddingTop = 0.0F
        Return cell
    End Function
    Private Shared Function ImageCell(ByVal path As String, ByVal scale As Single, ByVal align As Integer) As PdfPCell
        Dim image As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path))
        image.ScalePercent(scale)
        Dim cell As New PdfPCell(image)
        cell.BorderColor = Color.WHITE
        cell.VerticalAlignment = PdfCell.ALIGN_TOP
        cell.HorizontalAlignment = align
        cell.PaddingBottom = 0.0F
        cell.PaddingTop = 0.0F
        Return cell
    End Function
    Protected Function rellena(ByVal nombre As Integer) As String


        Return nombre.ToString("d4")


    End Function


    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        conclusiones.Disabled = False
        estado.SelectedIndex = "2"
    End Sub
  
    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        If (lote_i.Value <> "") Then
            lotes_inv.Items.Add(lote_i.Value)
            lote_i.Value = ""
        End If

    End Sub

    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click

        If (lotes_inv.Text <> "") Then
            lotes_inv.Items.Remove(lotes_inv.SelectedItem)

        End If
    End Sub
End Class
