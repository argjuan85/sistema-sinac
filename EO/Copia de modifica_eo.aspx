﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Copia de modifica_eo.aspx.vb" Inherits="EO_modifica_eo" title="Sistema Sinac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 	<style type="text/css" class="init">
	      	#ctl00_ContentPlaceHolder1_TextArea1, #ctl00_ContentPlaceHolder1_TextArea2, #ctl00_ContentPlaceHolder1_TextArea3, #ctl00_ContentPlaceHolder1_TextArea4, #ctl00_ContentPlaceHolder1_conclusiones       
	      	{
	      		width: 96%;
	      		height: 52px;
	      		}

</style>

  <!-- librerias calendario y css -->
       <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.8.20.custom.css" type="text/css" media="screen" charset="utf-8"/>
       <script src="js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>        
       <script src="js/jquery-ui-1.8.20.custom.min.js" type="text/javascript" charset="utf-8"></script> 
       

   

<script type="text/javascript" src="js/jquery.numeric.js"></script>

      <!--  calendarios  --> 
    <script type="text/javascript" >
    
	$(document).ready(function(){
	       $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

	    
		$("#<%= fecha_cierre.ClientID %>").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true,changeYear: true ,yearRange: '-10:+10', maxDate: 'today', stepMonths: 0}).val();
		//Con este codigo indico que la fecha de ingreso no puede ser menor a la pedido
	

	
		
		
			 
		
		
	
});



   </script>
   <!--  validaciones  --> 
  <style type="text/css">
        body {
            
            font-family: Arial;
            font-size: 10pt;
        }
        input, select
        {
            width: 150px;
        }
    </style>
	<style type="text/css" class="init">
	      	#ctl00_ContentPlaceHolder1_TextArea1, #ctl00_ContentPlaceHolder1_TextArea2, #ctl00_ContentPlaceHolder1_TextArea3, #ctl00_ContentPlaceHolder1_TextArea4, #ctl00_ContentPlaceHolder1_conclusiones       
	      	{
	      		width: 96%;
	      		height: 50px;
	      		}

 	#ctl00_ContentPlaceHolder1_producto     
	      	{
	      		width: 96%;
	      		height: 19px;
	      		}
</style>

  <link href="../css/ValidationEngine.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="../js/jquery/languages/jquery.validationEngine-es.js"
        charset="utf-8"></script>
    <script type="text/javascript" src="../js/jquery/jquery.validationEngine.js"
        charset="utf-8"></script>
    <script type="text/javascript">
        $(function () {
            $("#aspnetForm").validationEngine('attach', { promptPosition: "topRight" });
        });
    </script>
    <script type="text/javascript">
        function DateFormat(field, rules, i, options) {
            var regex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
            if (!regex.test(field.val())) {
                return "Please enter date in dd/MM/yyyy format."
            }
        }
    </script>
  <!-- fin validaciones  --> 
     



 <form id="form2" runat="server" autocomplete="off">
    
<div id="contenedor1">
<div id="contenedor2">
     <div id="titulo_seccion">
       
         Modifique los datos del Error Obvio.
 </div>
    <ul>
    <li class="paneles">
    <div>

  
        <span class="completo1">
      <label for="superior">Encargado/Supervisor(*)</label>
      <asp:DropDownList ID="superior" runat="server" DataSourceID="SqlDataSource3" 
            DataTextField="Denominacion" DataValueField="Id" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
      
    </span>
     <span class="dostercios">
      <label for="numero_cuaderno">Nº de cuaderno/Folio</label>
       <input id="numero_cuaderno" name="numero_cuaderno" value="" runat="server" maxlength="50" />
      
    </span>

 
   <span class="completo1">
    <label for="analista">Analista</label>
            <asp:DropDownList ID="analista" runat="server" DataSourceID="SqlDataSource2" 
            DataTextField="Denominacion" DataValueField="Id" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
      
    </span>    
      <span class="dostercios">
    <label for="laboratorio">Laboratorio(*)</label>
           <asp:DropDownList ID="laboratorio" runat="server">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
          <asp:ListItem  Value="V+E">V+E.</asp:ListItem>
          <asp:ListItem  Value="CC-FQ">CC-FQ</asp:ListItem>
        </asp:DropDownList> 
    </span>
  
 


 <span class="completo">
   <label for="conclusiones">Conclusiones(*)</label>
        <textarea id="conclusiones" cols="20" name="S1" rows="2" runat="server"></textarea>
    
      
    </span>
    
    
     <span class="dostercios">
      <label for="tipo_error">Fallas en el Sistema de Adecuación</label>
      <asp:ListBox runat="server" ID="adecuacion" SelectionMode="Multiple" >
           
</asp:ListBox> 
    
    
    </span>
    
      <span class="dostercios">
      <label for="tipo_error">Desviaciones de Procedimientos o Métodos</label>
      <asp:ListBox runat="server" ID="desviaciones" SelectionMode="Multiple" >
           
</asp:ListBox> 
    
    
    </span>

    
  
                     <span class="dostercios">
   <label for="TextArea3">Observaciones</label>
      <textarea id="TextArea3" cols="20" name="S1" rows="2" runat="server" maxlenght="270"></textarea>
      
    </span>

    
  
    <span class="dostercios">
<label for="TextArea2">
        Observaciones</label>
      <textarea id="TextArea2" cols="20" name="S1" rows="2" runat="server" maxlength="270"></textarea>
      
    </span>
   
 
 
   
 

    

      
    
  </div>
  </li>
   <li class="paneles">
    <div>
              <span class="tercio">
      <label for="fecha_carga">Número de EO</label>
      <input id="numero_eo" name="fecha_carga" value="" runat="server" 
            readonly="readonly"/>
     
    </span>
  
 
    <span class="dostercios">
    
      <label for="estado" runat="server" id="label_estado">Estado</label>
           <asp:DropDownList ID="estado" runat="server">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
          <asp:ListItem  Value="En Curso">En Curso</asp:ListItem>
          <asp:ListItem  Value="Concluido">Concluido</asp:ListItem>
           <asp:ListItem  Value="Anulado">Anulado</asp:ListItem>
        </asp:DropDownList> 
            
       
    </span>
        
    <span class="completo1">
      <label for="producto">Producto</label>
      <asp:DropDownList ID="producto" runat="server" DataSourceID="SqlDataSource1" 
            DataTextField="CodPdto" DataValueField="IDPdto" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione uno...</asp:ListItem>
        </asp:DropDownList>
        
      
        <asp:AccessDataSource ID="AccessDataSource1" runat="server">
        </asp:AccessDataSource>
        
      
    </span>
     <span class="tercio">
     <label for="etapa">Etapa</label>
         <asp:DropDownList ID="etapa" runat="server" DataSourceID="SqlDataSource4" 
            DataTextField="nombre_etapa" DataValueField="idEtapa" class=":required">
          <asp:ListItem Selected="True" Value="0">Seleccione una...</asp:ListItem>
        </asp:DropDownList>
  
     
    </span>
 
    
   
    
    
 
 
 
    <span class="completo1">
     <label for="lote">Lote(*)</label>
      <input id="lote" name="lote" value="" runat="server" 
            maxlength="10"/> 
  
     
    </span>
      
 
   <span class="dostercios">
      <label for="ensayo">Ensayo</label>
      <input id="ensayo" name="ensayo" value="" runat="server" 
            class=":integer :only_on_blur " maxlength="10"/> 
      
      
    </span>


          <span class="completo1">
      <label for="fecha_carga">Fecha de Carga</label>
      <input id="fecha_carga" name="fecha_carga" value="" runat="server" 
            readonly="readonly"/>
     
    </span>
  
 
    <span class="tercio">
    
      <label for="fecha_cierre" runat="server" id="label_fecha_ingreso">Fecha de Cierre(*)</label>
      <input id="fecha_cierre" name="fecha_cierre" value="" runat="server" readonly="true"/>
            
       
    </span>

      
    
         <span class="completo1">
      <label for="tipo_error">Error de calculo </label>
      <asp:ListBox runat="server" ID="calculo" SelectionMode="Multiple" >
           
</asp:ListBox> 
    
    
    </span>
    
    <span class="dostercios3">
      <label for="tipo_error">Tipo de Error</label>
      <asp:ListBox runat="server" ID="tipo_error" SelectionMode="Multiple" >
           
</asp:ListBox> 
    
    
    </span>
    
     <span class="completo1">
   <label for="TextArea4">Observaciones</label>
        <textarea id="TextArea4" cols="20" name="S1" rows="2" runat="server" maxlenght="270"></textarea>
      
    </span>
             <span class="dostercios">
   <label for="TextArea1">Observaciones</label>
      <textarea id="TextArea1" cols="20" name="S1" rows="2" runat="server" maxlength="270" onclick="return TextArea1_onclick()"></textarea>
    
    </span>
    
    
  
    
        
           
    
 
   
  
  </div>
  </li>
     
  </ul>
    <ul>
        
    <li class="panel_boton">
     <div>
     <div id="etiqueta">
         <asp:Label ID="confirma_cambios" runat="server"></asp:Label>
   </div>
      <span class="boton">
       <asp:Button ID="Button2" runat="server" Text="Concluir" />        
       <asp:Button ID="btnReport" runat="server" Text="Generar FOGL" 
             style="height: 26px" />
         &nbsp;<asp:Button ID="Button1" runat="server" Text="Grabar" />
   
        
   <input type="button" value="Volver" id="volver" onclick="location.href='consulta_eo.aspx'"></span>
   
   </div>

    </li>
                 <li>
                 </li>
     </ul>
         
  </div>
  </div>
  <br>
  <div id="confirmacion">
  <asp:Label ID="Label1" runat="server"></asp:Label>
  </div>
    <div id="error">
  <asp:Label ID="Label2" runat="server"></asp:Label>
  </div>
   
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [IDPdto], [CodPdto] FROM [Productos]">
        </asp:SqlDataSource>
        
         <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [Id], [Denominacion] FROM [User]">
        </asp:SqlDataSource>
        
         <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [Id], [Denominacion] FROM [User]">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SinacConnectionString %>" 
            SelectCommand="SELECT [idEtapa], [nombre_etapa] FROM Etapas">
        </asp:SqlDataSource>
         
    </form>
</asp:Content>

