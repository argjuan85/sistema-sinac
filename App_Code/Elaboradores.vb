﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Elaboradores
#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Elaboradores(ByVal strconex As String)
        strconexion = strconex
    End Sub
    Public Function Consultarelaboradores() As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            Dim sql As String
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            ' agregar where condition
            sql = "select * from Elaboradores order by NomElab"
            adapter.SelectCommand = New SqlCommand(sql, OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception
            MsgBox("hubo un error" + ex.Message)
        End Try
        Return dtable
    End Function
    Public Function Consultarelaboradoresasoc(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select * from Elaboradores e INNER JOIN OOT o on o.Elaborador = e.IDElab where o.Elaborador=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    Public Function Consultarelaborador1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Elaboradores where IDElab=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    Public Function Consultaelaborador2() As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Elaboradores", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    Public Function BorrarElaborador(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from Elaboradores where IDElab=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function Agregarelaborador(ByVal nombre As String, ByVal habilitado As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "insert into Elaboradores (NomElab, Habilitado) values ('" + nombre + "','" + habilitado + "');  SELECT SCOPE_IDENTITY();"
            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception
            'MsgBox("error:" + ex.Message)
            Return -1
        End Try
        Return id
    End Function
    
    Public Function ActualizarElaborador(ByVal ID As String, ByVal nombre As String, ByVal deshabilitado As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update Elaboradores set NomElab='" + nombre + "',Habilitado='" + deshabilitado + "' where IDElab='" + ID + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
