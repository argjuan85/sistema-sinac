﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports globales

Public Class Parametros


#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Parametros(ByVal strconex As String)
        strconexion = strconex
    End Sub
    ' devuelve el valor de un parametro en particular 

  

    Public Function consulta_valor(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbccom As SqlCommand
        Dim Sql As String
        Dim valor As String
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Sql = "select Dato from Parametros where id=" + ID
            odbccom = New SqlCommand(Sql, OdbcConn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.Read()) Then
                valor = dtresultado(0).ToString
            End If
            dtresultado.Close()
            OdbcConn.Close()
        Catch ex As Exception
            ' no encontro el parametro
            Return "-1"
        End Try
        Return valor
    End Function
    'devuelve el valor de un parametro dado el nombre del mismo
    Public Function consulta_valor_nombre(ByVal nombre As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbccom As SqlCommand
        Dim Sql As String
        Dim valor As String
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Sql = "select Dato from Parametros where Campo='" + nombre + "'"
            odbccom = New SqlCommand(Sql, OdbcConn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.Read()) Then
                valor = dtresultado(0).ToString
            End If
            dtresultado.Close()
            OdbcConn.Close()
        Catch ex As Exception
            ' no encontro el parametro
            Return "-1"
        End Try
        Return valor
    End Function

    Public Function actualizar_parametro(ByVal ID As String, ByVal valor As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update Parametros set Dato='" + valor + "' where id='" + ID + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    'actualiza un parametro dado el nombre
    Public Function actualizar_parametro_nombre(ByVal nombre As String, ByVal valor As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update Parametros set Dato='" + valor + "' where Campo='" + nombre + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
   
#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class