﻿Imports Microsoft.VisualBasic




Public Class globales
    Public Shared niveluser As String
    Public Shared conexion As String = ConfigurationManager.ConnectionStrings("SinacConnectionString").ConnectionString
    Public Shared conexion2 As String = ConfigurationManager.ConnectionStrings("SinacConnectionString").ConnectionString
    Public Shared conexion3 As String = ConfigurationManager.ConnectionStrings("SinacConnectionString").ConnectionString
    Public Shared grupoadminsup As String
    Public Shared grupoadminsupve As String
    Public Shared grupoadmin As String
    Public Shared grupoadminve As String
    Public Shared grupoconsulta As String
    Public Shared grupoestandares As String
    Public Shared grupoanalistas As String
    Public Shared grupoanalistasve As String
    Public Shared caduca As Integer = 15 ' cargarla de los parametros, ahora harcodeo para probar
    Public Shared ultimo_acceso As String
    Public Shared nivel0 As String  'Jefes y sup
    Public Shared nivel1 As String  'admin
    Public Shared nivel2 As String  'consulta
    Public Shared nivel3 As String  'analista estandares
    Public Shared nivel4 As String  'analista mp y pt
    Public Shared dominio As String = "RAFFO"
    Public Shared sectoradmin As String = "Control de Calidad"
    Public Shared sectorsecun As String = "V+E"
    Public Shared sectoruser As String
    Public Shared direccionad As String = "LDAP://DC=raffo,DC=local"
    Public Shared decimalestsdc As Integer = "3"
    Public Shared rutaexcel As String
    'Public Shared rutapdf As String
    Public Shared informe_EP As String
    Public Shared informe_ES As String
    Public Shared informe_ET As String
    Public Shared informe_PP As String
    Public Shared sops As String
    Public Shared fogl As String
    Public Shared titulo As String
    Public Shared titulo2 As String
    Public Shared vigencia As String
    Public Shared maxviales As Integer
    Public Shared minvalida As String = "1" 'cantidad de caracteres minimos a usar en destinatarios  y licenciantes (mayor que)
    Public Shared meses_vencimiento As Integer = "9" 'meses a considerar para filtro "prox a vencer" en consulta de estandares
    Public Shared mailsistema As String
    Public Shared mailadmincc As String
    Public Shared mailadminve As String
    Public Shared mailport As String
    Public Shared mailserver As String
    Public Shared mailuser As String
    Public Shared mailpass As String
    Public Shared longitudnro As String = 4
    Public Shared rutapdf As String = "exportedfiles/" ' donde se almacenan los pdf adjuntos en los estandares

    Public Shared Function Islogged(ByVal nivel As String) As Boolean
        'hardcode para evitar log con ad
        If (nivel <> "") Then
            Return True
        Else
            Return False
        End If
    End Function

    'retorna true si tiene permiso, false si no.
    Public Shared Function validapermiso(ByVal permiso As Integer, ByVal nivel As String) As Boolean



        If ((permiso And nivel) = (permiso)) Then
            Return True
        Else
            'no tiene permiso

            Return False
        End If


    End Function  'retorna true si tiene permiso, false si no.
    Public Shared Function validacaducidad() As Boolean
        'Dim ahora As String
        'Dim diferencia As Integer
        'Dim tiempo_transcurrido As String 'diferencia en minutos, por ende el parametro tambien lo esta
        'ahora = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))


        ' Diferencias en minutos
        'tiempo_transcurrido = DateDiff("n", ultimo_acceso, ahora)
        'diferencia = tiempo_transcurrido
   
        'If (tiempo_transcurrido >= caduca) Then 'me fijo si no caduco la sesion $caduca es un parametro

        '        Return False

        '       Else

        ' ultimo_acceso = cambiaformatofechahora2(DateTime.Now.ToString("dd/MM/yyyy HH:mm"))
        Return True
        'End If

    End Function
    'cambia el formato de fecha de yyyy/mm/dd a dd/mm/yyyy
    Public Shared Function invertirfecha(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 6, 2)
            bDia = Right(Fechatest, 2)
            bAño = Left(Fechatest, 4)
            Fechanueva = bDia & "/" & bMes & "/" & bAño
            Return Fechanueva
        End If
    End Function
    'cambia el formato de fecha de  dd/mm/yyyy a yyyy/mm/dd 
    Public Shared Function revertirfecha(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 4, 2)
            bDia = Left(Fechatest, 2)
            bAño = Right(Fechatest, 4)
            Fechanueva = bAño & "/" & bMes & "/" & bDia
            Return Fechanueva
        End If
    End Function
    'cambia el formato de fecha de  dd/mm/yyyy a yyyy-mm-dd 
    Public Shared Function cambiaformatofecha(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 4, 2)
            bDia = Left(Fechatest, 2)
            bAño = Right(Fechatest, 4)
            Fechanueva = bAño & "-" & bMes & "-" & bDia
            Return Fechanueva
        End If
    End Function


    'cambia el formato de fecha de  dd/mm/yyyy a yyyy-dd-mm 
    Public Shared Function cambiaformatofecha2(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 4, 2)
            bDia = Left(Fechatest, 2)
            bAño = Right(Fechatest, 4)
            Fechanueva = bAño & "-" & bDia & "-" & bMes
            Return Fechanueva
        End If
    End Function

    'cambia el formato de fecha de  dd/mm/yyyy a yyyy-mm-dd 
    Public Shared Function cambiaformatofecha3(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 4, 2)
            bDia = Left(Fechatest, 2)
            bAño = Right(Fechatest, 4)
            Fechanueva = bAño & "-" & bMes & "-" & bDia
            Return Fechanueva
        End If
    End Function


    'cambia el formato de fecha de  dd/mm/yyyy hh:mm:ss.ssss a yyyy-mm-dd hh:mm:ss.ssss
    Public Shared Function cambiaformatofechahora(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        Dim bhora As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 4, 2)
            bDia = Left(Fechatest, 2)
            bAño = Mid(Fechatest, 7, 4)
            bhora = Right(Fechatest, 5)
            Fechanueva = bAño & "-" & bMes & "-" & bDia & " " & bhora
            Return Fechanueva
        End If
    End Function

    'cambia el formato de fecha de  dd/mm/yyyy hh:mm:ss.ssss a yyyy-dd-mm hh:mm:ss.ssss
    Public Shared Function cambiaformatofechahora2(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        Dim bhora As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 4, 2)
            bDia = Left(Fechatest, 2)
            bAño = Mid(Fechatest, 7, 4)
            bhora = Right(Fechatest, 5)
            Fechanueva = bAño & "-" & bDia & "-" & bMes & " " & bhora
            Return Fechanueva
        End If
    End Function

    'cambia el formato de fecha de  dd/mm/yyyy  a yyyy-dd-mm 
    Public Shared Function cambiaformatofechahora3(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        Dim bhora As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 4, 2)
            bDia = Left(Fechatest, 2)
            bAño = Mid(Fechatest, 7, 4)
            bhora = Right(Fechatest, 5)
            Fechanueva = bAño & "-" & bDia & "-" & bMes
            Return Fechanueva
        End If
    End Function
End Class
