﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports FormsAuth




Public Class EO_EO
#Region "Atributos"
    Public strconexion As String
   

#End Region


#Region "Metodos"
    Public Sub EO_EO(ByVal strconex As String)
        strconexion = strconex
    End Sub



    Public Function AgregarEO_EO(ByVal id_eo As String, ByVal id_eolist As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try

            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()

            sql = "insert into EO_EO (idEOlist,idEO) values ('" + id_eolist + "','" + id_eo + "'); SELECT SCOPE_IDENTITY();"

            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception

            Return -1
        End Try
        Return id
    End Function

    'dado id EO e id EOlist devuelve el id
    Public Function obteneridEO_EO(ByVal IDEO As String, ByVal IDEOlist As String, ByVal tipo As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select EO_list.id from EO_EO inner join  EO_list on EO_list.id=EO_EO.idEOlist where idEO=" + IDEO
            sql = sql & " and idEOlist=" + IDEOlist
            sql = sql & " and EO_list.tipo='" + tipo + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obteneridEO_list(ByVal IDEOlist As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select id from EO_list where Descripcion='" + IDEOlist + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function elimina_rel(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from EO_EO where idEO=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function


    Public Function obtener_cantidad_lotes(ByVal id_eo As String) As SqlDataReader
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Lote from EO_Loteinv where idEO='" + id_eo + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows()) Then
                numero = "0"
            End If

            'While dtresultadof.Read()
            '   numero = numero + 1

            'End While
            'dtresultadof.Close()
            'odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return dtresultadof
    End Function

    Public Function obtener_cantidad_lotes1(ByVal id_oot As String) As SqlDataReader
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Lote from OOT_LoteInv where idOOT='" + id_oot + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows()) Then
                numero = "0"
            End If

            'While dtresultadof.Read()
            '   numero = numero + 1

            'End While
            'dtresultadof.Close()
            'odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return dtresultadof
    End Function

    'cantidad de elementos en eo_list
    Public Function obtener_cantidad_eolist(ByVal tipo As String) As SqlDataReader
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Descripcion from EO_list where tipo='" + tipo + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows()) Then
                numero = "0"
            End If

            'While dtresultadof.Read()
            '   numero = numero + 1

            'End While
            'dtresultadof.Close()
            'odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return dtresultadof
    End Function

    'dado un id de EO y un tipo de EO list devuelve todos los registro
    Public Function obtener_tipo_eolist(ByVal tipo As String, ByVal idEO As String) As SqlDataReader
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Descripcion from EO_list where tipo='" + tipo + "' and idEO='" + idEO + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            'If (dtresultadof.HasRows()) Then

            '            End If

            'While dtresultadof.Read()
            '   numero = numero + 1

            'End While
            'dtresultadof.Close()
            'odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return dtresultadof
    End Function
#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Sub New()

    End Sub
End Class
