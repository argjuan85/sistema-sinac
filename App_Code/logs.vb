﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class logs

#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub logs(ByVal strconex As String)
        strconexion = strconex
    End Sub
    Public Function Consultarlogs(ByVal sql As String) As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            adapter.SelectCommand = New SqlCommand(sql, OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return dtable
    End Function
    Public Function Consultarlogs1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from AuditEO where idAudit=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    'LOG OOT
    Public Function Agregarlog3(ByVal UserSO As String, ByVal UserSitst As String, ByVal DptoUSist As String, ByVal Nivel As String, ByVal PC As String, ByVal Accion As String, ByVal FechaAccion As String, ByVal NroOOT As String, ByVal Pdto As String, ByVal Lote As String, ByVal FeCarga As String, ByVal Estado As String, ByVal Observaciones As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            'hay q ver si obtengo el user logueado del sinac en vb, de ser asi puedo pasar tambien los datos que no puedo conseguir via web

            sql = "insert into AuditOOT (UserSO, UserSist, DptoUSist, Nivel, PC, Accion, FechaAccion,NroOOT, Pdto, Lote, FeCarga, Estado, Observaciones) values ('" + UserSO + "','" + UserSitst + "','" + DptoUSist + "','" + Nivel + "','" + PC + "','" + Accion + "','" + FechaAccion + "','" + NroOOT + "','" + Pdto + "','" + Lote + "', '" + FeCarga + "' ,'" + Estado + "','" + Observaciones + "');"

            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True




    End Function

    'LOG EO
    Public Function Agregarlog2(ByVal UserSO As String, ByVal UserSitst As String, ByVal DptoUSist As String, ByVal Nivel As String, ByVal PC As String, ByVal Accion As String, ByVal FechaAccion As String, ByVal NroEO As String, ByVal Pdto As String, ByVal Lote As String, ByVal FeCarga As String, ByVal Estado As String, ByVal TipoError As String, ByVal Conclusion As String, ByVal FeCierre As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            'hay q ver si obtengo el user logueado del sinac en vb, de ser asi puedo pasar tambien los datos que no puedo conseguir via web
            If (FeCierre = "") Then
                sql = "insert into AuditEO (UserSO, UserSist, DptoUSist, Nivel, PC, Accion, FechaAccion,NroEO, Pdto, Lote, FeCarga, Estado, TipoError, Conclusion) values ('" + UserSO + "','" + UserSitst + "','" + DptoUSist + "','" + Nivel + "','" + PC + "','" + Accion + "','" + FechaAccion + "','" + NroEO + "','" + Pdto + "','" + Lote + "', '" + FeCarga + "' ,'" + Estado + "','" + TipoError + "','" + Conclusion + "');"
            Else
                sql = "insert into AuditEO (UserSO, UserSist, DptoUSist, Nivel, PC, Accion, FechaAccion,NroEO, Pdto, Lote, FeCarga, Estado, TipoError, Conclusion, FeCierre) values ('" + UserSO + "','" + UserSitst + "','" + DptoUSist + "','" + Nivel + "','" + PC + "','" + Accion + "','" + FechaAccion + "','" + NroEO + "','" + Pdto + "','" + Lote + "', '" + FeCarga + "' ,'" + Estado + "','" + TipoError + "','" + Conclusion + "','" + FeCierre + "');"

            End If

            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True




    End Function


    Public Function Agregarlog(ByVal fecha As String, ByVal user As String, ByVal pc As String, ByVal tran As String, ByVal obs As String, ByVal tabla As String, ByVal idregistro As String, ByVal registro As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("insert into Audit (Fecha,[User],Pc,[Tran],Obs, tabla, Idregistro, registro) values ('" + fecha + "','" + user + "','" + pc + "','" + tran + "','" + obs + "','" + tabla + "','" + idregistro + "','" + registro + "');", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True




    End Function




#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
