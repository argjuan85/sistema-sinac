﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports FormsAuth




Public Class OOT
#Region "Atributos"
    Public strconexion As String
    Public id As String
    Public NroOOT As String
    Public FeCarga As String
    Public Analista As String
    Public idPdto As String
    Public Lote As String
    Public Elaborador As String
    Public Tendencia As String
    Public Estado As String
    Public Ensayo As String
    Public Activo As String

#End Region


#Region "Metodos"
    Public Sub OOT(ByVal strconex As String)
        strconexion = strconex
    End Sub


    'funcion para fogl devuelve si o no en funcion de registros cargados en investigaciones
    Public Function si_no(ByVal ID As String) As String
        If (ID = "1") Then
            Return "Si"
        ElseIf (ID = "0") Then
            Return "No"
        Else
            Return "N/A"
        End If



    End Function
 

    'funcion para fogl devuelve si o no en funcion de registros cargados en investigaciones
    Public Function obs_na(ByVal ID As String) As String
        If ((ID = "0") Or (ID = "-1")) Then
            Return "N/A"
        Else
            Return ID

        End If



    End Function

    'devuelve el estado de un OOT dado su id
    Public Function obtenerestado(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim est As String = "-1"
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select Estado from OOT where idOOT=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultado.Read()
            est = dtresultado(0).ToString
            dtresultado.Close()
            odbcconn.Close() 'se agregó para eliminar un error de maximo de conexiones
        Catch ex As Exception
        End Try
        Return est
    End Function

   

    'indica si envia notificacion de stock minimo un estandar dado su id
    Public Function notificamail(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim est As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select aviso from Estandard where CodEst=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultado.Read()
            est = dtresultado(0).ToString
            dtresultado.Close()
            odbcconn.Close()
        Catch ex As Exception
        End Try
        Return est
    End Function

  
 
    'verifica si es factible el cambio de estado 
    Public Function validacambioestado(ByVal nuevo As String, ByVal actual As String, ByVal nivel As String, ByVal codigo As String) As String
   
        Dim u As OOT
        'hardcodeo de momento
        nivel0 = nivel
        nivel1 = nivel
        nivel2 = nivel
        nivel3 = nivel
        nivel4 = nivel
        u = New OOT()
        u.OOT(conexion)
        Dim r As String = "-1"
        ' r = 1 es el mismo estado
        ' r = 0 falta investigacion previa
        ' r = 2 cambio ok
        ' r =   
        Dim check As String = "0"
        If (actual = nuevo) Then
            r = "1"
        Else

            Select Case nuevo
                'debo validad que hayan cargado la investigacion tambien

                Case "PendienteCC"
                    check = u.verifica_investigacion(codigo, "CC")
                    If (actual = "En Curso") And (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) And (check = "1") Then
                        r = "2"
                    End If
                    If (check = "-1") Then
                        r = "0"
                    End If
                Case "ConcluidoCC"
                    check = u.verifica_investigacion(codigo, "CC")
                    'la validacion de arriba es redundante pero la dejo igual, de todas formas la validacion en este estado es seguramente por el nivel de superior
                    If (actual = "PendienteCC") And (nivel = nivel2) And (check = "1") Then
                        r = "2"
                    End If
                    If (check = "-1") Then
                        r = "0"
                    End If
                Case "PendienteQA"
                    check = u.verifica_investigacion(codigo, "RAP")
                    If (actual = "ConcluidoCC") And (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) And (check = "1") Then
                        r = "2"
                    End If
                    If (check = "-1") Then
                        r = "0"
                    End If
                Case "PendienteElab"
                    check = u.verifica_investigacion(codigo, "RAP")
                    If (actual = "PendienteQA") And (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) And (check = "1") Then
                        r = "2"
                    End If
                    If (check = "-1") Then
                        r = "0"
                    End If
                Case "PendienteCF"
                    If ((actual = "PendienteQA") And (u.verifica_investigacion(codigo, "GC") = "1")) Or ((actual = "PendienteElab") And (u.verifica_investigacion(codigo, "ELAB") = "1")) Then
                        check = "1"
                        r = "2"
                    Else
                        check = "-1"
                    End If

                    If (actual = "PendienteElab") And (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) And (check = "1") Then
                        r = "2"
                    End If
                    If (check = "-1") Then
                        r = "0"
                    End If
                Case "Concluido"
                    check = u.verifica_investigacion(codigo, "CGC")
                    If (actual = "PendienteCF") And (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) And (check = "0") Then
                        r = "2"
                    End If
                Case "Cerrado"
                    check = u.verifica_investigacion(codigo, "CGC")
                    If (actual = "Concluido") And (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) And (check = "0") Then
                        r = "2"
                    End If
                Case "Anulado"
                    If (nivel = nivel2 Or nivel = nivel1 Or nivel = nivel0) Then
                        r = "2"
                    End If



                Case Else
                    '...
            End Select
        End If


        Return r
    End Function

  





    Private Function Generafecha(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bDia As String
        Dim bMes As String
        Dim bAño As String
        Fechatest = fecha
        bMes = Left(Fechatest, 3)
        bDia = Mid(Fechatest, 5, 2)
        bAño = Mid(Fechatest, 8, 4)
        Fechanueva = bDia & "/" & NumeroMes(bMes) & "/" & bAño
        Return Format(Convert.ToDateTime(Fechanueva), "yyyy/MM/dd")
    End Function


    Private Function NumeroMes(ByVal eNomMes As String) As String
        Dim eMes As String
        Dim M As String
        M = Trim(eNomMes).ToLower
        If M = "jan" Then
            eMes = "01"
        ElseIf M = "feb" Then
            eMes = "02"
        ElseIf M = "mar" Then
            eMes = "03"
        ElseIf M = "apr" Then
            eMes = "04"
        ElseIf M = "may" Then
            eMes = "05"
        ElseIf M = "jun" Then
            eMes = "06"
        ElseIf M = "jul" Then
            eMes = "07"
        ElseIf M = "aug" Then
            eMes = "08"
        ElseIf M = "sep" Then
            eMes = "09"
        ElseIf M = "oct" Then
            eMes = "10"
        ElseIf M = "nov" Then
            eMes = "11"
        ElseIf M = "dec" Then
            eMes = "12"
        End If

        Return Trim(eMes)

    End Function








    Public Function invertirfecha(ByVal fecha As String) As String
        Dim Fechatest As String
        Dim Fechanueva As String
        Dim bMes As String
        Dim bDia As String  'yyyy/mm/dd
        Dim bAño As String
        If (fecha = "") Then
            Return ""
        Else
            Fechatest = fecha
            bMes = Mid(Fechatest, 6, 2)
            bDia = Right(Fechatest, 2)
            bAño = Left(Fechatest, 4)
            Fechanueva = bDia & "/" & bMes & "/" & bAño
            Return Fechanueva
        End If


    End Function



#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub



    'adjuntos
    'dado un id de adjunto devuelve el nombre
    Public Function obtener_nombre_adjunto(ByVal ID As String, ByVal tipo As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo

                Case "OOT"
                    sql = "select Adjunto from OOT_Adjunto where id=" + ID
                Case "DESV"
                    sql = "select Adjunto from DesvAdjuntos where id=" + ID
                Case "RECL"
                    sql = "select Adjunto from ReclAdjuntos where id=" + ID
                Case "CAPA"
                    sql = "select Adjunto from CapaAdjuntos where id=" + ID
            End Select

            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    'dado un id de adjunto devuelve el id del registro asociado
    Public Function obtener_registro_adjunto(ByVal ID As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select idOOT from OOT_adjunto where id=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function obtenerNroOOT(ByVal ID As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select NroOOT from OOT where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenerproducto(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select idPdto from OOT where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenercodpdto(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select CodPdto from Productos where IDPdto=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenerdescpdto(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select DesPdto from Productos where IDPdto=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenerfechacarga(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select FeCarga from OOT where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenerlote(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Lote from OOT where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenertendencia(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Tendencia from OOT where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenerelaborador(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Elaborador from OOT where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obteneranalista(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Analista from OOT where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtener_nombre_analista(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Denominacion from [User] where Id=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtener_nombre_elaborador(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select NomElab from Elaboradores where IDElab=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function AgregarOOT(ByVal idpdto As String, ByVal lote As String, ByVal elaborador As String, ByVal fecarga As String, ByVal tendencia As String, ByVal analista As String, ByVal estado As String, ByVal ensayo As String, ByVal activo As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Dim calcu As String
        Dim nro_oot As String
        Dim p As Parametros
        p = New Parametros()
        p.Parametros(conexion)
        Try
            calcu = p.consulta_valor_nombre("Nro_OOT")
            nro_oot = calcu
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()

            sql = "insert into OOT (NroOOT,idPdto,Lote,Elaborador,Fecarga,Tendencia,Analista,Estado,activo,ensayo) values ('" + nro_oot + "','" + idpdto + "','" + lote + "','" + elaborador + "','" + fecarga + "','" + tendencia + "','" + analista + "','" + estado + "','" + activo + "','" + ensayo + "'); SELECT SCOPE_IDENTITY();"

            odbccom = New SqlCommand(sql, odbcconn)
            'incremento numero de OOT y actualizo en parametros
            calcu = calcu + 1
            nro_oot = calcu
            p.actualizar_parametro_nombre("Nro_OOT", nro_oot)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception

            Return -1
        End Try
        Return id
    End Function


    Public Function AgregarOOT_Tendencia(ByVal id_oot As String, ByVal id_tendenica As String) As String
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try

            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()

            sql = "insert into OOT_tendencia (idOOT,idTendencia) values ('" + id_oot + "','" + id_tendenica + "'); SELECT SCOPE_IDENTITY();"

            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception

            Return -1
        End Try
        Return id
    End Function

    'cantidad de elementos tendencia cargados
    Public Function obtener_cantidad_tendencia(ByVal codigo As String) As SqlDataReader
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select tendencia from Tendencias"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows()) Then
                numero = "0"
            End If

            'While dtresultadof.Read()
            '   numero = numero + 1

            'End While
            'dtresultadof.Close()
            'odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return dtresultadof
    End Function

    Public Function obtenerid_tendencia(ByVal tendencia As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select id from Tendencias where tendencia like '%" + tendencia + "%'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function


    Public Function obtener_tendencia(ByVal id_oot As String, ByVal id_tendencia As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select id from OOT_tendencia where idOOT='" + id_oot + "' and idTendencia='" + id_tendencia + "'"

            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function obtener_descripcion_tendencia(ByVal id_oot As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select tendencia from Tendencias where id='" + id_oot + "'"

            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function elimina_tendencias(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from OOT_tendencia where idOOT=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function


    Public Function elimina_analistas(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "delete from OOT_InvCC_analistas where idOOTInvCC=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function cargar_firma(ByVal firmante As String, ByVal fecha As String, ByVal idOOT As String, ByVal tipo_inv As String) As Boolean

        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo_inv
                Case "PendienteCC"
                    sql = "update OOT_InvCC set id_firma='" + firmante + "',fecha_firma='" + fecha + "' where idOOT='" + idOOT + "' "
                Case "PendienteQA"
                    sql = "update OOT_InvRAP set id_firma='" + firmante + "',fecha_firma='" + fecha + "' where idOOT='" + idOOT + "' "
                Case "PendienteCF"
                    sql = "update OOT_InvElab set id_firma='" + firmante + "',fecha_firma='" + fecha + "' where idOOT='" + idOOT + "' "
                Case "Concluido"
                    sql = "update OOT_InvQA set id_firma2='" + firmante + "',fecha_firma2='" + fecha + "' where idOOT='" + idOOT + "' "
                Case "PendienteElab"
                    sql = "update OOT_InvQA set id_firma='" + firmante + "',fecha_firma='" + fecha + "' where idOOT='" + idOOT + "' "

            End Select
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()

        Catch ex As Exception

            Return False
        End Try
        Return True
    End Function

    Public Function obtener_firmante(ByVal id_oot As String, ByVal tipo_inv As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim numero As String = "-1"

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo_inv
                Case "CC"
                    sql = "select id_firma from OOT_InvCC where idOOT='" + id_oot + "'"
                Case "RAP"
                    sql = "select id_firma from OOT_InvRAP where idOOT='" + id_oot + "'"
                Case "GC"
                    sql = "select id_firma from OOT_InvQA where idOOT='" + id_oot + "'"
                Case "ELAB"
                    sql = "select id_firma from OOT_InvElab where idOOT='" + id_oot + "'"
                Case "CGC"
                    sql = "select id_firma2 from OOT_InvQA where idOOT='" + id_oot + "'"

            End Select


            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function obtener_fecha_firma(ByVal id_oot As String, ByVal tipo_inv As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim numero As String = "-1"

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo_inv
                Case "CC"
                    sql = "select fecha_firma from OOT_InvCC where idOOT='" + id_oot + "'"
                Case "RAP"
                    sql = "select fecha_firma from OOT_InvRAP where idOOT='" + id_oot + "'"
                Case "GC"
                    sql = "select fecha_firma from OOT_InvQA where idOOT='" + id_oot + "'"
                Case "ELAB"
                    sql = "select fecha_firma from OOT_InvElab where idOOT='" + id_oot + "'"
                Case "CGC"
                    sql = "select fecha_firma2 from OOT_InvQA where idOOT='" + id_oot + "'"

            End Select


            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function ActualizarOOT(ByVal ID As String, ByVal idPdto As String, ByVal Lote As String, ByVal Elaborador As String, ByVal Tendencia As String, ByVal Analista As String, ByVal Ensayo As String, ByVal Activo As String) As Boolean


        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()     '                                                                                                                                                   
            odbccom = New SqlCommand("update OOT set idPdto='" + idPdto + "',Lote='" + Lote + "',Elaborador='" + Elaborador + "',Tendencia='" + Tendencia + "',Analista='" + Analista + "',Ensayo='" + Ensayo + "',Activo='" + Activo + "' where idOOT='" + ID + "'  ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    'obtener nombre adjunto

    Public Function obtenerarchivoadjunto(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Adjunto from OOT_Adjunto where id=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function verifica_adjunto(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select id from OOT_Adjunto where Adjunto='" + ID + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
                odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
                Return numero
            Else
                Return "-1"
            End If
        Catch ex As Exception
            Return "-1"
        End Try

    End Function

    Public Function eliminaradjunto(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from OOT_Adjunto where id=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    'agregar adjunto


    Public Function agregaradjuntoOOT(ByVal idOOT As String, ByVal nombre As String) As Boolean

        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "insert into OOT_Adjunto (idOOT, Adjunto) values ('" + idOOT + "','" + nombre + "'); SELECT SCOPE_IDENTITY();"
            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()

        Catch ex As Exception

            Return False
        End Try
        Return True
    End Function

    'actualiza el estado de un estandar

    Public Function ActualizarestadoOOT(ByVal ID As String, ByVal estado As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand


        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update OOT set Estado='" + estado + "' where idOOT='" + ID + "'", odbcconn)

            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    'para llenar la grilla

    Public Function ConsultaOOT(ByVal sql As String) As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            adapter.SelectCommand = New SqlCommand(sql, OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return dtable
    End Function

    Public Function Consultaroot1(ByVal ID As String) As OOT
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As OOT
        es = New OOT()
        es.OOT(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from OOT where idOOT=" + ID, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            es.id = dtresultadof(0).ToString()
            es.NroOOT = dtresultadof(1).ToString()
            es.FeCarga = dtresultadof(6).ToString()
            es.Analista = dtresultadof(8).ToString()
            es.Elaborador = dtresultadof(5).ToString()
            es.idPdto = dtresultadof(2).ToString()
            es.Lote = dtresultadof(4).ToString()
            es.Tendencia = dtresultadof(7).ToString()
            es.Analista = dtresultadof(8).ToString()
            es.Estado = dtresultadof(9).ToString()
            es.Ensayo = dtresultadof(10).ToString()
            es.Activo = dtresultadof(11).ToString()


            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador

        Catch ex As Exception
        End Try
        Return es
    End Function
    'conclusion gc

    Public Function Agregarnuevoconcgc(ByVal idOOT As String, ByVal causa_asignable As String, ByVal conclusion As String) As Boolean

        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update OOT_InvQA set Causa_Asignable='" + causa_asignable + "',ConclusionFinal='" + conclusion + "' where idOOT='" + idOOT + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()

        Catch ex As Exception

            Return False
        End Try
        Return True
    End Function

    Public Function Actualizarconcgc(ByVal idOOT As String, ByVal causa As String, ByVal conclusion As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update OOT_InvQA set Causa_asignable='" + causa + "',ConclusionFinal='" + conclusion + "' where idOOT='" + idOOT + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    'obtener nombre de elaborador 
    Public Function obtener_elaborador(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select NomElab from Elaboradores where IDElab=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    'inv elab 

    Public Function Agregarinvelab(ByVal idOOT As String, ByVal revision As String, ByVal capa_asoc As String, ByVal causa_conclusion As String, ByVal causa As String, ByVal observaciones As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "insert into OOT_InvElab (idOOT,RevisionPE,Capa_asoc, Conclusion,Observaciones, Causa) values ('" + idOOT + "','" + revision + "','" + capa_asoc + "','" + causa_conclusion + "','" + observaciones + "','" + causa + "'); SELECT SCOPE_IDENTITY();"
            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception

            Return -1
        End Try
        Return id
    End Function

    Public Function Actualizarinvelab(ByVal idOOT As String, ByVal revision As String, ByVal capa_asoc As String, ByVal causa_conclusion As String, ByVal causa As String, ByVal observaciones As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update OOT_InvElab set RevisionPE='" + revision + "',Capa_asoc='" + capa_asoc + "',Conclusion='" + causa_conclusion + "',Observaciones='" + observaciones + "',Causa='" + causa + "' where idOOT='" + idOOT + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function consultar_causa_conclusion(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Conclusion from OOT_InvElab where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function consultar_causa_elab(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Causa from OOT_InvElab where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function consultar_observaciones_elab(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Observaciones from OOT_InvElab where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtener_obs_revi(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Observaciones from OOT_InvElab where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function obtener_causa(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Causa from OOT_InvElab where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    'inv gc

    Public Function Agregarinvgc(ByVal idOOT As String, ByVal desv_asoc As String, ByVal obs_desv As String, ByVal ccam_asoc As String, ByVal obs_ccam As String, ByVal capa_asoc As String, ByVal obs_capa As String, ByVal causa_asignable As String, ByVal conclusion As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "insert into OOT_InvQA (idOOT,Desv_Asoc,Obs_Desv, Ccam_Asoc, Obs_Ccam, Capa_asoc, Obs_Capa,Causa_asignable, ConclusionFinal) values ('" + idOOT + "','" + desv_asoc + "','" + obs_desv + "','" + ccam_asoc + "','" + obs_ccam + "','" + capa_asoc + "','" + obs_capa + "','" + causa_asignable + "','" + conclusion + "'); SELECT SCOPE_IDENTITY();"
            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception

            Return -1
        End Try
        Return id
    End Function

    Public Function Actualizarinvgc(ByVal idOOT As String, ByVal desv_asoc As String, ByVal obs_desv As String, ByVal ccam_asoc As String, ByVal obs_ccam As String, ByVal capa_asoc As String, ByVal obs_capa As String, ByVal causa_asignable As String, ByVal conclusion As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update OOT_InvQA set Desv_Asoc='" + desv_asoc + "',Obs_Desv='" + obs_desv + "',Ccam_Asoc='" + ccam_asoc + "',Obs_Ccam='" + obs_desv + "',Capa_Asoc='" + capa_asoc + "',Obs_Capa='" + obs_capa + "',Causa_Asignable='" + causa_asignable + "',ConclusionFinal='" + conclusion + "' where idOOT='" + idOOT + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    'conclusion gc

    Public Function consultar_conclusiongc(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select ConclusionFinal from OOT_InvQA where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function consultar_causaasignable(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Causa_Asignable from OOT_InvQA where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function


    ' inv rap

    Public Function Agregarinvrap(ByVal obs As String, ByVal capa As String, ByVal idOOT As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()

            sql = "insert into OOT_InvRAP (idOOT,idCapa_Asoc,Observaciones) values ('" + idOOT + "','" + capa + "','" + obs + "'); SELECT SCOPE_IDENTITY();"

            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception

            Return -1
        End Try
        Return id
    End Function

    Public Function Actualizarinvrap(ByVal ID As String, ByVal obs As String, ByVal capa As String) As Boolean

        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update OOT_InvRAP set idCapa_Asoc='" + capa + "',Observaciones='" + obs + "' where idOOT='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function



    Public Function consultar_capa_invrap(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select idCapa_Asoc from OOT_InvRAP where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function consultar_capa_invelab(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Capa_Asoc from OOT_InvElab where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    'obtener numero de capa en base a id 
    Public Function obtener_nro_capa(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select indentCapa from Capa where idCapa=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function


    Public Function consultar_obs_invrap(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Observaciones from OOT_InvRAP where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    ' inv cc
   
    Public Function Agregarinvcc(ByVal idOOT As String, ByVal oos_Asoc As String, ByVal obs_oos As String, ByVal desv_asoc As String, ByVal obs_desv As String, ByVal ccam_asoc As String, ByVal obs_ccam As String, ByVal capa_asoc As String, ByVal obs_capa As String, ByVal revision As String, ByVal causa_asig As String, ByVal capa_gen As String, ByVal conclusiones As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()

            sql = "insert into OOT_InvCC (idOOT,Result_Asoc,Obs_Result,Desv_Asoc,Obs_Desv,Ccam_Asoc,Obs_Ccam,Capa_Asoc,Obs_Capa,RevisionDoc,CausaAsig ,CapaGenerado,Causa_Conclusion) values ('" + idOOT + "','" + oos_Asoc + "','" + obs_oos + "','" + desv_asoc + "','" + obs_desv + "','" + ccam_asoc + "','" + obs_ccam + "','" + capa_asoc + "','" + obs_capa + "','" + revision + "', '" + causa_asig + "','" + capa_gen + "','" + conclusiones + "'); SELECT SCOPE_IDENTITY();"

            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception

            Return -1
        End Try
        Return id
    End Function

    Public Function Actualizarinvcc(ByVal idOOT As String, ByVal oos_Asoc As String, ByVal obs_oos As String, ByVal desv_asoc As String, ByVal obs_desv As String, ByVal ccam_asoc As String, ByVal obs_ccam As String, ByVal capa_asoc As String, ByVal obs_capa As String, ByVal revision As String, ByVal causa_asig As String, ByVal capa_gen As String, ByVal conclusiones As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update OOT_InvCC set Result_Asoc='" + oos_Asoc + "',Obs_Result='" + obs_oos + "',Desv_Asoc='" + desv_asoc + "',Obs_Desv='" + obs_desv + "',Ccam_Asoc='" + ccam_asoc + "',Obs_Ccam='" + obs_ccam + "',Capa_Asoc='" + capa_asoc + "',Obs_Capa='" + obs_capa + "',RevisionDoc='" + revision + "',CausaAsig='" + causa_asig + "',CapaGenerado='" + capa_gen + "',Causa_Conclusion='" + conclusiones + "' where idOOT='" + idOOT + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function AgregarAnalista_InvCC(ByVal id_analista As String, ByVal id_invcc As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try

            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()

            sql = "insert into OOT_InvCC_analistas (idAnalista,idOOTInvCC) values ('" + id_analista + "','" + id_invcc + "'); SELECT SCOPE_IDENTITY();"

            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception

            Return -1
        End Try
        Return id
    End Function
    'obtener analistas cargados

    Public Function obtener_analistas() As SqlDataReader
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Denominacion from [User] order by Denominacion asc"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows()) Then
                numero = "0"
            End If

        Catch ex As Exception
        End Try
        Return dtresultadof
    End Function

    'obtener id de analista dado un nombre
    Public Function obtener_id_analistas(ByVal Analista As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim numero As Integer = "-1"

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select id from [User] where denominacion='" + Analista + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    'obtener nombre de analista dado un id
    Public Function obtener_nombre_analistas(ByVal Analista As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim numero As String = "-1"

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select denominacion from [User] where id='" + Analista + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    'dado id EO e id EOlist devuelve el id
    Public Function obtenerid_InvCC_analistas(ByVal idinv As String, ByVal Analista As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim numero As Integer = "-1"

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select id from OOT_InvCC_analistas where idOOTInvCC='" + idinv + "'"
            sql = sql & " and idAnalista='" + Analista + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function obtenerid_InvCC(ByVal oot As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim numero As Integer = "-1"

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select idInvCC from OOT_InvCC where idOOT='" + oot + "'"
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) Then
                dtresultadof.Read()
                numero = dtresultadof(0)
                dtresultadof.Close()
            End If

            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function


    Public Function verifica_investigacion(ByVal ID As String, ByVal tipo As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim aux As String = "0"
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo
                Case "RAP"
                    sql = "select OOT.idOOT from OOT inner join OOT_InvRAP on OOT_InvRAP.idOOT=OOT.idOOT where OOT.idOOT=" + ID
                Case "CC"
                    sql = "select OOT.idOOT from OOT inner join OOT_InvCC on OOT_InvCC.idOOT=OOT.idOOT where OOT.idOOT=" + ID
                Case "GC"
                    sql = "select OOT.idOOT from OOT inner join OOT_InvQA on OOT_InvQA.idOOT=OOT.idOOT where OOT.idOOT=" + ID
                Case "ELAB"
                    sql = "select OOT.idOOT from OOT inner join OOT_InvElab on OOT_InvElab.idOOT=OOT.idOOT where OOT.idOOT=" + ID
                Case "CGC"
                    aux = "1"
                    sql = "select OOT_InvQA.ConclusionFinal from OOT inner join OOT_InvQA on OOT_InvQA.idOOT=OOT.idOOT where OOT.idOOT=" + ID
            End Select


            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultadof.HasRows) And (aux = "0") Then
                Return "1"
            ElseIf (aux = "1") Then
                Return "0"
            Else
                Return "-1"
            End If
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try

    End Function

    ' modif cc


    Public Function consultar_causa(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select CausaAsig from OOT_InvCC where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_oos_asoc(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Result_Asoc from OOT_InvCC where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_desv_asoc(ByVal ID As String, ByVal tipo_inv As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo_inv
                Case "CC"
                    sql = "select Desv_Asoc from OOT_InvCC where idOOT=" + ID
                Case "QA"
                    sql = "select Desv_Asoc from OOT_InvQA where idOOT=" + ID

            End Select
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_ccam_asoc(ByVal ID As String, ByVal tipo_inv As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo_inv
                Case "CC"
                    sql = "select Ccam_Asoc from OOT_InvCC where idOOT=" + ID
                Case "QA"
                    sql = "select Ccam_Asoc from OOT_InvQA where idOOT=" + ID

            End Select
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_capa_asoc(ByVal ID As String, ByVal tipo_inv As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo_inv
                Case "CC"
                    sql = "select Capa_Asoc from OOT_InvCC where idOOT=" + ID
                Case "RAP"
                    sql = "select idCapa_Asoc from OOT_InvRAP where idOOT=" + ID
                Case "QA"
                    sql = "select Capa_Asoc from OOT_InvQA where idOOT=" + ID
                Case "ELAB"
                    sql = "select Capa_Asoc from OOT_InvElab where idOOT=" + ID
            End Select
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_capa_gen(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select CapaGenerado from OOT_InvCC where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_obs_oos(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Obs_Result from OOT_InvCC where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_obs_desv(ByVal ID As String, ByVal tipo_inv As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo_inv
                Case "CC"
                    sql = "select Obs_Desv from OOT_InvCC where idOOT=" + ID
                Case "QA"
                    sql = "select Obs_Desv from OOT_InvQA where idOOT=" + ID
            End Select

            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_obs_ccam(ByVal ID As String, ByVal tipo_inv As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo_inv
                Case "CC"
                    sql = "select Obs_Ccam from OOT_InvCC where idOOT=" + ID
                Case "QA"
                    sql = "select Obs_Ccam from OOT_InvQA where idOOT=" + ID
            End Select
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_obs_capa(ByVal ID As String, ByVal tipo_inv As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo_inv
                Case "CC"
                    sql = "select Obs_Capa from OOT_InvCC where idOOT=" + ID
                Case "QA"
                    sql = "select Obs_Capa from OOT_InvQA where idOOT=" + ID
            End Select

            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_conclusion(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Causa_Conclusion from OOT_InvCC where idOOT=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function consultar_revision(ByVal ID As String, ByVal tipo_inv As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String = "-1"
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Select Case tipo_inv
                Case "ELAB"
                    sql = "select RevisionPE from OOT_InvElab where idOOT=" + ID
                Case "CC"
                    sql = "select RevisionDoc from OOT_InvCC where idOOT=" + ID
            End Select

            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try

        Return numero


    End Function
    Public Function obtener_nombre_producto(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select CodPdto from Productos where IDPdto=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function


    ' inv gc final

    'obtener nombre de causa en base a id 
    Public Function obtener_nombre_causa(ByVal ID As String) As String
        Dim cadena As String = ""
        Dim band As String = "0"
        Try
        
            If ((1 And ID) = (1)) And (band = "0") Then
                cadena = cadena + "Granulación"
                band = "1"
            ElseIf ((1 And ID) = (1)) Then
                cadena = cadena + " / " + "Granulación"

            End If
            If ((2 And ID) = (2)) And (band = "0") Then
                cadena = cadena + "Compresión"
                band = "1"
            ElseIf ((2 And ID) = (2)) Then
                cadena = cadena + " / " + "Compresión"

            End If
            If ((4 And ID) = (4)) And (band = "0") Then
                cadena = cadena + "Recubrimiento"
                band = "1"
            ElseIf ((4 And ID) = (4)) Then
                cadena = cadena + " / " + "Recubrimiento"

            End If
            If ((8 And ID) = (8)) And (band = "0") Then
                cadena = cadena + "Control de Calidad"
                band = "1"
            ElseIf ((8 And ID) = (8)) Then
                cadena = cadena + " / " + "Control de Calidad"

            End If
       
        Catch ex As Exception
        End Try
        If cadena = "" Then
            Return "N/A"
        Else
            Return cadena
        End If

    End Function

    Public Sub New()

    End Sub
End Class
