﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Etapa
#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Etapa(ByVal strconex As String)
        strconexion = strconex
    End Sub
    Public Function consulta_id_etapa(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbccom As SqlCommand
        Dim Sql As String
        Dim cantidad As String
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Sql = "select idEtapa from Etapas where nombre_etapa=" + ID
            odbccom = New SqlCommand(Sql, OdbcConn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                cantidad = dtresultado(0)
            End If
            dtresultado.Close()
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return cantidad
    End Function

    Public Function consulta_nombre_etapa(ByVal ID As String) As String
        Dim dtresultado As SqlDataReader
        Dim odbccom As SqlCommand
        Dim Sql As String
        Dim cantidad As String
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Sql = "select nombre_etapa from Etapas where idEtapa=" + ID
            odbccom = New SqlCommand(Sql, OdbcConn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            If (dtresultado.HasRows) Then
                dtresultado.Read()
                cantidad = dtresultado(0)
            End If
            dtresultado.Close()
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return cantidad
    End Function
#End Region
End Class
