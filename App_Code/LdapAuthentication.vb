﻿Imports System
Imports System.Text
Imports System.Collections
Imports System.DirectoryServices
Imports globales
Imports System.Web.SessionState
Imports System.Web.UI.Page




Namespace FormsAuth
    Public Class LdapAuthentication

        Dim _path As String
        Dim _filterAttribute As String

        Public Sub New(ByVal path As String)
            _path = path
        End Sub


        Public Function ObtenerGruposUsuario(ByVal strLoginName As String) As String
            ' Dim dsDirectoryEntry As New DirectoryEntry("LDAP://DC=monteverde,DC=tecnofarma,DC=com,DC=ar")
            Dim dsDirectoryEntry As New DirectoryEntry(direccionad)
            Dim dsSearcher As New DirectorySearcher(dsDirectoryEntry)
            Dim contador As Integer
            Dim strGrupos As String
            Dim intEqualsIndex As Integer
            Dim intCommaIndex As Integer
            Dim sbUserGroups As New StringBuilder
            dsSearcher.Filter = "saMAccountName=" & strLoginName
            dsSearcher.PropertiesToLoad.Add("memberOf")
            Try
                Dim dsResult As SearchResult = dsSearcher.FindOne
                If dsResult Is Nothing Then
                    Return Nothing
                End If
                For contador = 0 To (dsResult.Properties("memberOf").Count - 1)
                    strGrupos = CType(dsResult.Properties("memberOf")(contador), String)
                    intEqualsIndex = strGrupos.IndexOf("=", 1)
                    intCommaIndex = strGrupos.IndexOf(",", 1)
                    If intEqualsIndex = -1 Then
                        Return Nothing
                    End If
                    sbUserGroups.Append(strGrupos.Substring((intEqualsIndex + 1), (intCommaIndex - intEqualsIndex) - 1))
                    sbUserGroups.Append(ControlChars.CrLf)
                Next
            Catch ex As Exception
                '   MessageBox.Show("Error en funcion ObtenerGruposUsuario" & vbNewLine & vbNewLine _
                '   & ex.Message, "Active Directory Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Return sbUserGroups.ToString.Trim
        End Function

        Public Shared Function Islogged(ByVal nivel As String) As Boolean
            If (nivel <> "") Then
                Return True
            Else
                Return False
            End If
        End Function






        Public Function IsAuthenticated(ByVal username As String, ByVal pwd As String) As String

            '  Dim domainAndUsername As String = dominio & "\" & username
            '  Dim entry As DirectoryEntry = New DirectoryEntry(_path, domainAndUsername, pwd)
            ''  Dim strGrupos As String
            '  Dim intResult As Integer
            '  Dim intResult2 As Integer
            '  Dim intResult3 As Integer
            '  Dim intResult4 As Integer
            '  Dim intResult5 As Integer
            '  Dim intResult6 As Integer
            '  Dim intResult7 As Integer
            '  Dim intResult1 As Integer
            '  Dim blnUserValidated As String = "-1"
      

            Try
                'Bind to the native AdsObject to force authentication.			
                '     Dim obj As Object = entry.NativeObject
                '     Dim search As DirectorySearcher = New DirectorySearcher(entry)
                '
                '                search.Filter = "(SAMAccountName=" & username & ")"
                '                search.PropertiesToLoad.Add("cn")
                '                Dim result As SearchResult = search.FindOne()
                '
                '                If (result Is Nothing) Then
                ' Return False
                ' End If
                '
                '                'Update the new path to the user in the directory.
                '                _path = result.Path
                '                _filterAttribute = CType(result.Properties("cn")(0), String)
                '
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

            'strGrupos = ObtenerGruposUsuario(username)
            'intResult = strGrupos.IndexOf(grupoadmin)
            'intResult1 = strGrupos.IndexOf(grupoconsulta)
            'intResult2 = strGrupos.IndexOf(grupoestandares)
            'intResult3 = strGrupos.IndexOf(grupoanalistas)
            'intResult4 = strGrupos.IndexOf(grupoanalistasve)
            'intResult5 = strGrupos.IndexOf(grupoadminve)
            'intResult6 = strGrupos.IndexOf(grupoadminsup)
            'intResult7 = strGrupos.IndexOf(grupoadminsupve)
            '' Por alguna razon da como pertenencia a ambos grupos de administradores cuando es admin ve es por eso que es importante preguntar primeros por ellos por que un admin de ve da true en LOS DOS grupos mientras que estandares solo UNO.
            'If intResult4 > -1 Then
            ' 'User found in group
            ' blnUserValidated = "5"
            'niveluser = nivel4
            'sectoruser = sectorsecun

            'ElseIf intResult5 > -1 Then
            'User found in group
            'blnUserValidated = "6"
            'niveluser = nivel1
            'sectoruser = sectorsecun
            'ElseIf intResult7 > -1 Then
            'User found in group
            'blnUserValidated = "7"
            'niveluser = nivel1
            'sectoruser = sectorsecun
            'ElseIf intResult > -1 Then
            'User found in group
            ' blnUserValidated = "1"
            'niveluser = nivel1
            ' 'sectoruser = sectoradmin
            'ElseIf intResult1 > -1 Then
            'User found in group
            'blnUserValidated = "2"
            ' 'niveluser = nivel2
            ' 'sectoruser = sectoradmin
            '
            '           ElseIf intResult2 > -1 Then
            'User found in group
            '          blnUserValidated = "3"
            '          'niveluser = nivel3
            '          'sectoruser = sectoradmin
            '          'User not found in group
            '          ' blnUserValidated = True
            '          ElseIf intResult3 > -1 Then
            '          'User found in group
            '          blnUserValidated = "4"
            '          'niveluser = nivel4
            '          'sectoruser = sectoradmin
            '          ElseIf intResult6 > -1 Then
            '          'User found in group
            '          blnUserValidated = "8"
            '          'niveluser = nivel4
            '          'sectoruser = sectoradmin
            '
            '
            '
            '           Else

            'User not found in group
            '          blnUserValidated = "-1"
            '          End If


            '         Return blnUserValidated

        End Function


        Public Shared Function validausuario(ByVal username As String, ByVal sector As String) As Boolean


            Dim strGrupos As String
            Dim intResult As Integer
            Dim intResult2 As Integer
            Dim intResult3 As Integer
            Dim intResult4 As Integer
            Dim intResult5 As Integer
            Dim intResult1 As Integer
            Dim blnUserValidated As Boolean = False
            Dim adPath As String = direccionad 'Path to your LDAP directory server
            Dim adAuth As LdapAuthentication = New LdapAuthentication(adPath)
           

            Try
                ' ver en seguimiento q pasa si no es user de ningun grupo
                strGrupos = adAuth.ObtenerGruposUsuario(username)
                If (strGrupos <> Nothing) Then
                    If (sector = sectoradmin) Then
                        intResult = strGrupos.IndexOf(grupoadmin)
                        intResult1 = strGrupos.IndexOf(grupoconsulta)
                        intResult2 = strGrupos.IndexOf(grupoestandares)
                        intResult3 = strGrupos.IndexOf(grupoanalistas)
                        If intResult > -1 Then
                            'User found in group
                            blnUserValidated = True

                        ElseIf intResult1 > -1 Then
                            'User found in group
                            blnUserValidated = True


                        ElseIf intResult2 > -1 Then
                            'User found in group
                            blnUserValidated = True

                        ElseIf intResult3 > -1 Then
                            'User found in group
                            blnUserValidated = True

                        End If



                    Else
                        intResult4 = strGrupos.IndexOf(grupoanalistasve)
                        intResult5 = strGrupos.IndexOf(grupoadminve)
                        If intResult4 > -1 Then
                            'User found in group
                            blnUserValidated = True
                        ElseIf intResult5 > -1 Then
                            'User found in group
                            blnUserValidated = True

                        End If
                    End If
                Else
                    blnUserValidated = False
                End If





                Return blnUserValidated
            Catch ex As Exception
                Throw New Exception("Error autenticando usuario. " & ex.Message)
            End Try
        End Function



     
        Public Function GetGroups() As String
            Dim search As DirectorySearcher = New DirectorySearcher(_path)
            search.Filter = "(cn=" & _filterAttribute & ")"
            search.PropertiesToLoad.Add("memberOf")
            Dim groupNames As StringBuilder = New StringBuilder()

            Try
                Dim result As SearchResult = search.FindOne()
                Dim propertyCount As Integer = result.Properties("memberOf").Count

                Dim dn As String
                Dim equalsIndex, commaIndex

                Dim propertyCounter As Integer

                For propertyCounter = 0 To propertyCount - 1
                    dn = CType(result.Properties("memberOf")(propertyCounter), String)

                    equalsIndex = dn.IndexOf("=", 1)
                    commaIndex = dn.IndexOf(",", 1)
                    If (equalsIndex = -1) Then
                        Return Nothing
                    End If

                    groupNames.Append(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1))
                    groupNames.Append("|")
                Next

            Catch ex As Exception
                Throw New Exception("Error obtaining group names. " & ex.Message)
            End Try

            Return groupNames.ToString()
        End Function


    End Class
End Namespace