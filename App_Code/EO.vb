﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports globales
Imports FormsAuth




Public Class EO
#Region "Atributos"
    Public strconexion As String
    Public id As String
    Public NroEO As String
    Public FeCarga As String
    Public Laboratorio As String
    Public Analista As String
    Public idPdto As String
    Public Encargado As String
    Public Supervisor As String
    Public Lote As String
    Public Ensayo As String
    Public Etapa As String
    Public CuadernoFolio As String
    Public TipoError As String
    Public Conclusiones As String
    Public Estado As String
    Public FeCierre As String
    Public Obs_EO As String
    Public Obs_DE As String
    Public Obs_FA As String
    Public Obs_EC As String

#End Region


#Region "Metodos"
    Public Sub EO(ByVal strconex As String)
        strconexion = strconex
    End Sub
    'grilla consulta general de documentacion
    Public Function ConsultaDoc(ByVal sql As String) As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            adapter.SelectCommand = New SqlCommand(sql, OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return dtable
    End Function

    'para llenar la grilla

    Public Function ConsultaEO(ByVal sql As String) As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            adapter.SelectCommand = New SqlCommand(sql, OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return dtable
    End Function
    Public Function Consultareo1(ByVal ID As String) As EO
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from EO where idEO=" + ID, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            es.id = dtresultadof(0).ToString()
            es.NroEO = dtresultadof(1).ToString()
            es.FeCarga = dtresultadof(2).ToString()
            es.Laboratorio = (dtresultadof(3).ToString())
            es.Analista = dtresultadof(4).ToString()
            es.idPdto = dtresultadof(5).ToString()
            es.Etapa = dtresultadof(6).ToString()
            es.Encargado = dtresultadof(7).ToString()
            es.Supervisor = dtresultadof(8).ToString()
            es.Lote = dtresultadof(9).ToString()
            es.Ensayo = dtresultadof(10).ToString()
            es.CuadernoFolio = dtresultadof(11).ToString()
            es.TipoError = dtresultadof(12).ToString()
            es.Conclusiones = dtresultadof(13).ToString()
            es.Estado = dtresultadof(14).ToString()
            es.FeCierre = dtresultadof(15).ToString()
            es.Obs_EO = dtresultadof(16).ToString()
            es.Obs_DE = dtresultadof(17).ToString()
            es.Obs_FA = dtresultadof(18).ToString()
            es.Obs_EC = dtresultadof(19).ToString()


            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador

        Catch ex As Exception
        End Try
        Return es
    End Function
    Public Function obtener_laboratorio(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select nombre_lab from laboratorios where idLab=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenerNroEO(ByVal ID As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As Integer
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select NroEO from EO where idEO=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenernombreAnalista(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Denominacion from [User] where Id=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenerlaboratorio(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Laboratorio from EO where idEO=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenerobservaciones(ByVal ID As String, ByVal observaciones As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select " + observaciones + " from EO where idEO=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenerfechacarga(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select FeCarga from EO where idEO=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function


    Public Function obteneranalista(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Analista from EO where idEO=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function


    Public Function obtenersuperior(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select Supervisor from EO where idEO=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function obtenercuadernofolio(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select CuadernoFolio from EO where idEO=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function

    Public Function obtenerproducto(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select idPdto from EO where idEO=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
    Public Function obtenercodpdto(ByVal ID As String) As String
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim es As EO
        Dim sql As String
        Dim numero As String
        es = New EO()
        es.EO(conexion)
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select CodPdto from Productos where IDPdto=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
 

   

    Public Function AgregarEO(ByVal laboratorio As String, ByVal producto As String, ByVal analista As String, ByVal etapa As String, ByVal superior As String, ByVal lote As String, ByVal ensayo As String, ByVal numero_cuaderno As String, ByVal conclusiones As String, ByVal estado As String, ByVal fecha_carga As String, ByVal obs_eo As String, ByVal obs_de As String, ByVal obs_fa As String, ByVal obs_ec As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Dim calcu As String
        Dim nro_eo As String
        Dim p As Parametros
        p = New Parametros()
        p.Parametros(conexion)
        Try
            calcu = p.consulta_valor_nombre("Nro_EO")
            nro_eo = calcu
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()

            sql = "insert into EO (NroEO,Fecarga,Laboratorio,Analista,idPdto,Etapa,Encargado,Supervisor,Lote,Ensayo,CuadernoFolio,Conclusiones,Estado, Obs_EO, Obs_DE, Obs_FA, Obs_EC) values ('" + nro_eo + "','" + fecha_carga + "','" + laboratorio + "','" + analista + "','" + producto + "','" + etapa + "','" + superior + "','" + superior + "','" + lote + "','" + ensayo + "','" + numero_cuaderno + "','" + conclusiones + "','" + estado + "','" + obs_eo + "','" + obs_de + "','" + obs_fa + "','" + obs_ec + "'); SELECT SCOPE_IDENTITY();"

            odbccom = New SqlCommand(sql, odbcconn)
            'incremento numero de EO y actualizo en parametros
            calcu = calcu + 1
            nro_eo = calcu
            p.actualizar_parametro_nombre("Nro_EO", nro_eo)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception

            Return -1
        End Try
        Return id
    End Function

    Public Function Actualizareo(ByVal ID As String, ByVal Fecarga As String, ByVal Laboratorio As String, ByVal Analista As String, ByVal etapa As String, ByVal idPdto As String, ByVal Encargado As String, ByVal Supervisor As String, ByVal Lote As String, ByVal Ensayo As String, ByVal CuadernoFolio As String, ByVal Conclusiones As String, ByVal Estado As String, ByVal fecha_cierre As String, ByVal Obs_EO As String, ByVal Obs_DE As String, ByVal Obs_FA As String, ByVal Obs_EC As String) As Boolean

        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            If (fecha_cierre = "") Then
                odbccom = New SqlCommand("update EO set Fecarga='" + Fecarga + "',Laboratorio='" + Laboratorio + "',Analista='" + Analista + "',Etapa='" + etapa + "',idPdto='" + idPdto + "',Encargado='" + Encargado + "',Supervisor='" + Supervisor + "',Lote='" + Lote + "',Ensayo='" + Ensayo + "',CuadernoFolio='" + CuadernoFolio + "',Conclusiones='" + Conclusiones + "',Estado='" + Estado + "',Obs_EO='" + Obs_EO + "',Obs_DE='" + Obs_DE + "' ,Obs_FA='" + Obs_FA + "',Obs_EC='" + Obs_EC + "' where idEO='" + ID + "' ", odbcconn)

            Else
                odbccom = New SqlCommand("update EO set Fecarga='" + Fecarga + "',Laboratorio='" + Laboratorio + "',Analista='" + Analista + "',Etapa='" + etapa + "',idPdto='" + idPdto + "',Encargado='" + Encargado + "',Supervisor='" + Supervisor + "',Lote='" + Lote + "',Ensayo='" + Ensayo + "',CuadernoFolio='" + CuadernoFolio + "',Conclusiones='" + Conclusiones + "',FeCierre='" + fecha_cierre + "',Estado='" + Estado + "',Obs_EO='" + Obs_EO + "',Obs_DE='" + Obs_DE + "' ,Obs_FA='" + Obs_FA + "',Obs_EC='" + Obs_EC + "' where idEO='" + ID + "' ", odbcconn)

            End If
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function Actualizareocierre(ByVal ID As String, ByVal Fecarga As String, ByVal Laboratorio As String, ByVal Analista As String, ByVal idPdto As String, ByVal Encargado As String, ByVal Supervisor As String, ByVal Lote As String, ByVal Ensayo As String, ByVal CuadernoFolio As String, ByVal TipoError As String, ByVal Conclusiones As String, ByVal Estado As String, ByVal FeCierre As String) As Boolean

        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand

        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update EO set Fecarga='" + Fecarga + "',Laboratorio='" + Laboratorio + "',Analista='" + Analista + "',idPdto='" + idPdto + "',Encargado='" + Encargado + "',Supervisor='" + Supervisor + "',Lote='" + Lote + "',Ensayo='" + Ensayo + "',CuadernoFolio='" + CuadernoFolio + "',TipoError='" + TipoError + "',Conclusiones='" + Conclusiones + "',Estado='" + Estado + "', FeCierre='" + FeCierre + "' where idEO='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    'actualiza el estado de un EO

    Public Function ConcluirEO(ByVal estado As String, ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update EO set Estado='" + estado + "' where IdEO='" + ID + "'", odbcconn)

            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function valida_cierre(ByVal carga As String, ByVal cierre As String) As Boolean
        Dim fechacargo As Date = CDate(carga)
        Dim fechacierre As Date
        If (cierre <> "") Then
            fechacierre = CDate(cierre)
        Else
            Return True
        End If


        Try

            If (fechacargo > fechacierre) Then
                Return False
            Else
                Return True
            End If


        Catch ex As Exception
            Return False
        End Try

    End Function
#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Sub New()

    End Sub
End Class
