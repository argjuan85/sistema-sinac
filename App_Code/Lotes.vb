﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Lotes
#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Lotes(ByVal strconex As String)
        strconexion = strconex
    End Sub
    Public Function Consultarlotes(ByVal ID As String) As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            Dim sql As String
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            ' agregar where condition
            sql = "select * from OOT_LoteInv where idOOT=" + ID
            adapter.SelectCommand = New SqlCommand(sql, OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception
            MsgBox("hubo un error" + ex.Message)
        End Try
        Return dtable
    End Function
    Public Function Consultardestinatariosasoc(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select * from Destinatarios d INNER JOIN Envios e on e.CodDest = d.destinatario_id where e.CodDest=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    Public Function Consultarlotes1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select * from OOT_LoteInv where idOOT_loteInv=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    'consulta todos los destinatarios en un datareader
    Public Function Consultalote2(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from OOT_LoteInv where idOOT=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    Public Function BuscarDestinatario(ByVal cadena As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Destinatarios where nombre like '%" + cadena + "%' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    'check repetido
    Public Function verificalote(ByVal lote As String, ByVal codigo As String, ByVal codigolote As String) As Integer
        Dim dtresultado As SqlDataReader
        dtresultado = Consultalote2(codigo)
        Dim band As String = 1
        Dim borrar As String
        While (dtresultado.Read())
            borrar = dtresultado(0).ToString()
            If (dtresultado(0).ToString() <> codigolote) Then
                If (lote.ToUpper() = dtresultado(2).ToString().ToUpper()) Then
                    band = 0
                    Return (band)
                End If
            End If
        End While

        dtresultado.Close()

        Return (band)


    End Function


    Public Function buscardestinatario_paginacion(ByVal cadena As String) As DataTable
        Dim dtable As DataTable
        dtable = New DataTable()
        Try
            Dim odbconn As SqlConnection
            odbconn = New SqlConnection(strconexion)
            odbconn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter()
            adapter.SelectCommand = New SqlCommand("Select * from Destinatarios where nombre like '%" + cadena + "%' ", odbconn)
            adapter.Fill(dtable)
            odbconn.Close()
        Catch ex As Exception
        End Try
        Return dtable
    End Function

    Public Function Borrarlote(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from OOT_LoteInv where idOOT_LoteInv=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function Agregarlote(ByVal idoot As String, ByVal lote As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "insert into OOT_LoteInv (idOOT,Lote) values ('" + idoot + "','" + lote + "');  SELECT SCOPE_IDENTITY();"
            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception
            'MsgBox("error:" + ex.Message)
            Return -1
        End Try
        Return id
    End Function
    Public Function Agregarlote_eo(ByVal idoot As String, ByVal lote As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "insert into EO_Loteinv (idEO,Lote) values ('" + idoot + "','" + lote + "');  SELECT SCOPE_IDENTITY();"
            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception
            'MsgBox("error:" + ex.Message)
            Return -1
        End Try
        Return id
    End Function

    Public Function elimina_lotes_eo(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from EO_Loteinv where idEO=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function elimina_lotes_oot(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from OOT_LoteInv where idOOT=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function


    Public Function AgregarDestinatariox(ByVal nombre As String, ByVal deshabilitado As String) As String
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("insert into Destinatarios (nombre,deshabilitado) values ('" + nombre + "','" + deshabilitado + "');  SELECT SCOPE_IDENTITY();", odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception
            'MsgBox("error:" + ex.Message)
            Return ex.Message
        End Try
        Return "anduvo bien =/"
    End Function

    Public Function Actualizarlote(ByVal ID As String, ByVal nombre As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update OOT_LoteInv set Lote='" + nombre + "' where idOOT_LoteInv='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function obtenerNroOOT(ByVal ID As String) As Integer
        Dim dtresultadof As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Dim numero As Integer
        
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "select idOOT from OOT_LoteInv where idOOT_LoteInv=" + ID
            odbccom = New SqlCommand(sql, odbcconn)
            dtresultadof = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
            dtresultadof.Read()
            numero = dtresultadof(0)
            dtresultadof.Close()
            odbcconn.Close() 'no puedo cerrar conexion cuando devuelvo un datareader coloco el parametro "commandbehavior" de arriba para que la conex se cierre cuando se cierre el lector en el metodo invocador
        Catch ex As Exception
        End Try
        Return numero
    End Function
#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
