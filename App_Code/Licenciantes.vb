﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Licenciantes
#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Licenciantes(ByVal strconex As String)
        strconexion = strconex
    End Sub
 

    Public Function Consultarlicenciantesasoc(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select * from Licenciantes l INNER JOIN Estandard e on e.CodLic = l.licenciante_id where l.licenciante_id=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    'consulta todos los destinatarios en un datareader
    Public Function Consultalicenciantes2() As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Licenciantes", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function


    Public Function Consultarlicenciantes() As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            adapter.SelectCommand = New SqlCommand("select * from Licenciantes order by nombre ", OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception

        End Try
        Return dtable
    End Function
    Public Function Consultarlicenciantes1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Licenciantes where licenciante_id=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    Public Function BuscarLicenciante(ByVal cadena As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Licenciantes where nombre like '%" + cadena + "%' ", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function



    Public Function BorrarLicenciante(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from Licenciantes where licenciante_id=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function AgregarLicenciante(ByVal nombre As String, ByVal deshabilitado As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As Integer
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("insert into Licenciantes (nombre,deshabilitado) values ('" + nombre + "','" + deshabilitado + "');  SELECT SCOPE_IDENTITY();", odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception
            Return -1
        End Try
        Return id
    End Function

    Public Function ActualizarLicenciante(ByVal ID As String, ByVal nombre As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "update Licenciantes set nombre='" + nombre + "',deshabilitado='N' where licenciante_id='" + ID + "' "
            odbccom = New SqlCommand(sql, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Sub New()

    End Sub
End Class
