﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class Tendencias
#Region "Atributos"
    Public strconexion As String
#End Region
#Region "Metodos"
    Public Sub Tendencias(ByVal strconex As String)
        strconexion = strconex
    End Sub
    Public Function Consultartendencias() As DataTable
        Dim dtable As DataTable
        dtable = New DataTable
        Try
            Dim OdbcConn As SqlConnection
            Dim sql As String
            OdbcConn = New SqlConnection(strconexion)
            OdbcConn.Open()
            Dim adapter As SqlDataAdapter
            adapter = New SqlDataAdapter
            ' agregar where condition
            sql = "select * from Tendencias order by tendencia"
            adapter.SelectCommand = New SqlCommand(sql, OdbcConn)
            adapter.Fill(dtable)
            OdbcConn.Close()
        Catch ex As Exception
            MsgBox("hubo un error" + ex.Message)
        End Try
        Return dtable
    End Function
    Public Function Consultartendenciasasoc(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim Sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            Sql = "select * from OOT_tendencia where idTendencia=" + ID
            odbccom = New SqlCommand(Sql, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function
    Public Function Consultartendencia1(ByVal ID As String) As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Tendencias where id=" + ID, odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    Public Function Consultatendencia2() As SqlDataReader
        Dim dtresultado As SqlDataReader
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("select * from Tendencias", odbcconn)
            dtresultado = odbccom.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
        End Try
        Return dtresultado
    End Function

    Public Function BorrarTendencia(ByVal ID As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("delete from Tendencias where id=" + ID, odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function Agregartendencia(ByVal nombre As String, ByVal habilitada As String) As Integer
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Dim id As String
        Dim sql As String
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            sql = "insert into Tendencias (tendencia, habilitada) values ('" + nombre + "','" + habilitada + "');  SELECT SCOPE_IDENTITY();"
            odbccom = New SqlCommand(sql, odbcconn)
            id = odbccom.ExecuteScalar().ToString()
            odbcconn.Close()
        Catch ex As Exception
            'MsgBox("error:" + ex.Message)
            Return -1
        End Try
        Return id
    End Function

    Public Function ActualizarTendencia(ByVal ID As String, ByVal nombre As String, ByVal habilitado As String) As Boolean
        Dim odbcconn As SqlConnection
        Dim odbccom As SqlCommand
        Try
            odbcconn = New SqlConnection(strconexion)
            odbcconn.Open()
            odbccom = New SqlCommand("update Tendencias set tendencia='" + nombre + "',habilitada='" + habilitado + "' where id='" + ID + "' ", odbcconn)
            odbccom.ExecuteNonQuery()
            odbcconn.Close()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class
